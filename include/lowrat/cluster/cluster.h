#ifndef LOWRAT_CLUSTER_CLUSTER_H_
#define LOWRAT_CLUSTER_CLUSTER_H_

#include <cstddef>//for size_t
#include <memory>//for std::unique_ptr
#include <iostream>//for ostream
#include <vector>//for vector

namespace lowrat
{

//!
//! \class  Cluster
//! \brief  Corresponds to one cluster (i.e. one node) in the 'cluster tree' of the Hierarchical Tensor Format.
//!
//! The Cluster tree of all nodes should be available on each of the nodes.
//!

class Cluster
{
public:

    //! Constructor (vector of tensor directions).
    Cluster( const std::vector< std::size_t >& modes );
    //! Constructor (first/last tensor direction).
    Cluster( std::size_t first = 0, std::size_t last = 0 );
    //! Constructor for combining two clustertrees
    Cluster( Cluster* cl1, Cluster* cl2 );
    //! Copy constructor.
    Cluster( const Cluster& cl );

    // move constructor
    Cluster(Cluster&& other);

    //! Destructor (in case of a tree recursively from root to leaves).
    ~Cluster() = default;
    // virtual ~Cluster();

    //! Assignment. Creates a hard copy.
    Cluster& operator =( const Cluster& cl );

    // move assignment
    Cluster& operator=(Cluster&& other);

    int mpi_rank() const
    {
        return mpi_rank_;
    }

    std::size_t  first() const;
    std::size_t  last()  const;
    std::size_t  size()  const
    {
        return modes_.size();
    }

    std::vector< std::size_t >   modes() const
    {
        return modes_;
    }

    //! Returns the number of levels of the whole cluster tree.
    std::size_t levels() const;

    //! Returns the level (i.e. distance to root) of this.
    std::size_t level() const;

    bool    is_leaf()       const
    {
        return is_leaf_;
    }
    bool    is_root()       const
    {
        return is_root_;
    }
    bool    is_first_son()  const
    {
        return is_first_son_;
    }

    //! Checks if mode mu is contained.
    bool contains( std::size_t mu ) const;

    //! Access to modes.
    std::size_t operator ()( std::size_t i ) const;

    //! Returns the leaf number of this, if this is a leaf, and size_t(-1) otherwise.
    std::size_t leaf_number() const;

    Cluster* father()  const
    {
        return father_;
    }
    Cluster* son1()    const
    {
        // if(son1_)
        // {
        return son1_.get();
        // }
        // throw "son1 of cluster is the nullptr";
    }
    Cluster* son2()    const
    {
        // if (son2_)
        // {
        return son2_.get();
        // }
        // throw "son1 of cluster is the nullptr";
    }

    //! Sets the sons of this (and automatically sets this as father of the sons).
    //! son1 and son2 must be allocated by hand (as new Cluster *)
    //! and will automatically be deleted.
    void set_sons( Cluster* son1, Cluster* son2 );
    //! Sets son1 as 1st son of this (and automatically sets this as father of son1).
    //! son1 must be allocated by hand (as new Cluster *)
    //! and will automatically be deleted.
    void set_son1( Cluster* son1 );
    //! Sets son2 as 2nd son of this (and automatically sets this as father of son2).
    //! son2 must be allocated by hand (as new Cluster *)
    //! and will automatically be deleted.
    void set_son2( Cluster* son2 );

    //! Returns root of the cluster tree, which this belongs to.
    const Cluster* root() const;

    //! Returns a vector of pointers to all nodes, that are on level lvl in the tree of this.
    std::vector< const Cluster* > level( std::size_t lvl ) const;

    //! Returns a vector of all nodes in the tree of this.
    std::vector< const Cluster* > all_nodes() const;

    //! Returns a vector of all nodes of a certain MPI rank.
    std::vector< const Cluster* > all_nodes_of_rank( int mpi_rank ) const;

    void set_mpi_rank( int mpi_rank )
    {
        mpi_rank_ = mpi_rank;
    }

    //! Sets the MPI rank of this and all its successors.
    void set_mpi_rank_recursive( int mpi_rank );

    //! Builds a balanced cluster tree including _mpi_ranks such that the tree nodes
    //! on each MPI rank form a sub tree.
    //! num_ranks is the total number of MPI ranks available.
    //! num_ranks <= 0 results in each tree node having its own MPI rank.
    void build_balanced_tree( int num_ranks = 0 );

    //! Old version of build_balanced_tree() which for some cases results in a very
    //! unbalanced distribution of tree nodes to MPI ranks.
    void build_balanced_tree_old( int num_ranks = 0 );

    //! Sends the entire tree (starting from this) to ostream.
    void tree_to_ostream( std::ostream& os, std::size_t lvl = 0 ) const;

    //! Sends a cluster to ostream (without its father and sons).
    friend std::ostream & operator <<( std::ostream& os, const Cluster& t );

    //! Auxiliary function for build_balanced_tree.
    //! Distributes MPI ranks over a tree (level-wise, as long there are MPI ranks left).
    void create_mpi_rank_distribution( int num_ranks );

private:

    bool    is_leaf_;
    bool    is_root_;
    bool    is_first_son_;

    //! MPI rank where the corresponding tensor node is stored on.
    int     mpi_rank_;

    //! Rank of the respective matricization belonging to the modes
    Cluster *   father_;
    std::unique_ptr<Cluster> son1_ = nullptr;
    std::unique_ptr<Cluster> son2_ = nullptr;

    //! The cluster (set of tensor directions).
    std::vector< std::size_t >   modes_;

    //! Auxilairy function for Cluster(Cluster*,Cluster*)
    //! Increases all modes in the tree by inc
    void increase_modes(const std::size_t inc);

    //! Auxiliary function for build_balanced_tree.
    //! Builds a balanced tree.
    void build_balanced_tree_recursively( Cluster* father = nullptr );

    //! Auxiliary function for build_balanced_tree_old.
    //! Builds a balanced tree including an MPI rank distribution.
    void build_balanced_tree_recursively_old( int& mpi_rank, int& num_ranks, Cluster* father = nullptr );

    //! Auxiliary function. Collects const Cluster * pointers to all nodes on level lvl in vector v.
    void level( std::size_t lvl, std::vector< const Cluster* >& v ) const;

    //! Auxiliary function. Collects Cluster * pointers to all nodes on level lvl in vector v.
    void level( std::size_t lvl, std::vector< Cluster* >& v );

    //! Auxiliary function. Counts number of levels from this on.
    std::size_t num_levels( std::size_t lvl = 1 ) const;

    //! Counts the number of nodes of the (sub)tree starting at this.
    std::size_t num_nodes() const;
};

} //namespace lowrat


#endif //LOWRAT_CLUSTER_CLUSTER_H_
