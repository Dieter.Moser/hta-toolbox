#ifndef LOWRAT_FULL_TENSOR_TENSOR_MODE_H_
#define LOWRAT_FULL_TENSOR_TENSOR_MODE_H_

#include <cstddef>//for size_t
#include <vector>//for vector


namespace lowrat
{

namespace tensor_mode
{

std::vector< std::size_t > complement(std::vector< std::size_t > modes_a, std::vector< std::size_t > modes_b);

std::vector< std::size_t > set_union(std::vector< std::size_t > modes_a, std::vector< std::size_t > modes_b);

std::vector< std::size_t > absolute_complement(std::vector< std::size_t > modes_a, std::size_t dim);

} //namespace tensor_mode

} //namespace lowrat


#endif //LOWRAT_FULL_TENSOR_TENSOR_MODE_H_