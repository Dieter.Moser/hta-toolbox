#ifndef LOWRAT_FULL_TENSOR_TENSOR_H_
#define LOWRAT_FULL_TENSOR_TENSOR_H_

#include <cstddef>//for size_t
#include <algorithm>//for sort, set_difference, set_union
#include <functional>//for std::function
#include <iterator>//for inserter
#include <map>//for map
#include <numeric>//for iota
#include <vector>//for vector


namespace lowrat
{

typedef std::vector< std::size_t > tensorindex;
typedef std::vector< std::size_t > tensorsize;
typedef std::vector< std::size_t > tensormode;

//!
//! \class      Tensor
//! \brief      Base class for tensors.
//!

class Tensor
{
public:
    //! Constructor.
    Tensor(const std::size_t dim = 0 );
    Tensor(const std::size_t dim, const tensorsize& size );
    Tensor(const std::size_t dim, const tensorsize& size, const std::map< tensorindex, double >& data);
    Tensor(const std::size_t dim, const tensorsize& size, const std::function<void(const std::vector< tensorindex >&, std::vector< double >&)>& eval );


    //! Destructor.
    //is needed because of the implementation of the python interface, we should change this and enforce the rule of zero on this object
    virtual ~Tensor() = default;

    Tensor(const Tensor&) = default;             // copy constructor
    Tensor& operator=(const Tensor&) = default;  // copy assignment
    Tensor(Tensor&&) = default;                  // move constructor
    Tensor& operator=(Tensor&&) = default;


    std::size_t dim() const
    {
        return dim_;
    };

    //! Returns size of mu-th mode.
    std::size_t size(const std::size_t mu ) const;

    std::size_t size(const tensormode& m) const
    {
        std::size_t mul = 1;
        for (const auto& k : m)
        {
            mul *= size(k);
        }
        return mul;
    };

    //! Returns vector of all sizes.
    tensorsize vector_of_sizes() const;

    //! Resizes tensor.
    void resize( const tensorsize& size );

    //! Returns number of already computed entries.
    std::size_t computed_entries() const;


    //! Set already computed coeffs
    //WE do not want this
    void set_computed_coeff(const std::map< tensorindex, double >& computed_coeff)
    {
        computed_coeff_ = computed_coeff;
    }

    //! Returns tensor coefficient for one single index.
    double entry( const tensorindex& index ) const;


    //! Computes tensor coefficients for a vector of indices.
    void entries( const std::vector< tensorindex >& indices, std::vector< double >& entries) const;

    //!
    double operator()( const tensorindex& index ) const
    {
        return entry(index);
    }


    //!
    bool tensorindex_out_of_range(const tensorindex& index ) const;
    bool vectorindex_out_of_range(const size_t vectorindex, const std::vector<size_t>& modes) const;

    //! random_index
    void random_index(tensorindex& idx) const;

    const std::map< tensorindex, double >& get_computed_coeff() const
    {
        return computed_coeff_;
    }

    //! Evalutation of tensor coefficients for a vector of indices.
    //! Has to be implemented by derived tensor classes.
    virtual void eval( const std::vector< tensorindex >& indices, std::vector< double >& entries) const;

protected:
    //! Tensor dimension.
    std::size_t dim_;

    //this has to be set by the child class
    //! Sizes of tensor modes.
    tensorsize size_;

private:

    std::function< void ( const std::vector< tensorindex >&, std::vector< double >& ) > eval_ = [](const std::vector< tensorindex >& idx, std::vector< double >& )
    {
        switch (idx.size())
        {
        case 0:
            break;
        default:
            throw "eval not implemented";
        }

    } ;

    //! Already computed coefficients.
    //! Mutable since functionality is not altered.
    mutable std::map< tensorindex, double > computed_coeff_;


};


} //namespace lowrat


#endif //LOWRAT_FULL_TENSOR_TENSOR_H_