#ifndef LOWRAT_FULL_TENSOR_MATRICIZATION_VIEW_H_
#define LOWRAT_FULL_TENSOR_MATRICIZATION_VIEW_H_

#include <cstddef>//for size_t
#include <vector>//for vector

#include <blaze/Blaze.h> //for DynamicMatrix, DynamicVector, columnVector, rowVector
#include "lowrat/full_tensor/matricization.h" // for Matricization

namespace lowrat
{

typedef std::vector< std::size_t >  tensorindex;

//!
//! \class      MatricizationView
//! \brief      This class gives us a view at the underlying matricization.
//!
class MatricizationView
{
public:
    //! Constructor.
    MatricizationView(const Matricization& M, const std::vector<tensorindex>& pivots );

    std::size_t rows() const;
    std::size_t columns() const;

    double operator ()(std::size_t i, std::size_t j) const;
    double operator ()(const tensorindex& p) const;
    std::vector<double> operator ()(const std::vector<tensorindex>& p) const;

    std::vector<tensorindex> get_column_tensor_indices(std::size_t j) const;
    std::vector<tensorindex> get_row_tensor_indices(std::size_t i) const;

    blaze::DynamicVector<double, blaze::columnVector> get_column(std::size_t j) const;
    blaze::DynamicVector<double, blaze::rowVector> get_row(std::size_t i) const;

    blaze::DynamicMatrix<double, blaze::columnMajor> get_full_matrix() const;

    tensorindex get_tensorindex(std::size_t i, std::size_t j) const;
    void get_tensorindex(std::size_t i, std::size_t j, tensorindex& temp) const;

private:
    const Matricization& M_;
    std::vector<tensorindex> pivots_;

    std::size_t rows_ = 0;
    std::size_t columns_ = 0;

};

//! Some free functions for MatricizationView
tensorindex get_tensorindex(const MatricizationView& M, std::size_t i, std::size_t j);

blaze::DynamicVector<double, blaze::columnVector> column(const MatricizationView& M, std::size_t j);
blaze::DynamicVector<double, blaze::rowVector> row(const MatricizationView& M, std::size_t i);

blaze::DynamicMatrix<double, blaze::columnMajor> full_matrix(const MatricizationView& M);


} //namespace lowrat

#endif //LOWRAT_FULL_TENSOR_MATRICIZATION_VIEW_H_