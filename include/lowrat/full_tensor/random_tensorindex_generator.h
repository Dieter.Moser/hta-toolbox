#ifndef LOWRAT_FULL_TENSOR_RANDOM_TENSORINDEX_GENERATOR_H_
#define LOWRAT_FULL_TENSOR_RANDOM_TENSORINDEX_GENERATOR_H_

#include <cstddef>//for size_t
#include <random> //uniform_int_distribution
#include <vector>//for vector

#include "lowrat/full_tensor/tensorindex_range.h" //for TensorindexRange

namespace lowrat
{

typedef std::vector< std::size_t >  tensorindex;

class RandomTensorindexGenerator
{

public:
    RandomTensorindexGenerator(const TensorindexRange& range, const std::size_t n_samples = 50);
    tensorindex get_random_index() const;
    tensorindex next_index() const;
    //void push_back(tensorindex ti);
    std::vector<tensorindex> sample(const std::size_t j) const;
    std::vector<tensorindex> get_sampled() const;
    void sample(const std::size_t K, std::vector<tensorindex>& index_list) const;
    void randomize(tensorindex& ti) const;

private:
    const TensorindexRange& tir_;
    std::size_t maxRTI_ = 50;
    //mutable because this is more or less lazy evaluation
    mutable std::vector<std::uniform_int_distribution<std::size_t>> random_dists_;
    mutable std::vector<tensorindex> generated_indices_;

};

} //namespace lowrat


#endif //LOWRAT_FULL_TENSOR_RANDOM_TENSORINDEX_GENERATOR_H_