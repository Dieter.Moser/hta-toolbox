#ifndef LOWRAT_FULL_TENSOR_MATRICIZATION_H_
#define LOWRAT_FULL_TENSOR_MATRICIZATION_H_

#include <cstddef>//for size_t
#include <vector>//for vector

#include <blaze/Blaze.h> //for DynamicMatrix, DynamicVector, columnVector, rowVector
#include "lowrat/full_tensor/tensor.h" //for Tensor
#include "lowrat/full_tensor/tensorindex_range.h" //for TensorindexRange

namespace lowrat
{

typedef     std::vector< size_t >  tensorindex;
typedef     std::vector< size_t >  tensorsize;
typedef     std::vector< size_t >  tensormode;

//!
//!   \class      Matricization
//!   \brief      A class which yields a Matrix view on a Tensor
//!

class Matricization
{
public:
    Matricization(const Tensor& T, const tensormode& row_modes);

    double operator()(const tensorindex& p) const;
    std::vector<double> operator ()(const std::vector<tensorindex>& p) const;



    tensorsize row_mode_sizes() const; // return sizes of row modes
    tensorsize column_mode_sizes() const; // return sizes of col modes

    std::size_t rows() const;
    std::size_t columns() const;

    std::size_t dim() const
    {
        return T_.dim();
    }

    std::vector<std::size_t> row_modes() const
    {
        return row_range_.modes();
    }
    std::vector<std::size_t> column_modes() const
    {
        return col_range_.modes();
    }

    //we need to make col_range_ mutable to make this const! is this okay?
    blaze::DynamicMatrix<double> get_full_matrix();

    blaze::DynamicVector<double, blaze::rowVector> get_row(const tensorindex& ti) ;
    blaze::DynamicVector<double, blaze::columnVector> get_column(const tensorindex& ti) ;

    void get_tensorindex(std::size_t i, std::size_t j, const std::vector<tensorindex>& pivots, tensorindex& temp) const;
    tensorindex get_tensorindex(const size_t i, const size_t j, const std::vector<tensorindex> & pivots) const;

    void get_random_indices(size_t K, std::vector<tensorindex> &indices, bool generate_non_quadratic_matrices = false) const;
    void get_random_indices_nested(size_t K, const std::vector<tensorindex> &pre_sampled_indices, tensormode &t_bracket, std::vector<tensorindex> &indices, bool generate_non_quadratic_matrices = false) const;

    blaze::DynamicMatrix<double> get_submatrix_from_columns(std::vector<tensorindex> & pivots);

    // get M_t list
    std::vector< blaze::DynamicMatrix<double> > get_M_t_list(const std::vector<tensorindex>& P_t1, const std::vector<tensorindex>& P_t2, const std::vector<tensorindex>& Q_t, const tensormode& t1) const;

private:
    const Tensor& T_;
    TensorindexRange row_range_;
    TensorindexRange col_range_;
    TensorindexRange full_range_;

    std::size_t row_size_ = 0;
    bool row_size_overflow_ = false;
    std::size_t column_size_ = 0;
    bool column_size_overflow_ = false;

    void set_row_size_();
    void set_column_size_();
};

} //namespace lowrat


#endif //LOWRAT_FULL_TENSOR_MATRICIZATION_H_
