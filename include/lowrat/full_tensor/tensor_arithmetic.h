#ifndef LOWRAT_FULL_TENSOR_TENSOR_ARITHMETIC_H_
#define LOWRAT_FULL_TENSOR_TENSOR_ARITHMETIC_H_

#include <cstddef>//for size_t
#include <vector>//for vector

#include <blaze/Blaze.h>
#include "lowrat/full_tensor/tensor.h"

namespace lowrat
{

Tensor contract_tensor(const Tensor& T, const blaze::DynamicMatrix< double, blaze::rowMajor >& U, std::size_t d);
Tensor contract_tensor_multiple_directions(const Tensor& T, const std::vector<blaze::DynamicMatrix< double, blaze::rowMajor>>& U_vec, const std::vector<std::size_t>& directions);
Tensor contract_tensor(const Tensor& T, const blaze::DynamicMatrix< double, blaze::rowMajor >& U, const tensormode& tm);
// Tensor contract_tensor(const Tensor& T, const blaze::DynamicMatrix< double, blaze::rowMajor >& U, const tensormode& tm, const tensorsize& sizes);
// Tensor contract_tensor(const Tensor& T, const std::vector<blaze::DynamicMatrix< double, blaze::rowMajor>>& U_vec, const std::vector<tensormode>& modes, const std::vector<tensorsize>& sizes);

} //namespace lowrat


#endif //LOWRAT_FULL_TENSOR_TENSOR_ARITHMETIC_H_
