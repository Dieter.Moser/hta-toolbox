#ifndef LOWRAT_FULL_TENSOR_TENSORINDEX_RANGE_H_
#define LOWRAT_FULL_TENSOR_TENSORINDEX_RANGE_H_

#include <cstddef>//for size_t
#include <iterator> //for reverse_iterator, bidirectional_iterator_tag
#include <vector>//for vector

#include "lowrat/full_tensor/tensor.h" //for Tensor

namespace lowrat
{

typedef     std::vector< std::size_t >  tensorindex;
typedef     std::vector< std::size_t >  tensorsize;
typedef     std::vector< std::size_t >  tensormode;


//this class represent the range of a tensorindex and provides an iterator, therefore this iterator is completly const
class TensorindexRange
{

    friend class RandomTensorindexGenerator;

public:
    class iterator;
    typedef iterator const_iterator;
    typedef std::reverse_iterator<iterator> reverse_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

    iterator         begin();
    iterator         end();
    reverse_iterator rbegin();
    reverse_iterator rend();

    TensorindexRange() {};
    TensorindexRange(const Tensor& T, const tensormode& modes)
    {
        TensorindexRange(T.vector_of_sizes(), modes);
    };
    TensorindexRange(const tensorsize& size, const tensormode& modes, const tensorindex& ti);
    //TensorindexRange(const TensorindexRange& other);
    TensorindexRange(const tensorsize& size, const tensormode& modes);

    void set(const tensorindex& ti);
    void set(const std::size_t pos, const std::size_t fixed);
    void merge(const tensorindex& this_one, const tensorindex& and_that, tensorindex& into) const;

    tensormode modes() const;

private:
    //we should make this const because it should never change
    tensorsize size_;

    //this is all optinal and more or less a new extra range
    tensormode modes_;       // used modes
    tensormode fixed_modes_;
    tensorindex fixed_;       // the fixed indices itself

    std::size_t dim() const
    {
        return size_.size();
    }
};

class TensorindexRange::iterator
{
public:
    typedef std::vector<size_t> value_type;
    typedef std::vector<size_t>  const*     pointer;
    typedef std::vector<size_t>  const&     reference;
    typedef std::ptrdiff_t difference_type;
    //should we provide random acces?
    typedef std::bidirectional_iterator_tag iterator_category;

    iterator(TensorindexRange* tir);
    bool operator== (iterator const& other) const;
    bool operator!= (iterator const& other) const;
    value_type operator*() const;

    void one_up();
    void one_down();

    void set_to_first();
    void set_to_last();
    void set_to_start();
    void set_to_end();

    iterator& operator++()
    {
        if (at_end_ == false)
        {
            one_up();
            if (at_end_)
            {
                set_to_end();
            }
        }
        else
        {
            set_to_end();
        }
        return *this;
    }

    iterator  operator++(int)
    {
        iterator rc(*this);
        this->operator++();
        return rc;
    }

    iterator& operator--()
    {
        if (at_beginning_ == false)
        {
            one_down();
            if (at_beginning_)
            {
                set_to_start();
            }
        }
        else
        {
            set_to_start();
        }
        return *this;
    }

    iterator  operator--(int)
    {
        iterator rc(*this);
        this->operator--();
        return rc;
    }

private:
    std::vector<std::size_t> ti_;
    TensorindexRange*        tir_;
    bool                     at_beginning_, at_end_;
};

} //namespace lowrat



#endif //LOWRAT_FULL_TENSOR_TENSORINDEX_RANGE_H_