#ifndef LOWRAT_HT_TENSOR_HTENSOR_IPP_
#define LOWRAT_HT_TENSOR_HTENSOR_IPP_

#include <cstddef>//for std::size_t
#include <limits>// for numeric_limits
#include <vector>//for vector

#include <blaze/Blaze.h> //DynamicVector, DynamicMatrix
#if MPI_ACTIVE
#include "mpi.h" //for MPI
#include "lowrat/util/parallel.h" //for blaze MPI, PStream
#endif
#include "lowrat/config/config.h"//for lowrat::value_t
#include "lowrat/cluster/cluster.h" //for Cluster
#include "lowrat/util/io.h" //for cout of a vector

namespace lowrat
{


typedef     blaze::DynamicMatrix< value_t, blaze::columnMajor > DMatrix;



//! Template for the development of leaves-to-root algorithms.
//! The functions can be defined by lambda functions in order to capture variables
//!     from outside (e.g. further input arguments of the algorithm which are
//!     requried during leaf_op, inner_op or root_op.
//! @param ext_operands contains pointers to possible further operands.
//! @param leaf_op function defining the algorithm at the leaf nodes.
//! @param inner_op function defining the algorithm at the inner nodes.
//! @param root_op function defining the algorithm at the root node.
//! @param father_data is used to transport data between nodes at the same MPI
//!                    process (the method recursively calls itself).
//!                    Just handle an empy matrix.
//! @param reduce_result when set to \link rootBcast \endlink (default),
//!                      the return matrix of the root process is broadcast to all
//!                      other processes (same return matrix at each process).\n
//!                      When set to \link maxReduction \endlink,
//!                      \link minReduction \endlink, \link sumReduction \endlink,
//!                      \link prodReduction \endlink, the respective
//!                      reduction of the return matrices is returned at each
//!                      process.\n
//!                      When set to \link none \endlink, each process returns
//!                      its own matrix.
//! @param i corresponds to the index of the internal data
//!          \link HTensor::data_ \endlink which the algorithm currently
//!          is processing. Used to recursively process the son nodes.
template< typename Type, typename returnType, bool SO >
blaze::DynamicMatrix< returnType, SO > HTensor::leaves_to_root_algorithm(
    const std::vector< const HTensor * > ext_operands,
    std::function< blaze::DynamicMatrix< returnType, SO >(
        leaf_data & data,
        const size_t leaf_number,
        std::vector< const leaf_data * > data_ext,
        blaze::DynamicMatrix< Type, SO > & father_data,
        const size_t data_index ) > leaf_op,
    std::function< blaze::DynamicMatrix< returnType, SO >(
        node_data & data,
        std::vector< const node_data * > data_ext,
        blaze::DynamicMatrix< Type, SO > & father_data,
        const blaze::DynamicMatrix< Type, SO > & son1_data,
        const blaze::DynamicMatrix< Type, SO > & son2_data,
        const size_t data_index ) > inner_op,
    std::function< blaze::DynamicMatrix< returnType, SO >(
        root_data & data,
        std::vector< const root_data * > data_ext,
        const blaze::DynamicMatrix< Type, SO > & son1_data,
        const blaze::DynamicMatrix< Type, SO > & son2_data,
        const size_t data_index ) > root_op,
    blaze::DynamicMatrix< Type, SO > & father_data,
    ReductionType reduce_result,
    size_t i )
{
    int my_rank = 0;
#if MPI_ACTIVE
    MPI_Comm_rank( comm_, &my_rank );
#endif
    std::vector< const Cluster * > local_nodes = tree_.all_nodes_of_rank( my_rank );

    size_t num_ext_op = ext_operands.size();

    blaze::DynamicMatrix< returnType, SO > return_matrix,
          return_matrix_son1, return_matrix_son2;

#if MPI_ACTIVE
    MPI_Request request, request1, request2;
#endif

    // Determine MPI_Datatype:
#if MPI_ACTIVE
    MPI_Datatype mpi_datatype = MPI_CHAR;
    if ( typeid( Type ) == typeid( short int ) )
    {
        mpi_datatype = MPI_SHORT;
    }
    else if ( typeid( Type ) == typeid( int ) )
    {
        mpi_datatype = MPI_INT;
    }
    else if ( typeid( Type ) == typeid( long int ) )
    {
        mpi_datatype = MPI_LONG;
    }
    else if ( typeid( Type ) == typeid( unsigned char ) )
    {
        mpi_datatype = MPI_UNSIGNED_CHAR;
    }
    else if ( typeid( Type ) == typeid( unsigned short int ) )
    {
        mpi_datatype = MPI_UNSIGNED_SHORT;
    }
    else if ( typeid( Type ) == typeid( unsigned int ) )
    {
        mpi_datatype = MPI_UNSIGNED;
    }
    else if ( typeid( Type ) == typeid( unsigned long int ) )
    {
        mpi_datatype = MPI_UNSIGNED_LONG;
    }
    else if ( typeid( Type ) == typeid( float ) )
    {
        mpi_datatype = MPI_FLOAT;
    }
    else if ( typeid( Type ) == typeid( double ) )
    {
        mpi_datatype = MPI_DOUBLE;
    }
    else if ( typeid( Type ) == typeid( long double ) )
    {
        mpi_datatype = MPI_LONG_DOUBLE;
    }
    else if ( typeid( Type ) == typeid( long long int ) )
    {
        mpi_datatype = MPI_LONG_LONG_INT;
    }
#endif

    if ( node_used_ )
    {
        if ( local_nodes[ i ]->is_leaf() )
        {
            std::vector< const leaf_data * > ext_data( num_ext_op );
            for ( size_t j = 0; j < num_ext_op; ++j )
            {
                ext_data[ j ] = &(ext_operands[ j ]->data_[ i ][ 0 ]);
            }
            size_t mu = local_nodes[ i ]->leaf_number();
            return_matrix = leaf_op( data_[ i ][ 0 ], mu, ext_data, father_data, i );
        }
        else
        {
            blaze::DynamicMatrix< Type, SO > son1_data, son2_data;
            if ( local_nodes[ i ]->son1()->mpi_rank() == my_rank )
            {
                // Sons on this MPI rank.
                size_t i_son1 = i;
                size_t i_son2 = i;
                for ( size_t j = i; j < local_nodes.size(); ++j )
                {
                    if ( local_nodes[ j ] == local_nodes[ i ]->son1() )
                    {
                        i_son1 = j;
                    }
                    else if ( local_nodes[ j ] == local_nodes[ i ]->son2() )
                    {
                        i_son2 = j;
                    }
                }

                // Recursion
                return_matrix_son1 = leaves_to_root_algorithm( ext_operands, leaf_op, inner_op, root_op, son1_data, reduce_result, i_son1 );
                return_matrix_son2 = leaves_to_root_algorithm( ext_operands, leaf_op, inner_op, root_op, son2_data, reduce_result, i_son2 );
            }
            else
            {
                // Sons NOT on this MPI rank.
                // Receive data via MPI.
                blaze::CustomMatrix< Type, blaze::aligned, blaze::usePadding, SO > C1;
                blaze::CustomMatrix< Type, blaze::aligned, blaze::usePadding, SO > C2;
#if MPI_ACTIVE
                MPI_Irecv_DynamicMatrix( C1, mpi_datatype, local_nodes[ i ]->son1()->mpi_rank(), 0, comm_, &request1 );
                MPI_Irecv_DynamicMatrix( C2, mpi_datatype, local_nodes[ i ]->son2()->mpi_rank(), 0, comm_, &request2 );

                MPI_Status status;

                MPI_Wait( &request1, &status );
                son1_data = C1;

                MPI_Wait( &request2, &status );
                son2_data = C2;
#endif
            }

            if ( local_nodes[ i ]->is_root() )
            {
                std::vector< const root_data * > ext_data( num_ext_op );
                for ( size_t j = 0; j < num_ext_op; ++j )
                {
                    ext_data[ j ] = &(ext_operands[ j ]->data_[ i ][ 0 ]);
                }
                return_matrix = root_op( data_[ i ][ 0 ], ext_data, son1_data, son2_data, i );
            }
            else
            {
                // Inner node.
                std::vector< const node_data * > ext_data( num_ext_op );
                for ( size_t j = 0; j < num_ext_op; ++j )
                {
                    ext_data[ j ] = &(ext_operands[ j ]->data_[ i ]);
                }
                return_matrix = inner_op( data_[ i ], ext_data, father_data, son1_data, son2_data, i );
            }
        }

        if ( !local_nodes[ i ]->is_leaf() && local_nodes[ i ]->son1()->mpi_rank() == my_rank )
        {
            // Sons on this MPI rank, reduction:
            if ( reduce_result != none && reduce_result != rootBcast )
            {
                switch ( reduce_result )
                {
                case minReduction:
                    for ( size_t l = 0; l < return_matrix.columns(); ++l )
                    {
                        for ( size_t j = 0; j < return_matrix.rows(); ++j )
                        {
                            return_matrix( j, l ) = std::min( return_matrix( j, l ), std::min( return_matrix_son1( j, l ), return_matrix_son2( j, l ) ) );
                        }
                    }
                    break;
                case sumReduction:
                    return_matrix += return_matrix_son1;
                    return_matrix += return_matrix_son2;
                    break;
                case prodReduction:
                    return_matrix %= return_matrix_son1;
                    return_matrix %= return_matrix_son2;
                    break;
                default:  // maxReduction
                    for ( size_t l = 0; l < return_matrix.columns(); ++l )
                    {
                        for ( size_t j = 0; j < return_matrix.rows(); ++j )
                        {
                            return_matrix( j, l ) = std::max( return_matrix( j, l ), std::max( return_matrix_son1( j, l ), return_matrix_son2( j, l ) ) );
                        }
                    }
                }// switch ( reduce_result )
            }// if ( reduce_result != none && reduce_result != rootBcast )
        }// if ( !local_nodes[ i ]->is_leaf() && local_nodes[ i ]->son1()->mpi_rank() == my_rank )

        if ( !local_nodes[ i ]->is_root() )
        {
            if ( local_nodes[ i ]->father()->mpi_rank() != my_rank )
            {
                // Send computed data to father:
#if MPI_ACTIVE
                MPI_Isend_DynamicMatrix( father_data, mpi_datatype, local_nodes[ i ]->father()->mpi_rank(), 0, comm_, &request, true );
                MPI_Status status;
                MPI_Wait( &request, &status );
#endif
            }
        }
    }// if ( node_used_ )

    // Broadcast/Reduction:
    if ( reduce_result != none )
    {
        // Determine MPI_Datatype for the reduction:
#if MPI_ACTIVE
        MPI_Datatype mpi_datatype_return = MPI_CHAR;
        if ( typeid( returnType ) == typeid( short int ) )
        {
            mpi_datatype_return = MPI_SHORT;
        }
        else if ( typeid( returnType ) == typeid( int ) )
        {
            mpi_datatype_return = MPI_INT;
        }
        else if ( typeid( returnType ) == typeid( long int ) )
        {
            mpi_datatype_return = MPI_LONG;
        }
        else if ( typeid( returnType ) == typeid( unsigned char ) )
        {
            mpi_datatype_return = MPI_UNSIGNED_CHAR;
        }
        else if ( typeid( returnType ) == typeid( unsigned short int ) )
        {
            mpi_datatype_return = MPI_UNSIGNED_SHORT;
        }
        else if ( typeid( returnType ) == typeid( unsigned int ) )
        {
            mpi_datatype_return = MPI_UNSIGNED;
        }
        else if ( typeid( returnType ) == typeid( unsigned long int ) )
        {
            mpi_datatype_return = MPI_UNSIGNED_LONG;
        }
        else if ( typeid( returnType ) == typeid( float ) )
        {
            mpi_datatype_return = MPI_FLOAT;
        }
        else if ( typeid( returnType ) == typeid( double ) )
        {
            mpi_datatype_return = MPI_DOUBLE;
        }
        else if ( typeid( returnType ) == typeid( long double ) )
        {
            mpi_datatype_return = MPI_LONG_DOUBLE;
        }
        else if ( typeid( returnType ) == typeid( long long int ) )
        {
            mpi_datatype_return = MPI_LONG_LONG_INT;
        }

        if ( reduce_result == rootBcast )
        {
            // Broadcast return value from root (MPI rank 0):
            if ( i == 0 )
            {
                MPI_Bcast_DynamicMatrix( return_matrix, mpi_datatype_return, 0, comm_ );
            }
        }
        else
        {
            // For reduction, return_matrix must be of same size at
            // each MPI process (also those which are not used).
            // Set new matrix entries in such way that they do not affect the
            // reduction:
            if ( i == 0 )
            {
                size_t * sizes = new size_t[ 2 ];
                sizes[ 0 ] = return_matrix.rows();
                sizes[ 1 ] = return_matrix.columns();
                size_t * max_sizes = new size_t[ 2 ];
                MPI_Allreduce( sizes, max_sizes, 2, _MPI_SIZE_T, MPI_MAX, comm_ );
                if ( sizes[ 0 ] != max_sizes[ 0 ] || sizes[ 1 ] != max_sizes[ 1 ] )
                {
                    return_matrix.resize( max_sizes[ 0 ], max_sizes[ 1 ] );
                    // Set new values
                    double new_value = std::numeric_limits<double>::lowest();    // case of maxReduction
                    switch ( reduce_result )
                    {
                    case minReduction:
                        new_value = std::numeric_limits<double>::max();
                        break;
                    case sumReduction:
                        new_value = 0.0;
                        break;
                    case prodReduction:
                        new_value = 1.0;
                        break;
                    default:
                        new_value = std::numeric_limits<double>::lowest();// for maxReduction
                    }
                    for ( size_t l = sizes[ 1 ]; l < max_sizes[ 1 ]; ++l )
                    {
                        for ( size_t j = 0; j < max_sizes[ 0 ]; ++j )
                        {
                            return_matrix( j, l ) = new_value;
                        }
                    }
                    for ( size_t l = 0; l < sizes[ 1 ]; ++l )
                    {
                        for ( size_t j = sizes[ 0 ]; j < max_sizes[ 0 ]; ++j )
                        {
                            return_matrix( j, l ) = new_value;
                        }
                    }
                }

                delete[] sizes;
                delete[] max_sizes;

                blaze::DynamicMatrix< returnType, SO > reduced_return_matrix;

                // Set MPI_Op op:
                MPI_Op op = MPI_MAX;    // case of maxReduction
                switch ( reduce_result )
                {
                case minReduction:
                    op = MPI_MIN;
                    break;
                case sumReduction:
                    op = MPI_SUM;
                    break;
                case prodReduction:
                    op = MPI_PROD;
                    break;
                default:
                    op = MPI_MAX;
                }

                MPI_Allreduce_DynamicMatrix( return_matrix, reduced_return_matrix, mpi_datatype_return, op, comm_ );
                return_matrix = reduced_return_matrix;
            }// if ( i == 0 )
        }// else
#endif
    }// if ( reduce_result != none )

    return return_matrix;
}

//! Template for the development of root-to-leaves algorithms.
//! The functions can be defined by lambda functions in order to capture variables
//!     from outside (e.g. further input arguments of the algorithm which are
//!     required during leaf_op, inner_op or root_op.
//! @param ext_operands contains pointers to possible further operands.
//! @param leaf_op function defining the algorithm at the leaf nodes.
//! @param inner_op function defining the algorithm at the inner nodes.
//! @param root_op function defining the algorithm at the root node.
//! @param father_data is used to transport data between nodes at the same MPI
//!                    process (the method recursively calls itself).
//!                    Just handle an empty matrix.
//! @param reduce_result when set to \link rootBcast \endlink,
//!                      the return matrix of the root process is broadcast to all
//!                      other processes (same return matrix at each process).\n
//!                      When set to \link maxReduction \endlink,
//!                      \link minReduction \endlink, \link sumReduction \endlink,
//!                      \link prodReduction \endlink, the respective
//!                      reduction of the return matrices is returned at each
//!                      process.\n
//!                      When set to \link none \endlink (default), each process
//!                      returns its own matrix.
//! @param i corresponds to the index of the internal data
//!          \link HTensor::data_ \endlink which the algorithm currently
//!          is processing. Used to recursively process the son nodes.
template< typename Type, typename returnType, bool SO >
blaze::DynamicMatrix< returnType, SO > HTensor::root_to_leaves_algorithm(
    const std::vector< const HTensor * > ext_operands,
    std::function< blaze::DynamicMatrix< returnType, SO >(
        leaf_data & data,
        const size_t leaf_number,
        std::vector< const leaf_data * > data_ext,
        const blaze::DynamicMatrix< Type, SO > & father_data,
        const size_t data_index ) > leaf_op,
    std::function< blaze::DynamicMatrix< returnType, SO >(
        node_data & data,
        std::vector< const node_data * > data_ext,
        const blaze::DynamicMatrix< Type, SO > & father_data,
        blaze::DynamicMatrix< Type, SO > & son1_data,
        blaze::DynamicMatrix< Type, SO > & son2_data,
        const size_t data_index ) > inner_op,
    std::function< blaze::DynamicMatrix< returnType, SO >(
        root_data & data,
        std::vector< const root_data * > data_ext,
        blaze::DynamicMatrix< Type, SO > & son1_data,
        blaze::DynamicMatrix< Type, SO > & son2_data,
        const size_t data_index ) > root_op,
    blaze::DynamicMatrix< Type, SO > & father_data,
    ReductionType reduce_result,
    size_t i )
{
    int my_rank = 0;
#if MPI_ACTIVE
    MPI_Comm_rank( comm_, &my_rank );
#endif
    std::vector< const Cluster * > local_nodes = tree_.all_nodes_of_rank( my_rank );

    size_t num_ext_op = ext_operands.size();

    blaze::DynamicMatrix< returnType, SO >  return_matrix;
    blaze::DynamicMatrix< Type, SO >        son1_data, son2_data;

#if MPI_ACTIVE
    MPI_Request request, request1, request2;
#endif

    // Determine MPI_Datatype:
#if MPI_ACTIVE
    MPI_Datatype mpi_datatype = MPI_CHAR;
    if ( typeid( Type ) == typeid( short int ) )
    {
        mpi_datatype = MPI_SHORT;
    }
    else if ( typeid( Type ) == typeid( int ) )
    {
        mpi_datatype = MPI_INT;
    }
    else if ( typeid( Type ) == typeid( long int ) )
    {
        mpi_datatype = MPI_LONG;
    }
    else if ( typeid( Type ) == typeid( unsigned char ) )
    {
        mpi_datatype = MPI_UNSIGNED_CHAR;
    }
    else if ( typeid( Type ) == typeid( unsigned short int ) )
    {
        mpi_datatype = MPI_UNSIGNED_SHORT;
    }
    else if ( typeid( Type ) == typeid( unsigned int ) )
    {
        mpi_datatype = MPI_UNSIGNED;
    }
    else if ( typeid( Type ) == typeid( unsigned long int ) )
    {
        mpi_datatype = MPI_UNSIGNED_LONG;
    }
    else if ( typeid( Type ) == typeid( float ) )
    {
        mpi_datatype = MPI_FLOAT;
    }
    else if ( typeid( Type ) == typeid( double ) )
    {
        mpi_datatype = MPI_DOUBLE;
    }
    else if ( typeid( Type ) == typeid( long double ) )
    {
        mpi_datatype = MPI_LONG_DOUBLE;
    }
    else if ( typeid( Type ) == typeid( long long int ) )
    {
        mpi_datatype = MPI_LONG_LONG_INT;
    }
#endif

    if ( node_used_ )
    {
        if ( !local_nodes[ i ]->is_root() )
        {
            if ( local_nodes[ i ]->father()->mpi_rank() != my_rank )
            {
                // Father NOT on this MPI rank.
                // Receive data via MPI.
                blaze::CustomMatrix< Type, blaze::aligned, blaze::usePadding, SO > Cf;
#if MPI_ACTIVE
                MPI_Irecv_DynamicMatrix( Cf, mpi_datatype, local_nodes[ i ]->father()->mpi_rank(), 0, comm_, &request );

                MPI_Status status;

                MPI_Wait( &request, &status );
                father_data = Cf;
#endif
            }

            if ( local_nodes[ i ]->is_leaf() )
            {
                std::vector< const leaf_data * > ext_data( num_ext_op );
                for ( size_t j = 0; j < num_ext_op; ++j )
                {
                    ext_data[ j ] = &(ext_operands[ j ]->data_[ i ][ 0 ]);
                }
                size_t mu = local_nodes[ i ]->leaf_number();
                return_matrix = leaf_op( data_[ i ][ 0 ], mu, ext_data, father_data, i );
            }
            else
            {
                // Inner node.
                std::vector< const node_data * > ext_data( num_ext_op );
                for ( size_t j = 0; j < num_ext_op; ++j )
                {
                    ext_data[ j ] = &(ext_operands[ j ]->data_[ i ]);
                }
                return_matrix = inner_op( data_[ i ], ext_data, father_data, son1_data, son2_data, i );
            }
        }// if ( !local_nodes[ i ]->is_root() )
        else
        {
            // Root node.
            std::vector< const root_data * > ext_data( num_ext_op );
            for ( size_t j = 0; j < num_ext_op; ++j )
            {
                ext_data[ j ] = &(ext_operands[ j ]->data_[ i ][ 0 ]);
            }
            return_matrix = root_op( data_[ i ][ 0 ], ext_data, son1_data, son2_data, i );
        }

        if ( !local_nodes[ i ]->is_leaf() )
        {
            if ( local_nodes[ i ]->son1()->mpi_rank() != my_rank )
            {
                // Send computed data to sons:
#if MPI_ACTIVE
                MPI_Isend_DynamicMatrix( son1_data, mpi_datatype, local_nodes[ i ]->son1()->mpi_rank(), 0, comm_, &request1, true );
                MPI_Isend_DynamicMatrix( son2_data, mpi_datatype, local_nodes[ i ]->son2()->mpi_rank(), 0, comm_, &request2, true );
                MPI_Status status;
                MPI_Wait( &request1, &status );
                MPI_Wait( &request2, &status );
#endif
            }
            else
            {
                // Sons on this MPI rank.
                std::size_t i_son1 = i;
                std::size_t i_son2 = i;
                for ( std::size_t j = i; j < local_nodes.size(); ++j )
                {
                    if ( local_nodes[ j ] == local_nodes[ i ]->son1() )
                    {
                        i_son1 = j;
                    }
                    else if ( local_nodes[ j ] == local_nodes[ i ]->son2() )
                    {
                        i_son2 = j;
                    }
                }
                // Recursion:
                blaze::DynamicMatrix< returnType, SO > return_matrix_son1 = root_to_leaves_algorithm( ext_operands, leaf_op, inner_op, root_op, son1_data, reduce_result, i_son1 );
                blaze::DynamicMatrix< returnType, SO > return_matrix_son2  = root_to_leaves_algorithm( ext_operands, leaf_op, inner_op, root_op, son2_data, reduce_result, i_son2 );

                // Reduction:
                if ( reduce_result != none && reduce_result != rootBcast )
                {
                    switch ( reduce_result )
                    {
                    case minReduction:
                        for ( size_t l = 0; l < return_matrix.columns(); ++l )
                        {
                            for ( size_t j = 0; j < return_matrix.rows(); ++j )
                            {
                                return_matrix( j, l ) = std::min( return_matrix( j, l ), std::min( return_matrix_son1( j, l ), return_matrix_son2( j, l ) ) );
                            }
                        }
                        break;
                    case sumReduction:
                        return_matrix += return_matrix_son1;
                        return_matrix += return_matrix_son2;
                        break;
                    case prodReduction:
                        return_matrix %= return_matrix_son1;
                        return_matrix %= return_matrix_son2;
                        break;
                    default:  // maxReduction
                        for ( size_t l = 0; l < return_matrix.columns(); ++l )
                        {
                            for ( size_t j = 0; j < return_matrix.rows(); ++j )
                            {
                                return_matrix( j, l ) = std::max( return_matrix( j, l ), std::max( return_matrix_son1( j, l ), return_matrix_son2( j, l ) ) );
                            }
                        }
                    }// switch ( reduce_result )
                }// if ( reduce_result != none && reduce_result != rootBcast )
            }// else
        }// if ( !local_nodes[ i ]->is_leaf() )
    }// if ( node_used_ )

    // Broadcast/Reduction:
    if ( reduce_result != none )
    {
        // Determine MPI_Datatype for the reduction:
#if MPI_ACTIVE
        MPI_Datatype mpi_datatype_return = MPI_CHAR;
        if ( typeid( returnType ) == typeid( short int ) )
        {
            mpi_datatype_return = MPI_SHORT;
        }
        else if ( typeid( returnType ) == typeid( int ) )
        {
            mpi_datatype_return = MPI_INT;
        }
        else if ( typeid( returnType ) == typeid( long int ) )
        {
            mpi_datatype_return = MPI_LONG;
        }
        else if ( typeid( returnType ) == typeid( unsigned char ) )
        {
            mpi_datatype_return = MPI_UNSIGNED_CHAR;
        }
        else if ( typeid( returnType ) == typeid( unsigned short int ) )
        {
            mpi_datatype_return = MPI_UNSIGNED_SHORT;
        }
        else if ( typeid( returnType ) == typeid( unsigned int ) )
        {
            mpi_datatype_return = MPI_UNSIGNED;
        }
        else if ( typeid( returnType ) == typeid( unsigned long int ) )
        {
            mpi_datatype_return = MPI_UNSIGNED_LONG;
        }
        else if ( typeid( returnType ) == typeid( float ) )
        {
            mpi_datatype_return = MPI_FLOAT;
        }
        else if ( typeid( returnType ) == typeid( double ) )
        {
            mpi_datatype_return = MPI_DOUBLE;
        }
        else if ( typeid( returnType ) == typeid( long double ) )
        {
            mpi_datatype_return = MPI_LONG_DOUBLE;
        }
        else if ( typeid( returnType ) == typeid( long long int ) )
        {
            mpi_datatype_return = MPI_LONG_LONG_INT;
        }

        if ( reduce_result == rootBcast )
        {
            // Broadcast return value from root (MPI rank 0):
            if ( i == 0 )
            {
                MPI_Bcast_DynamicMatrix( return_matrix, mpi_datatype_return, 0, comm_ );
            }
        }
        else
        {
            // For reduction, return_matrix must be of same size at
            // each MPI process (also those which are not used).
            // Set new matrix entries in such way that they do not affect the
            // reduction:
            if ( i == 0 )
            {
                size_t * sizes = new size_t[ 2 ];
                sizes[ 0 ] = return_matrix.rows();
                sizes[ 1 ] = return_matrix.columns();
                size_t * max_sizes = new size_t[ 2 ];
                MPI_Allreduce( sizes, max_sizes, 2, _MPI_SIZE_T, MPI_MAX, comm_ );
                if ( sizes[ 0 ] != max_sizes[ 0 ] || sizes[ 1 ] != max_sizes[ 1 ] )
                {
                    return_matrix.resize( max_sizes[ 0 ], max_sizes[ 1 ] );
                    // Set new values
                    double new_value = std::numeric_limits<double>::lowest();    // case of maxReduction
                    switch ( reduce_result )
                    {
                    case minReduction:
                        new_value = std::numeric_limits<double>::max();
                        break;
                    case sumReduction:
                        new_value = 0.0;
                        break;
                    case prodReduction:
                        new_value = 1.0;
                        break;
                    default:
                        new_value = std::numeric_limits<double>::lowest();// for maxReduction
                    }
                    for ( size_t l = sizes[ 1 ]; l < max_sizes[ 1 ]; ++l )
                    {
                        for ( size_t j = 0; j < max_sizes[ 0 ]; ++j )
                        {
                            return_matrix( j, l ) = new_value;
                        }
                    }
                    for ( size_t l = 0; l < sizes[ 1 ]; ++l )
                    {
                        for ( size_t j = sizes[ 0 ]; j < max_sizes[ 0 ]; ++j )
                        {
                            return_matrix( j, l ) = new_value;
                        }
                    }
                }

                delete[] sizes;
                delete[] max_sizes;

                blaze::DynamicMatrix< returnType, SO > reduced_return_matrix;

                // Set MPI_Op op:
                MPI_Op op = MPI_MAX;    // case of maxReduction
                switch ( reduce_result )
                {
                case minReduction:
                    op = MPI_MIN;
                    break;
                case sumReduction:
                    op = MPI_SUM;
                    break;
                case prodReduction:
                    op = MPI_PROD;
                    break;
                default:
                    op = MPI_MAX;
                }

                MPI_Allreduce_DynamicMatrix( return_matrix, reduced_return_matrix, mpi_datatype_return, op, comm_ );
                return_matrix = reduced_return_matrix;
            }// if ( i == 0 )
        }// else
#endif
    }// if ( reduce_result != none )

    return return_matrix;
}

template< typename returnType, bool SO >
blaze::DynamicMatrix< returnType, SO > HTensor::fully_parallel_algorithm(
    const std::vector< const HTensor * > ext_operands,
    std::function< blaze::DynamicMatrix< returnType, SO >(
        leaf_data & data,
        const size_t leaf_number,
        std::vector< const leaf_data * > data_ext,
        const size_t data_index ) > leaf_op,
    std::function< blaze::DynamicMatrix< returnType, SO >(
        node_data & data,
        std::vector< const node_data * > data_ext,
        const size_t data_index ) > inner_op,
    std::function< blaze::DynamicMatrix< returnType, SO >(
        root_data & data,
        std::vector< const root_data * > data_ext,
        const size_t data_index ) > root_op,
    ReductionType reduce_result,
    size_t i )
{
    int my_rank = 0;
#if MPI_ACTIVE
    MPI_Comm_rank( comm_, &my_rank );
#endif
    std::vector< const Cluster * > local_nodes = tree_.all_nodes_of_rank( my_rank );

    size_t num_ext_op = ext_operands.size();

    blaze::DynamicMatrix< returnType, SO > return_matrix,
          return_matrix_son1, return_matrix_son2;

    if ( node_used_ )
    {
        if ( !local_nodes[ i ]->is_leaf() )
        {
            if ( local_nodes[ i ]->son1()->mpi_rank() == my_rank )
            {
                // Sons on this MPI rank.
                std::size_t i_son1 = i;
                std::size_t i_son2 = i;
                for ( std::size_t j = i; j < local_nodes.size(); ++j )
                {
                    if ( local_nodes[ j ] == local_nodes[ i ]->son1() )
                    {
                        i_son1 = j;
                    }
                    else if ( local_nodes[ j ] == local_nodes[ i ]->son2() )
                    {
                        i_son2 = j;
                    }
                }

                // Recursion
                return_matrix_son1 = fully_parallel_algorithm( ext_operands, leaf_op, inner_op, root_op, reduce_result, i_son1 );
                return_matrix_son2 = fully_parallel_algorithm( ext_operands, leaf_op, inner_op, root_op, reduce_result, i_son2 );
            }

            if ( local_nodes[ i ]->is_root() )
            {
                std::vector< const DMatrix * > ext_data( num_ext_op );
                for ( size_t j = 0; j < num_ext_op; ++j )
                {
                    ext_data[ j ] = &(ext_operands[ j ]->data_[ i ][ 0 ]);
                }
                return_matrix = root_op( data_[ i ][ 0 ], ext_data, i );
            }
            else
            {
                // Inner node.
                std::vector< const blaze::DynamicVector< DMatrix > * > ext_data( num_ext_op );
                for ( std::size_t j = 0; j < num_ext_op; ++j )
                {
                    ext_data[ j ] = &(ext_operands[ j ]->data_[ i ]);
                }
                return_matrix = inner_op( data_[ i ], ext_data, i );
            }
        }
        else
        {
            // Leaf node.
            std::vector< const DMatrix * > ext_data( num_ext_op );
            for ( std::size_t j = 0; j < num_ext_op; ++j )
            {
                ext_data[ j ] = &(ext_operands[ j ]->data_[ i ][ 0 ]);
            }
            size_t mu = local_nodes[ i ]->leaf_number();
            return_matrix = leaf_op( data_[ i ][ 0 ], mu, ext_data, i );
        }

        if ( !local_nodes[ i ]->is_leaf() && local_nodes[ i ]->son1()->mpi_rank() == my_rank )
        {
            // Sons on this MPI rank, reduction:
            if ( reduce_result != none && reduce_result != rootBcast )
            {
                switch ( reduce_result )
                {
                case minReduction:
                    for ( std::size_t l = 0; l < return_matrix.columns(); ++l )
                    {
                        for ( std::size_t j = 0; j < return_matrix.rows(); ++j )
                        {
                            return_matrix( j, l ) = std::min( return_matrix( j, l ), std::min( return_matrix_son1( j, l ), return_matrix_son2( j, l ) ) );
                        }
                    }
                    break;
                case sumReduction:
                    return_matrix += return_matrix_son1;
                    return_matrix += return_matrix_son2;
                    break;
                case prodReduction:
                    return_matrix %= return_matrix_son1;
                    return_matrix %= return_matrix_son2;
                    break;
                default:  // maxReduction
                    for ( std::size_t l = 0; l < return_matrix.columns(); ++l )
                    {
                        for ( std::size_t j = 0; j < return_matrix.rows(); ++j )
                        {
                            return_matrix( j, l ) = std::max( return_matrix( j, l ), std::max( return_matrix_son1( j, l ), return_matrix_son2( j, l ) ) );
                        }
                    }
                }// switch ( reduce_result )
            }// if ( reduce_result != none && reduce_result != rootBcast )
        }// if ( !local_nodes[ i ]->is_leaf() && local_nodes[ i ]->son1()->mpi_rank() == my_rank )
    }// if ( _node_used )

    // Broadcast/Reduction (non-local):
    if ( reduce_result != none )
    {
        // Determine MPI_Datatype for the reduction:
#if MPI_ACTIVE
        MPI_Datatype mpi_datatype_return = MPI_CHAR;
        if ( typeid( returnType ) == typeid( short int ) )
        {
            mpi_datatype_return = MPI_SHORT;
        }
        else if ( typeid( returnType ) == typeid( int ) )
        {
            mpi_datatype_return = MPI_INT;
        }
        else if ( typeid( returnType ) == typeid( long int ) )
        {
            mpi_datatype_return = MPI_LONG;
        }
        else if ( typeid( returnType ) == typeid( unsigned char ) )
        {
            mpi_datatype_return = MPI_UNSIGNED_CHAR;
        }
        else if ( typeid( returnType ) == typeid( unsigned short int ) )
        {
            mpi_datatype_return = MPI_UNSIGNED_SHORT;
        }
        else if ( typeid( returnType ) == typeid( unsigned int ) )
        {
            mpi_datatype_return = MPI_UNSIGNED;
        }
        else if ( typeid( returnType ) == typeid( unsigned long int ) )
        {
            mpi_datatype_return = MPI_UNSIGNED_LONG;
        }
        else if ( typeid( returnType ) == typeid( float ) )
        {
            mpi_datatype_return = MPI_FLOAT;
        }
        else if ( typeid( returnType ) == typeid( double ) )
        {
            mpi_datatype_return = MPI_DOUBLE;
        }
        else if ( typeid( returnType ) == typeid( long double ) )
        {
            mpi_datatype_return = MPI_LONG_DOUBLE;
        }
        else if ( typeid( returnType ) == typeid( long long int ) )
        {
            mpi_datatype_return = MPI_LONG_LONG_INT;
        }

        if ( reduce_result == rootBcast )
        {
            // Broadcast return value from root (MPI rank 0):
            if ( i == 0 )
            {
                MPI_Bcast_DynamicMatrix( return_matrix, mpi_datatype_return, 0, comm_ );
            }
        }
        else
        {
            // For reduction, return_matrix must be of same size at
            // each MPI process (also those which are not used).
            // Set new matrix entries in such way that they do not affect the
            // reduction:
            if ( i == 0 )
            {
                std::size_t * sizes = new size_t[ 2 ];
                sizes[ 0 ] = return_matrix.rows();
                sizes[ 1 ] = return_matrix.columns();
                std::size_t * max_sizes = new size_t[ 2 ];
                MPI_Allreduce( sizes, max_sizes, 2, _MPI_SIZE_T, MPI_MAX, comm_ );
                if ( sizes[ 0 ] != max_sizes[ 0 ] || sizes[ 1 ] != max_sizes[ 1 ] )
                {
                    return_matrix.resize( max_sizes[ 0 ], max_sizes[ 1 ] );
                    // Set new values
                    double new_value = std::numeric_limits<double>::lowest();    // case of maxReduction
                    switch ( reduce_result )
                    {
                    case minReduction:
                        new_value = std::numeric_limits<double>::max();
                        break;
                    case sumReduction:
                        new_value = 0.0;
                        break;
                    case prodReduction:
                        new_value = 1.0;
                        break;
                    default:
                        new_value = std::numeric_limits<double>::lowest();// for maxReduction
                    }
                    for ( std::size_t l = sizes[ 1 ]; l < max_sizes[ 1 ]; ++l )
                    {
                        for ( std::size_t j = 0; j < max_sizes[ 0 ]; ++j )
                        {
                            return_matrix( j, l ) = new_value;
                        }
                    }
                    for ( std::size_t l = 0; l < sizes[ 1 ]; ++l )
                    {
                        for ( std::size_t j = sizes[ 0 ]; j < max_sizes[ 0 ]; ++j )
                        {
                            return_matrix( j, l ) = new_value;
                        }
                    }
                }

                delete[] sizes;
                delete[] max_sizes;

                blaze::DynamicMatrix< returnType, SO > reduced_return_matrix;

                // Set MPI_Op op:
                MPI_Op op = MPI_MAX;    // case of maxReduction
                switch ( reduce_result )
                {
                case minReduction:
                    op = MPI_MIN;
                    break;
                case sumReduction:
                    op = MPI_SUM;
                    break;
                case prodReduction:
                    op = MPI_PROD;
                    break;
                default:
                    op = MPI_MAX;
                }

                MPI_Allreduce_DynamicMatrix( return_matrix, reduced_return_matrix, mpi_datatype_return, op, comm_ );
                return_matrix = reduced_return_matrix;
            }// if ( i == 0 )
        }// else
#endif
    }// if ( reduce_result != none )

    return return_matrix;
}


} //namespace lowrat


#endif //LOWRAT_HT_TENSOR_HTENSOR_IPP_
