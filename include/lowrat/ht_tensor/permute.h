#ifndef LOWRAT_HT_TENSOR_PERMUTE_H_
#define LOWRAT_HT_TENSOR_PERMUTE_H_

#include <cstddef> //for std::size_t
#include <vector>//for std::vector

#include <blaze/Blaze.h> //for blaze::DynamicMatrix, blaze::DynamicVector, blaze::columnMajor

namespace lowrat
{

//Permute dimensions
//rearranges the dimensions of an array in the order specified by the vector dimorder.
//For example, permute(A,[1 0 2]) switches the first and second dimensions of node.
blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > > permute(const blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >& node, const std::vector<std::size_t>& dimorder);

} //namespace lowrat


#endif // LOWRAT_HT_TENSOR_PERMUTE_H_
