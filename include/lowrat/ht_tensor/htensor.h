#ifndef LOWRAT_HT_TENSOR_HTENSOR_H_
#define LOWRAT_HT_TENSOR_HTENSOR_H_

#include <cstddef>//for std::size_t
#include <fstream>// for ofstream
#include <string> //for string
#include <tuple>//for tuple
#include <vector>//for vector

#include <blaze/Blaze.h> //DynamicVector, DynamicMatrix
#include "lowrat/config/config.h"//for lowrat::value_t
#include "lowrat/cluster/cluster.h" //for Cluster
#include "lowrat/util/io.h" //for cout of a vector
//#include "lowrat/util/parallel.h" //for blaze MPI, PStream

namespace lowrat
{

typedef     std::vector< std::size_t >  tensorindex;
typedef     std::vector< std::size_t >  tensorsize;
typedef     std::vector< std::size_t >  tensormode;

typedef     blaze::DynamicMatrix< value_t, blaze::columnMajor > leaf_data;
typedef     blaze::DynamicMatrix< value_t, blaze::columnMajor > root_data;
typedef     blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > > node_data;

//! Kinds of reductions for the return value of
//! \link HTensor::leaves_to_root_algorithm() \endlink and
//! \link HTensor::root_to_leaves_algorithm() \endlink.
enum ReductionType { none, rootBcast, maxReduction, minReduction, sumReduction, prodReduction };

class HTensor
{
public:
    //! Constructor.
    HTensor( std::size_t d, std::vector< std::size_t > n, std::size_t k = 0,
             const Cluster& tree = 0 );

    //! Constructor (all tensor directions of equal size).
    HTensor( std::size_t d = 2, std::size_t n = 0, std::size_t k = 0,
             const Cluster & tree = 0 );


    //! Copy structure fill with value
    HTensor( const HTensor& ht, value_t value);




    //! get sizes of all dimensions
    std::vector<std::size_t> sizes() const;

    const std::vector< blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > > >& data() const;


    //! Returns a vector of the tensor ranks on this MPI node.
    std::vector< std::size_t > ranks() const;

    //! Returns a vector of all tensor ranks, according to tree_.all_nodes().
    //std::vector< std::size_t > all_ranks() const;

    //! Returns the number of all tensor entries (of the corresponding full tensor).
    std::size_t size() const;

    //! Returns the size of the mu-th tensor direction.
    std::size_t size( std::size_t mu ) const;

    //! Returns the rank of the mu-th tensor leaf.
    std::size_t rank_of_leaf( std::size_t mu ) const;

    //! Returns node number of leaf
    std::size_t node_number(const std::size_t leaf_number) const;

    void set_data( const blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > >& data, const tensormode& tm);

    // i th node, j th layer, k th row or column
    void set_data( blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > >& part_of_data, std::size_t i);
    void set_data( blaze::DynamicMatrix< value_t, blaze::columnMajor >  &part_of_data, std::size_t i, std::size_t j);
    void set_data( blaze::DynamicVector< value_t >  &vec, std::size_t i, std::size_t j, std::size_t k, bool row = true);
    void set_data( value_t val, std::size_t i, std::size_t j, std::size_t k, std::size_t l)
    {
        data_[i][j](k, l) = val;
    }
    void set_leaf( leaf_data &leaf, std::size_t mu)
    {
        set_data(leaf, node_number(mu), 0);
    }

    void get_data( blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > >& part_of_data, std::size_t i) const;
    void get_data( blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > >& data, const tensormode& tm) const;
    void get_data( blaze::DynamicMatrix< value_t, blaze::columnMajor >& part_of_data, const std::size_t i, const std::size_t j) const;

    const blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > >& get_data( std::size_t i) const;
    const blaze::DynamicMatrix< value_t, blaze::columnMajor >& get_data(std::size_t i, std::size_t j) const;

    void set_data(const blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > >& data, std::size_t i);
    void set_data(const blaze::DynamicMatrix< value_t, blaze::columnMajor >& data, std::size_t i, std::size_t j);

    void set_data(blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > >&& data, std::size_t i);
    void set_data(blaze::DynamicMatrix< value_t, blaze::columnMajor >&& data, std::size_t i, std::size_t j);

    void set_data( const std::vector< blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > > >& data );
    void set_data( std::vector< blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > > >&& data );


    void get_data( blaze::DynamicVector< value_t >& vec, std::size_t i, std::size_t j, std::size_t k, bool row = true) const;
    void get_data( value_t &val, std::size_t i, std::size_t j, std::size_t k, std::size_t l) const
    {
        val = data_[i][j](k, l);
    }
    void get_leaf( leaf_data& leaf, std::size_t mu) const
    {
        get_data(leaf, node_number(mu), 0);
    }

    // multiply leaf
    template<typename MatrixType2>
    void multiply_leaf(const MatrixType2& M, std::size_t mu, bool from_left = true)
    {
        std::size_t i_th_node = node_number(mu);
        if (from_left)
        {
            data_[i_th_node][0] = M * data_[i_th_node][0];
        }
        else
        {
            data_[i_th_node][0] = data_[i_th_node][0] * M;
        }
    }


    void save_data(std::string f_name)
    {
        blaze::Archive<std::ofstream> archive_out(f_name, std::ofstream::trunc);
        archive_out << data_.size();
        for (std::size_t i = 0; i < data_.size(); ++i)
        {
            archive_out << data_[i];
        }
    }

    void load_data(std::string f_name)
    {
        blaze::Archive<std::ifstream> archive_in(f_name);
        std::size_t n;
        archive_in >> n;
        data_.resize(n);
        for (std::size_t i = 0; i < n ; ++i)
        {
            archive_in >> data_[i];
        }
    }

    std::tuple<std::size_t, std::size_t, std::size_t> get_tensor_rank(std::size_t data_index) const;

    // sets data for a node
    void set_partial_data(const node_data &partial_data, int node_number);

    // sets data in a single entry
    void set_partial_data(value_t value, int node_number, int rank, int i, int j);

    //! Returns a copy of the cluster tree.
    Cluster clustertree() const;

    //! Evaluates the tensor at one index.
    value_t entry( const std::vector< std::size_t > & index ) const;

    //! Same method like \link entry() \endlink which uses the leaves-to-root template
    //! \link HTensor::leaves_to_root_algorithm() \endlink.
    value_t entry_n( const tensorindex & index );

    //! Evaluates the tensor at a list of indices
    void entry( const std::vector< std::vector< std::size_t > > & index, blaze::DynamicVector<value_t> &res ) const;
    void entry( const std::vector< std::vector< std::size_t > > & index, std::vector<value_t>& res ) const
    {
        res.resize(index.size());
        for (std::size_t i = 0; i < index.size(); ++i)
        {
            res[i] = entry(index[i]);
        }
    }
    //! Sets the tensor entry at index to value. Increases the HT-ranks by one.
    void set_entry( const std::vector< std::size_t > & index, value_t value );

    //! Scales this by alpha.
    void scale( value_t alpha );

    //! Adds alpha to each tensor entry. Increases the HT-ranks by one.
    void add( value_t alpha );

    //! Adds alpha to the tensor entry at index. Increases the HT-ranks by one.
    void add( const std::vector< std::size_t > & index, value_t alpha );

    //! Computes the inner product of this with X, where X is a tensor of the same HT-structure as
    value_t dot( const HTensor & X ) const;

    //! Same method like \link dot() \endlink which uses the leaves-to-root template
    //! \link HTensor::leaves_to_root_algorithm() \endlink.
    value_t dot_n( const HTensor & X );

    //! Computes the inner product of this with X w.r.t the range defined by
    //! lower_index and upper_index.
    //! X is a tensor of the same HT-structure as
    value_t dot( const HTensor & X, const std::vector< std::size_t > & lower_index, const std::vector< std::size_t > & upper_index ) const;

    //! Computes the inner product of this with X, where the mu-th direction is
    //! restricted to 'indices'.
    //! X is a tensor of the same HT-structure as
    value_t dot( const HTensor & X, std::size_t mu, const std::vector< std::size_t > & indices ) const;

    //! Computes the inner product of this with X with arbitrary restrictions,
    //! which are defined by the vector 'indices' (one index vector for each tensor dimension).
    //! If the indices[ mu ] is empty, the mu-th direction gets fully included.
    //! X is a tensor of the same HT-structure as
    value_t dot( const HTensor & X, const std::vector< std::vector< std::size_t > > & indices ) const;

    //! Computes this = this + alpha * X, where X is a tensor of the same HT-structure as
    void add( value_t alpha, const HTensor & X );

    //! Same method like \link add( double, const HTensor & ) \endlink which uses the
    //! template \link HTensor::fully_parallel_algorithm() \endlink for fully parallel
    //! algorithms.
    void add_n( value_t alpha, const HTensor & X );

    //! Computes this = this + alpha * X, where X is a tensor of the same HT-structure as
    //! This method first computes a joint orthonormal basis of both tensors, s.t. the addition
    //! can be carried out on the root node of the HT-tree.
    void add2( value_t alpha, const HTensor & X );

    //! Computes the Hadamard product (i.e. entry-wise multiplication) this = this o X.
    void hadamard( const HTensor & X );

    //! Computes the Hadamard product this = this o this.
    void hadamard();

    //! Orthogonalizes the tensor:
    //! On the leaves:      orthonormal columns,
    //! Transfer tensors:   orthonormal layers.
    void orthogonalize();


    //! Creates joint orthonormal bases for the HTensors pointed to by ht_pt.
    friend void joint_orthonormal_bases( const std::vector< HTensor * > & ht_pt );

    //! Computes a QR decomposition for a set of HTensors, i.e. orthonormalizes the HTensors
    //! and computes a matrix R, the columns of which define the linear combinations by which
    //! the original HTensors can be represented in the new orthonormal basis.
    //! Notice that ht_pt will not be resized, even though the number of
    //! orthonormal HTensors may be smaller than the original number of HTensors in ht_pt.
    //! The number of orthonormal HTensors can be obtained by R.rows().
    friend void QR( const std::vector< HTensor * > & ht_pt, blaze::DynamicMatrix< value_t, blaze::columnMajor > & R );

    //! Truncate down to rank k.
    //! Returns an upper estimate for the relative truncation error.
    double truncate( std::size_t k );

    //! Truncate with a relative error of at most eps.
    //! Returns an upper estimate for the relative truncation error.
    double truncate( double eps );

    //! Tries to truncate with a relative error of at most eps, but with additional rank bound k,
    //! which might result in a less accurate truncation.
    //! Returns an upper estimate for the relative truncation error.
    double truncate( double eps, std::size_t k );

    //! HOSVD truncation (same algorithm as \link truncate() \endlink) which uses the
    //! root-to-leaves template \link HTensor::root_to_leaves_algorithm() \endlink
    //! for the computation of accumulated transfer tensors and the
    //! leaves-to-root template \link HTensor::leaves_to_root_algorithm() \endlink
    //! for the computation of singular values/vectors and truncation.
    //! If both, k and eps, are set, the option which results in a smaller tensor rank
    //! is chosen.
    //!
    //! @param k prescribed rank for the truncation. In order to specify only eps,
    //!          set k = -1, which results in k = max( size_t ).
    //! @param eps prescribed relative accuracy for the truncation. In order to specify
    //!            only k, choose eps < 0 (e.g. eps = -1).
    double truncate_n( std::size_t k, double eps );

    //! HOSVD truncation which truncates to prescribed rank k,
    //! cf. \link truncate_n( size_t, double ) \endlink.
    double truncate_n( std::size_t k )
    {
        return truncate_n( k, -1.0 );
    }

    //! Adaptive HOSVD truncation which truncates w.r.t. prescribed accuracy eps,
    //! cf. \link truncate_n( size_t, double ) \endlink.
    double truncate_n( double eps )
    {
        return truncate_n( -1, eps );
    }


    //! Imitates the truncate_n algorithm in order to compute the singular values of the tensor
    std::vector< blaze::DynamicVector< value_t > > singular_values();


    //! Fills the tensor with random data from the interval [a,b].
    void random_fill( value_t a, value_t b );

    //! Fills the tensor with random integers between a and b.
    void random_integer_fill( int a, int b );

    //! Prints the content  to std::cout.
    void display_content() const;

    //! Prints the dimensions to std::cout
    void display_dimensions() const;

    //! Returns algebraic ranks of a certain node
    std::vector<std::size_t> get_rank(std::size_t node_number) const;

    //! Prints the algabraic ranks to std::cout.
    void display_ranks() const;

    //! Checks if there is any NAN-part (not a number) in the tensor representation.
    bool isnan() const;

    //! Auxiliary for entry().
    blaze::DynamicVector< value_t, blaze::rowVector > get_row( const std::vector< std::size_t > & index, std::size_t i = 0 ) const;

    //! Auxiliary for dot().
    void inner_product_matrix( const HTensor & X, blaze::DynamicMatrix< value_t, blaze::columnMajor > & G, std::size_t i = 0 ) const;

    //! Auxiliary for dot() with restricted index range.
    void inner_product_matrix_restricted( const HTensor & X, blaze::DynamicMatrix< value_t, blaze::columnMajor > & G, const std::vector< std::size_t > & lower_index, const std::vector< std::size_t > & upper_index, std::size_t i = 0 ) const;

    //! Auxiliary for dot() with restricted index range.
    void inner_product_matrix_restricted( const HTensor & X, blaze::DynamicMatrix< value_t, blaze::columnMajor > & G, std::size_t mu, const std::vector< std::size_t > & indices, std::size_t i = 0 ) const;

    //! Auxiliary for dot() with restricted index range.
    void inner_product_matrix_restricted( const HTensor & X, blaze::DynamicMatrix< value_t, blaze::columnMajor > & G, const std::vector< std::vector< std::size_t > > & indices, std::size_t i = 0 ) const;

    //! Auxiliary for orthogonalize(), orthogonalizes node i.
    //! Right factor is stored in R.
    void orthogonalize_node( blaze::DynamicMatrix< value_t, blaze::columnMajor > & R, std::size_t i = 0 );

    void orthogonalize_node_with_respect_to_father( std::size_t orthogonalize_node_number, std::size_t  father_number );

    void orthogonalize_node_with_respect_to_child( std::size_t orthogonalize_node_number, std::size_t  respect_to_node_number );
    //child_number should be 0 or 1
    void orthogonalize_node_with_respect_to_child_number( std::size_t orthogonalize_node_number, std::size_t  respect_to_node_number, std::size_t child_number );

    void orthogonalize_node_with_respect_to( std::size_t orthogonalize_node_number, std::size_t  respect_to_node_number );


    //! performs a vector iteration to compute the maximum norm of the vector
    void vector_iteration(double &max_val, const std::size_t k_max = 10, const double eps = 1e-7) const;
    //! find maximum by variation of just one parameter after the other
    double maximum_als(tensorindex& current_idx, std::size_t k_max) const;

    //! Returns HTensor with the same dimension tree for the identity f(p) = 1
    HTensor ones() const;
    //! Auxiliary for joint_orthonormal_bases().
    friend void joint_orthonormal_bases_node( const std::vector< HTensor * > & ht_pt, blaze::DynamicMatrix< value_t, blaze::columnMajor > & R, std::size_t i  );

    //! Auxiliary for the truncation of orthogonalized tensors.
    //! The relative accuracy is controlled by eps, whereas k is an upper bound for all ranks.
    //! If both, eps and k are specified, the option which results in a lower tensor rank will be chosen.
    //! To specify only eps, choose k = -1 which results in k = max( std::size_t ).
    //! To specify only k, choose eps < 0, e.g. eps = -1.
    //! acc_tr are the accumulated transfer tensors.
    //! sq_rel_errors is filled with the squared relative truncation errors.
    void truncate_orthogonalized_node( const std::vector< blaze::DynamicMatrix< value_t, blaze::columnMajor > > & acc_tr, std::vector< blaze::DynamicMatrix< value_t, blaze::columnMajor > > & eigenvectors, std::vector< double > & sq_rel_errors, std::size_t i, std::size_t k, double eps );

    //! Auxiliary for truncate_orthogonalized_tensor().
    //! Computes the accumulated transfer tensors.
    void accumulated_transfer_tensors( std::vector< blaze::DynamicMatrix< value_t, blaze::columnMajor > > & acc_tr, std::size_t i ) const;

    bool is_node_used() const
    {
        return node_used_;
    }

    friend double norm(const HTensor& ht);

    double norm() const;

    friend double norm_orthogonalize(HTensor ht);

    double norm_orthogonalize() const;

    friend std::size_t dimension(const HTensor& ht);

    friend std::size_t number_of_nodes(const HTensor& ht);

    bool is_leaf(std::size_t nodenumber) const;

    bool is_root(std::size_t nodenumber) const;

    std::size_t get_root_number() const;

    std::vector<std::size_t> get_child_numbers(std::size_t parent_number) const;

    std::size_t get_parent_number(std::size_t child_number) const;

    std::vector<std::size_t> get_sibling_numbers(std::size_t child_number) const;

    std::size_t get_brother_number(std::size_t child_number) const;

    bool is_left_son_of_parent(std::size_t son_number, std::size_t parent_number) const;

    //we want the following operator
    friend HTensor operator+(HTensor left, const HTensor& right);
    // friend HTensor operator+(const HTensor& left, HTensor right);

    friend HTensor operator-(HTensor left, const HTensor& right);

    //we could template this, but int and size_t will be implicitly convert to double, therefore this does all we want
    friend HTensor operator*(HTensor left, value_t right);
    friend HTensor operator*(value_t left, HTensor right);
    friend HTensor operator/(HTensor left, value_t right);

    HTensor& operator+=(const HTensor& right);
    HTensor& operator-=(const HTensor& right);

    //we could template this, but int and size_t will be implicitly convert to double, therefore this does all we want
    HTensor& operator*=(value_t right);
    HTensor& operator/=(value_t right);

    std::vector<std::size_t> random_tensorindex();

private:
    //! Tensor data for all HT-nodes, which are stored.
    //! If data_[ i ] corresponds to the root of the cluster tree:
    //!     data_[ i ][ 0 ] contains the root transfer tensor as a DynamicMatrix,
    //! if data_[ i ] corresponds to a leaf of the cluster tree:
    //!     data_[ i ][ 0 ] contains the leaf frame as a DynamicMatrix,
    //! if data_[ i ] corresponds to an inner node of the cluster tree:
    //!     data_[ i ][ j ] contains the j-th layer of the transfer tensor as a DynamicMatrix.
    std::vector< blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > > >    data_;

    Cluster                tree_;

    bool                   node_used_;

    //! Template for the development of leaves-to-root algorithms.
    //! The functions can be defined by lambda functions in order to capture variables
    //!     from outside (e.g. further input arguments of the algorithm which are
    //!     requried during leaf_op, inner_op or root_op.
    //! @param ext_operands contains pointers to possible further operands.
    //! @param leaf_op function defining the algorithm at the leaf nodes.
    //! @param inner_op function defining the algorithm at the inner nodes.
    //! @param root_op function defining the algorithm at the root node.
    //! @param father_data is used to transport data between nodes
    //!                    process (the method recursively calls itself).
    //!                    Just handle an empy matrix.
    //! @param reduce_result when set to \link rootBcast \endlink (default),
    //!                      the return matrix of the root process is broadcast to all
    //!                      other processes (same return matrix at each process).\n
    //!                      When set to \link maxReduction \endlink,
    //!                      \link minReduction \endlink, \link sumReduction \endlink,
    //!                      \link prodReduction \endlink, the respective
    //!                      reduction of the return matrices is returned at each
    //!                      process.\n
    //!                      When set to \link none \endlink, each process returns
    //!                      its own matrix.
    //! @param i corresponds to the index of the internal data
    //!          \link HTensor::data_ \endlink which the algorithm currently
    //!          is processing. Used to recursively process the son nodes.
    template< typename Type, typename returnType, bool SO >
    blaze::DynamicMatrix< returnType, SO > leaves_to_root_algorithm(
        const std::vector< const HTensor * > ext_operands,
        std::function< blaze::DynamicMatrix< returnType, SO >(
            leaf_data & data,
            const std::size_t leaf_number,
            std::vector< const leaf_data * > data_ext,
            blaze::DynamicMatrix< Type, SO > & father_data,
            const std::size_t data_index ) > leaf_op,
        std::function< blaze::DynamicMatrix< returnType, SO >(
            node_data & data,
            std::vector< const node_data * > data_ext,
            blaze::DynamicMatrix< Type, SO > & father_data,
            const blaze::DynamicMatrix< Type, SO > & son1_data,
            const blaze::DynamicMatrix< Type, SO > & son2_data,
            const std::size_t data_index ) > inner_op,
        std::function< blaze::DynamicMatrix< returnType, SO >(
            root_data & data,
            std::vector< const root_data * > data_ext,
            const blaze::DynamicMatrix< Type, SO > & son1_data,
            const blaze::DynamicMatrix< Type, SO > & son2_data,
            const std::size_t data_index ) > root_op,
        blaze::DynamicMatrix< Type, SO > & father_data,
        ReductionType reduce_result = rootBcast,
        std::size_t i = 0 );

    //! Template for the development of root-to-leaves algorithms.
    //! The functions can be defined by lambda functions in order to capture variables
    //!     from outside (e.g. further input arguments of the algorithm which are
    //!     required during leaf_op, inner_op or root_op.
    //! @param ext_operands contains pointers to possible further operands.
    //! @param leaf_op function defining the algorithm at the leaf nodes.
    //! @param inner_op function defining the algorithm at the inner nodes.
    //! @param root_op function defining the algorithm at the root node.
    //! @param father_data is used to transport data between nodes
    //!                    process (the method recursively calls itself).
    //!                    Just handle an empty matrix.
    //! @param reduce_result when set to \link rootBcast \endlink,
    //!                      the return matrix of the root process is broadcast to all
    //!                      other processes (same return matrix at each process).\n
    //!                      When set to \link maxReduction \endlink,
    //!                      \link minReduction \endlink, \link sumReduction \endlink,
    //!                      \link prodReduction \endlink, the respective
    //!                      reduction of the return matrices is returned at each
    //!                      process.\n
    //!                      When set to \link none \endlink (default), each process
    //!                      returns its own matrix.
    //! @param i corresponds to the index of the internal data
    //!          \link HTensor::data_ \endlink which the algorithm currently
    //!          is processing. Used to recursively process the son nodes.
    template< typename Type, typename returnType, bool SO >
    blaze::DynamicMatrix< returnType, SO > root_to_leaves_algorithm(
        const std::vector< const HTensor * > ext_operands,
        std::function< blaze::DynamicMatrix< returnType, SO >(
            leaf_data & data,
            const std::size_t leaf_number,
            std::vector< const leaf_data * > data_ext,
            const blaze::DynamicMatrix< Type, SO > & father_data,
            const std::size_t data_index ) > leaf_op,
        std::function< blaze::DynamicMatrix< returnType, SO >(
            node_data & data,
            std::vector< const node_data * > data_ext,
            const blaze::DynamicMatrix< Type, SO > & father_data,
            blaze::DynamicMatrix< Type, SO > & son1_data,
            blaze::DynamicMatrix< Type, SO > & son2_data,
            const std::size_t data_index ) > inner_op,
        std::function< blaze::DynamicMatrix< returnType, SO >(
            root_data & data,
            std::vector< const root_data * > data_ext,
            blaze::DynamicMatrix< Type, SO > & son1_data,
            blaze::DynamicMatrix< Type, SO > & son2_data,
            const std::size_t data_index ) > root_op,
        blaze::DynamicMatrix< Type, SO > & father_data,
        ReductionType reduce_result = none,
        std::size_t i = 0 );



    template< typename returnType, bool SO >
    blaze::DynamicMatrix< returnType, SO > fully_parallel_algorithm(
        const std::vector< const HTensor * > ext_operands,
        std::function< blaze::DynamicMatrix< returnType, SO >(
            leaf_data & data,
            const std::size_t leaf_number,
            std::vector< const leaf_data * > data_ext,
            const std::size_t data_index ) > leaf_op,
        std::function< blaze::DynamicMatrix< returnType, SO >(
            node_data & data,
            std::vector< const node_data * > data_ext,
            const std::size_t data_index ) > inner_op,
        std::function< blaze::DynamicMatrix< returnType, SO >(
            root_data & data,
            std::vector< const root_data * > data_ext,
            const std::size_t data_index ) > root_op,
        ReductionType reduce_result = none,
        std::size_t i = 0 );


};

//! Computes the QR decomposition of a transfer tensor for the HT format.
//! The transfer tensor gets overwritten by Q.
void qr_transfer_tensor( blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > > & tensor, blaze::DynamicMatrix< value_t, blaze::columnMajor > & R );

//! Computes the reduced QR decomposition of a transfer tensor for the HT format.
//! The transfer tensor gets overwritten by Q.
void reduced_qr_transfer_tensor( blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > > & tensor, blaze::DynamicMatrix< value_t, blaze::columnMajor > & R );


HTensor ht_construct_from_canonical(const std::vector<blaze::DynamicMatrix<value_t, blaze::columnMajor>>& cp_cores, double eps = 0.0);


void add_cp_to_ht(const std::vector<blaze::DynamicMatrix<value_t, blaze::columnMajor>>& cp_cores, HTensor& htensor, double eps = 0.0);

//std::size_t maximal_rank_on_level(const HTensor& ht, std::size_t level);

//std::size_t maximal_rank(const HTensor& ht);

//! Computes the Hadamard product X o Y.
HTensor hadamard(HTensor X, const HTensor& Y);

std::size_t dimension_number_of_leaf_number(const HTensor& ht, std::size_t leaf_number);

HTensor truncate(HTensor ht, double eps);

HTensor train_network(size_t maxrank, const std::vector<std::size_t>& sizes, const std::vector<double>& weights, const leaf_data& input, const leaf_data& output);
} //namespace lowrat

#include "htensor.ipp"

#endif //LOWRAT_HT_TENSOR_HTENSOR_H_
