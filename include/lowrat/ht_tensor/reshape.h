#ifndef LOWRAT_HT_TENSOR_RESHAPE_H_
#define LOWRAT_HT_TENSOR_RESHAPE_H_

#include <cstddef> //for std::size_t
#include <vector>//for std::vector

#include <blaze/Blaze.h> //for lowrat::DynamicMatrix, lowrat::DynamicVector, lowrat::columnMajor

namespace lowrat
{

//this should become something like
//result reshape(input, size_vector);
//or void reshape(input, size_vector);


blaze::DynamicVector< double, blaze::columnVector > reshape_3D_into_1D(const blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >& tensor_node);

blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > > reshape_1D_into_3D(const blaze::DynamicVector< double, blaze::columnVector >& vector, std::size_t size_dim_0, std::size_t size_dim_1,  std::size_t size_dim_2 );


blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > > reshape_2D_into_3D(const blaze::DynamicMatrix< double, blaze::columnMajor >& matrix, std::size_t dim, std::size_t size_dim_0, std::size_t size_dim_1, std::size_t size_dim_2);


blaze::DynamicMatrix< double, blaze::columnMajor > reshape_3D_into_2D(const blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >& tensor_node, std::size_t dim);

blaze::DynamicMatrix< double, blaze::columnMajor > reshape_special_6D_into_2D(const std::vector<std::vector<std::vector<blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >>>>& tensor_node);


blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > > reshape_4D_into_3D(const std::vector<blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >>& tensor, std::size_t dim);


} //namespace lowrat


#endif // LOWRAT_HT_TENSOR_RESHAPE_H_
