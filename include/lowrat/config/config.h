#ifndef LOWRAT_CONFIG_CONFIG_H_
#define LOWRAT_CONFIG_CONFIG_H_

// #include <complex>//for std::complex<float> or std::complex<double>

namespace lowrat
{

//Specifies the data type of the library.
//Should be selected from float, double, std::complex<float> or std::complex<double>.
typedef double value_t;

} //namespace lowrat

#endif //LOWRAT_CONFIG_CONFIG_H_
