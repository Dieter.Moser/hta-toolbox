#ifndef LOWRAT_CROSS_HT_CROSS_H_
#define LOWRAT_CROSS_HT_CROSS_H_

#include <cstddef> //for size_t
#include <functional>//for function
#include <vector> //for vector

#include "lowrat/full_tensor/matricization_view.h" //for MatricizationView
#include "lowrat/full_tensor/tensor.h" //for Tensor
#include "lowrat/ht_tensor/htensor.h" //for HTensor

namespace lowrat
{

typedef std::vector< size_t > tensorindex;
typedef std::vector< size_t > tensorsize;
typedef std::vector< size_t > tensormode;

struct HTCrossSettings
{
    bool ensure_nestedness = true;
    std::size_t sub_matrix_size = 50;
    std::size_t maximal_rank = 10;
    double accuracy_epsilon = 1e-4;
    std::size_t pivot_strategy = 2;
    //PivotStrategy pivot_strategy = PivotStrategy::adaptive;
};


class HTCross
{
public:

    HTCross() noexcept {}

    HTCross(const HTCrossSettings& settings) : settings_{settings} { }

    HTCrossSettings getSettings() const
    {
        return settings_;
    }

    void setSettings(const HTCrossSettings& settings)
    {
        settings_ = settings;
    }

    void approximate(const Tensor& T, HTensor& HT);

    HTensor approximate(const Tensor& T);

    void approximate(const Tensor& T, const tensorindex& pre_pivot, HTensor& HT);

    HTensor approximate(const Tensor& T, const tensorindex& pre_pivot);

private:

    HTCrossSettings settings_;

    void tree_cross_approximation_nested(const Tensor& T, const std::function<void (MatricizationView&, tensorindex&, tensorindex& ) > & find_pivots_in_submatrix, std::size_t K, const tensorindex& pre_pivot, HTensor& HT );


};

} //namespace lowrat

#endif //LOWRAT_CROSS_HT_CROSS_H_
