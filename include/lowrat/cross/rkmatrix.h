#ifndef LOWRAT_CROSS_RKMATRIX_H_
#define LOWRAT_CROSS_RKMATRIX_H_

#include <cstddef>//for std::size_t
#include <iostream>//for ostream
#include <vector>//for vector

#include <blaze/Blaze.h> //for DynamicVector, rowVector, columnVector, DynamicMatrix, columnMajor, Column, Row

namespace lowrat
{

class RkMatrix
{
public:
    //! Constructor.
    RkMatrix(const std::size_t rows = 1, const std::size_t cols = 1, const std::size_t k = 0 );

    std::size_t rows() const
    {
        return rows_;
    }
    std::size_t cols() const
    {
        return cols_;
    }
    std::size_t columns() const
    {
        return cols_;
    }
    std::size_t k()    const
    {
        return k_;
    }
    std::size_t rank()    const
    {
        return k_;
    }
    //! Resizes the matrix.
    //! All entries, as well as k, are set to zero.
    void resize(const std::size_t rows, const std::size_t cols );
    //! Element access.
    double   operator ()( const std::size_t i, const std::size_t j ) const;
    blaze::DynamicVector<double, blaze::columnVector>       get_column(const std::size_t j) const;
    blaze::DynamicVector<double, blaze::rowVector>          get_row(const std::size_t i) const;
    //! Return full matrix.
    blaze::DynamicMatrix<double, blaze::columnMajor> operator ()() const;
    blaze::DynamicMatrix<double, blaze::columnMajor>   get_full_matrix() const
    {
        return this->operator()();
    }
    //! Return first matrix with first k ranks.
    blaze::DynamicMatrix<double, blaze::columnMajor>  operator()(const std::size_t k) const;
    //! Adds one rank a * b^T.
    void add_rank( const std::vector< double >& a, const std::vector< double >& b );
    void add_rank( const blaze::DynamicVector<double, blaze::columnVector>& a, const blaze::DynamicVector<double, blaze::rowVector>& b );
    void add_rank( const blaze::Column< blaze::DynamicMatrix<double> >& a, const blaze::Row< blaze::DynamicMatrix<double> >& b);
    //! Set rank to k
    void set_rank(const std::size_t k);
    void set_rank(const std::size_t k, const double val);
    //! Change the nu-th rank
    void insert_rank( const std::vector< double > & a, const std::vector< double > & b, const std::size_t nu );
    void insert_rank( const blaze::DynamicVector<double, blaze::columnVector>& a, const blaze::DynamicVector<double, blaze::rowVector>& b, const std::size_t nu );
    void insert_rank( const blaze::Column< blaze::DynamicMatrix<double> >& a, const blaze::Row< blaze::DynamicMatrix<double> >& b, const std::size_t nu );
    //! Displays the content of a RkMatrix.
    void display_content() const;
    //! RkMatrix to ostream.
    friend std::ostream & operator <<( std::ostream& os, const RkMatrix& M );

    //TODO reduced_truncated_svd

private:
    std::size_t                         rows_;
    std::size_t                         cols_;
    std::size_t                         k_;

    blaze::DynamicMatrix<double, blaze::columnMajor>  A_;
    blaze::DynamicMatrix<double, blaze::rowMajor>     B_;
};

}//namespace lowrat


#endif //LOWRAT_CROSS_RKMATRIX_H_