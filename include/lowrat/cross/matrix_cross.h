#ifndef LOWRAT_CROSS_MATRIX_CROSS_H_
#define LOWRAT_CROSS_MATRIX_CROSS_H_

#include <cmath> //for fabs
#include <cstddef> //for std::size_t
#include <algorithm> //for min
#include <limits>// for std::numeric_limits<double>::max()
#include <random> //for std::uniform_int_distribution<std::size_t>
#include <set> //for set
#include <utility>//for pair, makepair
#include <vector> //for vector

#include "lowrat/cross/rkmatrix.h" //for RkMatrix
#include "lowrat/util/random.h" //for lowrat::random::mt_

namespace lowrat
{

template <typename Mat>
std::pair<std::size_t, std::size_t> arg_max_modulus(const Mat& M);

template <typename Mat>
std::size_t arg_max_modulus_in_row(const std::size_t rowIndex, const std::set<std::size_t>& avoidList, const Mat& M, const RkMatrix& rk);

template <typename Mat>
std::size_t arg_max_modulus_in_column(const std::size_t columnIndex, const std::set<std::size_t>& avoidList, const Mat& M, const RkMatrix& rk);

//enum class PivotStrategy : { full, partial, adaptive };

struct MatrixCrossSettings
{
    std::size_t maximal_rank = 10;
    double accuracy_epsilon = 1e-3;
    std::size_t pivot_strategy = 2;
    //PivotStrategy pivot_strategy = PivotStrategy::adaptive;
};

class MatrixCross
{
public:

    MatrixCross() noexcept { }

    MatrixCross(const MatrixCrossSettings& settings) : settings_{settings} { }

    MatrixCrossSettings getSettings() const
    {
        return settings_;
    }

    void setSettings(const MatrixCrossSettings& settings)
    {
        settings_ = settings;
    }

    template <typename Mat>
    std::vector<std::pair<std::size_t, std::size_t>> find_pivots(const Mat& M) const;

    template <typename Mat>
    void approximate(const Mat& M, RkMatrix& Rk) const;

    template <typename Mat>
    RkMatrix approximate(const Mat& M) const;

private:

    MatrixCrossSettings settings_;

    template <typename Mat>
    std::vector<std::pair<std::size_t, std::size_t>> find_pivots_and_cross_approximation_full(Mat M, RkMatrix& Rk) const;

    template <typename Mat>
    std::vector<std::pair<std::size_t, std::size_t>> find_pivots_and_cross_approximation_partial_or_adaptive(const bool use_adaptive, const Mat& M, RkMatrix& Rk) const;

};

template <typename Mat>
std::vector<std::pair<std::size_t, std::size_t>> MatrixCross::find_pivots(const Mat& M) const
{
    RkMatrix RK;
    switch (settings_.pivot_strategy)
    {
    case 0://PivotStrategy::full:
        return find_pivots_and_cross_approximation_full(M, RK);
    case 1://PivotStrategy::partial:
        return find_pivots_and_cross_approximation_partial_or_adaptive(false, M, RK);
    case 2://PivotStrategy::adaptive:
        return find_pivots_and_cross_approximation_partial_or_adaptive(true, M, RK);
    default:
        throw "such a pivot_strategy is not avaible";
    }
}

template <typename Mat>
void MatrixCross::approximate(const Mat& M, RkMatrix& Rk) const
{
    switch (settings_.pivot_strategy)
    {
    case 0://PivotStrategy::full:
        find_pivots_and_cross_approximation_full(M, Rk);
        break;
    case 1://PivotStrategy::partial:
        find_pivots_and_cross_approximation_partial_or_adaptive(false, M, Rk);
        break;
    case 2://PivotStrategy::adaptive:
        find_pivots_and_cross_approximation_partial_or_adaptive(true, M, Rk);
        break;
    default:
        throw "such a pivot_strategy is not avaible";
    }
}

template <typename Mat>
RkMatrix MatrixCross::approximate(const Mat& M) const
{
    RkMatrix rk;
    approximate(M, rk);
    return rk;
}

template <typename Mat>
std::pair<std::size_t, std::size_t> arg_max_modulus(const Mat& M, const RkMatrix& rk)
{
    double maxValue = std::fabs( M(0, 0) - rk(0, 0) );
    double tempValue = maxValue;
    const std::size_t rows = M.rows();
    const std::size_t columns = M.columns();

    std::size_t rowIndex = 0;
    std::size_t columnIndex = 0;

    for ( std::size_t i = 0; i < rows; ++i )
    {
        for ( std::size_t j = 0; j < columns; ++j )
        {
            tempValue = std::fabs( M(i, j) - rk(i, j) );
            if ( maxValue < tempValue )
            {
                rowIndex  = i;
                columnIndex = j;
                maxValue = tempValue;
            }
        }
    }
    return std::make_pair(rowIndex, columnIndex);
}

template <typename Mat>
std::size_t arg_max_modulus_in_row(const std::size_t rowIndex, const std::set<std::size_t>& avoidList, const Mat& M, const RkMatrix& rk)
{
    double maxValue = std::fabs( M(rowIndex, 0) - rk(rowIndex, 0) );
    double tempValue = maxValue;
    std::size_t columnIndex = 0;
    auto search = avoidList.find(0);

    for ( std::size_t j = 0; j < M.columns(); ++j )
    {
        search = avoidList.find(j);
        if (search == avoidList.end())
        {
            tempValue = std::fabs( M(rowIndex, j) - rk(rowIndex, j) );
            if ( maxValue < tempValue )
            {
                columnIndex = j;
                maxValue = tempValue;
            }
        }
    }
    return columnIndex;
}


template <typename Mat>
std::size_t arg_max_modulus_in_column(const std::size_t columnIndex, const std::set<std::size_t>& avoidList, const Mat& M, const RkMatrix& rk)
{
    double maxValue = std::fabs(M(0, columnIndex) - rk(0, columnIndex));
    double tempValue = maxValue;
    std::size_t rowIndex = 0;
    auto search = avoidList.find(0);

    for ( std::size_t i = 0; i < M.rows(); ++i )
    {
        search = avoidList.find(i);
        if (search == avoidList.end())
        {
            tempValue = std::fabs( M(i, columnIndex) - rk(i, columnIndex) );
            if ( maxValue < tempValue )
            {
                rowIndex = i;
                maxValue = tempValue;
            }
        }
    }
    return rowIndex;
}

template <typename Mat>
std::vector<std::pair<std::size_t, std::size_t>> MatrixCross::find_pivots_and_cross_approximation_full(Mat M, RkMatrix& Rk) const
{
    std::vector<std::pair<std::size_t, std::size_t> > list;
    std::pair<std::size_t, std::size_t> index;
    double delta = 0;
    const std::size_t rows = M.rows();
    const std::size_t columns = M.columns();

    const std::size_t dim = std::min(std::min(rows, columns), settings_.maximal_rank);
    Rk.resize(rows, columns);
    Rk.set_rank(dim, 0);

    blaze::DynamicVector<double, blaze::columnVector> current_column(rows);
    blaze::DynamicVector<double, blaze::rowVector> current_row(columns);

    for (std::size_t nu = 0; nu < dim; ++nu)
    {
        index = arg_max_modulus(M, Rk);
        list.push_back(index);
        delta = M(index.first, index.second) - Rk(index.first, index.second);

        if (delta != 0)
        {
            current_column = column(M, index.second) - Rk.get_column(index.second);
            current_row = row(M, index.first) - Rk.get_row(index.first);
            Rk.insert_rank( current_column, current_row / delta, nu);
        }
        else
        {
            list.pop_back();
            Rk.set_rank(nu);
            break;
        }
    }
    return list;
}

template <typename Mat>
std::vector<std::pair<std::size_t, std::size_t>> MatrixCross::find_pivots_and_cross_approximation_partial_or_adaptive(const bool use_adaptive, const Mat& M, RkMatrix& Rk) const
{
    std::vector<std::pair<std::size_t, std::size_t> > list;
    std::pair<std::size_t, std::size_t> index;
    double delta = 0;

    //we only need this in the adaptive case
    double eps_abs_k = std::numeric_limits<double>::max();
    double eps_abs_zero = eps_abs_k;
    double eps_rel_k = eps_abs_k;

    const std::size_t rows = M.rows();
    const std::size_t columns = M.columns();

    std::size_t dim = std::min(rows, columns);
    if (!use_adaptive)
    {
        dim = std::min(std::min(rows, columns), settings_.maximal_rank);
    }

    Rk.resize(rows, columns);
    Rk.set_rank(dim, 0.0);
    std::set<std::size_t> rowList;
    std::set<std::size_t> columnList;
    blaze::DynamicVector<double, blaze::columnVector> current_column(rows);
    blaze::DynamicVector<double, blaze::rowVector> current_row(columns);

    //find random row
    std::uniform_int_distribution<std::size_t> random_row_index(0, rows - 1);
    index.first = random_row_index(lowrat::random::mt_);
    rowList.insert(index.first);

    for (std::size_t nu = 0; nu < dim; ++nu)
    {
        if (use_adaptive)
        {
            if (settings_.accuracy_epsilon > eps_rel_k)
            {
                Rk.set_rank(nu);
                return list;
            }
        }
        index.second = arg_max_modulus_in_row(index.first, columnList, M, Rk);
        list.push_back(index);
        columnList.insert(index.second);
        delta = M(index.first, index.second) - Rk(index.first, index.second);
        if (delta != 0)
        {
            current_column = column(M, index.second) - Rk.get_column(index.second);
            current_row = row(M, index.first) - Rk.get_row(index.first);
            if (use_adaptive)
            {
                if (nu == 0)
                {
                    eps_abs_zero = blaze::l2Norm( current_column ) * blaze::l2Norm ( current_row );
                }
                eps_abs_k = blaze::l2Norm( current_column ) * blaze::l2Norm ( current_row );
                eps_rel_k = eps_abs_k / eps_abs_zero;
            }
            index.first = arg_max_modulus_in_column(index.second, rowList, M, Rk);
            rowList.insert(index.first);
            Rk.insert_rank( current_column, current_row / delta, nu);
        }
        else
        {
            list.pop_back();
            Rk.set_rank(nu);
            return list;
        }
    }

    return list;
}

} //namespace lowrat


#endif // LOWRAT_CROSS_MATRIX_CROSS_H_
