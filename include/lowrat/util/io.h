#ifndef LOWRAT_UTIL_IO_H_
#define LOWRAT_UTIL_IO_H_

#include <algorithm>//for copy
#include <iostream>//for ostream
#include <iterator>//for ostream_iterator
#include <vector> //for vector 

//HERE WE USE NO NAMESPACE BECAUSE THIS FUNCTION SHOULD BE AVAILABLE IN THE GLOBAL NAMESPACE

//! Sends an std::vector to ostream.
template< typename Type >
std::ostream & operator<<( std::ostream & out, const std::vector< Type > & v )
{
    if ( !v.empty() )
    {
        out << '[';
        std::copy( v.begin(), v.end(), std::ostream_iterator< Type >( out, ", " ) );
        out << "\b\b]";
    }
    return out;
}


#endif //LOWRAT_UTIL_IO_H_