#ifndef LOWRAT_UTIL_PARALLEL_SHARED_MEMORY_H_
#define LOWRAT_UTIL_PARALLEL_SHARED_MEMORY_H_

#include <cmath>//for std::round
#include <cstddef>//for std::size_t
#include <algorithm>//for std::max, std::min
#include <thread>//for std::thread
#include <vector>//for std::vector

namespace lowrat
{


template<typename Index, typename Callable>
void ParallelFor(Index start, Index end, Callable func)
{
    // Estimate number of threads in the pool
    const std::size_t nb_threads_hint = std::thread::hardware_concurrency();
    const std::size_t kzero = 0;
    const Index kone = 1;
    const std::size_t nb_threads = (nb_threads_hint == kzero ? kone : nb_threads_hint);

    // Size of a slice for the range functions
    Index n = end - start + kone;
    Index slice = (Index) std::round(n / static_cast<double> (nb_threads));
    slice = std::max(slice, Index(kone));

    // [Helper] Inner loop
    auto launchRange = [&func] (Index k1, Index k2)
    {
        for (Index k = k1; k < k2; ++k)
        {
            func(k);
        }
    };

    // Create pool and launch jobs
    std::vector<std::thread> pool;
    pool.reserve(nb_threads);
    Index i1 = start;
    Index i2 = std::min(start + slice, end);
    for (Index i = 0; i + 1 < nb_threads && i1 < end; ++i)
    {
        pool.emplace_back(launchRange, i1, i2);
        i1 = i2;
        i2 = std::min(i2 + slice, end);
    }
    if (i1 < end)
    {
        pool.emplace_back(launchRange, i1, end);
    }

    // Wait for jobs to finish
    for (std::thread& t : pool)
    {
        if (t.joinable())
        {
            t.join();
        }
    }
}


//use this as

//#include <mutex>


//    std::mutex critical;
//    ThreadPool::ParallelFor(0, 16, [&] (int i) {
//        std::lock_guard<std::mutex> lock(critical);
//        std::cout << i << std::endl;
//    });


} //namespace lowrat

#endif//LOWRAT_UTIL_PARALLEL_SHARED_MEMORY_H_
