#ifndef LOWRAT_UTIL_MATRIX_H_
#define LOWRAT_UTIL_MATRIX_H_

#include <cassert> //for assert
#include <cmath>//for sqrt
#include <cstddef>//for size_t
#include <limits>//for std::numeric_limits<double>::epsilon()
#include <vector>//for vector

#include <blaze/Math.h> //for DynamicMatrix, DynamicVector, DiagonalMatrix
#include "lowrat/config/config.h"//for lowrat::value_t

namespace lowrat
{

//! Interprets the given matrix as a x_dim x y_dim matrix assuming the specified storage order. The
//! given dimensions must match the matrix.
//!
//! Implements a Matlab like reshape function for matrices. Since Blaze uses padding and alignment
//! of the underlying data, it is not possible to just consider the data vector of origin as a
//! matrix of different dimensions. Therefore, the reshaping is implemented by copying the data to a
//! different matrix. result will be resized to the specified dimensions. This functions treats the
//! matrix as if it has the specified storage order (i.e. the reshaping is done as specified by
//! storage_order, not the physical order in memory). Therefore, the access of elements in the
//! matrix is only in order with the physical memory if storage_order matches the matrix type.
template<class MT, bool SO2>
void reshape( MT& origin, bool storage_order, std::size_t x_dim, std::size_t y_dim,
              blaze::DynamicMatrix<double, SO2> & result )
{
    result.resize(x_dim, y_dim, false);

    std::size_t x_new = 0;
    std::size_t y_new = 0;

    // If storage order is assumed as rowMajor, copy the elements row-wise.
    if ( storage_order == blaze::rowMajor )
    {
        for ( std::size_t row = 0; row < origin.rows(); ++row )
        {
            for ( std::size_t col = 0; col < origin.columns(); ++col )
            {
                result(x_new, y_new) = origin(row, col);
                ++y_new;
                if ( y_new >= y_dim )
                {
                    y_new = 0;
                    ++x_new;
                }
            }
        }
    }
    // Else, copy the elements column-wise.
    else
    {
        for ( std::size_t col = 0; col < origin.columns(); ++col )
        {
            for ( std::size_t row = 0; row < origin.rows(); ++row )
            {

                result(x_new, y_new) = origin(row, col);
                ++x_new;
                if ( x_new >= x_dim )
                {
                    x_new = 0;
                    ++y_new;
                }
            }
        }
    }
}


//! This does the same as the function above, but allows a submatrix as result argument. Since
//! submatrices cannot be resized, this can only be called with a sufficiently large submatrix.
template<class MT1, class MT2>
void reshape( MT1& origin, bool storage_order, std::size_t x_dim, std::size_t y_dim, blaze::Submatrix<MT2>& result )
{
    assert(result.rows() >= x_dim);
    assert(result.columns() >= y_dim);

    std::size_t x_new = 0;
    std::size_t y_new = 0;

    // If storage order is assumed as rowMajor, copy the elements row-wise.
    if ( storage_order == blaze::rowMajor )
    {
        for ( std::size_t row = 0; row < origin.rows(); ++row )
        {
            for ( std::size_t col = 0; col < origin.columns(); ++col )
            {
                result(x_new, y_new) = origin(row, col);
                ++y_new;
                if ( y_new >= y_dim )
                {
                    y_new = 0;
                    ++x_new;
                }
            }
        }
    }
    // Else, copy the elements column-wise.
    else
    {
        for ( std::size_t col = 0; col < origin.columns(); ++col )
        {
            for ( std::size_t row = 0; row < origin.rows(); ++row )
            {

                result(x_new, y_new) = origin(row, col);
                ++x_new;
                if ( x_new >= x_dim )
                {
                    x_new = 0;
                    ++y_new;
                }
            }
        }
    }
}


//! For the reduced Kronecker product A and B must be of the same size.
template< typename Type, bool SO = blaze::defaultStorageOrder >
void kronecker( const blaze::DynamicMatrix< Type, SO >& A, const blaze::DynamicMatrix< Type, SO > & B, blaze::DynamicMatrix< Type, SO > & kronecker_product, bool reduced = false )
{
    std::size_t A_rows       = A.rows();
    std::size_t A_columns    = A.columns();
    std::size_t B_rows       = B.rows();
    std::size_t B_columns    = B.columns();

    kronecker_product.resize( A_rows * B_rows, A_columns * B_columns );

    if ( SO )
    {
        // ColumnMajor
        for ( std::size_t j = 0; j < A_columns; ++j )
        {
            for ( std::size_t i = 0; i < A_rows; ++i )
            {
                blaze::submatrix( kronecker_product, i * B_rows, j * B_columns, B_rows, B_columns ) = A( i, j ) * B;
            }
        }
    }
    else
    {
        // RowMajor
        for ( std::size_t i = 0; i < A_rows; ++i )
        {
            for ( std::size_t j = 0; j < A_columns; ++j )
            {
                blaze::submatrix( kronecker_product, i * B_rows, j * B_columns, B_rows, B_columns ) = A( i, j ) * B;
            }
        }
    }


    if ( reduced )
    {
        if ( kronecker_product.rows() > 0 && kronecker_product.columns() > 0 )
        {
            // Reduction of the columns:
            std::size_t reduced_columns = 0;
            for ( std::size_t jA = A_columns - 1; jA > 0; --jA )
            {
                for ( std::size_t jB = 0; jB < jA; ++jB )
                {
                    blaze::column( kronecker_product, jA + jB * B_columns ) += blaze::column( kronecker_product, jB + jA * B_columns );
                }
                blaze::DynamicMatrix< Type, SO > move = blaze::submatrix( kronecker_product, 0UL, jA + jA * B_columns, A_rows * B_rows, A_columns * B_columns - jA * ( B_columns + 1 ) );
                blaze::submatrix( kronecker_product, 0UL, jA * B_columns, A_rows * B_rows, A_columns * B_columns - jA * ( B_columns + 1 ) ) = move;
                reduced_columns += jA;
            }

            std::size_t AB_columns = A_columns * B_columns - reduced_columns;
            kronecker_product.resize( A_rows * B_rows, AB_columns, true );

            // Reduction of the rows:
            std::size_t reduced_rows = 0;
            for ( std::size_t iA = A_rows - 1; iA > 0; --iA )
            {
                for ( std::size_t iB = 0; iB < iA; ++iB )
                {
                    blaze::row( kronecker_product, iA + iB * B_rows ) += blaze::row( kronecker_product, iB + iA * B_rows );
                }
                blaze::DynamicMatrix< Type, SO > move = blaze::submatrix( kronecker_product, iA + iA * B_rows, 0UL, A_rows * B_rows - iA * ( B_rows + 1 ), AB_columns );
                blaze::submatrix( kronecker_product, iA * B_rows, 0UL, A_rows * B_rows - iA * ( B_rows + 1 ), AB_columns ) = move;
                reduced_rows += iA;
            }

            kronecker_product.resize( A_rows * B_rows - reduced_rows, AB_columns, true );
        }// if ( kronecker_product.rows() > 0 && kronecker_product.columns() > 0 )
    }// if ( reduced )
}

template<bool SO>
blaze::DynamicMatrix<double, SO> hadamard_division(blaze::DynamicMatrix<double, SO> m1, blaze::DynamicMatrix<double, SO> m2)
{
    blaze::DynamicMatrix<double, SO> res(m1);

    if ( SO == blaze::rowMajor )
    {
        for ( std::size_t row = 0; row < m1.rows(); ++row )
        {
            for ( std::size_t col = 0; col < m1.columns(); ++col )
            {
                res(row, col) /= m2(row, col);
            }
        }
    }
    else
    {
        for ( std::size_t col = 0; col < m1.columns(); ++col )
        {
            for ( std::size_t row = 0; row < m1.rows(); ++row )
            {
                res(row, col) /= m2(row, col);
            }
        }
    }

    return res;
}

//! Reduced QR decomposition, based on the numerical rank.
template< typename Type, bool SO = blaze::defaultStorageOrder >
void reduced_qr( const blaze::DynamicMatrix< Type, SO > & A, blaze::DynamicMatrix< Type, SO > & Q, blaze::DynamicMatrix< Type, SO > & R )
{
    // Determine the numerical rank of A:
    blaze::DynamicVector< value_t > s;
    blaze::svd( A, s );
    std::size_t rank = 0;
    double precision_factor = std::sqrt( A.rows() * A.columns() );
    for ( std::size_t i = 0; i < s.size(); ++i )
    {
        if ( std::abs( s[ i ] ) > std::abs( s[ 0 ] * precision_factor * std::numeric_limits<double>::epsilon() ) )
        {
            rank++;
        }
        else
        {
            break;
        }
    }

    blaze::qr( A, Q, R );
    Q = blaze::submatrix( Q, 0UL, 0UL, Q.rows(), rank );
    R = blaze::submatrix( R, 0UL, 0UL, rank, R.columns() );
}

blaze::DiagonalMatrix<blaze::DynamicMatrix<double, blaze::columnMajor>> diagMatrix(blaze::DynamicVector<double> vec);

template<bool SO>
double inner_matrix(blaze::DynamicMatrix<double, SO> m1, blaze::DynamicMatrix<double, SO> m2)
{
    double res = 0.0;
    if ( SO == blaze::rowMajor )
    {
        for ( std::size_t row = 0; row < m1.rows(); ++row )
        {
            for ( std::size_t col = 0; col < m1.columns(); ++col )
            {
                res += m1(row, col) * m2(row, col);
            }
        }
    }
    else
    {
        for ( std::size_t col = 0; col < m1.columns(); ++col )
        {
            for ( std::size_t row = 0; row < m1.rows(); ++row )
            {
                res += m1(row, col) * m2(row, col);
            }
        }
    }
    return res;
}

} //namespace lowrat


#endif //LOWRAT_UTIL_MATRIX_H_