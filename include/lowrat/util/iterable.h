#ifndef LOWRAT_UTIL_ITERABLE_H_
#define LOWRAT_UTIL_ITERABLE_H_

#include <cmath>     // for fabs
#include <cstddef>   // for size_t
#include <vector>    // for vector
#include <algorithm> // for stable_partition

namespace lowrat
{
//! Removes all non-zero elements with absolute value less than eps from X.
template< typename Type >
void remove_small_non_zeros( Type& X, double eps )
{
    X = forEach( X, [ eps ]( double d )
    {
        return std::fabs( d ) < eps ? 0 : d;
    } );
}

//! Returns index != j of largest entry in modulus.
size_t argmax_modulus( const std::vector< double >& v, const size_t j = 0 );

//! Scales vector v with factor alpha.
void scale_vector(const double alpha, std::vector< double >& v );

template<typename Cont, typename It>
auto ToggleIndices(Cont &cont, It beg, It end) -> decltype(std::end(cont))
{
    size_t helpIndx(0);
    return std::stable_partition(std::begin(cont), std::end(cont),
                                 [&](decltype(*std::begin(cont)) const&) -> bool
    {
        return std::find(beg, end, helpIndx++) == end;
    });
}

template<typename Cont, typename RemoverIDX>
void delete_elements(Cont &v, const RemoverIDX &idx)
{
    v.erase(ToggleIndices(v, std::begin(idx), std::end(idx)), v.end());
}

template<typename Cont, typename RemoverIDXIterator>
void delete_elements(Cont &v, const RemoverIDXIterator &idx_begin, const RemoverIDXIterator &idx_end)
{
    v.erase(ToggleIndices(v, idx_begin, idx_end), v.end());
}

} //namespace lowrat


#endif // LOWRAT_UTIL_ITERABLE_H_