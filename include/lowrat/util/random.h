#ifndef LOWRAT_UTIL_RANDOM_H_
#define LOWRAT_UTIL_RANDOM_H_

#include <cstddef> //for  size_t
#include <random>  //for  mt19937
#include <vector>  //for  vector

namespace lowrat
{
namespace random
{
extern std::mt19937 mt_;// = getRand();
} //namespace random


typedef std::vector<std::size_t> index_vec;

//! Generates a vector of random tensor indices.
//!
//! This constructs random indices of a tensor of given dimension and size. The number of samples
//! correspond to the number of degrees of freedom for a tttensor with assumed sampling_rank
//! multiplied by a the given (over)sampling_factor. This can be used to create random indices
//! for a tensor completion problem (e.g. see tests/test-ttcompletion-salsa.cpp). It is ensured
//! that every slice has at least sampling_rank * sampling_factor * sampling_factor entries of every
//! index.
std::vector<std::vector<std::size_t>> constructSamples(std::size_t sampling_factor, std::size_t sampling_rank,
                                   std::vector<std::size_t> size, std::size_t dim);

//! Split off a random subset of the given indices of the relative size of control_ration. Returns
//! the original set without the control values and the control set.
std::pair<std::vector<index_vec>, std::vector<index_vec>> splitControlSet(std::vector<index_vec> set,
        double control_ratio);


}//namespace lowrat

#endif //LOWRAT_UTIL_RANDOM_H_

