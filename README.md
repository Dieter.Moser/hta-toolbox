This project contains the generalized cross approximation code used in the CMAME paper  

*A two-stage surrogate model for Neo-Hookean problems based on adaptive proper orthogonal decomposition and hierarchical tensor approximation*

This code was extracted from a larger project which was rewritten at the time and was not ready for publication.
Hence, no further development of this code is envisaged. The main usage is presented in the the htensor\_example.cpp and more importantly in the ht\_cross\_example.cpp. 

Installation instructions 

	mkdir build && cd build
	cmake ..
	make

After compilation the ".\build\bin" folder contains the binaries of the two examples.
