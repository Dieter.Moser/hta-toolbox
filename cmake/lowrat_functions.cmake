function(lowrat_set_standard_target_options OBJECTNAME)

  target_compile_features( ${OBJECTNAME} PRIVATE cxx_std_14)

  target_compile_options( ${OBJECTNAME} PRIVATE
                           $<$<OR:$<CXX_COMPILER_ID:Clang>,$<CXX_COMPILER_ID:AppleClang>,$<CXX_COMPILER_ID:GNU>>:
                                -Wall -Wextra>)

  if(MARCH_NATIVE_ACTIVE)
      target_compile_options( ${OBJECTNAME} PRIVATE
                           $<$<OR:$<CXX_COMPILER_ID:Clang>,$<CXX_COMPILER_ID:AppleClang>,$<CXX_COMPILER_ID:GNU>>: -march=native>)
  endif()

  target_include_directories(${OBJECTNAME} PRIVATE "${lowrat_SOURCE_DIR}/include")

  if(blaze_NOT_installed)
    target_include_directories(${OBJECTNAME} PRIVATE "${lowrat_SOURCE_DIR}/include/blaze" )
  endif()

endfunction()


function(lowrat_add_to_all DEPEND FOLDER FILENAME)

    add_executable(${FILENAME} "${FOLDER}/${FILENAME}.cpp" ${ARGN})

    add_dependencies(${DEPEND} ${FILENAME})

    target_link_libraries( ${FILENAME} PRIVATE blaze_target )
 
    lowrat_set_standard_target_options( ${FILENAME} )

    set_target_properties( ${FILENAME} PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin" )

endfunction()

function(lowrat_add_to DEPEND FOLDER FILENAME)

    add_executable(${FILENAME} EXCLUDE_FROM_ALL "${FOLDER}/${FILENAME}.cpp" ${ARGN})

    add_dependencies(${DEPEND} ${FILENAME})

    target_link_libraries( ${FILENAME} PRIVATE blaze_target )
 
    lowrat_set_standard_target_options( ${FILENAME} )

    set_target_properties( ${FILENAME} PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin" )

endfunction()

