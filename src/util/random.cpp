#include "lowrat/util/random.h"

#include <algorithm>//for sort, shuffle
#include <numeric> //for accumulate
#include <functional>//for ref
#include <array>//for array

namespace lowrat
{

using namespace std;

std::mt19937 getRand()
{
    std::array<unsigned int, std::mt19937::state_size> seed_data;
    std::random_device r;
    std::generate(seed_data.begin(), seed_data.end(), std::ref(r));
    std::seed_seq seq(std::begin(seed_data), std::end(seed_data)); //perform warmup
    std::mt19937 mt_(seq);
    return mt_;
}

std::mt19937 getFixed()
{
    std::mt19937 mt_;
    mt_.seed(123456);
    return mt_;
}

namespace random
{
std::mt19937 mt_ = getRand();
// std::mt19937 mt_ = getFixed();
} //namespace random


vector<vector<size_t>> constructSamples(size_t sampling_factor, size_t sampling_rank,
                                        vector<size_t> size, size_t dim)
{
    size_t p = sampling_factor * sampling_rank * sampling_rank
               * accumulate(begin(size), end(size), 0U);
    size_t m = sampling_factor * sampling_rank * sampling_rank;

    // Create all random samples.

    vector<vector<size_t>> result(p, vector<size_t>(dim));
    for ( auto & vec : result )
    {
        for ( size_t j = 0; j < dim; ++j )
        {
            uniform_int_distribution<size_t> dist(0, size[j] - 1);
            vec[j] = dist(lowrat::random::mt_);
        }
    }

    // Enforce, that every slice density is at least
    //   m = sampling_rank * sampling_factor * sampling_factor.

    size_t count = 0;
    for ( size_t d = 0; d < dim; ++d )
    {
        for ( size_t val = 0; val < size[d]; ++val )
        {
            for ( size_t i = 0; i < m; ++i )
            {
                result[count][d] = val;
                ++count;
            }
        }
    }

    // Only return unique elements.
    sort( begin(result), end(result) );
    result.erase( unique( begin(result), end(result) ), end(result) );

    return result;
}

pair<vector<index_vec>, vector<index_vec>> splitControlSet(vector<index_vec> set,
                                        double control_ratio)
{
    shuffle(begin(set), end(set), lowrat::random::mt_);

    size_t control_size = (size_t) (control_ratio * set.size());
    vector<index_vec> control_set;
    copy(begin(set), begin(set) + control_size, back_inserter(control_set));

    vector<index_vec> sample_set;
    copy(begin(set) + control_size, end(set), back_inserter(sample_set));

    return make_pair(sample_set, control_set);
}

} //namespace lowrat

