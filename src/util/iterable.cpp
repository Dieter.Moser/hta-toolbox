#include "lowrat/util/iterable.h"

namespace lowrat
{

using namespace std;
//! Returns index != j of largest entry in modulus.
size_t argmax_modulus( const vector< double >& v, const size_t j )
{
    size_t k = 0;
    if ( j == 0 )
    {
        k = 1;
    }
    //comparing of signed and unsigned int
    for ( size_t i = 0; i < v.size(); ++i )
    {
        if ( i != j && fabs( v[ i ] ) > fabs( v[ k ] ) )
        {
            k = i;
        }
    }

    return k;
}

//! Scales vector v with factor alpha.
void scale_vector(const double alpha, vector< double >& v )
{
    for (size_t i = 0; i < v.size(); ++i )
    {
        v[ i ] *= alpha;
    }
}

} //namespace lowrat
