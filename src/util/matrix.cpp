#include "lowrat/util/matrix.h"

namespace lowrat
{

using namespace blaze;
using namespace std;


DiagonalMatrix<DynamicMatrix<double, columnMajor>> diagMatrix(DynamicVector<double> vec)
{
    DiagonalMatrix<DynamicMatrix<double, columnMajor>> A(vec.size());
    for ( size_t i = 0; i < vec.size(); ++i )
    {
        A(i, i) = vec[i];
    }

    return A;
}

} //namespace lowrat
