#include "lowrat/cross/ht_cross.h"

#include <cstddef> //for size_t
#include <algorithm> //for min
#include <functional>//for placeholders, bind
#include <vector> //for vector

#include <blaze/Blaze.h> //for DynamicMatrix, svd
#if MPI_ACTIVE
#include "mpi.h" //for MPI
#include "lowrat/util/parallel.h" //for blaze_MPI
#endif
#include "lowrat/cross/matrix_cross.h"  //for MatrixCross
#include "lowrat/cluster/cluster.h" //for Cluster
#include "lowrat/full_tensor/matricization.h" //for Matricization
#include "lowrat/full_tensor/matricization_view.h" //for MatricizationView
#include "lowrat/full_tensor/tensor.h" //for Tensor
#include "lowrat/full_tensor/tensor_mode.h"//for tensormode::absolute_complement
#include "lowrat/ht_tensor/htensor.h" //for HTensor

namespace lowrat
{

typedef std::vector< size_t > tensorindex;
typedef std::vector< size_t > tensorsize;
typedef std::vector< size_t > tensormode;


//can probably be a templated fill with permutation function
void fill_S_with_M(const MatricizationView& M, const std::vector<std::size_t>& row_sub_indices, const std::vector<std::size_t>& col_sub_indices, blaze::DynamicMatrix<double>& S)
{
    //S will be inverted therefore we have to gurante that this will be quadratic
    const std::size_t rows = row_sub_indices.size();
    const std::size_t cols = col_sub_indices.size();
    S.resize(rows, cols);
    for (std::size_t i = 0; i < rows; ++i)
    {
        for (std::size_t j = 0; j < cols; ++j)
        {
            S(i, j) = M(row_sub_indices[i], col_sub_indices[j]);
        }
    }
}

void find_pivots(Matricization mt, std::size_t K,
                 std::function<void(MatricizationView&, std::vector<std::size_t>&, std::vector<std::size_t>&)>& find_pivots_in_submatrix,
                 blaze::DynamicMatrix<double>& S,
                 std::vector<tensorindex>& pivots)
{

    std::vector<tensorindex> indices;
    std::vector<std::size_t> row_sub_indices, col_sub_indices;

    mt.get_random_indices(K, indices);

    MatricizationView M(mt, indices);
    find_pivots_in_submatrix(M, row_sub_indices, col_sub_indices);
    fill_S_with_M(M, row_sub_indices, col_sub_indices, S);

    const std::size_t kmin = std::min(row_sub_indices.size(), col_sub_indices.size());
    for (std::size_t i = 0 ; i < kmin; ++i)
    {
        pivots.push_back(M.get_tensorindex(row_sub_indices[i], col_sub_indices[i]));
    }
}

void nested_safety(const MatricizationView& M, std::vector<std::size_t>& row_sub_indices, std::vector<std::size_t>& col_sub_indices, blaze::DynamicMatrix<double>& S)
{
    blaze::DynamicVector<double, blaze::columnVector> s; // The vector for the singular values

    blaze::svd( S, s );
    double cond = s[0] / (s[s.size() - 1]);
    while ( cond > 1e10)//max_condition_number_ )
    {
        row_sub_indices.pop_back();
        col_sub_indices.pop_back();
        fill_S_with_M(M, row_sub_indices, col_sub_indices, S);
        s.clear();
        blaze::svd(S, s);
        cond = s[0] / (s[s.size() - 1]);
    }
}

void find_pivots_nested(Matricization mt, std::size_t K, const std::vector<tensorindex>& pre_sampled_indices, tensormode & t_bracket,
                        const std::function<void(MatricizationView &, std::vector<std::size_t> &, std::vector<std::size_t> &)> &find_pivots_in_submatrix,
                        blaze::DynamicMatrix<double>& S,
                        std::vector<tensorindex>& pivots)
{
    std::vector<tensorindex> indices(0);
    std::vector<std::size_t> row_sub_indices(0), col_sub_indices(0);
    //if nested

    mt.get_random_indices_nested(K, pre_sampled_indices, t_bracket, indices);

    //std::cout << indices.size() << "\n";

    MatricizationView M(mt, indices);
    find_pivots_in_submatrix(M, row_sub_indices, col_sub_indices);

    fill_S_with_M(M, row_sub_indices, col_sub_indices, S);

    if ( (row_sub_indices.size() > 1) && (col_sub_indices.size() > 1) )
    {
        nested_safety(M, row_sub_indices, col_sub_indices, S);
    }

    const std::size_t kmin = std::min(row_sub_indices.size(), col_sub_indices.size());
    for (std::size_t i = 0; i < kmin; ++i)
    {
        pivots.push_back(M.get_tensorindex(row_sub_indices[i], col_sub_indices[i]));
    }
}

//we need to put this three functions somewhere
std::vector<std::pair<std::size_t, std::size_t>> get_index_pairs(const MatricizationView& M, std::size_t type, std::size_t k, double eps)
{
    MatrixCrossSettings settings = {k, eps, type};
    MatrixCross mc(settings);

    return mc.find_pivots( M );
    //return mc.find_pivots( M.get_full_matrix() );
}
void find_pivots_in_matrix_view(MatricizationView& M, std::vector<tensorindex>& pivots, std::size_t type, std::size_t k, double eps)
{
    const std::vector<std::pair<std::size_t, std::size_t>> index_pairs = get_index_pairs(M, type, k, eps);

    pivots.resize(0);
    for (const auto& ip : index_pairs )
    {
        pivots.push_back( M.get_tensorindex(ip.first, ip.second) );
    }
}
void find_pivots_in_matrix_view(MatricizationView& M, tensorindex& row_indices, tensorindex& col_indices, std::size_t type, std::size_t k, double eps)
{
    const std::vector<std::pair<std::size_t, std::size_t>> index_pairs = get_index_pairs(M, type, k, eps);

    row_indices.resize( 0 );
    col_indices.resize( 0 );
    for (const auto& ip : index_pairs )
    {
        row_indices.push_back( ip.first  );
        col_indices.push_back( ip.second );
    }
}

void HTCross::approximate(const Tensor& T, const tensorindex& pre_pivot, HTensor& HT)
{
    const std::size_t d = T.dim();
    const std::vector< std::size_t > n = T.vector_of_sizes();
    const std::size_t k = settings_.maximal_rank;

    //is automatically parallel though the constructor
    HT = HTensor(d, n, k);

    // Use the proper pivot finding function
    void (*fn)(MatricizationView&, tensorindex&, tensorindex&, std::size_t, std::size_t, double) = &find_pivots_in_matrix_view;


    if ( settings_.ensure_nestedness )
    {
        auto f = std::bind(fn, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, settings_.pivot_strategy, settings_.maximal_rank, settings_.accuracy_epsilon);

        tree_cross_approximation_nested(T, f, settings_.sub_matrix_size, pre_pivot, HT );

    }
    else // non-nested pivot element choice
    {
        throw "Non-nested pivot search is not implemented!";
    }
}

HTensor HTCross::approximate(const Tensor& T, const tensorindex& pre_pivot)
{
    HTensor ht;
    approximate(T, pre_pivot, ht);
    return ht;
}

void HTCross::approximate(const Tensor& T, HTensor& HT)
{
    tensorindex pivot(T.dim(), 0);
    approximate(T, pivot, HT);
}

HTensor HTCross::approximate(const Tensor& T)
{
    HTensor ht;
    approximate(T, ht);
    return ht;
}



void HTCross::tree_cross_approximation_nested(const Tensor& T, const std::function<void(MatricizationView&, tensorindex&, tensorindex&)> & find_pivots_in_submatrix, std::size_t K, const tensorindex& pre_pivot, HTensor& HT)
{
    int my_rank = 0;

#if MPI_ACTIVE
    //MPI_Comm comm = Ht.mpi_communicator();
    MPI_Comm_rank( HT.mpi_communicator(), &my_rank );
#endif

    Cluster ht_tree = HT.clustertree();
    std::vector< const Cluster * > nodes_on_this_rank = ht_tree.all_nodes_of_rank( my_rank );
    std::size_t num_nodes = nodes_on_this_rank.size();

    // pivot elements
    std::vector< std::vector< tensorindex > > pivots(num_nodes);

    // S matrices
    std::vector< blaze::DynamicMatrix<double> > S_matr(num_nodes);

    // Matricizations
    std::vector< Matricization > matr_data;

#if MPI_ACTIVE
    MPI_Status status;
#endif

    //begin find pivot
    //herefore we could write an free funtion into HTensor bool is_used(HT){return HT.node_used_;}
    if ( HT.is_node_used() )
    {
        // find all pivots
        for (const auto& node : nodes_on_this_rank)
        {
            matr_data.push_back( Matricization (T, node->modes() ) );
        }

        // get pivot elements and submatrices
        std::size_t i = 0;
        for (const auto& node : nodes_on_this_rank )
        {
            if (!node->is_root())
            {
                auto t_bracket = tensor_mode::absolute_complement(node->father()->modes(), T.dim());
                // check if father node is on the same mpi rank
                if (node->father()->mpi_rank() == my_rank)
                {
                    //find father
                    std::size_t i_father = i;// = std::floor((i-1)/2.0);
                    for ( std::size_t j = i; j < nodes_on_this_rank.size(); --j )
                    {
                        if ( nodes_on_this_rank[ j ] == nodes_on_this_rank[ i ]->father() )
                        {
                            i_father = j;
                        }
                    }
                    //end find father
                    bool found_pivots = false;
                    std::size_t n_try = 0;

                    while (!found_pivots)
                    {

                        //here the program sometimes dies without no known reason
                        //matr_data[i].find_pivots_nested(K, pivots[i_father], t_bracket, find_pivots_in_submatrix, S_matr[i], pivots[i]);
                        find_pivots_nested(matr_data[i], K, pivots[i_father], t_bracket, find_pivots_in_submatrix, S_matr[i], pivots[i]);
                        ++n_try;
                        if (n_try > 100)
                        {
                            std::cout << "error Unable to find pivots in submatrix after 100 random sub matrices! \n";
                            throw "Unable to find pivots in submatrix after 100 random sub matrices!";
                        }
                        if (pivots[i].size() > 0)
                        {
                            found_pivots = true;
                        }
                    }

                }
#if MPI_ACTIVE
                else
                {
                    std::size_t size = 0;
                    std::vector< tensorindex > fix_pivots;
                    tensormode father_modes;
                    // receive fix pivots Q_t

                    MPI_Recv(&size, 1, _MPI_SIZE_T, node->father()->mpi_rank(), 0, HT.mpi_communicator(), &status );

                    fix_pivots.resize(size);

                    MPI_Recv(fix_pivots.data(), size, _MPI_SIZE_T, node->father()->mpi_rank(), 0, HT.mpi_communicator(), &status);
                    // find the pivots

                    bool found_pivots = false;
                    size_t n_try = 0;

                    while (!found_pivots)
                    {
                        find_pivots_nested(matr_data[i], K, fix_pivots, t_bracket, find_pivots_in_submatrix, S_matr[i], pivots[i]);
                        ++n_try;
                        if (n_try > 100)
                        {
                            throw "Unable to find pivots in submatrix after 100 random sub matrices!";
                        }

                        if (pivots[i].size() > 0)
                        {
                            found_pivots = true;
                        }
                    }
                }
#endif
            }
            else
            {
                // root node,
                pivots[i].clear();
                // pivots[i].push_back(tensorindex(matr_data[i].dim(), 0));
                pivots[i].push_back(pre_pivot);

            }

#if MPI_ACTIVE
            // send pivots
            if (!node->is_leaf())
            {

                size_t size = pivots[i].size();

                if (node->son1()->mpi_rank() != my_rank )
                {
                    MPI_Send( &size, 1, _MPI_SIZE_T, node->son1()->mpi_rank(), 0, HT.mpi_communicator());
                    MPI_Send( pivots[i].data(), size, _MPI_SIZE_T, node->son1()->mpi_rank(), 0, HT.mpi_communicator() );
                }

                if (node->son2()->mpi_rank() != my_rank )
                {
                    MPI_Send( &size, 1, _MPI_SIZE_T, node->son2()->mpi_rank(), 0, HT.mpi_communicator() );
                    MPI_Send( pivots[i].data(), size, _MPI_SIZE_T, node->son2()->mpi_rank(), 0, HT.mpi_communicator() );
                }
            }
#endif
            ++i;
        }
        //end find pivot

        // Use pivots to construct H-Tensor
        i = 0;
        for (const auto& node : nodes_on_this_rank )
        {
#if MPI_ACTIVE
            // Send Pt and S to father
            if ( !node->is_root() && node->father()->mpi_rank() != my_rank )
            {

                size_t size = pivots[i].size();

                if ( node->is_first_son() )
                {
                    MPI_Send( &size, 1, _MPI_SIZE_T, node->father()->mpi_rank(), 1, HT.mpi_communicator());
                    MPI_Send( pivots[i].data(), size, _MPI_SIZE_T, node->father()->mpi_rank(), 1, HT.mpi_communicator() );
                    MPI_Send_DynamicMatrix( S_matr[i], MPI_DOUBLE, node->father()->mpi_rank(), 1, HT.mpi_communicator(), true );
                }
                else
                {
                    MPI_Send( &size, 1, _MPI_SIZE_T, node->father()->mpi_rank(), 2, HT.mpi_communicator());
                    MPI_Send( pivots[i].data(), size, _MPI_SIZE_T, node->father()->mpi_rank(), 2, HT.mpi_communicator() );
                    MPI_Send_DynamicMatrix( S_matr[i], MPI_DOUBLE, node->father()->mpi_rank(), 2, HT.mpi_communicator(), true );
                }
            }
#endif
            // construct the Transfertensor B = S_t1^{-1} * M_tj * S_t2^{-H}
            if (!node -> is_leaf())
            {
                std::vector< tensorindex > p_t1, p_t2;

                blaze::DynamicMatrix< double > S_t1_inv;
                blaze::DynamicMatrix< double > S_t2_inv_trans;

                tensormode t1 = node->son1()->modes();
                // get P_t1

                if (node->son1()->mpi_rank() == my_rank)
                {
                    //find son
                    std::size_t i_son1;
                    for ( std::size_t j = i; j < nodes_on_this_rank.size(); ++j )
                    {
                        if ( nodes_on_this_rank[ j ] == nodes_on_this_rank[ i ]->son1() )
                        {
                            i_son1 = j;
                        }
                    }
                    //end find son

                    p_t1 = pivots[i_son1];
                    S_t1_inv = blaze::inv(S_matr[i_son1]);
                }
#if MPI_ACTIVE
                else
                {
                    size_t size_pt1 = 0;
                    MPI_Recv(&size_pt1, 1, _MPI_SIZE_T, node->son1()->mpi_rank(), 1, HT.mpi_communicator(), &status);
                    p_t1.resize(size_pt1);
                    MPI_Recv(p_t1.data(), size_pt1, _MPI_SIZE_T, node->son1()->mpi_rank(), 1, HT.mpi_communicator(), &status);
                    // Receive S matrix

                    blaze::CustomMatrix< double, blaze::aligned, blaze::usePadding > S_t1_inv_custom;
                    MPI_Recv_DynamicMatrix( S_t1_inv_custom, MPI_DOUBLE, node->son1()->mpi_rank(), 1, HT.mpi_communicator(), &status, -1, -1 );
                    S_t1_inv = S_t1_inv_custom;
                    invert(S_t1_inv);
                }
#endif
                // get P_t2

                if (node->son2()->mpi_rank() == my_rank)
                {
                    //find son
                    std::size_t i_son2;
                    for ( std::size_t j = i; j < nodes_on_this_rank.size(); ++j )
                    {
                        if ( nodes_on_this_rank[ j ] == nodes_on_this_rank[ i ]->son2() )
                        {
                            i_son2 = j;
                        }
                    }
                    //end find son

                    p_t2 = pivots[i_son2];
                    S_t2_inv_trans = blaze::inv(S_matr[i_son2]);
                    S_t2_inv_trans = blaze::trans(S_t2_inv_trans);


                }
#if MPI_ACTIVE
                else
                {

                    // Receive pivot elements

                    size_t size_pt2 = 0;
                    MPI_Recv(&size_pt2, 1,  _MPI_SIZE_T, node->son2()->mpi_rank(), 2, HT.mpi_communicator(), &status );
                    p_t2.resize(size_pt2);
                    MPI_Recv(p_t2.data(), size_pt2,  _MPI_SIZE_T, node->son2()->mpi_rank(), 2, HT.mpi_communicator(), &status );
                    // Receive S matrix

                    blaze::CustomMatrix< double, blaze::aligned, blaze::usePadding > S_t2_inv_trans_custom;
                    MPI_Recv_DynamicMatrix( S_t2_inv_trans_custom, MPI_DOUBLE, node->son2()->mpi_rank(), 2, HT.mpi_communicator(), &status, -1, -1 );
                    S_t2_inv_trans = S_t2_inv_trans_custom;
                    blaze::invert(S_t2_inv_trans);
                    blaze::ctranspose(S_t2_inv_trans);
                }
#endif

                std::vector<blaze::DynamicMatrix<double>> M_t_l = matr_data[i].get_M_t_list( p_t1, p_t2, pivots[i], t1); // get M_t l

                //HT.set_data( blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > > &part_of_data, size_t i)

                blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > > part_of_data(M_t_l.size());
                std::size_t j = 0;

                for (const auto& M : M_t_l)
                {
                    //part_of_data[j].resize(S_t1_inv.rows(), S_t2_inv_trans.columns());
                    part_of_data[j] = S_t1_inv * M * S_t2_inv_trans;
                    ++j;
                }
                HT.set_data(part_of_data, i);
            }
            else
            {
                // node is leaf
                blaze::DynamicMatrix< double, blaze::columnMajor > mat_data = matr_data[i].get_submatrix_from_columns( pivots[i] );
                HT.set_data(mat_data, i, 0UL);
                //HT.data_[i][0] = matr_data[i].get_submatrix_from_columns( pivots[i] );
            }
            ++i;
        }
    }
}

} //namespace lowrat
