#include "lowrat/cross/rkmatrix.h"

#include <cstddef>//for std::size_t
#include <iomanip> //for setprecision
#include <iostream>//for cout, ostream
#include <stdexcept> //for invalid_argument, out_of_range
#include <vector>//for vector

#include <blaze/Blaze.h> //for DynamicVector, rowVector, columnVector, DynamicMatrix, columnMajor, Column, Row

namespace lowrat
{

using namespace std;
using namespace blaze;

//
// Constructor.
//
RkMatrix::RkMatrix(const size_t rows, const size_t cols, const size_t k )
{
    if ( rows < 1 || cols < 1 )
    {
        throw std::invalid_argument( "RkMatrix::RkMatrix(): Dimension must be at least 1x1." );
    }
    rows_ = rows;
    cols_ = cols;
    k_    = k;

    A_.resize(rows_, k_);
    B_.resize(k_, cols_);
}
//
// Resizes the matrix.
// All entries, as well as k, are set to zero.
//
void RkMatrix::resize(const size_t rows, const size_t cols )
{
    rows_ = rows;
    cols_ = cols;
    k_    = 0;

    A_.resize(rows_, k_ );
    B_.resize(k_, cols_ );
}
//
// Element access (const).
//
double RkMatrix::operator()( const size_t i, const size_t j ) const
{
    if ( i >= rows_ || j >= cols_ )
    {
        throw std::out_of_range( "Matrix::operator(): Indices out of range." );
    }

    return blaze::row(A_, i) * blaze::column(B_, j);
}


DynamicVector<double, columnVector> RkMatrix::get_column(const size_t j) const
{
    DynamicVector<double, columnVector> col_vec(rows_);
    col_vec = A_ * blaze::column(B_, j);
    return col_vec;
}

DynamicVector<double, rowVector> RkMatrix::get_row(const size_t i) const
{
    DynamicVector<double, rowVector> row_vec(cols_);
    row_vec = blaze::row(A_, i) * B_;
    return row_vec;
}
//
//  Return the whole matrix
//
blaze::DynamicMatrix<double, blaze::columnMajor>  RkMatrix::operator()() const
{
    return A_ * B_;
}
//
//  Return matrix which is use the first k ranks
//
blaze::DynamicMatrix<double, blaze::columnMajor>  RkMatrix::operator()(const size_t k) const
{
    if (k > k_)
    {
        throw std::out_of_range("RkMatrix::operator(): Rank < k");
    }

    return blaze::submatrix(A_, 0UL, 0UL, A_.rows(), k) * blaze::submatrix(B_, 0UL, 0UL, k, B_.columns());
}
//
// Adds one rank a * b^T.
//
void RkMatrix::add_rank( const std::vector< double >& a, const std::vector< double >& b )
{
    ++k_;
    A_.resize(rows_, k_, true);
    B_.resize(k_, cols_, true);

    for ( size_t i = 0; i < rows_; ++i )
    {
        A_(i, k_ - 1) = a[i];
    }
    for ( size_t i = 0; i < cols_; ++i )
    {
        B_(k_ - 1, i) = b[i];
    }
}

void RkMatrix::add_rank( const blaze::DynamicVector<double, blaze::columnVector>& a, const blaze::DynamicVector<double, blaze::rowVector>& b )
{
    ++k_;
    A_.resize(rows_, k_, true);
    B_.resize(k_, cols_, true );

    blaze::column( A_, k_ - 1 ) = a;
    blaze::row( B_, k_ - 1 ) = b;
}

void RkMatrix::add_rank( const blaze::Column< blaze::DynamicMatrix<double> >& a, const blaze::Row< blaze::DynamicMatrix<double> >& b)
{
    ++k_;
    A_.resize(rows_, k_, true);
    B_.resize(k_, cols_, true );

    blaze::column( A_, k_ - 1 ) = a;
    blaze::row( B_, k_ - 1 ) = b;
}

void RkMatrix::set_rank(const size_t k)
{
    k_ = k;
    A_.resize(rows_, k_, true);
    B_.resize(k_, cols_, true);
}

void RkMatrix::set_rank(const size_t k, const double val)
{
    k_ = k;
    A_.resize(rows_, k_, false);
    A_ = val;
    B_.resize(k_, cols_, false);
    B_ = val;
}

void RkMatrix::insert_rank( const std::vector< double >& a, const std::vector< double >& b, const size_t nu )
{
    for ( size_t i = 0; i < rows_; ++i )
    {
        A_(i, nu) = a[i];
    }
    for ( size_t i = 0; i < cols_; ++i )
    {
        B_(nu, i) = b[i];
    }
}

void RkMatrix::insert_rank( const blaze::DynamicVector<double, blaze::columnVector>& a, const blaze::DynamicVector<double, blaze::rowVector>& b, const size_t nu )
{
    blaze::column( A_, nu) = a;
    blaze::row( B_, nu) = b;
}

void RkMatrix::insert_rank( const blaze::Column< blaze::DynamicMatrix<double> >& a, const blaze::Row< blaze::DynamicMatrix<double> >& b, const size_t nu )
{
    for ( size_t i = 0; i < rows_; ++i )
    {
        A_(i, nu) = a[i];
    }
    for ( size_t i = 0; i < cols_; ++i )
    {
        B_(nu, i) = b[i];
    }
}
//
// Displays the content of a RkMatrix.
//
void RkMatrix::display_content() const
{
    std::cout << "------------------------\n";
    std::cout << "|        RkMatrix      |\n";
    std::cout << "------------------------\n";
    std::cout << "rows     = " << rows_ << "\n";
    std::cout << "cols     = " << cols_ << "\n";
    std::cout << "k        = " << k_ << "\n";
    std::cout << "------------------------\n";
    std::cout << "A : \n";
    std::cout << A_ << "\n";
    std::cout << "B : \n";
    std::cout << B_ << "\n";
}
//
// RkMatrix to ostream.
//
std::ostream & operator <<( std::ostream & os, const RkMatrix & M )
{
    os << std::setprecision( 3 ) << std::scientific;
    os << M();
    return os;
}

} //namespace lowrat
