#include "lowrat/cross/ht_cross_approximation.h"

#include <cstddef> //for size_t
#include <vector> //for vector

#include "lowrat/ht_tensor/htensor.h" //for HT-tensor
#include "lowrat/cross/ht_cross.h" //for HT-cross-approximation
#include "lowrat/full_tensor/tensor.h" // for full tensor

namespace lowrat
{
HTensor cross_approximation(std::function<void(const std::vector< std::vector<std::size_t>>&, std::vector<double>&)> eval,
                            std::size_t dimension,
                            const std::vector<std::size_t>& mode_sizes,
                            const std::vector<std::size_t>& pivot,
                            double tolerance)
{
    Tensor eval_tensor(dimension, mode_sizes, eval);

    // ToDo: no default values
    HTCrossSettings settings;
    settings.accuracy_epsilon = tolerance;
    settings.sub_matrix_size = 50;
    HTCross ht_cross{settings};

    return ht_cross.approximate(eval_tensor, pivot);
}

} //namespace lowrat
