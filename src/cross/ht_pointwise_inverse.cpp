#include "lowrat/cross/ht_pointwise_inverse.h"

#include <cstddef> //for size_t
#include <functional>//for std::function
#include <vector> //for vector

#include "lowrat/ht_tensor/htensor.h" //for HT-tensor
#include "lowrat/cross/ht_cross_approximation.h" //for HT-cross-approximation
#include "lowrat/cross/ht_cross.h" // for cross
#include "lowrat/ht_tensor/ht_maximum.h"//for maximum

namespace lowrat
{

HTensor pointwise_inverse_update(
    const HTensor& ht_denominator,
    const HTensor& current_tensor,
    const std::vector<std::size_t>& pivot,
    double cross_tol,
    double delete_eps)
{
    const std::size_t dimension = pivot.size();
    const std::vector<std::size_t> mode_sizes = ht_denominator.sizes();

    std::function<void(const std::vector< std::vector< std::size_t > >&, std::vector< double >&)> eval
        = [ht_denominator, current_tensor, delete_eps] (const std::vector< std::vector< size_t > >& index, std::vector< double >& entries)
    {
        const std::size_t number = index.size();

        // old version
        /*
        for (std::size_t i = 0; i < number; ++i)
        {
        	entries[i] = (1.0 /  ht_denominator.entry(index[i])) - current_tensor.entry(index[i]);
        }
        */

        //ToDo: so okay
        for (std::size_t i = 0; i < number; ++i)
        {
            double value =  ht_denominator.entry(index[i]);

            if (std::abs(value) < delete_eps)
            {
                entries[i] = 0.0;
            }
            else
            {
                entries[i] = (1.0 /  value) - current_tensor.entry(index[i]);
            }
        }
    };

    return cross_approximation(eval, dimension, mode_sizes, pivot, cross_tol);

}

HTensor pointwise_inverse(
    const HTensor& ht_denominator,
    std::size_t max_iter,
    double eps_truncation,
    double error_tol,
    double cross_tol,
    double delete_eps)
{
    const HTensor ones(ht_denominator, 1.0);
    HTensor current_tensor(ht_denominator, 0.0);

    // ToDo: in case i = 0: pivot = argmin ht_denominator
    std::vector<std::size_t> pivot = current_tensor.random_tensorindex();


    for (std::size_t i = 0; i < max_iter; ++i)
    {
        current_tensor += pointwise_inverse_update(ht_denominator, current_tensor, pivot, cross_tol, delete_eps);
        current_tensor.truncate(eps_truncation);

        HTensor error_rel = hadamard(ht_denominator, current_tensor) - ones;
        error_rel.truncate(eps_truncation);

        pivot = argmax(error_rel);

        const double max_error = std::abs(error_rel.entry(pivot));

        if (max_error < error_tol)
        {
            return current_tensor;
        }
    }

    return current_tensor;
}


} //namespace lowrat
