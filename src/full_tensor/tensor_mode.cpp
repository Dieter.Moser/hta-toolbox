#include "lowrat/full_tensor/tensor_mode.h"

#include <cstddef>//for size_t
#include <algorithm>//for sort, set_difference, set_union
#include <iterator>//for inserter
#include <numeric>//for iota
#include <vector>//for vector


namespace lowrat
{

namespace tensor_mode
{

std::vector< std::size_t > complement(std::vector< std::size_t > modes_a, std::vector< std::size_t > modes_b)
{
    std::vector< std::size_t > indices;
    std::sort(modes_a.begin(), modes_a.end());
    std::sort(modes_b.begin(), modes_b.end());
    std::set_difference(modes_a.begin(), modes_a.end(), modes_b.begin(), modes_b.end(), std::inserter(indices, indices.end()));
    return indices;
};

std::vector< std::size_t > set_union(std::vector< std::size_t > modes_a, std::vector< std::size_t > modes_b)
{
    std::vector< std::size_t > indices;
    std::sort(modes_a.begin(), modes_a.end());
    std::sort(modes_b.begin(), modes_b.end());
    std::set_union(modes_a.begin(), modes_a.end(), modes_b.begin(), modes_b.end(), std::back_inserter(indices));
    return indices;
};


std::vector< std::size_t > absolute_complement(std::vector< std::size_t > modes_a, std::size_t dim)
{
    std::vector< std::size_t > indices;
    std::vector< std::size_t > all_modes(dim);
    std::iota(all_modes.begin(), all_modes.end(), 0);
    std::sort(modes_a.begin(), modes_a.end());
    std::set_difference(all_modes.begin(), all_modes.end(), modes_a.begin(), modes_a.end(), std::inserter(indices, indices.end()));
    return indices;
};

} //namespace tensor_mode

} //namespace lowrat