#include "lowrat/full_tensor/random_tensorindex_generator.h"

#include <cstddef>//for size_t
#include <vector>//for vector
#include <algorithm> //for find, sort
#include <random>//for uniform_int_distribution

#include "lowrat/full_tensor/tensorindex_range.h" //for TensorindexRange
#include "lowrat/util/random.h" //for lowrat::random::mt_

namespace lowrat
{

typedef std::vector< std::size_t >  tensorindex;

RandomTensorindexGenerator::RandomTensorindexGenerator(const TensorindexRange& range, const std::size_t n_samples) : tir_(range)
{
    maxRTI_ = n_samples;
    for (std::size_t i = 0; i < (tir_.modes()).size(); ++i)
    {
        random_dists_.push_back(std::uniform_int_distribution<std::size_t>(0, tir_.size_[tir_.modes_[i]] - 1));
    }
    generated_indices_.resize(0);
}

tensorindex RandomTensorindexGenerator::get_random_index() const
{
    tensorindex ti( tir_.dim() );
    std::size_t j = 0;
    for (auto&& i : tir_.fixed_modes_)
    {
        ti[i] = tir_.fixed_[j];
        ++j;
    }

    j = 0;
    for (auto& dis : random_dists_)
    {
        ti[tir_.modes_[j]] = dis(lowrat::random::mt_);
        ++j;
    }
    return ti;
}

tensorindex RandomTensorindexGenerator::next_index() const
{
    tensorindex ti( tir_.dim() );
    std::size_t j = 0;
    do
    {
        ti = get_random_index();
        ++j;
    }
    while (std::find( generated_indices_.begin(), generated_indices_.end(), ti ) != generated_indices_.end() && j < maxRTI_);

    if (j < maxRTI_)
    {
        generated_indices_.push_back(ti);
    }
    return generated_indices_.back();
}

// void RandomTensorindexGenerator::push_back(tensorindex ti)
// {
//     generated_indices_.push_back(ti);
// }

std::vector<tensorindex> RandomTensorindexGenerator::sample(const std::size_t j) const
{
    for (std::size_t i = 0; i < j; ++i)
    {
        next_index();
    }

    std::sort(generated_indices_.begin(), generated_indices_.end());
    return generated_indices_;
}

std::vector<tensorindex> RandomTensorindexGenerator::get_sampled() const
{
    return generated_indices_;
}

void RandomTensorindexGenerator::sample(const std::size_t K, std::vector<tensorindex>& index_list) const
{
    for (std::size_t i = 0; i < K; ++i)
    {
        index_list.push_back(get_random_index());
    }
}

//can not be const because dis(lowrat::random::mt_) changes random_dists_ and is therefore not const
void RandomTensorindexGenerator::randomize(tensorindex& ti) const
{
    std::size_t j = 0;
    //only work on reference such that dis can be changed and not generate the same random number in worst case
    for (auto& dis : random_dists_)
    {
        ti[tir_.modes_[j]] = dis(lowrat::random::mt_);
        ++j;
    }
}

} //namespace lowrat

