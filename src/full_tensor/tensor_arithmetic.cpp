#include "lowrat/full_tensor/tensor_arithmetic.h"

#include <cassert>//for assert
#include <cstddef>//for size_t
#include <algorithm>//for sort, set_difference, set_union
#include <iterator>//for inserter
#include <map>//for map
#include <numeric>//for iota
#include <vector>//for vector

#include <blaze/Blaze.h>
#include "lowrat/full_tensor/matricization.h"
#include "lowrat/full_tensor/tensor.h"
#include "lowrat/full_tensor/tensorindex_range.h"
#include "lowrat/full_tensor/tensor_mode.h"
#include "lowrat/util/iterable.h"
#include "lowrat/util/io.h"


namespace lowrat
{

Tensor contract_tensor(const Tensor& T, const blaze::DynamicMatrix< double, blaze::rowMajor >& U, std::size_t d)
{
    // check if sizes match
    assert( U.rows() == T.size(d));

    Tensor F( T );

    std::map< tensorindex, double > new_computed_coeff;
    tensorsize new_size(T.vector_of_sizes());
    new_size[d] = U.columns();

    tensorindex act;
    tensormode tm(1, d);
    tensormode tm_c = tensor_mode::absolute_complement(tm, T.dim()); // all but mode d define iteration over columns of the matricisation

    TensorindexRange tir_c(F.vector_of_sizes(), tm_c);


    for (auto it = tir_c.begin(); it != tir_c.end(); ++it  )
    {
        act = *it;
        for (std::size_t i = 0; i < U.columns(); ++i)
        {
            double sum = 0;
            for (std::size_t k = 0; k < U.rows(); ++k)
            {
                act[d] = k;
                sum += U(k, i) * F(act);
            }
            act[d] = i;
            new_computed_coeff[act] = sum;
        }
    }

    F.resize(new_size);
    F.set_computed_coeff(new_computed_coeff);
    return F;
}


//
// Contract Tensor in different directions sequentially
//
Tensor contract_tensor_multiple_directions(const Tensor& T, const std::vector<blaze::DynamicMatrix< double, blaze::rowMajor>>& U_vec, const std::vector<std::size_t>& directions)
{
    assert( U_vec.size() == directions.size() );
    Tensor F(T);
    for (std::size_t i = 0; i < U_vec.size(); ++i )
    {
        F = contract_tensor(F, U_vec[i], directions[i]);
    }
    return F;
}



//
//  Contract Tensor
//
Tensor contract_tensor(const Tensor& T, const blaze::DynamicMatrix< double, blaze::rowMajor >& U, const tensormode& tm)
{
    // check if sizes match

    assert( U.rows() == T.size(tm) );

    tensorsize new_size(T.vector_of_sizes());
    // std::cout << new_size << "\n";
    new_size[tm[0]] = U.columns();
    for (std::size_t i = 1; i < tm.size(); ++i)
    {
        new_size[tm[i]] = 1;    // set all but the first mode to size 1
    }
    // set first mode to the new size
    // delete_elements(new_size, tm.begin()+1,tm.end());   // delete remaining indices
    // std::cout << new_size << "\n";
    Tensor F( new_size.size(), new_size );
    // for(std::size_t i=1; i<tm.size(); ++i){
    // new_size.erase(new_size.begin() + tm[i]-i+1); // needs testing
    // }


    std::map< tensorindex, double > new_computed_coeff;
    tensorindex act_outer, act_inner, act_new;
    tensormode tm_c = tensor_mode::absolute_complement(tm, T.dim()); // all but mode d

    TensorindexRange tir_c( T.vector_of_sizes(), tm_c );

    // std::cout << tm << "\n";
    // std::cout << tm_c << "\n";

    for (auto it_c = tir_c.begin(); it_c != tir_c.end(); ++it_c  ) // outer loop
    {
        act_outer = *it_c;
        // std::cout << "outer_loop : " << act_outer << "\n";
        std::size_t column_index = 0;
        TensorindexRange tir_new(new_size, tm, act_outer);

        for (auto it_new = tir_new.begin(); it_new != tir_new.end(); ++it_new  ) // loop over the columns of U
        {
            double sum = 0;
            TensorindexRange tir(T.vector_of_sizes(), tm, act_outer);
            // std::cout << "mid_loop : " << *it_new << "\n";
            std::size_t row_index = 0;
            for (auto it = tir.begin(); it != tir.end(); ++it  )   // actual vector x vector multiplication
            {
                // std::cout << "inner_loop : " << *it << "\n";
                sum += U(row_index, column_index) * T(*it);
                row_index++;
            }
            column_index++;
            new_computed_coeff[*it_new] = sum;
        }
    }
    F.resize(new_size);
    F.set_computed_coeff(new_computed_coeff);
    return F;
}

//
//  Contract Tensor, where U has column size of prod(sizes) not needed until now thus not tested
//
// Tensor contract_tensor(const Tensor& T, const blaze::DynamicMatrix< double, blaze::rowMajor >& U, const tensormode& tm, const tensorsize& sizes)
// {
//     // check if sizes match


//     assert( U.rows() == T.size(tm) );
//     assert( tm.size() == sizes.size() );

//     Tensor F( T );

//     std::map< tensorindex, double > new_computed_coeff;
//     tensorsize new_size(T.vector_of_sizes());

//     std::size_t i = 0;
//     std::size_t col_size = 1;
//     for(auto m : tm){ new_size[m] = sizes[i]; col_size *= sizes[i]; }
//     assert( U.columns() == col_size );

//     tensorindex act_outer, act_inner, act_new;
//     tensormode tm_c = T.absolute_complement(tm); // all but mode d

//     TensorindexRange tir_c( F.vector_of_sizes(), tm_c );

//     for (auto it_c = tir_c.begin(); it_c != tir_c.end(); ++it_c  ) // outer loop
//     {
//         act_outer = *it_c;
//         std::size_t column_index=0;
//         TensorindexRange tir_new(new_size, tm, act_outer);

//         for (auto it_new = tir_new.begin(); it_new != tir_new.end(); ++it_new  ) // loop over the columns of U
//         {
//             double sum = 0;
//             TensorindexRange tir(F.vector_of_sizes(), tm, act_outer);
//             std::size_t row_index=0;
//             for (auto it = tir.begin(); it != tir.end(); ++it  ){ // actual vector x vector multiplication
//                 sum += U(row_index,column_index) * F(*it);
//                 row_index++;
//             }
//             column_index++;
//             new_computed_coeff[*it_new] = sum;
//         }
//     }
//     F.resize(new_size);
//     F.set_computed_coeff(new_computed_coeff);
//     return F;
// }

// Tensor contract_tensor(const Tensor& T, const std::vector<blaze::DynamicMatrix< double, blaze::rowMajor>>& U_vec, const std::vector<tensormode>& modes, const std::vector<tensorsize>& sizes)
// {
//     assert( U_vec.size() == modes.size() );
//     assert( U_vec.size() == sizes.size() );
//     Tensor F(T);
//     for(std::size_t i=0; i<U_vec.size(); ++i ){
//         F = contract_tensor(F, U_vec[i], modes[i], sizes[i]);
//     }
//     return F;
// }

}