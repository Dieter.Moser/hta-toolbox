#include "lowrat/full_tensor/matricization_view.h"

#include <cstddef>//for size_t
#include <algorithm>//for std::min
#include <vector>//for vector

#include <blaze/Blaze.h> //for DynamicMatrix, DynamicVector, columnVector, rowVector
#include "lowrat/full_tensor/matricization.h" // for Matricization

namespace lowrat
{

typedef std::vector< std::size_t > tensorindex;

MatricizationView::MatricizationView(const Matricization& M, const std::vector<tensorindex>& pivots ) : M_(M), pivots_(pivots)
{
    rows_ = std::min(pivots_.size(), M_.rows());
    columns_ = std::min(pivots_.size(), M_.columns());
}


std::size_t MatricizationView::rows() const
{
    return rows_;
}
std::size_t MatricizationView::columns() const
{
    return columns_;
}


double MatricizationView::operator()(std::size_t i, std::size_t j) const
{
    return M_(get_tensorindex(i, j));
}

double MatricizationView::operator ()(const tensorindex& p) const
{
    return M_(p);
}

std::vector<double> MatricizationView::operator ()(const std::vector<tensorindex>& p) const
{
    return M_(p);
}

std::vector<tensorindex> MatricizationView::get_column_tensor_indices(std::size_t j) const
{
    std::vector<tensorindex> indices(rows_);
    for (std::size_t i = 0; i < rows_; ++i)
    {
        indices[i] = get_tensorindex(i, j);
    }
    return indices;
}

std::vector<tensorindex> MatricizationView::get_row_tensor_indices(std::size_t i) const
{
    std::vector<tensorindex> indices(columns_);
    for (std::size_t j = 0; j < columns_; ++j)
    {
        indices[j] = get_tensorindex(i, j);
    }
    return indices;
}

blaze::DynamicVector<double, blaze::columnVector> MatricizationView::get_column(std::size_t j) const
{
    blaze::DynamicVector<double, blaze::columnVector> column_vector(rows_);
    std::vector<tensorindex> p = get_column_tensor_indices(j);
    auto v = operator()(p);
    for (std::size_t i = 0; i < rows_; ++i)
    {
        column_vector[i] = v[i];
    }
    return column_vector;
}
blaze::DynamicVector<double, blaze::rowVector> MatricizationView::get_row(std::size_t i) const
{
    blaze::DynamicVector<double, blaze::rowVector> row_vector(columns_);
    std::vector<tensorindex> p = get_row_tensor_indices(i);
    auto v = operator()(p);
    for (std::size_t j = 0; j < columns_; ++j)
    {
        row_vector[j] = v[j];
    }
    return row_vector;
}


blaze::DynamicMatrix<double, blaze::columnMajor> MatricizationView::get_full_matrix() const
{
    blaze::DynamicMatrix<double, blaze::columnMajor> full_matrix(rows_, columns_, 0.0);
    for (std::size_t i = 0; i < rows_; ++i)
    {
        for (std::size_t j = 0; j < columns_; ++j)
        {
            full_matrix(i, j) = operator()(i, j);
        }
    }
    return full_matrix;
}

tensorindex MatricizationView::get_tensorindex(std::size_t i, std::size_t j) const
{
    return M_.get_tensorindex(i, j, pivots_);
}
void MatricizationView::get_tensorindex(std::size_t i, std::size_t j, tensorindex& index) const
{
    index = get_tensorindex(i, j);
}

tensorindex get_tensorindex(const MatricizationView& M, std::size_t i, std::size_t j)
{
    return M.get_tensorindex(i, j);
}


blaze::DynamicVector<double, blaze::columnVector> column(const MatricizationView& M, std::size_t j)
{
    return M.get_column(j);
}
blaze::DynamicVector<double, blaze::rowVector> row(const MatricizationView& M, std::size_t i)
{
    return M.get_row(i);
}

blaze::DynamicMatrix<double, blaze::columnMajor> full_matrix(const MatricizationView& M)
{
    return M.get_full_matrix();
}

} //namespace lowrat