#include "lowrat/full_tensor/tensorindex_range.h"

#include <cstddef>//for size_t
#include <vector>//for vector
#include <stdexcept> //for out_of_range
#include <algorithm> //for max_element, find
#include <iterator> //for reverse_iterator, bidirectional_iterator_tag

namespace lowrat
{

typedef     std::vector< size_t >  tensorindex;
typedef     std::vector< size_t >  tensorsize;
typedef     std::vector< size_t >  tensormode;

TensorindexRange::TensorindexRange(const tensorindex& size, const tensorindex& modes, const tensorindex& ti)
{
    size_ = size;

    if (modes.empty())
    {
        modes_ = modes;
        for (std::size_t j = 0; j < size.size(); ++j)
        {
            fixed_modes_.push_back(j);
        }
    }
    else
    {
        if ( *std::max_element(modes.begin(), modes.end()) > dim())
        {
            throw std::out_of_range("Tensor dimension does not fit together with modes");
        }
        else
        {
            modes_ = modes;
            //std::sort(modes_.begin(), modes_.end());
        }
        for (std::size_t i = 0; i < dim(); ++i)
        {
            if (std::find(modes_.begin(), modes_.end(), i) == modes_.end())
            {
                fixed_modes_.push_back(i);
            }
        }

    }
    fixed_ = std::vector<std::size_t>(fixed_modes_.size());

    std::size_t j = 0;
    for (const auto& i : fixed_modes_)
    {
        fixed_[j] = ti[i];
        ++j;
    }
}

TensorindexRange::TensorindexRange(const tensorsize& size, const tensormode& modes)
{
    size_ = size;

    if (modes.empty())
    {
        modes_ = modes;
        for (size_t j = 0; j < size.size(); ++j)
        {
            fixed_modes_.push_back(j);
        }
    }
    else
    {
        if ( *std::max_element(modes.begin(), modes.end()) > dim())
        {
            throw std::out_of_range("Tensor dimension does not fit together with modes");
        }
        else
        {
            modes_ = modes;
            // std::sort(modes_.begin(), modes_.end());
        }

        for (size_t i = 0; i < dim(); ++i)
        {
            if (std::find(modes_.begin(), modes_.end(), i) == modes_.end())
            {
                fixed_modes_.push_back(i);
            }
        }

    }
    fixed_ = std::vector<size_t>( fixed_modes_.size(), 0);

}

void TensorindexRange::set(const tensorindex& ti)
{
    std::size_t j = 0;
    for (const auto& i : fixed_modes_)
    {
        fixed_[j] = ti[i];
        ++j;
    }
}

void TensorindexRange::set(const std::size_t pos, const std::size_t fixed)
{
    fixed_[pos] = fixed;
}

void TensorindexRange::merge(const tensorindex& this_one, const tensorindex& and_that, tensorindex& into) const
{
    into.clear();
    into.resize(modes_.size() + fixed_modes_.size());
    for (const auto& i : modes_)
    {
        into[i] = this_one[i];
    }
    for (const auto& i : fixed_modes_)
    {
        into[i] = and_that[i];
    }
}

tensormode TensorindexRange::modes() const
{
    return modes_;
}

TensorindexRange::iterator::iterator(TensorindexRange* tir)
{
    tir_ = tir;
    ti_ = std::vector<std::size_t>(tir->dim(), 0);
    std::size_t j = 0;
    for (const auto& i : this->tir_->fixed_modes_)
    {
        ti_[i] = this->tir_->fixed_[j];
        ++j;
    }
}

bool TensorindexRange::iterator::operator== (iterator const& other) const
{
    if (at_beginning_ == other.at_beginning_ && at_end_ == other.at_end_)
    {
        // Check if modes are alike
        for (std::size_t i = 0; i < this->tir_->modes_.size(); ++i)
        {
            if (this->tir_->modes_[i] != other.tir_->modes_[i])
            {
                return false;
            }
        }

        // Check if indices are the same
        for (std::size_t i = 0; i < ti_.size(); ++i)
        {
            if (ti_[i] != other.ti_[i])
            {
                return false;
            }
        }

        return true;

    }
    else
    {
        return false;
    }
}

bool TensorindexRange::iterator::operator!= (iterator const& other) const
{
    return !(*this == other);
}

tensorindex TensorindexRange::iterator::operator*() const
{
    return this->ti_;
}

void TensorindexRange::iterator::one_up()
{
    at_end_ = true;

    for (std::size_t i = 0; i < this->tir_->modes_.size(); ++i)
    {
        if ( ti_[this->tir_->modes_[i]] == this->tir_->size_[this->tir_->modes_[i]] - 1)
        {
            ti_[ this->tir_->modes_[i] ] = 0;
        }
        else
        {
            ti_[ this->tir_->modes_[i] ] += 1;
            i = this->tir_->modes_.size();
            at_end_ = false;
        }
    }
}

void TensorindexRange::iterator::one_down()
{
    at_beginning_ = true;

    for (std::size_t i = 0; i < this->tir_->modes_.size(); ++i)
    {
        if ( ti_[ this->tir_->modes_[i] ] == 0)
        {
            ti_[ this->tir_->modes_[i] ] = this->tir_->size_[ this->tir_->modes_[i] ] - 1;
        }
        else
        {
            ti_[ this->tir_->modes_[i] ] -= 1;
            i = this->tir_->modes_.size();
            at_beginning_ = false;
        }
    }
}


void TensorindexRange::iterator::set_to_first()
{
    at_beginning_ = false;
    at_end_ = false;
    for (const auto& i : this->tir_->modes_)
    {
        ti_[i] = 0;
    }
}


void TensorindexRange::iterator::set_to_last()
{
    at_beginning_ = false;
    at_end_ = false;
    for (const auto& i : this->tir_->modes_)
    {
        ti_[i] = this->tir_->size_[i] - 1;
    }
}


void TensorindexRange::iterator::set_to_start()
{
    at_beginning_ = true;
    at_end_ = false;
    for (const auto& i : this->tir_->modes_)
    {
        ti_[i] = 0;
    }
}


void TensorindexRange::iterator::set_to_end()
{
    at_beginning_ = false;
    at_end_ = true;
    for (const auto& i : this->tir_->modes_)
    {
        ti_[i] = this->tir_->size_[i] - 1;
    }
}

TensorindexRange::iterator TensorindexRange::begin()
{
    TensorindexRange::iterator it(this);
    it.set_to_start();
    return it;
}

TensorindexRange::iterator TensorindexRange::end()
{
    TensorindexRange::iterator it(this);
    it.set_to_end();
    return it;
}

TensorindexRange::reverse_iterator TensorindexRange::rbegin()
{
    return reverse_iterator(this->end());
}

TensorindexRange::reverse_iterator TensorindexRange::rend()
{
    return reverse_iterator(this->begin());
}

} //namespace lowrat

