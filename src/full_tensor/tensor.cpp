#include "lowrat/full_tensor/tensor.h"

#include <cstddef>//for size_t
#include <cstdlib>//for rand
#include <stdexcept> //for invalid_argument, out_of_range
#include <vector>//for vector

#include "lowrat/util/random.h" //for lowrat::random::mt_

namespace lowrat
{
//
// Constructor.
//
Tensor::Tensor(const size_t dim )
    :   dim_( dim )
{
    size_.resize( dim_ );
}

Tensor::Tensor(const std::size_t dim, const tensorsize& size ) : dim_( dim ), size_( size )
{

}

Tensor::Tensor(const std::size_t dim, const tensorsize& size, const std::function<void(const std::vector< tensorindex >&, std::vector< double >&)>& eval )
    : dim_(dim), size_(size), eval_(eval)
{

}


Tensor::Tensor(const std::size_t dim, const tensorsize& size, const std::map< tensorindex, double >& data): dim_(dim), size_(size), computed_coeff_(data)
{

}


//
// Returns size of mu-th mode.
//
std::size_t Tensor::size(const std::size_t mu ) const
{
    if ( size_.size() == dim_ )
    {
        if ( mu >= dim_ )
        {
            throw std::invalid_argument( "Tensor::size(): Wrong index." );
        }
        return size_[ mu ];
    }

    return 0;
}

//
// Returns vector of all sizes.
//
tensorindex Tensor::vector_of_sizes() const
{
    return size_;
}

//
// Resizes the tensor.
//
void Tensor::resize( const tensorsize& size )
{
    if ( size.size() != dim_ )
    {
        throw std::invalid_argument( "Tensor::resize(): Wrong dimension of size." );
    }
    size_ = size;
}

//
// Returns number of already computed entries.
//
std::size_t Tensor::computed_entries() const
{
    return computed_coeff_.size();
}

//
// Returns tensor coefficient for one single index.
//
double Tensor::entry( const tensorindex& index ) const
{
    //gets checked in entries
    // if ( tensorindex_out_of_range( index ) )
    // {
    //     throw std::out_of_range( "Tensor::entry(): Index out of range." );
    // }

    std::vector< double >        coeff( 1 );
    std::vector< tensorindex >   idc( 1 );
    idc[ 0 ] = index;

    entries( idc, coeff );

    return coeff[ 0 ];
}

//
// Computes tensor coefficients for a vector of indices.
//
void Tensor::entries( const std::vector< tensorindex >& indices, std::vector< double >& entries) const
{
    std::size_t size = indices.size();
    entries.resize(size);

    for ( std::size_t i = 0; i < size; ++i )
    {
        if ( tensorindex_out_of_range( indices[ i ] ) )
        {
            throw std::out_of_range( "Tensor::entries(): Index out of range." );
        }
    }

    std::vector< bool >          computed( size, false );
    std::vector< tensorindex >   to_be_computed;
    for ( std::size_t i = 0; i < size; ++i )
    {
        size_t count = computed_coeff_.count( indices[ i ] );
        if ( count > 0 )
        {
            computed[ i ] = true;
        }
        else
        {
            to_be_computed.push_back( indices[ i ] );
        }
    }

    std::vector< double > computed_entries( to_be_computed.size() );

    if (eval_)
    {
        eval( to_be_computed, computed_entries );
    }

    size_t count = 0;
    for ( std::size_t i = 0; i < size; ++i )
    {
        if ( computed[ i ] )
        {
            entries[ i ] = computed_coeff_[ indices[ i ] ];
        }
        else
        {
            entries[ i ] = computed_entries[ count ];
            computed_coeff_[ indices[ i ] ] = entries[ i ];
            ++count;
        }
    }
}

//
// Returns true if tensorindex is out of range (according to size_).
//
bool Tensor::tensorindex_out_of_range( const tensorindex& index) const
{
    //comparing of signed and unsigned int
    for ( size_t i = 0; i < dim_; ++i )
    {
        //comparing of a unsigned value with < 0 results in false, therefore removed index[ i ] < 0 ||
        if ( index[ i ] >= size_[ i ] )
        {
            return true;
        }
    }

    return false;
}


bool Tensor::vectorindex_out_of_range(const size_t vectorindex, const tensormode& modes) const
{
    size_t size_of_indexset = 1;
    for (const auto& r : modes)
    {
        size_of_indexset *= size_[r];
    }

    if ( vectorindex >= size_of_indexset )
    {
        return true;
    }
    //else
    //{
    return false;
    //}

}


void Tensor::random_index( tensorindex& index) const
{

    index.resize( dim_ );

    for ( size_t i = 0; i < dim_; ++i )
    {
        std::uniform_int_distribution<std::size_t> rf(0,size_[i]-1);
        index[ i ] = rf(lowrat::random::mt_);
    }

}

void Tensor::eval( const std::vector< tensorindex >& indices, std::vector< double >& entries) const
{
    eval_(indices, entries);
}


} //namespace lowrat
