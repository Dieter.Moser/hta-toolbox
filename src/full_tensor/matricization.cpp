#include "lowrat/full_tensor/matricization.h"

#include <cassert>//for assert
#include <cstddef>//for size_t
#include <algorithm> //for min
#include <limits>// for std::numeric_limits<std::size_t>::max()
#include <random> //for std::uniform_int_distribution

#include <blaze/Blaze.h> //for dynamic matrix
#include "lowrat/full_tensor/random_tensorindex_generator.h" //for RandomTensorindexGenerator
#include "lowrat/full_tensor/tensor.h"//for tensor
#include "lowrat/full_tensor/tensorindex_range.h" //for TensorindexRange
#include "lowrat/full_tensor/tensor_mode.h" //for tensor_mode::absolute_complement
#include "lowrat/util/random.h"//for mt_

namespace lowrat
{

Matricization::Matricization(const Tensor& T, const tensormode& row_modes)  : T_(T)
{
    auto col_modes = tensor_mode::absolute_complement(row_modes, T_.dim());
    row_range_  = TensorindexRange(  T_.vector_of_sizes(), row_modes );
    col_range_  = TensorindexRange(  T_.vector_of_sizes(), col_modes );
    full_range_ = TensorindexRange(  T_.vector_of_sizes(), tensor_mode::set_union(row_modes, col_modes) );
    set_row_size_();
    set_column_size_();
}

double Matricization::operator()(const tensorindex& p) const
{
    return T_.entry(p);
}

std::vector<double> Matricization::operator ()(const std::vector<tensorindex>& p) const
{
    std::vector<double> res(p.size());
    T_.eval(p,res);
    return res;
}

tensorsize Matricization::row_mode_sizes() const
{
    tensorsize ts;
    for (const auto& i : row_range_.modes())
    {
        ts.push_back(T_.size(i));
    }

    return ts;
}

tensorsize Matricization::column_mode_sizes() const
{
    tensorsize ts;
    for (const auto& i : col_range_.modes())
    {
        ts.push_back(T_.size(i));
    }
    return ts;
}

std::size_t Matricization::rows() const
{
    return row_size_;
}

std::size_t Matricization::columns() const
{
    return column_size_;
}

blaze::DynamicMatrix<double> Matricization::get_full_matrix()
{
    if (column_size_overflow_ || row_size_overflow_)
    {
        throw "we had an overflow therefore this call does not make sense";
    }
    blaze::DynamicMatrix<double> M(row_size_, column_size_);
    std::size_t i = 0;
    for (auto itr = row_range_.begin(); itr != row_range_.end(); ++itr)
    {
        col_range_.set(*itr);
        std::size_t j = 0;
        for (auto itc = col_range_.begin(); itc != col_range_.end(); ++itc)
        {
            M(i, j) = T_.entry(*itc);
            ++j;
        }
        ++i;
    }
    return M;
}

blaze::DynamicVector<double, blaze::rowVector> Matricization::get_row(const tensorindex& ti)
{
    if (column_size_overflow_)
    {
        throw "we had an overflow therefore this call does not make sense";
    }
    col_range_.set(ti);
    blaze::DynamicVector<double, blaze::rowVector> V(column_size_);
    std::size_t j = 0;
    for (auto itc = col_range_.begin(); itc != col_range_.end(); ++itc)
    {
        V[j] = T_.entry(*itc);
        ++j;
    }
    return V;
}

blaze::DynamicVector<double, blaze::columnVector> Matricization::get_column(const tensorindex& ti)
{
    if (row_size_overflow_)
    {
        throw "we had an overflow therefore this call does not make sense";
    }
    row_range_.set(ti);
    blaze::DynamicVector<double, blaze::columnVector> V(row_size_);
    std::size_t j = 0;
    for (auto itr = row_range_.begin(); itr != row_range_.end(); ++itr)
    {
        V[j] = T_.entry(*itr);
        ++j;
    }
    return V;
}

// get tensorindex of submatrix spanned by the pivots
void Matricization::get_tensorindex(const size_t i, const size_t j, const std::vector<tensorindex>& pivots, tensorindex& temp) const
{
    assert(i < pivots.size());
    assert(j < pivots.size());
    col_range_.merge(pivots[j], pivots[i], temp);
}

// get tensorindex of submatrix spanned by the pivots
tensorindex Matricization::get_tensorindex(const size_t i, const size_t j, const std::vector<tensorindex>& pivots) const
{
    tensorindex index;
    get_tensorindex(i, j, pivots, index);
    return index;
}

void Matricization::get_random_indices(size_t K, std::vector<tensorindex>& indices, bool generate_non_quadratic_matrices) const
{
    RandomTensorindexGenerator full_rng(full_range_, 1);
    std::size_t k = 0;
    bool hit_row = true;
    bool hit_col = true;
    bool valid = false;

    if (generate_non_quadratic_matrices)
    {
        std::size_t temp = std::max(column_size_, row_size_);
        K = std::min(K, temp);
    }
    else
    {
        K = std::min(K, row_size_);
        K = std::min(K, column_size_);
    }

    std::size_t loop_counter = 0;
    const std::size_t loop_max = 10000;
    tensorindex ti_temp;
    while ( k < K && loop_counter < loop_max)
    {
        ++loop_counter;
        ti_temp = full_rng.get_random_index();
        // check if a certain row or col was already hit
        valid = true;
        for (const auto& ti : indices)
        {
            hit_row = true;
            hit_col = true;
            for (const auto& i : row_range_.modes())
            {
                if (ti_temp[i] != ti[i])
                {
                    hit_row = false;
                    break;
                }
            }
            for (const auto& i : col_range_.modes())
            {
                if (ti_temp[i] != ti[i])
                {
                    hit_col = false;
                    break;
                }
            }

            if (hit_row || hit_col)
            {
                valid = false;
                break;
            }
        }
        if (valid)
        {
            loop_counter = 0;
            ++k;
            indices.push_back(ti_temp);
        }

    }

    if (generate_non_quadratic_matrices)
    {
        if (loop_max == loop_counter)
        {
            loop_counter = 0;
            while ( k < K && loop_counter < loop_max)
            {
                ++loop_counter;


                ti_temp = full_rng.get_random_index();
                // check if a certain row or col was already hit
                valid = true;
                // hit_col = true;
                for (const auto& ti : indices)
                {
                    if (ti_temp == ti)
                    {
                        valid = false;
                        break;
                    }
                }
                for (std::size_t i = 0; i < indices.size(); ++i)
                {
                    for (std::size_t j = 0; j < indices.size(); ++j)
                    {

                        if (ti_temp == get_tensorindex(i, j, indices))
                        {
                            valid = false;
                            break;
                        }
                    }
                }

                if (valid)
                {
                    loop_counter = 0;
                    ++k;
                    indices.push_back(ti_temp);
                }

            }
        }
    }
}

void Matricization::get_random_indices_nested(size_t K, const std::vector<tensorindex>& pre_sampled_indices, tensormode& t_bracket, std::vector<tensorindex>& indices, bool generate_non_quadratic_matrices) const
{
    tensorindex ti_temp(T_.dim());
    bool hit_row = true;
    bool hit_col = true;
    bool valid = true;
    const bool empty_pre_samples = pre_sampled_indices.empty();
    std::uniform_int_distribution<std::size_t> rnd_ps;
    if (!empty_pre_samples)
    {
        rnd_ps = std::uniform_int_distribution<std::size_t>(0, pre_sampled_indices.size() - 1);
    }

    auto t = tensor_mode::absolute_complement(t_bracket, T_.dim());
    auto t_range = TensorindexRange(T_.vector_of_sizes(), t);
    RandomTensorindexGenerator t_bracket_rng(t_range, 1);

    if (generate_non_quadratic_matrices)
    {
        std::size_t temp = std::max(column_size_, row_size_);
        K = std::min(K, temp);
    }
    else
    {
        K = std::min(K, row_size_);
        K = std::min(K, column_size_);
    }

    std::size_t k = 0;
    std::size_t loop_counter = 0;

    const std::size_t loop_max = 10000;

    while ( k < K && loop_counter < loop_max)
    {
        ++loop_counter;
        //here we start an infinity loop
        // assert that the sampling ist nested
        // 1. choose one of the pre sampled indices
        if (!empty_pre_samples)
        {
            std::size_t p = rnd_ps(lowrat::random::mt_);
            ti_temp = pre_sampled_indices[p];
        }
        // 2. sample the remaining indices
        t_bracket_rng.randomize(ti_temp);
        // check if a certain row or col was already hit
        valid = true;
        for (const auto& ti : indices)
        {
            hit_row = true;
            hit_col = true;
            for (const auto& i : row_range_.modes())
            {
                if (ti_temp[i] != ti[i])
                {
                    hit_row = false;
                    break;
                }
            }
            for (const auto& i : col_range_.modes())
            {
                if (ti_temp[i] != ti[i])
                {
                    hit_col = false;
                    break;
                }
            }
            if (hit_row || hit_col)
            {
                valid = false;
                break;
            }
        }
        if (valid)
        {
            loop_counter = 0;
            ++k;
            indices.push_back(ti_temp);
        }
    }

    if (generate_non_quadratic_matrices)
    {
        if (loop_max == loop_counter)
        {
            loop_counter = 0;
            while ( k < K && loop_counter < loop_max)
            {
                ++loop_counter;
                if (!empty_pre_samples)
                {
                    std::size_t p = rnd_ps(lowrat::random::mt_);
                    ti_temp = pre_sampled_indices[p];
                }
                // 2. sample the remaining indices
                t_bracket_rng.randomize(ti_temp);

                // check if a certain row or col was already hit
                valid = true;
                // hit_col = true;
                for (const auto& ti : indices)
                {
                    if (ti_temp == ti)
                    {
                        valid = false;
                        break;
                    }
                }
                for (std::size_t i = 0; i < indices.size(); ++i)
                {
                    for (std::size_t j = 0; j < indices.size(); ++j)
                    {

                        if (ti_temp == get_tensorindex(i, j, indices))
                        {
                            valid = false;
                            break;
                        }
                    }
                }

                if (valid)
                {
                    loop_counter = 0;
                    ++k;
                    indices.push_back(ti_temp);
                }

            }
        }
    }
}

blaze::DynamicMatrix<double> Matricization::get_submatrix_from_columns(std::vector<tensorindex>& pivots)
{
    if (row_size_overflow_)
    {
        throw "we had an overflow therefore this call does not make sense";
    }
    blaze::DynamicMatrix<double> M( row_size_, pivots.size() );
    std::size_t i = 0;
    for (auto && p : pivots)
    {
        row_range_.set(p);
        std::size_t j = 0;
        for (auto itr = row_range_.begin(); itr != row_range_.end(); ++itr)
        {
            M(j, i) = T_.entry(*itr);
            ++j;
        }
        ++i;
    }
    return M;
}

std::vector< blaze::DynamicMatrix<double> > Matricization::get_M_t_list(const std::vector<tensorindex>& P_t1, const std::vector<tensorindex>& P_t2, const std::vector<tensorindex>& Q_t, const tensormode& t1) const
{
    std::vector< blaze::DynamicMatrix<double> > M_t_list;
    tensormode t2 = tensor_mode::complement(row_range_.modes(), t1);
    tensorindex t_idx(T_.dim());
    for (const auto& qt : Q_t)
    {
        t_idx = qt;
        M_t_list.push_back(blaze::DynamicMatrix<double>( P_t1.size(), P_t2.size() ));
        for (std::size_t q = 0; q < P_t2.size(); ++q)
        {
            for (const auto& m2 : t2)
            {
                t_idx[m2] = P_t2[q][m2];
            }
            for (std::size_t p = 0; p < P_t1.size(); ++p)
            {
                for (const auto& m1 : t1)
                {
                    t_idx[m1] = P_t1[p][m1];
                }
                M_t_list.back()(p, q) =  this->operator()(t_idx);
            }
        }
    }
    return M_t_list;
}

void Matricization::set_row_size_()
{
    row_size_ = 1;
    std::size_t old_row_size = 1;
    for (const auto& i : row_mode_sizes())
    {
        old_row_size = row_size_;
        row_size_ = old_row_size * i;
        if (old_row_size != 0 && row_size_ / old_row_size != i)
        {
            row_size_ = std::numeric_limits<std::size_t>::max();
            row_size_overflow_ = true;
            break;
        }
    }
}

void Matricization::set_column_size_()
{
    column_size_ = 1;
    std::size_t old_col_size = 1;
    for (const auto& i : column_mode_sizes())
    {
        old_col_size = column_size_;
        column_size_ = old_col_size * i;
        if (old_col_size != 0 && column_size_ / old_col_size != i)
        {
            column_size_ = std::numeric_limits<std::size_t>::max();
            column_size_overflow_ = true;
            break;
        }
    }
}

} //namespace lowrat

