#include "lowrat/ht_tensor/ht_maximum.h"//for lowrat::HTensor

#include <cstddef>//for std::size_t
#include <algorithm>//for max_element
#include <memory>//for std::unique_ptr
#include <vector>//for std::vector

#if MPI_ACTIVE
#include "mpi.h" //for MPI
#include "lowrat/util/parallel.h" //for blaze MPI, PStream
#endif
#include "lowrat/config/config.h"//for lowrat::value_t
#include "lowrat/ht_tensor/htensor.h" //for HTensor

namespace lowrat
{
static constexpr double numeric_zero_ = 1e-16;
//
// Computes an approximation of the maximum norm of the tensor by a power iteration.
//
double maximum_original(const HTensor& ht, std::size_t maxiter )
{
    HTensor eigenvector = ht;
    return maximum_original(ht, maxiter, eigenvector);
}

//
// Computes an approximation of the maximum norm of the tensor by a power iteration.
//
double maximum_original(const HTensor& ht, std::size_t maxiter, HTensor& eigenvec )
{
    if ( norm(ht) < numeric_zero_ ) //arbitrary bound
    {
        return 0.0;
    }

    const std::size_t maxrank = maximal_rank(ht);

    HTensor iter_tensor = eigenvec;
    double alpha = norm( iter_tensor );
    iter_tensor /= alpha;
    for ( std::size_t i = 0; i < maxiter; ++i ) //Power method
    {
        iter_tensor.hadamard( ht );
        alpha = norm( iter_tensor );
        if (alpha < numeric_zero_ ) // When alpha gets too small, truncation will fail. Better criterion likely Frobenius norm of Root Matrix of iter_tensor?
        {
            return 0.0;
        }
        iter_tensor /= alpha;
        iter_tensor.truncate( maxrank );
    }
    eigenvec = iter_tensor;

    return alpha;
}


//
// Computes an approximation of the maximum norm of the tensor by a power iteration
// with squaring.
//
double maximum(const HTensor& ht, std::size_t maxiter, std::size_t maxrank )
{
    HTensor eigenvector = ht;
    return maximum(ht, maxiter, eigenvector, maxrank);
}

//
// Computes an approximation of the maximum norm of the tensor by a power iteration
// with squaring.
//
double maximum(const HTensor& ht, std::size_t maxiter, HTensor& eigenvec, std::size_t maxrank)
{
    if ( norm(ht) < numeric_zero_ ) //arbitrary bound
    {
        return 0.0;
    }

    if (0 == maxrank)
    {
        maxrank = maximal_rank( ht ); //use this for truncation?
    }

    HTensor iter_tensor = eigenvec;
    double alpha = norm( iter_tensor );
    iter_tensor /= alpha;
    for ( std::size_t i = 0; i < maxiter; ++i ) //Power method
    {
        iter_tensor.hadamard();
        double norm_value = norm( iter_tensor );
        if ( norm_value < numeric_zero_ )
        {
            return 0.0;
        }
        iter_tensor /= norm_value ;
        iter_tensor.truncate(maxrank);
        HTensor y = iter_tensor;
        y.hadamard( ht );
        alpha = norm( y );
    }
    eigenvec = iter_tensor;

    return alpha;
}



// double maximum_qr_ht(const HTensor& ht, std::size_t maxiter, std::size_t max_subspace_size  )
// {
// 	HTensor def_parameter = ht;
// 	return maximum_qr_ht(ht, maxiter, def_parameter, max_subspace_size );
// }

double maximum_qr_ht(const HTensor& ht, std::size_t maxiter, std::size_t max_subspace_size )
{
    if ( norm(ht) < numeric_zero_ ) //arbitrary bound
    {
        return 0.0;
    }

    const std::size_t maxrank = maximal_rank( ht ); //use this for truncation?

    if ( max_subspace_size == 0 )
    {
        max_subspace_size = 1;
    }

    double computed_maximum = 0.0;

    HTensor iter_tensor = ht;
    double alpha = norm( iter_tensor );
    iter_tensor /= alpha;
    std::vector< HTensor > iter_vector;
    for ( std::size_t i = 0; i < maxiter; ++i ) //Power method
    {
        iter_tensor.hadamard( ht );
        alpha = norm( iter_tensor ); //There is no .dot()?
        if (alpha < numeric_zero_)
        {
            return alpha;
        } // When alpha gets too small, truncation will fail. Better criterion likely Frobenius norm of Root Matrix of iter_tensor?
        iter_tensor /= alpha;
        iter_tensor.truncate( maxrank );

        std::size_t subspace_size = iter_vector.size();

        if ( subspace_size >= max_subspace_size )
        {
            // Shift iter_vector (iter_vector[ 0 ] is thrown away):
            for ( std::size_t j = 0; j < subspace_size - 1; ++j )
            {
                iter_vector[ j ] = iter_vector[ j + 1 ];
            }
            // Set last element of iter_vector to current iter_tensor:
            iter_vector[ subspace_size - 1 ] = iter_tensor;
        }
        else
        {
            iter_vector.push_back( iter_tensor );
            ++subspace_size;
        }


        computed_maximum = alpha;

        if ( subspace_size > 1 )
        {
            // Orthogonalize iter_vector:
            std::vector< HTensor * > orth_iter_vector( subspace_size );
            for ( size_t j = 0; j < subspace_size; ++j )
            {
                orth_iter_vector[ j ] = &iter_vector[ j ];
            }
            blaze::DynamicMatrix< value_t, blaze::columnMajor > R;
            QR( orth_iter_vector, R );
            std::size_t orth_subspace_size = R.rows();
            if ( orth_subspace_size < subspace_size )
            {
                orth_iter_vector.resize( orth_subspace_size );
            }
            for ( std::size_t j = 0; j < orth_subspace_size; ++j )
            {
                orth_iter_vector[ j ]->truncate( maxrank );
            }

            // Assemble matrix 'orthog_system' of projected eigenvalue problem:
            blaze::SymmetricMatrix< blaze::DynamicMatrix< value_t > > orthog_system( orth_subspace_size );
            for ( size_t j = 0; j < orth_subspace_size; ++j )
            {
                HTensor product_tensor = ht;
                product_tensor.hadamard( *orth_iter_vector[ j ] );
                for ( std::size_t l = 0; l <= j; ++l )
                {
                    orthog_system( j, l ) = orth_iter_vector[ l ]->dot( product_tensor );
                }
            }
            // Compute eigenvalues (and eigenvectors):
            blaze::DynamicVector< value_t > eigenvals;
            //DynamicMatrix< value_t > eigenvecs;
            blaze::eigen( orthog_system, eigenvals );
            //eigen( orthog_system, eigenvals, eigenvecs );
            double max_eigenval = std::fabs( eigenvals[ 0 ] );
            //size_t max_eigenval_index = 0;
            for ( std::size_t j = 0; j < subspace_size; ++j )
            {
                if ( std::fabs( eigenvals[ j ] ) > max_eigenval )
                {
                    max_eigenval = std::fabs( eigenvals[ j ] );
                    //max_eigenval_index = j;
                }
            }

            computed_maximum = max_eigenval;

        }// if ( subspace_size > 1 )
    }// for ( size_t i = 0; i < maxiter; ++i )

    return computed_maximum;
}


//
// Calculates an approximation of the maximum (largest absolute value) using maxiter steps of the power method,
// then projects the eigenvectorapproximation on the subspace of the last few iterations to
// achieve a better result. Reccommended over maximum( maxiter ) for "large" maxiter.
//
double projection_maximum(const HTensor& ht, size_t maxiter )
{
    HTensor eigenvector = ht;
    return projection_maximum(ht, maxiter, eigenvector, false);
}

//
// Same as projection_maximum( maxiter ), but starts with eigenvec as the eigenvectorapproximation
// and improves it.
//
double projection_maximum(const HTensor& ht, size_t maxiter, HTensor& eigenvec, bool calculate_eigenvector) //Calculate maxiter during program?
{
    if ( norm(ht) < numeric_zero_ ) //arbitrary bound
    {
        return 0.0;
    }

    constexpr std::size_t subspace_size_max = 5;
    const std::size_t subspace_size = std::min(maxiter, subspace_size_max);
    std::size_t maxrank = maximal_rank( ht );

    //Power method to approximate eigenvector
    std::vector< HTensor > iter_vector(subspace_size);
    for ( std::size_t i = 0; i < maxiter; ++i )
    {
        eigenvec.hadamard( ht );
        eigenvec.truncate( maxrank );
        eigenvec.orthogonalize();
        eigenvec /= norm( eigenvec );
        if (i >= maxiter - subspace_size)
        {
            iter_vector[ i - maxiter + subspace_size ] = eigenvec;
        }
    }
    constexpr double max_rel_error = 1e-5; //arbitrary
    //Gram–Schmidt on the last iterations to orthogonalize
    for ( std::size_t i = 1; i < subspace_size; ++i )
    {
        for ( std::size_t j = 0; j < i; ++j )
        {
            iter_vector[i] -= ( iter_vector[i].dot(iter_vector[j]) ) * iter_vector[j] ;
            iter_vector[i].truncate( max_rel_error );
        }
        iter_vector[i].orthogonalize();
        iter_vector[i] /= norm( iter_vector[i] );
    }

    //Calculate maximum Eigenvalue using Ritz-Rayleigh
    blaze::DynamicMatrix<double> orthog_system_matrix(subspace_size, subspace_size, 0.0);
    for ( std::size_t i = 0; i < subspace_size; ++i)
    {
        HTensor product_tensor = ht;
        product_tensor.hadamard(iter_vector[i]);
        for ( std::size_t j = 0; j < subspace_size; ++j)
        {
            orthog_system_matrix(i, j) = iter_vector[j].dot(product_tensor);
        }
    }
    blaze::SymmetricMatrix< blaze::DynamicMatrix<double> > orthog_system = blaze::declsym(orthog_system_matrix);
    blaze::DynamicMatrix<double> eigenvecs(subspace_size, subspace_size, 0.0);
    blaze::DynamicVector<double> eigenvals(subspace_size);
    blaze::eigen(orthog_system, eigenvals, eigenvecs);
    double maxeigenval = std::abs(eigenvals[0]);
    std::size_t maxeigenvalind = 0;
    for ( std::size_t i = 1; i < subspace_size; ++i )
    {
        if ( std::abs(eigenvals[i]) > maxeigenval )
        {
            maxeigenval = fabs(eigenvals[i]);
            maxeigenvalind = i;
        }
    }

    if ( calculate_eigenvector )
    {
        // Determine the Eigenvector in iter_vector[0]
        iter_vector[0] *= eigenvecs( maxeigenvalind, 0 );
        for ( size_t i = 1; i < subspace_size; ++i )
        {
            iter_vector[0] += eigenvecs(maxeigenvalind, i) * iter_vector[i];
            iter_vector[0].truncate( max_rel_error );
        }
        eigenvec = iter_vector[0]; //Swap objects of eigenvec and iter_vector [0]
    }

    return maxeigenval;
}

double adaptive_maximum(const HTensor& ht, std::size_t power_steps, std::size_t quad_steps, std::size_t maxiter )
{
    HTensor def_parameter = ht;
    return adaptive_maximum(ht, def_parameter, power_steps, quad_steps, maxiter);
}

double adaptive_maximum(const HTensor& ht, HTensor& eigenvec, std::size_t power_steps, std::size_t quad_steps, std::size_t maxiter) //Calculate maxiter during program?
{
    const std::size_t subspace_size = std::min(power_steps, 5UL);

    const std::size_t maxrank = maximal_rank( ht );
    //Power method to approximate eigenvector
    std::vector< HTensor > iter_vector(subspace_size);

    double alpha = 0.0;
    double alpha_pi = 0.0;
    double alpha_old = 0.0;
    for (std::size_t iter = 0; iter < maxiter; ++iter)
    {
        for ( std::size_t i = 0; i < power_steps; ++i )
        {
            eigenvec.hadamard( ht );
            eigenvec.truncate( maxrank );
            eigenvec.orthogonalize();
            alpha_pi = norm(eigenvec);
            eigenvec /= alpha_pi; //There is no .dot()?

            if (i >= power_steps - subspace_size)
            {
                iter_vector[ i - power_steps + subspace_size ] = eigenvec;
            }
        }
        constexpr double max_rel_error = 1e-5; //arbitrary
        blaze::DynamicMatrix< value_t, blaze::columnMajor > R;

        std::vector< HTensor * > iter_vector_ptr(iter_vector.size());
        for (std::size_t i = 0; i < iter_vector_ptr.size(); ++i)
        {
            iter_vector_ptr[i] = &iter_vector[i];
        }
        QR( iter_vector_ptr, R );
        for ( std::size_t i = 0; i < subspace_size; ++i)
        {
            iter_vector[i].truncate(max_rel_error);
        }
        //Calculate maximum Eigenvalue using Ritz-Rayleigh
        blaze::DynamicMatrix<value_t> orthog_system_matrix(subspace_size, subspace_size, 0.0);
        for ( size_t i = 0; i < subspace_size; ++i)
        {
            HTensor product_tensor = ht;
            iter_vector[i].truncate(max_rel_error);
            product_tensor.hadamard(iter_vector[i]);
            for ( std::size_t j = 0; j <= i; ++j)
            {
                orthog_system_matrix(i, j) = iter_vector[j].dot(product_tensor);
            }
        }
        blaze::SymmetricMatrix< blaze::DynamicMatrix<value_t> > orthog_system = blaze::declsym(orthog_system_matrix);
        blaze::DynamicMatrix<value_t> eigenvecs(subspace_size, subspace_size, 0.0);
        blaze::DynamicVector<value_t> eigenvals(subspace_size);
        blaze::eigen(orthog_system, eigenvals, eigenvecs);
        double maxeigenval = std::fabs(eigenvals[0]);
        std::size_t maxeigenvalind = 0;
        for ( std::size_t i = 1; i < subspace_size; ++i )
        {
            if ( std::fabs(eigenvals[i]) > maxeigenval )
            {
                maxeigenval = std::fabs(eigenvals[i]);
                maxeigenvalind = i;
            }
        }
        iter_vector[0] *= eigenvecs( maxeigenvalind, 0 ) ;
        for ( std::size_t i = 1; i < subspace_size; ++i )
        {
            iter_vector[0] += ( eigenvecs(maxeigenvalind, i) ) * iter_vector[i];
            iter_vector[0].truncate( max_rel_error );
        }
        iter_vector[0].truncate(maxrank);
        eigenvec = iter_vector[0]; //Swap objects of eigenvec and iter_vector [0]

        bool aborted = false;
        HTensor iter_tensor = eigenvec;
        alpha = norm( iter_tensor );
        iter_tensor /= alpha;
        for ( std::size_t i = 0; i < quad_steps; ++i )
        {
            iter_tensor.hadamard();
            double norm_value = norm( iter_tensor );
            if ( norm_value < numeric_zero_ )
            {
                return 0.0;
            }
            iter_tensor /= norm_value;
            double trerr = iter_tensor.truncate(maxrank);
            if (trerr > 5e-2)
            {
                aborted = true;
                alpha = alpha_pi;
                break;
            }
            HTensor y = iter_tensor;
            y.hadamard( ht );
            alpha_old = alpha;
            alpha = norm( y );
            if ( std::fabs(alpha - alpha_old) / alpha < numeric_zero_ )
            {
                break;
            }
        }
        if (!aborted)
        {
            eigenvec = iter_tensor;
            break;
        }
    }
    return alpha;
}


//
// Auxiliary for argmax().
//
static double nonconstmaximum(HTensor& ht, std::size_t maxiter )
{
    if ( norm(ht) < numeric_zero_ ) //arbitrary bound
    {
        return 0.0;
    }

    std::vector< std::size_t > ranks = ht.all_ranks();
    std::size_t maxrank = *( std::max_element( ranks.begin(), ranks.end() ) ); //use this for truncation?

    HTensor iter_tensor = ht;
    HTensor last_iteration = ht;
    for ( std::size_t i = 0; i < maxiter; ++i ) //Power method
    {
        ht.hadamard( iter_tensor );
        if ( i == maxiter - 1 )
        {
            last_iteration = ht;
        }
        ht /= norm( ht );
        ht.truncate( maxrank );
    }

    return std::abs( ht.dot(last_iteration) );
}



//
// Auxiliary for argmax().
//
static std::vector< std::vector<std::size_t> > non_zero_rows( const HTensor& ht, std::vector< std::vector<std::size_t> >& nonzeroindices )
{
    std::vector< std::vector<std::size_t> > current_nonzeroindices( dimension(ht) );
    nonzeroindices.resize( dimension(ht) );

    int my_rank = 0;
#if MPI_ACTIVE
    MPI_Comm_rank( ht.mpi_communicator(), &my_rank );
#endif

    const Cluster ht_tree = ht.clustertree();
    std::vector< const Cluster * > local_nodes = ht_tree.all_nodes_of_rank( my_rank );

    std::vector< blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > > > ht_data = ht.data();

    for ( std::size_t i = 0; i < local_nodes.size(); ++i )
    {
        if ( local_nodes[ i ]->is_leaf() )
        {
            std::size_t columns  = ht_data[ i ][ 0 ].columns();
            std::size_t rows     = ht_data[ i ][ 0 ].rows();
            std::vector< std::size_t > newindices;
            std::vector< std::size_t > current_newindices;
            for ( std::size_t k = 0; k < rows; ++k )
            {
                bool nonzero = false;
                for ( std::size_t l = 0; l < columns; ++l )
                {
                    if ( std::abs( ht_data[ i ][ 0 ]( k, l ) ) > numeric_zero_ )
                    {
                        nonzero = true;
                        break;
                    }
                }

                if ( nonzero )
                {
                    if ( nonzeroindices[ local_nodes[ i ]->leaf_number() ].size() != 0 )
                    {
                        newindices.push_back( nonzeroindices[ local_nodes[ i ]->leaf_number() ][ k ] );
                    }
                    else
                    {
                        newindices.push_back( k );
                    }
                    current_newindices.push_back( k );
                }
            }
            nonzeroindices[ local_nodes[ i ]->leaf_number() ] = newindices;
            current_nonzeroindices[ local_nodes[ i ]->leaf_number() ] = current_newindices;
        }
    }

    // Broadcast current_nonzeroindices, nonzeroindices to all MPI processes:
#if MPI_ACTIVE
    std::vector< const Cluster * > nodes = ( ht_tree ).all_nodes();
    for ( std::size_t i = 0; i < nodes.size(); ++i )
    {
        if ( nodes[ i ]->is_leaf() )
        {
            int root    = nodes[ i ]->mpi_rank();
            std::size_t mu   = nodes[ i ]->leaf_number();
            MPI_Bcast_StdVector( current_nonzeroindices[ mu ], _MPI_SIZE_T, root, ht.mpi_communicator() );
            MPI_Bcast_StdVector( nonzeroindices[ mu ], _MPI_SIZE_T, root, ht.mpi_communicator() );
        }
    }
#endif
    return current_nonzeroindices;
}

static void subtensor(HTensor& ht,  const std::vector< std::vector< std::size_t > >& indices )
{
    int my_rank = 0;
#if MPI_ACTIVE
    MPI_Comm_rank(  ht.mpi_communicator(), &my_rank );
#endif
    const Cluster ht_tree = ht.clustertree();
    std::vector< const Cluster * > local_nodes = ( ht_tree ).all_nodes_of_rank( my_rank );

    for ( std::size_t i = 0; i < local_nodes.size(); ++i )
    {
        if ( local_nodes[ i ]->is_leaf() )
        {
            blaze::DynamicMatrix< double, blaze::columnMajor > old_data = ht.get_data(i, 0UL);
            std::size_t mu       = local_nodes[ i ]->leaf_number();
            std::size_t columns  = old_data.columns();
            std::size_t rows     = old_data.rows();
            if ( indices[ mu ].size() != 0 && indices[ mu ].size() != rows ) // Use all entries in case of an empty vector
            {
                blaze::DynamicMatrix< double, blaze::columnMajor > newdata( indices[ mu ].size(), columns );
                // Copy all relevant entries into the new matirx:
                for ( std::size_t k = 0; k < indices[ mu ].size(); ++k )
                {
                    for ( std::size_t l = 0; l < columns; ++l )
                    {
                        newdata( k, l ) = old_data( indices[ mu ][ k ], l );
                    }
                }
                ht.set_data( newdata, i, 0UL);
            }
        }
    }
}

static void selectLeafMatrixHalf(HTensor& ht, bool upper, std::size_t index)
{
    blaze::DynamicMatrix< double, blaze::columnMajor > old_data = ht.get_data(index, 0UL);
    std::size_t rows = old_data.rows();
    std::size_t columns = old_data.columns();
    if ( upper )
    {
        blaze::DynamicMatrix< double, blaze::columnMajor > newdata = blaze::submatrix( old_data, 0UL, 0UL, std::floor(rows / 2), columns );
        ht.set_data( newdata, index, 0UL);
    }
    else
    {
        blaze::DynamicMatrix< double, blaze::columnMajor > newdata = blaze::submatrix( old_data, std::floor(rows / 2), 0, rows - std::floor(rows / 2), columns );
        ht.set_data( newdata, index, 0UL);
    }
}


std::vector< std::size_t > argmax_original(const HTensor& ht, std::size_t maxiter )
{
    std::vector< std::size_t > index( dimension(ht), 0UL );

    HTensor lower_eigenvec = ht; //"lower" for naming later
    projection_maximum(ht, maxiter, lower_eigenvec );

    double max_ges = nonconstmaximum( lower_eigenvec, std::min( 50UL, maxiter ) ); //The Eigenvector has the same argmax as the tensor, so we will now calculate the eigenvectors

    const std::size_t maxrank = maximal_rank( lower_eigenvec );
    const Cluster ht_tree = ht.clustertree();
    std::vector< const Cluster * > nodes = ht_tree.all_nodes();
    int my_rank = 0;
#if MPI_ACTIVE
    MPI_Comm_rank( ht.mpi_communicator(), &my_rank );
#endif
    std::vector< const Cluster * > local_nodes = ht_tree.all_nodes_of_rank( my_rank );

    if (maxrank > 1) //argmax can easily be determined for rank 1 tensors
    {
        std::vector< std::vector< std::size_t > > nonzeroindices;
        std::vector< std::vector< std::size_t > > current_nonzeroindices;

        current_nonzeroindices = non_zero_rows(lower_eigenvec, nonzeroindices);

        subtensor(lower_eigenvec, current_nonzeroindices ); //Remove Entries which are 0 for sure and thus not relevant.

        for ( std::size_t i = 0; i < nodes.size(); ++i )
        {
            if ( nodes[ i ]->is_leaf() )
            {
                if ( my_rank == nodes[ i ]->mpi_rank() )
                {
                    std::size_t mu = nodes[ i ]->leaf_number();
                    HTensor upper_eigenvec = lower_eigenvec;

                    std::size_t j = 0; // Corresponding index for local _data
                    // Find corresponding index j for local _data
                    for ( std::size_t l = 0; l < local_nodes.size(); ++l )
                    {
                        if ( local_nodes[ l ] == nodes[ i ] )
                        {
                            j = l;
                            break;
                        }
                    }

                    selectLeafMatrixHalf(upper_eigenvec, true, j );
                    selectLeafMatrixHalf(lower_eigenvec, false, j );

                    while ( nonzeroindices[ mu ].size() > 1 ) // Iterate over the matrix until the final index is found
                    {
                        HTensor upper_eigenvec_eigenvec = upper_eigenvec;
                        HTensor lower_eigenvec_eigenvec = lower_eigenvec;
                        double max1 = maximum_original(upper_eigenvec, 10, upper_eigenvec_eigenvec);
                        double max2 = maximum_original(lower_eigenvec, 10, lower_eigenvec_eigenvec);
                        double err1 = std::abs( (max_ges - max1) / max_ges ); //relative errors of both halves
                        double err2 = std::abs( (max_ges - max2) / max_ges );
                        //TODO fix this heuristic
                        if ( std::min(err1, err2) < 1e-1 ) //error margin arbitrary, works well with a large value though
                        {
                            if (err1 < err2)
                            {
                                upper_eigenvec = upper_eigenvec_eigenvec;
                                lower_eigenvec = upper_eigenvec_eigenvec;
                                // index j from above corresponding to local _data
                                if ( my_rank == nodes[ i ]->mpi_rank() )
                                {
                                    selectLeafMatrixHalf(upper_eigenvec, true, j );
                                    selectLeafMatrixHalf(lower_eigenvec, false, j );
                                }

                                nonzeroindices[ mu ] = std::vector<std::size_t>( nonzeroindices[ mu ].begin(), nonzeroindices[ mu ].begin() + std::floor(nonzeroindices[ mu ].size() / 2) );
                                max_ges = max1;
                            }
                            else
                            {
                                upper_eigenvec = lower_eigenvec_eigenvec;
                                lower_eigenvec = lower_eigenvec_eigenvec;
                                // index j from above corresponding to local _data
                                if ( my_rank == nodes[ i ]->mpi_rank() )
                                {
                                    selectLeafMatrixHalf(upper_eigenvec, true, j );
                                    selectLeafMatrixHalf(lower_eigenvec, false, j );
                                }

                                nonzeroindices[ mu ] = std::vector<std::size_t>( nonzeroindices[ mu ].begin() + std::floor(nonzeroindices[ mu ].size() / 2), nonzeroindices[ mu ].end() );
                                max_ges = max2;
                            }
                        }
                        else //No side is good, so not enough iterations on the whole tensor have been executed, maxiter should have been higher
                        {

                            max_ges = std::max( max1, max2 ); //max_ges is (in theory) monotonically increasing
                        }
                    }
                    index[ mu ] = nonzeroindices[ mu ][ 0 ];
                }
            }
        }
    }
    else //Determine Maximum of each Leaf-Matrix, this still needs testing
    {
        const auto eigenvec_data = lower_eigenvec.data();
        for ( std::size_t i = 0; i < eigenvec_data.size(); ++i )
        {
            if ( local_nodes[ i ]->is_leaf() )
            {
                double max = 0.0;
                for ( std::size_t k = 0; k < eigenvec_data[ i ][ 0 ].rows(); ++k )
                {
                    if ( std::abs( eigenvec_data[ i ][ 0 ]( k, 0 ) ) > max )
                    {
                        max = std::abs( eigenvec_data[ i ][ 0 ]( k, 0 ) );
                        index[ local_nodes[ i ]->leaf_number() ] = k;
                    }
                }
            }
        }
    }
#if MPI_ACTIVE
    // Broadcast index to all MPI processes:
    for ( size_t i = 0; i < index.size(); ++i )
    {
        int index_i = int(index[ i ]);
        int new_index_i;
        MPI_Allreduce( &index_i, &new_index_i, 1, MPI_INT, MPI_SUM, ht.mpi_communicator() );
        index[ i ] = size_t( new_index_i );
    }
#endif
    return index;
}


//was argmax_squared
std::vector< std::size_t > argmax( const HTensor& ht ) //Calculate maxiter during program?
{
    std::vector< std::size_t > index( dimension(ht), 0UL );


    HTensor lower_eigenvec = ht; //"lower" for naming later

    // double max_ges = adaptive_maximum(ht, lower_eigenvec);
    adaptive_maximum(ht, lower_eigenvec);

    HTensor lower_tensor = ht;
    const std::size_t maxrank = maximal_rank( lower_eigenvec );

    const Cluster ht_tree = ht.clustertree();
    std::vector< const Cluster * > nodes = ht_tree.all_nodes();

    int my_rank = 0;
#if MPI_ACTIVE
    MPI_Comm_rank( ht.mpi_communicator(), &my_rank );
#endif
    std::vector< const Cluster * > local_nodes = ht_tree.all_nodes_of_rank( my_rank );

    if (maxrank > 1) //argmax can easily be determined for rank 1 tensors
    {
        std::vector< std::vector< std::size_t > > nonzeroindices;
        std::vector< std::vector< std::size_t > > current_nonzeroindices;
        current_nonzeroindices = non_zero_rows(lower_eigenvec, nonzeroindices );
        subtensor(lower_eigenvec, current_nonzeroindices ); //Remove Entries which are 0 for sure and thus not relevant.
        subtensor(lower_tensor, current_nonzeroindices ); //Remove Entries which are 0 for sure and thus not relevant.
        for ( std::size_t i = 0; i < nodes.size(); ++i )
        {
            if ( nodes[ i ]->is_leaf() )
            {
                std::size_t mu = nodes[ i ]->leaf_number();
                HTensor upper_eigenvec = lower_eigenvec;
                HTensor upper_tensor = lower_tensor;
                std::size_t j = 0; // Corresponding index for local _data
                if ( my_rank == nodes[ i ]->mpi_rank() )
                {
                    // Find corresponding index j for local _data
                    for ( size_t l = 0; l < local_nodes.size(); ++l )
                    {
                        if ( local_nodes[ l ] == nodes[ i ] )
                        {
                            j = l;
                            break;
                        }
                    }
                    selectLeafMatrixHalf(upper_eigenvec, true, j );
                    selectLeafMatrixHalf(lower_eigenvec, false, j );
                    selectLeafMatrixHalf(upper_tensor, true, j );
                    selectLeafMatrixHalf(lower_tensor, false, j );
                }

                double max1 = 0.0;
                double max2 = 0.0;
                while ( nonzeroindices[ mu ].size() > 1 ) // Iterate over the matrix until the final index is found
                {
                    HTensor t1 = upper_eigenvec;
                    double t1dt1 = t1.dot(t1);
                    if (t1dt1 > numeric_zero_)
                    {
                        t1 /= sqrt(t1dt1);
                        t1.hadamard(upper_tensor);
                        max1 = norm(t1);
                    }
                    else
                    {
                        max1 = 0.0;
                    }
                    t1 = lower_eigenvec;
                    t1dt1 = t1.dot(t1);
                    if (t1dt1 > numeric_zero_)
                    {
                        t1 /= sqrt(t1dt1);
                        t1.hadamard(lower_tensor);
                        max2 = norm(t1);
                    }
                    else
                    {
                        max2 = 0.0;
                    }
                    if ( max1 > max2 )
                    {
                        upper_eigenvec.truncate(maxrank);
                        lower_eigenvec = upper_eigenvec;
                        lower_tensor = upper_tensor;
                        // index j from above corresponding to local _data
                        if ( my_rank == nodes[ i ]->mpi_rank() )
                        {
                            selectLeafMatrixHalf(upper_eigenvec, true, j);
                            selectLeafMatrixHalf(lower_eigenvec, false, j);
                            selectLeafMatrixHalf(upper_tensor, true, j);
                            selectLeafMatrixHalf(lower_tensor, false, j);
                        }

                        nonzeroindices[mu] = std::vector<std::size_t>( nonzeroindices[mu].begin(), nonzeroindices[mu].begin() + std::floor(nonzeroindices[mu].size() / 2 ));
                    }
                    else
                    {
                        lower_eigenvec.truncate(maxrank);
                        upper_eigenvec = lower_eigenvec;
                        upper_tensor = lower_tensor;
                        // index j from above corresponding to local _data
                        if ( my_rank == nodes[ i ]->mpi_rank() )
                        {
                            selectLeafMatrixHalf(upper_eigenvec, true, j);
                            selectLeafMatrixHalf(lower_eigenvec, false, j);
                            selectLeafMatrixHalf(upper_tensor, true, j);
                            selectLeafMatrixHalf(lower_tensor, false, j);
                        }

                        nonzeroindices[mu] = std::vector<std::size_t>( nonzeroindices[mu].begin() + std::floor(nonzeroindices[mu].size() / 2), nonzeroindices[mu].end() );
                    }
                }
                index[ mu ] = nonzeroindices[ mu ][ 0 ];
            }
        }
    }
    else //Determine Maximum of each Leaf-Matrix, this still needs testing
    {
        const auto eigenvec_data = lower_eigenvec.data();
        for ( std::size_t i = 0; i < eigenvec_data.size(); ++i )
        {
            if ( local_nodes[ i ]->is_leaf() )
            {
                double max = 0.0;
                for ( std::size_t k = 0; k < eigenvec_data[ i ][ 0 ].rows(); ++k )
                {
                    if ( std::abs( eigenvec_data[ i ][ 0 ]( k, 0 ) ) > max )
                    {
                        max = std::abs( eigenvec_data[ i ][ 0 ]( k, 0 ) );
                        index[ local_nodes[ i ]->leaf_number() ] = k;
                    }
                }
            }
        }
#if MPI_ACTIVE
        // Broadcast index to all MPI processes:
        for ( size_t i = 0; i < index.size(); ++i )
        {
            int index_i = int(index[ i ]);
            int new_index_i;
            MPI_Allreduce( &index_i, &new_index_i, 1, MPI_INT, MPI_SUM, ht.mpi_communicator() );
            index[ i ] = size_t( new_index_i );
        }
#endif
    }
    return index;
}

} //namespace lowrat