#include "lowrat/ht_tensor/htensor.h"

#include <cmath>//for floor, fabs, sqrt
#include <cstddef>//for std::size_t
#include <cstdlib>// for rand
#include <algorithm>//for for_each
#include <iostream> //for std::cout
#include <limits>// for std::numeric_limits< double >::min()
#include <map> //for map
#include <memory>//for unique_ptr
#include <stdexcept> //invalid_argument
#include <tuple>//for tuple
#include <vector>//for vector
#include <utility>//for std::move

#include <blaze/Blaze.h> //DynamicVector, DynamicMatrix
#include "lowrat/config/config.h"//for lowrat::value_t
#include "lowrat/ht_tensor/permute.h" //for permute reorder
#include "lowrat/ht_tensor/reshape.h" //for reshape
#include "lowrat/cluster/cluster.h" //for cluster
#include "lowrat/util/io.h" //for vector cout
#include "lowrat/util/iterable.h" //for remove_small_non_zeros
#include "lowrat/util/matrix.h" //for kronecker, reduced_qr
#include "lowrat/util/random.h" //for lowrat::random::mt_
#include "lowrat/util/parallel_shared_memory.h"

namespace lowrat
{

constexpr bool htensor_use_shared_memory_ = false;

using namespace blaze;
using namespace std;


//
// Constructor.
//
HTensor::HTensor( std::size_t d, std::vector< std::size_t > n, std::size_t k,
                  const Cluster & tree )
{

    int my_rank     = 0;
    int num_ranks   = 1;

    node_used_ = false;


    //maybe we should throw, because this is undefined
    if ( d < 2 )
    {
        d = 2;
    }

    if ( tree.size() != d )
    {
        tree_ = Cluster( 0, d - 1 );
        tree_.build_balanced_tree( num_ranks );
    }
    else
    {
        tree_ = tree;
    }

    data_.resize(0);

    const std::vector< const Cluster * > tree_nodes = tree_.all_nodes();

    for ( std::size_t i = 0; i < tree_nodes.size(); ++i )
    {
        if ( my_rank == tree_nodes[ i ]->mpi_rank() )
        {
            node_used_ = true;

            blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > > new_object;

            if ( tree_nodes[ i ]->is_root() )
            {
                new_object.resize( 1 );
                new_object[ 0 ].resize( k, k );
                data_.push_back( new_object );
            }
            else if ( tree_nodes[ i ]->is_leaf() )
            {
                new_object.resize( 1 );
                new_object[ 0 ].resize( n[ tree_nodes[ i ]->leaf_number() ], k );
                data_.push_back( new_object );
            }
            else // inner node
            {
                new_object.resize( k );
                for ( size_t j = 0; j < k; ++j )
                {
                    new_object[ j ].resize( k, k );
                }
                data_.push_back( new_object );
            }
        }
    }
}

//! Constructor (all tensor directions of equal size).
HTensor::HTensor( std::size_t d, std::size_t n, std::size_t k,
                  const Cluster & tree )
    :   HTensor( d, std::vector< std::size_t >( d, n ), k,
                 tree ) {}

//
// Copy structure fill with value
//

HTensor::HTensor( const HTensor & ht, value_t value)
{
    int my_rank     = 0;

    node_used_ = false;


    // get the sizes of each dimension
    std::size_t d = ht.tree_.root()->size();
    std::vector<std::size_t> n(d);
    std::vector< const Cluster * > nodes = ht.tree_.all_nodes( );
    for (size_t i = 0; i < nodes.size(); i++)
    {
        if (nodes[i]->is_leaf())
        {
            n[nodes[i]->operator()(0)] = ht.data_[i][0].rows();
        }
    }
    //std::cout << n << "\n";

    std::vector< const Cluster * > tree_nodes = ht.tree_.all_nodes();

    for ( std::size_t i = 0; i < tree_nodes.size(); ++i )
    {
        if ( my_rank == tree_nodes[ i ]->mpi_rank() )
        {
            node_used_ = true;

            blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > > new_object;

            if ( tree_nodes[ i ]->is_root() )
            {
                new_object.resize( 1 );
                new_object[ 0 ].resize( 1, 1 );
                new_object[ 0 ](0, 0) = value;
                data_.push_back( new_object );
            }
            else if ( tree_nodes[ i ]->is_leaf() )
            {
                new_object.resize( 1 );
                new_object[ 0 ].resize( n[ tree_nodes[ i ]->leaf_number() ], 1 );
                new_object[ 0 ] = 1.0;
                data_.push_back( new_object );
            }
            else // inner node
            {
                new_object.resize( 1 );
                for ( size_t j = 0; j < 1; ++j )
                {
                    new_object[ j ].resize( 1, 1 );
                    new_object[ j ](0, 0) = 1.0;
                }
                data_.push_back( new_object );
            }
        }
    }


    tree_       = ht.tree_;
    node_used_  = ht.node_used_;
}


//
//  sizes
//
std::vector<std::size_t> HTensor::sizes() const
{
    std::size_t d = tree_.root()->size();
    std::vector<std::size_t> n(d);
    std::vector< const Cluster * > nodes = tree_.all_nodes( );
    for (std::size_t i = 0; i < nodes.size(); i++)
    {
        if (nodes[i]->is_leaf())
        {
            n[nodes[i]->operator()(0)] = data_[i][0].rows();
        }
    }
    return n;
}


const std::vector< blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > > >& HTensor::data() const
{
    return data_;
}



//
// Returns a vector of the tensor ranks.
//
std::vector< std::size_t > HTensor::ranks() const
{
    vector< size_t > ranks( data_.size() );

    if ( node_used_ )
    {
        int my_rank = 0;

        vector< const Cluster * > nodes_on_this_rank = tree_.all_nodes_of_rank( my_rank );

        for ( size_t i = 0; i < data_.size(); ++i )
        {
            if ( nodes_on_this_rank[ i ]->is_leaf() )
            {
                ranks[ i ] = data_[ i ][ 0 ].columns();
            }
            else
            {
                ranks[ i ] = data_[ i ].size();
            }
        }
    }// if ( node_used_ )

    return ranks;
}

//
// Returns a vector of all tensor ranks, according to tree_.all_nodes().
//
//vector< size_t > HTensor::all_ranks() const
//{
//    int my_rank = 0;
//    int num_ranks = 1;
//
//    vector< size_t > local_ranks = ranks();
//    vector< const Cluster * > all_clusters = tree_.all_nodes();
//    std::map< const Cluster *, size_t > clusters_to_position;
//    for ( size_t i = 0; i < all_clusters.size(); ++i )
//    {
//        clusters_to_position.insert( pair< const Cluster *, size_t >( all_clusters[ i ], i ) );
//    }
//
//   return all_ranks;
//}

//
// Returns the number of all tensor entries (of the corresponding full tensor).
//
size_t HTensor::size() const
{
    int n = 1;

    int my_rank = 0;

    vector< const Cluster * > nodes_on_this_rank = tree_.all_nodes_of_rank( my_rank );
    for ( size_t i = 0; i < nodes_on_this_rank.size(); ++i )
    {
        if ( nodes_on_this_rank[ i ]->is_leaf() )
        {
            n *= int( data_[ i ][ 0 ].rows() );
        }
    }

    int result = n;
    return size_t( result );
}

//
// Returns the size of the mu-th tensor direction.
//
size_t HTensor::size( size_t mu ) const
{
    int n = 1;

    int my_rank = 0;

    vector< const Cluster * > nodes_on_this_rank = tree_.all_nodes_of_rank( my_rank );
    for ( size_t i = 0; i < nodes_on_this_rank.size(); ++i )
    {
        if ( nodes_on_this_rank[ i ]->is_leaf() && nodes_on_this_rank[ i ]->leaf_number() == mu )
        {
            n *= int( data_[ i ][ 0 ].rows() );
        }
    }

    int result = n;
    return size_t( result );
}

//
// Returns the size of the mu-th tensor direction.
//
size_t HTensor::rank_of_leaf( size_t mu ) const
{
    int n = 1;

    int my_rank = 0;

    vector< const Cluster * > nodes_on_this_rank = tree_.all_nodes_of_rank( my_rank );
    for ( size_t i = 0; i < nodes_on_this_rank.size(); ++i )
    {
        if ( nodes_on_this_rank[ i ]->is_leaf() && nodes_on_this_rank[ i ]->leaf_number() == mu )
        {
            n *= int( data_[ i ][ 0 ].columns() );
        }
    }

    int result = n;
    return size_t( result );
}

//! Returns node number of leaf
std::size_t HTensor::node_number(const std::size_t leaf_number) const
{
    std::size_t i_th_node = 0;

    std::vector< const Cluster * > nodes = tree_.all_nodes( );
    for (std::size_t i = 0; i < nodes.size(); ++i)
    {
        if (nodes[i]->is_leaf())
        {
            if (nodes[i]->leaf_number() == leaf_number)
            {
                i_th_node = i;
                break;
            }
        }
    }
    return i_th_node;
}



void HTensor::set_data(const blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > >& data, const tensormode& tm)
{
    std::vector< const Cluster * > nodes = tree_.all_nodes( );
    for (std::size_t i = 0; i < nodes.size(); ++i)
    {
        if (nodes[i]->modes() == tm)
        {
            data_[i] = data;
            break;
        }
    }
}



void HTensor::set_data( blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > > &part_of_data, size_t i)
{
    data_[i].resize(part_of_data.size());
    for (std::size_t k = 0; k < data_[i].size(); ++k)
    {
        data_[i][k].resize(part_of_data[k].rows(), part_of_data[k].columns());
        data_[i][k] = part_of_data[k];
    }
}

void HTensor::set_data( blaze::DynamicMatrix< value_t, blaze::columnMajor >  &part_of_data, size_t i, size_t j)
{
    data_[i][j].resize(part_of_data.rows(), part_of_data.columns());
    data_[i][j] = part_of_data;
}

void HTensor::set_data( blaze::DynamicVector< value_t >  &vec, size_t i, size_t j, size_t k, bool row)
{
    if (row)
    {
        for (size_t l = 0; l < data_[i][j].columns(); ++l)
        {
            data_[i][j](k, l) = vec[l];
        }
    }
    else
    {
        for (size_t l = 0; l < data_[i][j].rows(); ++l)
        {
            data_[i][j](l, k) = vec[l];
        }
    }
}

void HTensor::get_data( blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > > &part_of_data, size_t i) const
{
    part_of_data.resize(data_[i].size());
    part_of_data = data_[i];
}

void HTensor::get_data( blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > >& data, const tensormode& tm) const
{
    std::vector< const Cluster * > nodes = tree_.all_nodes( );
    for (std::size_t i = 0; i < nodes.size(); ++i)
    {
        if (nodes[i]->modes() == tm)
        {
            data = data_[i];
            break;
        }
    }
}


void HTensor::get_data( blaze::DynamicMatrix< value_t, blaze::columnMajor >& part_of_data, const size_t i, const size_t j) const
{
    part_of_data.resize(data_[i][j].rows(), data_[i][j].columns());
    part_of_data = data_[i][j];
}

const blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > >& HTensor::get_data( std::size_t i) const
{
    return data_[i];
}

const blaze::DynamicMatrix< value_t, blaze::columnMajor >& HTensor::get_data(std::size_t i, std::size_t j) const
{
    return data_[i][j];
}

void HTensor::set_data(const blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > >& data, std::size_t i)
{
    data_[i] = data;
}
void HTensor::set_data(const blaze::DynamicMatrix< value_t, blaze::columnMajor >& data, std::size_t i, std::size_t j)
{
    data_[i][j] = data;
}

void HTensor::set_data(blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > >&& data, std::size_t i)
{
    data_[i] = std::move( data );
}

void HTensor::set_data(blaze::DynamicMatrix< value_t, blaze::columnMajor >&& data, std::size_t i, std::size_t j)
{
    data_[i][j] = std::move( data );
}

void HTensor::set_data( const std::vector< blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > > >& data )
{
    data_ = data;
}

void HTensor::set_data( std::vector< blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > > >&& data )
{
    data_ = std::move( data );
}

// void HTensor::get_data( blaze::DynamicMatrix< value_t, blaze::columnMajor >  &part_of_data, size_t i, size_t j) const
// {
//     part_of_data.resize(data_[i][j].rows(), data_[i][j].columns());
//     part_of_data = data_[i][j];
// }

void HTensor::get_data( blaze::DynamicVector< value_t >  &vec, size_t i, size_t j, size_t k, bool row) const
{

    if (row)
    {
        vec.resize(data_[i][j].columns());
        for (std::size_t l = 0; l < data_[i][j].columns(); l++)
        {
            vec[l] = data_[i][j](k, l);
        }
    }
    else
    {
        vec.resize(data_[i][j].rows());
        for (std::size_t l = 0; l < data_[i][j].rows(); l++)
        {
            vec[l] = data_[i][j](l, k);
        }
    }
}

std::tuple<size_t, size_t, size_t> HTensor::get_tensor_rank(size_t data_index) const
{
    size_t s1 = data_[data_index].size();
    size_t s2 = data_[data_index][0].rows();
    size_t s3 = data_[data_index][0].columns();

    return std::make_tuple(s1, s2, s3);
}

void HTensor::set_partial_data(const DynamicVector<DynamicMatrix<value_t, columnMajor>> &partial_data, int node_number)
{
    //we compare signed and unsigned values
    if (node_number < 0 || static_cast<int>(data_.size()) <= node_number)
    {
        throw invalid_argument("HTensor::set_partial_data: node_number too small or too large.");
    }
    data_[node_number] = partial_data;
}

void HTensor::set_partial_data(value_t value, int node_number, int rank, int i, int j)
{
    //we compare signed and unsigned values
    if (node_number < 0 || static_cast<int>(data_.size()) <= node_number)
    {
        throw invalid_argument("HTensor::set_partial_data: node_number too small or too large.");
    }
    data_[node_number][rank](i, j) = value;
}

//
// Returns a copy of the cluster tree.
//
Cluster HTensor::clustertree() const
{
    // return ( Cluster( tree_ ) );
    return tree_;
}

//
// Evaluates the tensor at one index.
//
value_t HTensor::entry( const std::vector< std::size_t >& index ) const
{
    DynamicVector< value_t, rowVector >  entry_vec = get_row( index, 0 );

    int my_rank = 0;
    vector< const Cluster * > nodes_on_this_rank = tree_.all_nodes_of_rank( my_rank );

    // Find the tensor entry on the root node, broadcast it to all nodes.
    value_t return_value = -1;
    if ( node_used_ )
    {
        if ( nodes_on_this_rank[ 0 ]->is_root() )
        {
            return_value = entry_vec[ 0 ];
        }
    }

    return return_value;
}

//
// Same method like entry() which uses the leaves-to-root template
// HTensor::leaves_to_root_algorithm().
// TODO make this method const?
//
value_t HTensor::entry_n( const tensorindex & index )
{
    // At leaf nodes: choose row corresponding to the tensor index and write it to
    //      father_data (data which will be made available at father node).
    function< DMatrix( leaf_data &, const size_t, vector< const leaf_data * >, DMatrix &, const size_t ) > leaf_op
        = [&]( leaf_data & data,
               const size_t leaf_number,
               vector< const leaf_data * >,
               DMatrix & father_data,
               const size_t ) -> DMatrix
    {
        father_data = submatrix( data, index[ leaf_number ], 0UL, 1UL, data.columns() );

        // Dummy return matrix.
        DMatrix return_matrix( 1UL, 1UL, 0.0 );
        return return_matrix;
    };

    // At inner nodes: Multiply the son data (son1_data, son2_data) to the transfer
    //      tensor ('data') and write the result to father_data.
    function< DMatrix( node_data &, vector< const node_data * >, DMatrix &, const DMatrix &, const DMatrix &, const size_t data_index ) > inner_op
        = [&]( node_data & data,
               vector< const node_data * >,
               DMatrix & father_data,
               const DMatrix & son1_data,
               const DMatrix & son2_data,
               const size_t  ) -> DMatrix
    {
        father_data.resize( 1UL, data.size() );
        for ( size_t j = 0; j < data.size(); ++j )
        {
            father_data( 0, j ) = ( son1_data * data[ j ] * trans( son2_data ) )( 0, 0 );
        }

        // Dummy return matrix.
        DMatrix return_matrix( 1UL, 1UL, 0.0 );
        return return_matrix;
    };

    // At root nodes: Multiply the son data to the transfer tensor.
    //      Return the result, which is the tensor entry.
    function< DMatrix( root_data &, vector< const root_data * >, const DMatrix &, const DMatrix &, const size_t ) > root_op
        = [&]( root_data & data,
               vector< const root_data * >,
               const DMatrix & son1_data,
               const DMatrix & son2_data,
               const size_t  ) -> DMatrix
    {
        return son1_data * data * trans( son2_data );
    };

    // No external operands.
    vector< const HTensor * > ext_operands;
    // Dummy matrix for father_data for the call at the first node
    //      (i=0).
    DMatrix father_data;

    DMatrix result = leaves_to_root_algorithm( ext_operands, leaf_op, inner_op, root_op, father_data );

    return result( 0, 0 );
}

//
// Sets the tensor entry at index to value. Increases the HT-ranks by one.
//
void HTensor::set_entry( const vector< size_t > & index, value_t value )
{
    value_t old_entry = entry( index );
    add( index, value - old_entry );
}

//TODO we should resize res or?
void HTensor::entry( const std::vector< std::vector< std::size_t > >& index, blaze::DynamicVector<value_t>& res ) const
{
    for (std::size_t i = 0; i < index.size(); ++i)
    {
        res[i] = entry(index[i]);
    }
}

//
// Scales this by alpha.
//
void HTensor::scale( value_t alpha )
{
    int my_rank = 0;
    vector< const Cluster * > nodes_on_this_rank = tree_.all_nodes_of_rank( my_rank );

    if ( node_used_ )
    {
        for ( size_t i = 0; i < nodes_on_this_rank.size(); ++i )
        {
            if ( nodes_on_this_rank[ i ]->is_root() )
            {
                data_[ i ][ 0 ].scale( alpha );
                break;
            }
        }
    }// if ( node_used_ )
}

//
// Adds alpha to each tensor entry. Increases the HT-ranks by one.
//
void HTensor::add( value_t alpha )
{
    int my_rank = 0;
    vector< const Cluster * > nodes_on_this_rank = tree_.all_nodes_of_rank( my_rank );

    if ( node_used_ )
    {
        for ( size_t i = 0; i < nodes_on_this_rank.size(); ++i )
        {
            if ( nodes_on_this_rank[ i ]->is_leaf() )
            {
                size_t rows     = 0;
                size_t columns  = 0;
                if ( data_[ i ].size() > 0 )
                {
                    rows    = data_[ i ][ 0 ].rows();
                    columns = data_[ i ][ 0 ].columns();
                }
                else
                {
                    data_[ i ].resize( 1 );
                }
                data_[ i ][ 0 ].resize( rows, columns + 1, true );
                DynamicVector< value_t, columnVector >   new_column( rows, 1.0 );
                column( data_[ i ][ 0 ], columns ) = new_column;
            }
            else if ( nodes_on_this_rank[ i ]->is_root() )
            {
                if ( data_[ i ].size() > 0 )
                {
                    size_t rows    = data_[ i ][ 0 ].rows();
                    size_t columns = data_[ i ][ 0 ].columns();
                    data_[ i ][ 0 ].resize( rows + 1, columns + 1, true );
                    DynamicVector< value_t, columnVector >   new_column( rows + 1, 0.0 );
                    DynamicVector< value_t, rowVector >      new_row( columns + 1, 0.0 );
                    column( data_[ i ][ 0 ], columns )  = new_column;
                    row( data_[ i ][ 0 ], rows )        = new_row;
                    data_[ i ][ 0 ]( rows, columns ) = alpha;
                }
            }
            else
            {
                // Inner transfer tensor.
                size_t layers   = data_[ i ].size();
                data_[ i ].resize( layers + 1 );
                size_t rows     = data_[ i ][ 0 ].rows();
                size_t columns  = data_[ i ][ 0 ].columns();
                for ( size_t j = 0; j <= layers; ++j )
                {
                    data_[ i ][ j ].resize( rows + 1, columns + 1, true );
                    if ( j < layers )
                    {
                        DynamicVector< value_t, columnVector >   new_column( rows + 1, 0.0 );
                        DynamicVector< value_t, rowVector >      new_row( columns + 1, 0.0 );
                        column( data_[ i ][ j ], columns )  = new_column;
                        row( data_[ i ][ j ], rows )        = new_row;
                    }
                    else
                    {
                        data_[ i ][ j ] = 0.0;
                        data_[ i ][ j ]( rows, columns ) = 1.0;
                    }
                }
            }
        }
    }
}

//
// Adds alpha to the tensor entry at index. Increases the HT-ranks by one.
//
void HTensor::add( const vector< size_t > & index, value_t alpha )
{
    int my_rank = 0;
    vector< const Cluster * > nodes_on_this_rank = tree_.all_nodes_of_rank( my_rank );

    if ( node_used_ )
    {
        for ( size_t i = 0; i < nodes_on_this_rank.size(); ++i )
        {
            if ( nodes_on_this_rank[ i ]->is_leaf() )
            {
                size_t rows         = 0;
                size_t columns      = 0;
                size_t leaf_number  = nodes_on_this_rank[ i ]->leaf_number();
                if ( data_[ i ].size() > 0 )
                {
                    rows    = data_[ i ][ 0 ].rows();
                    columns = data_[ i ][ 0 ].columns();
                }
                else
                {
                    data_[ i ].resize( 1 );
                }
                data_[ i ][ 0 ].resize( rows, columns + 1, true );
                DynamicVector< value_t, columnVector > new_column( rows, 0.0 );
                new_column[ index[ leaf_number ] ] = 1.0;
                column( data_[ i ][ 0 ], columns ) = new_column;
            }
            else if ( nodes_on_this_rank[ i ]->is_root() )
            {
                if ( data_[ i ].size() > 0 )
                {
                    size_t rows    = data_[ i ][ 0 ].rows();
                    size_t columns = data_[ i ][ 0 ].columns();
                    data_[ i ][ 0 ].resize( rows + 1, columns + 1, true );
                    DynamicVector< value_t, columnVector >   new_column( rows + 1, 0.0 );
                    DynamicVector< value_t, rowVector >      new_row( columns + 1, 0.0 );
                    column( data_[ i ][ 0 ], columns )  = new_column;
                    row( data_[ i ][ 0 ], rows )        = new_row;
                    data_[ i ][ 0 ]( rows, columns ) = alpha;
                }
            }
            else
            {
                // Inner transfer tensor.
                size_t layers   = data_[ i ].size();
                data_[ i ].resize( layers + 1 );
                size_t rows     = data_[ i ][ 0 ].rows();
                size_t columns  = data_[ i ][ 0 ].columns();
                for ( size_t j = 0; j <= layers; ++j )
                {
                    data_[ i ][ j ].resize( rows + 1, columns + 1, true );
                    if ( j < layers )
                    {
                        DynamicVector< value_t, columnVector >   new_column( rows + 1, 0.0 );
                        DynamicVector< value_t, rowVector >      new_row( columns + 1, 0.0 );
                        column( data_[ i ][ j ], columns )  = new_column;
                        row( data_[ i ][ j ], rows )        = new_row;
                    }
                    else
                    {
                        data_[ i ][ j ] = 0.0;
                        data_[ i ][ j ]( rows, columns ) = 1.0;
                    }
                }
            }
        }
    }
}

//
// Computes the inner product of this with X, where X is a tensor of the same HT-structure as
//
value_t HTensor::dot( const HTensor & X ) const
{
    int my_rank = 0;
    vector< const Cluster * > nodes_on_this_rank = tree_.all_nodes_of_rank( my_rank );

    DynamicMatrix< value_t, columnMajor >    G;
    inner_product_matrix( X, G, 0 );

    // Get the inner product on the root node, broadcast it to all nodes.
    value_t return_value = 0;
    if ( node_used_ )
    {
        if ( nodes_on_this_rank[ 0 ]->is_root() )
        {
            return_value = G( 0, 0 );
        }
    }

    return return_value;
}

//
// Same method like \link dot() \endlink which uses the leaves-to-root template
// \link HTensor::leaves_to_root_algorithm() \endlink.
//
value_t HTensor::dot_n( const HTensor & X )
{
    function< DMatrix( leaf_data &, const size_t, vector< const leaf_data * >, DMatrix &, const size_t ) > leaf_op
        = [&]( leaf_data & data,
               const size_t,
               vector< const leaf_data * > data_ext,
               DMatrix & father_data,
               const size_t  ) -> DMatrix
    {
        father_data = trans( data ) * (*data_ext[ 0 ]);

        // Dummy return matrix.
        DMatrix return_matrix( 1UL, 1UL, 0.0 );
        return return_matrix;
    };

    function< DMatrix( node_data &, vector< const node_data * >, DMatrix &, const DMatrix &, const DMatrix &, const size_t data_index ) > inner_op
        = [&]( node_data & data,
               vector< const node_data * > data_ext,
               DMatrix & father_data,
               const DMatrix & son1_data,
               const DMatrix & son2_data,
               const size_t  ) -> DMatrix
    {
        father_data.resize( data.size(), data_ext[ 0 ]->size() );
        DMatrix tmp1;
        for ( size_t l2 = 0; l2 < father_data.columns(); ++l2 )
        {
            tmp1 = son1_data * (*(data_ext[ 0 ]))[ l2 ] * trans( son2_data );
            for ( size_t l1 = 0; l1 < father_data.rows(); ++l1 )
            {
                value_t dot = 0.0;
                for ( size_t j = 0; j < tmp1.columns(); ++j )
                {
                    dot += ( column( tmp1, j ), column( data[ l1 ], j ) );
                }
                father_data( l1, l2 ) = dot;
            }
        }

        DMatrix return_matrix( 1UL, 1UL, 0.0 );
        return return_matrix;
    };

    function< DMatrix( root_data &, vector< const root_data * >, const DMatrix &, const DMatrix &, const size_t ) > root_op
        = [&]( root_data & data,
               vector< const root_data * > data_ext,
               const DMatrix & son1_data,
               const DMatrix & son2_data,
               const size_t  ) -> DMatrix
    {
        DMatrix return_matrix( 1, 1, 0.0 );
        DMatrix tmp1 = son1_data * *(data_ext[ 0 ]) * trans( son2_data );
        for ( size_t j = 0; j < tmp1.columns(); ++j )
        {
            return_matrix( 0, 0 ) += ( column( tmp1, j ), column( data, j ) );
        }

        // Return 1x1 inner product matrix.
        return return_matrix;
    };

    // External operand X:
    vector< const HTensor * > ext_operands( 1 );
    ext_operands[ 0 ] = &X;
    // Dummy matrix for father_data for the call at the first node
    //      (i=0).
    DMatrix father_data;

    DMatrix result = leaves_to_root_algorithm( ext_operands, leaf_op, inner_op, root_op, father_data );

    return result( 0, 0 );
}

//
// Computes the inner product of this with X w.r.t the range defined by
// lower_index and upper_index.
// X is a tensor of the same HT-structure as this.
//
value_t HTensor::dot( const HTensor & X, const vector< size_t > & lower_index, const vector< size_t > & upper_index ) const
{
    int my_rank = 0;
    vector< const Cluster * > nodes_on_this_rank = tree_.all_nodes_of_rank( my_rank );

    DynamicMatrix< value_t, columnMajor >    G;
    inner_product_matrix_restricted( X, G, lower_index, upper_index, 0 );

    // Get the inner product on the root node, broadcast it to all nodes.
    value_t return_value = 0;
    if ( node_used_ )
    {
        if ( nodes_on_this_rank[ 0 ]->is_root() )
        {
            return_value = G( 0, 0 );
        }
    }

    return return_value;
}

//
// Computes the inner product of this with X, where the mu-th direction is
// restricted to 'indices'.
// X is a tensor of the same HT-structure as this
//
value_t HTensor::dot( const HTensor & X, size_t mu, const vector< size_t > & indices ) const
{
    int my_rank = 0;
    vector< const Cluster * > nodes_on_this_rank = tree_.all_nodes_of_rank( my_rank );

    DynamicMatrix< value_t, columnMajor >    G;
    inner_product_matrix_restricted( X, G, mu, indices, 0 );

    // Get the inner product on the root node, broadcast it to all nodes.
    value_t return_value = 0;
    if ( node_used_ )
    {
        if ( nodes_on_this_rank[ 0 ]->is_root() )
        {
            return_value = G( 0, 0 );
        }
    }

    return return_value;
}

//
// Computes the inner product of this with X with arbitrary restrictions,
// which are defined by the vector 'indices' (one index vector for each tensor dimension).
// If the indices[ mu ] is empty, the mu-th direction gets fully included.
// X is a tensor of the same HT-structure as this.
//
value_t HTensor::dot( const HTensor & X, const vector< vector< size_t > > & indices ) const
{
    int my_rank = 0;
    vector< const Cluster * > nodes_on_this_rank = tree_.all_nodes_of_rank( my_rank );

    DynamicMatrix< value_t, columnMajor >    G;
    inner_product_matrix_restricted( X, G, indices, 0 );

    // Get the inner product on the root node, broadcast it to all nodes.
    value_t return_value = 0;
    if ( node_used_ )
    {
        if ( nodes_on_this_rank[ 0 ]->is_root() )
        {
            return_value = G( 0, 0 );
        }
    }

    return return_value;
}

//
// Computes this = this + alpha * X, where X is a tensor of the same HT-structure as this.
//
void HTensor::add( value_t alpha, const HTensor & X )
{
    int my_rank = 0;
    vector< const Cluster * > nodes_on_this_rank = tree_.all_nodes_of_rank( my_rank );

    if ( node_used_ )
    {
        for ( size_t i = 0; i < data_.size(); ++i )
        {
            if ( nodes_on_this_rank[ i ]->is_leaf() )
            {
                size_t rows     = 0;
                size_t columns1 = 0;
                size_t columns2 = 0;
                if ( data_[ i ].size() > 0 )
                {
                    rows        = data_[ i ][ 0 ].rows();
                    columns1    = data_[ i ][ 0 ].columns();
                }
                if ( X.data_[ i ].size() > 0 )
                {
                    columns2 = X.data_[ i ][ 0 ].columns();
                }
                if ( columns2 > 0 )
                {
                    if ( data_[ i ].size() == 0 )
                    {
                        data_[ i ].resize( 1 );
                    }
                    data_[ i ][ 0 ].resize( rows, columns1 + columns2, true );
                    submatrix( data_[ i ][ 0 ], 0UL, columns1, rows, columns2 ) = X.data_[ i ][ 0 ];
                }
            }
            else if ( nodes_on_this_rank[ i ]->is_root() )
            {
                size_t rows1    = 0;
                size_t rows2    = 0;
                size_t columns1 = 0;
                size_t columns2 = 0;
                if ( data_[ i ].size() > 0 )
                {
                    rows1       = data_[ i ][ 0 ].rows();
                    columns1    = data_[ i ][ 0 ].columns();
                }
                if ( X.data_[ i ].size() > 0 )
                {
                    rows2       = X.data_[ i ][ 0 ].rows();
                    columns2    = X.data_[ i ][ 0 ].columns();
                }
                if ( rows2 > 0 && columns2 > 0 )
                {
                    if ( data_[ i ].size() == 0 )
                    {
                        data_[ i ].resize( 1 );
                    }
                    data_[ i ][ 0 ].resize( rows1 + rows2, columns1 + columns2, true );
                    submatrix( data_[ i ][ 0 ], rows1, columns1, rows2, columns2 ) = alpha * X.data_[ i ][ 0 ];
                    submatrix( data_[ i ][ 0 ], rows1, 0UL, rows2, columns1 ) = 0.0;
                    submatrix( data_[ i ][ 0 ], 0UL, columns1, rows1, columns2 ) = 0.0;
                }
            }
            else
            {
                // Inner transfer tensor.
                size_t rows1    = 0;
                size_t rows2    = 0;
                size_t columns1 = 0;
                size_t columns2 = 0;
                size_t layers1  = data_[ i ].size();
                size_t layers2  = X.data_[ i ].size();
                if ( layers1 > 0 )
                {
                    rows1       = data_[ i ][ 0 ].rows();
                    columns1    = data_[ i ][ 0 ].columns();
                }
                if ( layers2 > 0 )
                {
                    rows2       = X.data_[ i ][ 0 ].rows();
                    columns2    = X.data_[ i ][ 0 ].columns();
                    for ( size_t j = 0; j < layers1; ++j )
                    {
                        data_[ i ][ j ].resize( rows1 + rows2, columns1 + columns2, true );
                        submatrix( data_[ i ][ j ], 0UL, columns1, rows1 + rows2, columns2 ) = 0.0;
                        submatrix( data_[ i ][ j ], rows1, 0UL, rows2, columns1 ) = 0.0;
                    }
                    data_[ i ].resize( layers1 + layers2, true );
                    for ( size_t j = 0; j < layers2; ++j )
                    {
                        data_[ i ][ layers1 + j ].resize( rows1 + rows2, columns1 + columns2 );
                        data_[ i ][ layers1 + j ] = 0.0;
                        submatrix( data_[ i ][ layers1 + j ], rows1, columns1, rows2, columns2 ) = X.data_[ i ][ j ];
                    }
                }// if ( layers2 > 0 )
            }
        }
    }// if ( node_used_ )
}

//
// Same method like add( value_t, const HTensor & ) which uses the
// template HTensor::fully_parallel_algorithm() for fully parallel
// algorithms.
//
void HTensor::add_n( value_t alpha, const HTensor & X )
{
    function< DMatrix( leaf_data &, const size_t, vector< const leaf_data * >, const size_t ) > leaf_op
        = [&]( leaf_data & data,
               const size_t,
               vector< const leaf_data * > data_ext,
               const size_t  ) -> DMatrix
    {
        size_t rows     = data.rows();
        size_t columns1 = data.columns();
        size_t columns2 = data_ext[ 0 ]->columns();

        data.resize( rows, columns1 + columns2, true );
        submatrix( data, 0UL, columns1, rows, columns2 ) = *data_ext[ 0 ];

        // Dummy return matrix.
        DMatrix return_matrix( 1UL, 1UL, 0.0 );
        return return_matrix;
    };

    function< DMatrix( node_data &, vector< const node_data * >, const size_t ) > inner_op
        = [&]( node_data & data,
               vector< const node_data * > data_ext,
               const size_t  ) -> DMatrix
    {
        size_t rows1    = data[ 0 ].rows();
        size_t columns1 = data[ 0 ].columns();
        size_t layers1  = data.size();
        size_t rows2    = (*data_ext[ 0 ])[ 0 ].rows();
        size_t columns2 = (*data_ext[ 0 ])[ 0 ].columns();
        size_t layers2  = data_ext[ 0 ]->size();

        for ( size_t j = 0; j < layers1; ++j )
        {
            data[ j ].resize( rows1 + rows2, columns1 + columns2, true );
            submatrix( data[ j ], 0UL, columns1, rows1 + rows2, columns2 ) = 0.0;
            submatrix( data[ j ], rows1, 0UL, rows2, columns1 ) = 0.0;
        }
        data.resize( layers1 + layers2, true );
        for ( size_t j = 0; j < layers2; ++j )
        {
            data[ layers1 + j ].resize( rows1 + rows2, columns1 + columns2 );
            data[ layers1 + j ] = 0.0;
            submatrix( data[ layers1 + j ], rows1, columns1, rows2, columns2 ) = (*data_ext[ 0 ])[ j ];
        }

        // Dummy return matrix.
        DMatrix return_matrix( 1UL, 1UL, 0.0 );
        return return_matrix;
    };

    function< DMatrix( root_data &, vector< const root_data * >, const size_t ) > root_op
        = [&]( root_data & data,
               vector< const root_data * > data_ext,
               const size_t  ) -> DMatrix
    {
        size_t rows1    = data.rows();
        size_t columns1 = data.columns();
        size_t rows2    = data_ext[ 0 ]->rows();
        size_t columns2 = data_ext[ 0 ]->columns();

        data.resize( rows1 + rows2, columns1 + columns2, true );
        submatrix( data, rows1, columns1, rows2, columns2 ) = alpha * *(data_ext[ 0 ]);
        submatrix( data, rows1, 0UL, rows2, columns1 )      = 0.0;
        submatrix( data, 0UL, columns1, rows1, columns2 )   = 0.0;

        // Dummy return matrix.
        DMatrix return_matrix( 1UL, 1UL, 0.0 );
        return return_matrix;
    };

    // External operand X:
    vector< const HTensor * > ext_operands( 1 );
    ext_operands[ 0 ] = &X;

    fully_parallel_algorithm( ext_operands, leaf_op, inner_op, root_op );
}

//
// Computes this = this + alpha * X, where X is a tensor of the same HT-structure as this.
// This method first computes a joint orthonormal basis of both tensors, s.t. the addition
// can be carried out on the root node of the HT-tree.
//
void HTensor::add2( value_t alpha, const HTensor & X )
{
    HTensor Y( X );
    vector< HTensor * > pt_vec( 2 );
    pt_vec[ 0 ] = this;
    pt_vec[ 1 ] = &Y;
    joint_orthonormal_bases( pt_vec );

    int my_rank = 0;
    vector< const Cluster * > nodes_on_this_rank = tree_.all_nodes_of_rank( my_rank );

    if ( node_used_ )
    {
        for ( size_t i = 0; i < data_.size(); ++i )
        {
            if ( nodes_on_this_rank[ i ]->is_root() )
            {
                data_[ i ][ 0 ] += alpha * Y.data_[ i ][ 0 ];
            }
        }
    }// if ( node_used_ )
}

//
// Computes the Hadamard product (i.e. entry-wise multiplication) this = this o X.
//
void HTensor::hadamard( const HTensor & X )
{
    int my_rank = 0;
    vector< const Cluster * > nodes_on_this_rank = tree_.all_nodes_of_rank( my_rank );

    if ( node_used_ )
    {
        for ( size_t i = 0; i < data_.size(); ++i )
        {
            if ( nodes_on_this_rank[ i ]->is_leaf() )
            {
                size_t rows         = 0;
                size_t columns      = 0;
                //is not used
                //size_t X_rows       = 0;
                size_t X_columns    = 0;
                if ( data_[ i ].size() > 0 && X.data_[ i ].size() > 0 )
                {
                    rows        = data_[ i ][ 0 ].rows();
                    columns     = data_[ i ][ 0 ].columns();
                    X_columns   = X.data_[ i ][ 0 ].columns();
                    DynamicMatrix< value_t, columnMajor > result_data( rows, columns * X_columns );
                    for ( size_t j1 = 0; j1 < columns; ++j1 )
                    {
                        for ( size_t j2 = 0; j2 < X_columns; ++j2 )
                        {
                            column( result_data, j2 + j1 * X_columns ) = column( data_[ i ][ 0 ], j1 ) * column( X.data_[ i ][ 0 ], j2 );
                        }
                    }
                    swap( data_[ i ][ 0 ], result_data );
                }
            }
            else
            {
                size_t layers   = data_[ i ].size();
                size_t X_layers = X.data_[ i ].size();
                if ( layers > 0 && X_layers > 0 )
                {
                    DynamicVector< DynamicMatrix< value_t, columnMajor > >   result_data( layers * X_layers );
                    for ( size_t l1 = 0; l1 < layers; ++l1 )
                    {
                        for ( size_t l2 = 0; l2 < X_layers; ++l2 )
                        {
                            kronecker( data_[ i ][ l1 ], X.data_[ i ][ l2 ], result_data[ l2 + l1 * X_layers ] );
                        }
                    }
                    swap( data_[ i ], result_data );
                }
            }
        }// for ( i )
    }// if ( node_used_ )
}

//
// Computes the Hadamard product this = this o this.
//
void HTensor::hadamard()
{
    int my_rank = 0;
    vector< const Cluster * > nodes_on_this_rank = tree_.all_nodes_of_rank( my_rank );

    if ( node_used_ )
    {
        for ( size_t i = 0; i < data_.size(); ++i )
        {
            if ( nodes_on_this_rank[ i ]->is_leaf() )
            {
                size_t rows         = 0;
                size_t columns      = 0;
                if ( data_[ i ].size() > 0 )
                {
                    rows        = data_[ i ][ 0 ].rows();
                    columns     = data_[ i ][ 0 ].columns();
                    DynamicMatrix< value_t, columnMajor > result_data( rows, ( columns * ( columns + 1 ) ) / 2 );
                    size_t count = 0;
                    for ( size_t j1 = 0; j1 < columns; ++j1 )
                    {
                        for ( size_t j2 = j1; j2 < columns; ++j2 )
                        {
                            column( result_data, count ) = column( data_[ i ][ 0 ], j1 ) * column( data_[ i ][ 0 ], j2 );
                            count++;
                        }
                    }
                    swap( data_[ i ][ 0 ], result_data );
                }
            }
            else
            {
                size_t layers   = data_[ i ].size();
                if ( layers > 0 )
                {
                    DynamicVector< DynamicMatrix< value_t, columnMajor > >   result_data( ( layers * ( layers + 1 ) ) / 2 );
                    size_t count = 0;
                    for ( size_t l1 = 0; l1 < layers; ++l1 )
                    {
                        for ( size_t l2 = l1; l2 < layers; ++l2 )
                        {
                            kronecker( data_[ i ][ l1 ], data_[ i ][ l2 ], result_data[ count ], true );
                            count++;
                        }
                    }
                    swap( data_[ i ], result_data );
                }
            }
        }// for ( i )
    }// if ( node_used_ )
}


//
// Orthogonalizes the tensor:
// On the leaves:      orthonormal columns,
// Transfer tensors:   orthonormal layers.
//
void HTensor::orthogonalize()
{
    DynamicMatrix< value_t, columnMajor >    R;
    orthogonalize_node( R, 0 );
}


//
// Creates joint orthonormal bases for the HTensors pointed to by ht_pt.
// All HTensors must be of the same format.
//
void joint_orthonormal_bases( const vector< HTensor * > & ht_pt )
{
    for ( size_t i = 0; i < ht_pt.size(); ++i )
    {
        ht_pt[ i ]->orthogonalize();
    }

    DynamicMatrix< value_t, columnMajor >    R;
    joint_orthonormal_bases_node( ht_pt, R, 0 );
}

//
// Computes a QR decomposition for a set of HTensors, i.e. orthonormalizes the HTensors
// and computes a matrix R, the columns of which define the linear combinations by which
// the original HTensors can be represented in the new orthonormal basis.
// Notice that ht_pt will not be resized, even though the number of
// orthonormal HTensors may be smaller than the original number of HTensors in ht_pt.
// The number of orthonormal HTensors can be obtained by R.rows().
//
void QR( const vector< HTensor * > & ht_pt, DynamicMatrix< value_t, columnMajor > & R )
{
    // Compute joint orthonormal bases for the HTensors in ht_pt
    joint_orthonormal_bases( ht_pt );

    if ( ht_pt[ 0 ]->node_used_ )
    {
        // Fully orthonormalize the HTensors by orthonormalizing the root transfer tensors
        // and save R
        int my_rank = 0;

        int root_mpi_rank = ht_pt[ 0 ]->tree_.root()->mpi_rank();

        if ( my_rank == root_mpi_rank )
        {
            vector< const Cluster * > nodes_on_this_rank = ht_pt[ 0 ]->tree_.all_nodes_of_rank( my_rank );

            for ( size_t i = 0; i < nodes_on_this_rank.size(); ++i )
            {
                if ( nodes_on_this_rank[ i ]->is_root() )
                {
                    DynamicVector< DynamicMatrix< value_t, columnMajor > > root_transfers( ht_pt.size() );
                    for ( size_t l = 0; l < ht_pt.size(); ++l )
                    {
                        root_transfers[ l ] = ht_pt[ l ]->data_[ i ][ 0 ];
                    }
                    qr_transfer_tensor( root_transfers, R );
                    for ( size_t l = 0; l < root_transfers.size(); ++l )
                    {
                        ht_pt[ l ]->data_[ i ][ 0 ] = root_transfers[ l ];
                    }
                }
            }// for ( size_t i = 0; i < nodes_on_this_rank.size(); ++i )
        }// if ( my_rank == root_mpi_rank )
    }// if ( ht_pt[ 0 ]->node_used_ )
}

//
// Truncate down to rank k.
// Returns an upper estimate for the relative truncation error.
//
double HTensor::truncate( size_t k )
{
    orthogonalize();

    vector< DynamicMatrix< value_t, columnMajor > >  acc_tr;
    accumulated_transfer_tensors( acc_tr, 0 );

    vector< DynamicMatrix< value_t, columnMajor > >  eigenvectors;
    vector< double > sq_rel_errors( acc_tr.size() );
    truncate_orthogonalized_node( acc_tr, eigenvectors, sq_rel_errors, 0, k, -1 );

    // Estimate the relative truncation error:
    double local_sq_rel_error_sum = 0.0;
    for ( size_t i = 0; i < sq_rel_errors.size(); ++i )
    {
        local_sq_rel_error_sum += sq_rel_errors[ i ];
    }
    double rel_err_estimate = 0.0;
    rel_err_estimate = local_sq_rel_error_sum;
    rel_err_estimate = sqrt( rel_err_estimate );
    return rel_err_estimate;
}

//
// Truncate with a relative error of at most eps.
// Returns an upper estimate for the relative truncation error.
//
double HTensor::truncate( double eps )
{
    orthogonalize();

    vector< DynamicMatrix< value_t, columnMajor > >  acc_tr;
    accumulated_transfer_tensors( acc_tr, 0 );

    vector< DynamicMatrix< value_t, columnMajor > >  eigenvectors;
    vector< double > sq_rel_errors( acc_tr.size() );
    truncate_orthogonalized_node( acc_tr, eigenvectors, sq_rel_errors, 0, -1, eps );

    // Estimate the relative truncation error:
    double local_sq_rel_error_sum = 0.0;
    for ( size_t i = 0; i < sq_rel_errors.size(); ++i )
    {
        local_sq_rel_error_sum += sq_rel_errors[ i ];
    }
    double rel_err_estimate;
    rel_err_estimate = local_sq_rel_error_sum;
    rel_err_estimate = sqrt( rel_err_estimate );
    return rel_err_estimate;
}

//
// Tries to truncate with a relative error of at most eps, but with additional rank bound k,
// which might result in a less accurate truncation.
// Returns an upper estimate for the relative truncation error.
//
double HTensor::truncate( double eps, size_t k )
{
    orthogonalize();

    vector< DynamicMatrix< value_t, columnMajor > >  acc_tr;
    accumulated_transfer_tensors( acc_tr, 0 );

    vector< DynamicMatrix< value_t, columnMajor > >  eigenvectors;
    vector< double > sq_rel_errors( acc_tr.size() );
    truncate_orthogonalized_node( acc_tr, eigenvectors, sq_rel_errors, 0, k, eps );

    // Estimate the relative truncation error:
    double local_sq_rel_error_sum = 0.0;
    for ( size_t i = 0; i < sq_rel_errors.size(); ++i )
    {
        local_sq_rel_error_sum += sq_rel_errors[ i ];
    }
    double rel_err_estimate;
    rel_err_estimate = local_sq_rel_error_sum;
    rel_err_estimate = sqrt( rel_err_estimate );
    return rel_err_estimate;
}

//
// HOSVD truncation (same algorithm as \link truncate() \endlink) which uses the
// root-to-leaves template \link HTensor::root_to_leaves_algorithm() \endlink
// for the computation of accumulated transfer tensors and the
// leaves-to-root template \link HTensor::leaves_to_root_algorithm() \endlink
// for the computation of singular values/vectors and truncation.
// If both, k and eps, are set, the option which results in a smaller tensor rank
// is chosen.
//
// @param k prescribed rank for the truncation. In order to specify only eps,
//          set k = -1, which results in k = max( size_t ).
// @param eps prescribed relative accuracy for the truncation. In order to specify
//            only k, choose eps < 0 (e.g. eps = -1).
//
double HTensor::truncate_n( std::size_t k, double eps )
{
    orthogonalize();

    // Compute accumulated transfer tensors (root-to-leaves);
    // acc_tr contains the acc. transfer tensors for all tree nodes
    vector< DMatrix > acc_tr( data_.size() );

    function< DMatrix( leaf_data &, const size_t, vector< const leaf_data * >, const DMatrix &, const size_t ) > leaf_op_acc
        = [&]( leaf_data &,
               const size_t,
               vector< const leaf_data * >,
               const DMatrix & father_data,
               const size_t data_index ) -> DMatrix
    {
        // Acc. transfer tensor has been computed by father:
        acc_tr[ data_index ] = father_data;

        // Dummy return matrix.
        DMatrix return_matrix( 1UL, 1UL, 0.0 );
        return return_matrix;
    };

    function< DMatrix( node_data &, vector< const node_data * >, const DMatrix &, DMatrix &, DMatrix &, const size_t ) > inner_op_acc
        = [&]( node_data & data,
               vector< const node_data * >,
               const DMatrix & father_data,
               DMatrix & son1_data,
               DMatrix & son2_data,
               const size_t data_index ) -> DMatrix
    {
        // Acc. transfer tensor has been computed by father:
        acc_tr[ data_index ] = father_data;

        // Compute acc. transfer tensors for sons:
        if ( data.size() > 0 )
        {
            son1_data.resize( data[ 0 ].rows(), data[ 0 ].rows() );
            son2_data.resize( data[ 0 ].columns(), data[ 0 ].columns() );
            son1_data = 0.0;
            son2_data = 0.0;
        }

        node_data transformed_data( data.size() );

        for ( size_t l2 = 0; l2 < data.size(); ++l2 )
        {
            transformed_data[ l2 ] = acc_tr[ data_index ]( 0, l2 ) * data[ 0 ];
            for ( size_t l1 = 1; l1 < data.size(); ++l1 )
            {
                transformed_data[ l2 ] += acc_tr[ data_index ]( l1, l2 ) * data[ l1 ];
            }
        }
        for ( size_t l2 = 0; l2 < data.size(); ++l2 )
        {
            son1_data += transformed_data[ l2 ] * trans( data[ l2 ] );
            son2_data += trans( transformed_data[ l2 ] ) * data[ l2 ];
        }

        // Dummy return matrix.
        DMatrix return_matrix( 1UL, 1UL, 0.0 );
        return return_matrix;
    };

    function< DMatrix( root_data &, vector< const root_data * >, DMatrix &, DMatrix &, const size_t ) > root_op_acc
        = [&]( root_data & data,
               vector< const root_data * >,
               DMatrix & son1_data,
               DMatrix & son2_data,
               const size_t data_index ) -> DMatrix
    {
        // Acc. transfer tensor at root is chosen 1.0 (1x1):
        acc_tr[ data_index ].resize( 1UL, 1UL );
        acc_tr[ data_index ]( 0, 0 ) = 1.0;

        // Acc. transfer tensors for sons are B*B^T and B^T*B (B root transfer tensor):
        son1_data = data * trans( data );
        son2_data = trans( data ) * data;

        // Dummy return matrix.
        DMatrix return_matrix( 1UL, 1UL, 0.0 );
        return return_matrix;
    };

    // No external operands.
    vector< const HTensor * > ext_operands;
    // Dummy matrix for father_data for the call at the first node
    //      (i=0).
    DMatrix father_data;

    root_to_leaves_algorithm( ext_operands, leaf_op_acc, inner_op_acc, root_op_acc, father_data );

    // Find rank k_epsilon which guarantees some prescribed
    // relative truncation accuracy epsilon
    function< size_t( double, const DynamicVector< double > & ) > adaptive_rank
        = [&]( double epsilon,
               const DynamicVector< double > & eigenvalues ) -> size_t
    {
        double sum = 0.0;
        size_t dim = tree_.size();
        double quasi_best_factor = 2.0 * dim - 3.0;

        for ( size_t j = 0; j < eigenvalues.size(); ++j )
        {
            sum += eigenvalues[ j ];
        }

        double sum_tr = 0.0;
        size_t k_epsilon = eigenvalues.size();

        for ( size_t j = 0; j < eigenvalues.size(); ++j )
        {
            sum_tr += eigenvalues[ j ];
            if ( sum_tr / sum < pow( epsilon, 2 ) / quasi_best_factor )
            {
                k_epsilon--;
            }
            else
            {
                break;
            }
        }
        return k_epsilon;
    };

    // Compute singular values/vectors and truncate (leaves-to-root):

    function< DMatrix( leaf_data &, const size_t, vector< const leaf_data * >, DMatrix &, const size_t ) > leaf_op_trunc
        = [&]( leaf_data & data,
               const size_t,
               vector< const leaf_data * >,
               DMatrix & father_data,
               const size_t data_index ) -> DMatrix
    {
        // The eigenvectors will be the columns of 'father_data':
        DynamicVector< value_t > eigenvalues;
        SymmetricMatrix< DMatrix > sym_acc_tr = declsym( acc_tr[ data_index ] );
        eigen( sym_acc_tr, eigenvalues, father_data );

        size_t k_eps = eigenvalues.size();
        if ( eps > 0 )
        {
            k_eps = adaptive_rank( eps, eigenvalues );
        }

        size_t k0 = min( k, k_eps );

        // Squared relative truncation error:
        double sq_rel_err   = 0.0;
        double sq_norm      = 0.0;
        for ( size_t j = 0; j < eigenvalues.size(); ++j )
        {
            sq_norm += std::abs( eigenvalues[ j ] );
            if ( j < eigenvalues.size() - k0 )
            {
                sq_rel_err += std::abs( eigenvalues[ j ] );
            }
        }
        sq_rel_err /= sq_norm;

        // Truncate
        father_data = submatrix( father_data, 0UL, father_data.columns() - k0, father_data.rows(), k0 );

        data = data * father_data;

        // Return matrix contains the squared relative error at this node:
        DMatrix return_matrix( 1UL, 1UL, sq_rel_err );
        return return_matrix;
    };

    function< DMatrix( node_data &, vector< const node_data * >, DMatrix &, const DMatrix &, const DMatrix &, const size_t data_index ) > inner_op_trunc
        = [&]( node_data & data,
               vector< const node_data * >,
               DMatrix & father_data,
               const DMatrix & son1_data,
               const DMatrix & son2_data,
               const size_t data_index ) -> DMatrix
    {
        // The eigenvectors will be the columns of 'father_data':
        DynamicVector< value_t > eigenvalues;
        SymmetricMatrix< DMatrix > sym_acc_tr = declsym( acc_tr[ data_index ] );
        eigen( sym_acc_tr, eigenvalues, father_data );

        size_t k_eps = eigenvalues.size();
        if ( eps > 0 )
        {
            k_eps = adaptive_rank( eps, eigenvalues );
        }

        size_t k0 = min( k, k_eps );

        // Squared relative truncation error:
        double sq_rel_err   = 0.0;
        double sq_norm      = 0.0;
        for ( size_t j = 0; j < eigenvalues.size(); ++j )
        {
            sq_norm += std::abs( eigenvalues[ j ] );
            if ( j < eigenvalues.size() - k0 )
            {
                sq_rel_err += std::abs( eigenvalues[ j ] );
            }
        }
        sq_rel_err /= sq_norm;

        // Truncate
        father_data = submatrix( father_data, 0UL, father_data.columns() - k0, father_data.rows(), k0 );

        node_data new_data( father_data.columns() );
        for ( size_t j = 0; j < new_data.size(); ++j )
        {
            new_data[ j ].resize( data[ 0 ].rows(), data[ 0 ].columns() );
            new_data[ j ] = 0.0;
        }
        for ( size_t j = 0; j < father_data.columns(); ++j )
        {
            for ( size_t l = 0; l < father_data.rows(); ++l )
            {
                new_data[ j ] += father_data( l, j ) * data[ l ];
            }
        }
        swap( new_data, data );

        // Transform the transfer tensor according to son transformations.
        for ( size_t l = 0; l < data.size(); ++l )
        {
            data[ l ] = trans( son1_data ) * data[ l ] * son2_data;
        }

        // Return matrix contains the squared relative error at this node:
        DMatrix return_matrix( 1UL, 1UL, sq_rel_err );
        return return_matrix;
    };

    function< DMatrix( root_data &, vector< const root_data * >, const DMatrix &, const DMatrix &, const size_t ) > root_op_trunc
        = [&]( root_data & data,
               vector< const root_data * >,
               const DMatrix & son1_data,
               const DMatrix & son2_data,
               const size_t  ) -> DMatrix
    {
        // Transform the transfer tensor according to son transformations.
        data = trans( son1_data ) * data * son2_data;

        // Dummy return matrix.
        DMatrix return_matrix( 1UL, 1UL, 0.0 );
        return return_matrix;
    };

    // The matrix 'result' (1x1) contains an upper bound for the total
    // relative truncation error.
    DMatrix result = leaves_to_root_algorithm( ext_operands, leaf_op_trunc, inner_op_trunc, root_op_trunc, father_data, sumReduction );

    return sqrt( std::abs( result( 0, 0 )) );
}

//
// Returns the singular values of the H-Tensor in a map : modes -> vector of singular values
//

vector< DynamicVector< value_t > > HTensor::singular_values()
{
    orthogonalize();

    // First Part is the same as for the truncation

    // Compute accumulated transfer tensors (root-to-leaves);
    // acc_tr contains the acc. transfer tensors for all tree nodes
    vector< DMatrix > acc_tr( data_.size() );

    function< DMatrix( leaf_data &, const size_t, vector< const leaf_data * >, const DMatrix &, const size_t ) > leaf_op_acc
        = [&]( leaf_data &,
               const size_t,
               vector< const leaf_data * >,
               const DMatrix & father_data,
               const size_t data_index ) -> DMatrix
    {
        // Acc. transfer tensor has been computed by father:
        acc_tr[ data_index ] = father_data;

        // Dummy return matrix.
        DMatrix return_matrix( 1UL, 1UL, 0.0 );
        return return_matrix;
    };

    function< DMatrix( node_data &, vector< const node_data * >, const DMatrix &, DMatrix &, DMatrix &, const size_t ) > inner_op_acc
        = [&]( node_data & data,
               vector< const node_data * >,
               const DMatrix & father_data,
               DMatrix & son1_data,
               DMatrix & son2_data,
               const size_t data_index ) -> DMatrix
    {
        // Acc. transfer tensor has been computed by father:
        acc_tr[ data_index ] = father_data;

        // Compute acc. transfer tensors for sons:
        if ( data.size() > 0 )
        {
            son1_data.resize( data[ 0 ].rows(), data[ 0 ].rows() );
            son2_data.resize( data[ 0 ].columns(), data[ 0 ].columns() );
            son1_data = 0.0;
            son2_data = 0.0;
        }

        node_data transformed_data( data.size() );

        for ( size_t l2 = 0; l2 < data.size(); ++l2 )
        {
            transformed_data[ l2 ] = acc_tr[ data_index ]( 0, l2 ) * data[ 0 ];
            for ( size_t l1 = 1; l1 < data.size(); ++l1 )
            {
                transformed_data[ l2 ] += acc_tr[ data_index ]( l1, l2 ) * data[ l1 ];
            }
        }
        for ( size_t l2 = 0; l2 < data.size(); ++l2 )
        {
            son1_data += transformed_data[ l2 ] * trans( data[ l2 ] );
            son2_data += trans( transformed_data[ l2 ] ) * data[ l2 ];
        }

        // Dummy return matrix.
        DMatrix return_matrix( 1UL, 1UL, 0.0 );
        return return_matrix;
    };

    function< DMatrix( root_data &, vector< const root_data * >, DMatrix &, DMatrix &, const size_t ) > root_op_acc
        = [&]( root_data & data,
               vector< const root_data * >,
               DMatrix & son1_data,
               DMatrix & son2_data,
               const size_t data_index ) -> DMatrix
    {
        // Acc. transfer tensor at root is chosen 1.0 (1x1):
        acc_tr[ data_index ].resize( 1UL, 1UL );
        acc_tr[ data_index ]( 0, 0 ) = 1.0;

        // Acc. transfer tensors for sons are B*B^T and B^T*B (B root transfer tensor):
        son1_data = data * trans( data );
        son2_data = trans( data ) * data;

        // Dummy return matrix.
        DMatrix return_matrix( 1UL, 1UL, 0.0 );
        return return_matrix;
    };

    // No external operands.
    vector< const HTensor * > ext_operands;
    // Dummy matrix for father_data for the call at the first node
    //      (i=0).
    DMatrix father_data;

    root_to_leaves_algorithm( ext_operands, leaf_op_acc, inner_op_acc, root_op_acc, father_data );


    vector< DynamicVector< value_t > > svd_vals( data_.size() );

    // Compute singular values(leaves-to-root):

    function< DMatrix( leaf_data &, const size_t, vector< const leaf_data * >, DMatrix &, const size_t ) > leaf_op_svd
        = [&]( leaf_data &,
               const size_t,
               vector< const leaf_data * >,
               DMatrix & father_data,
               const size_t data_index ) -> DMatrix
    {
        // The eigenvectors will be the columns of 'father_data':
        DynamicVector< value_t > eigenvalues;
        SymmetricMatrix< DMatrix > sym_acc_tr = declsym( acc_tr[ data_index ] );
        eigen( sym_acc_tr, eigenvalues, father_data );
        svd_vals[ data_index ] =  eigenvalues;

        // Dummy return matrix
        DMatrix return_matrix( 1UL, 1UL, 0.0 );
        return return_matrix;
    };

    function< DMatrix( node_data &, vector< const node_data * >, DMatrix &, const DMatrix &, const DMatrix &, const size_t data_index ) > inner_op_svd
        = [&]( node_data &,
               vector< const node_data * >,
               DMatrix & father_data,
               const DMatrix &,
               const DMatrix &,
               const size_t data_index ) -> DMatrix
    {
        // The eigenvectors will be the columns of 'father_data':
        DynamicVector< value_t > eigenvalues;
        SymmetricMatrix< DMatrix > sym_acc_tr = declsym( acc_tr[ data_index ] );
        eigen( sym_acc_tr, eigenvalues, father_data );
        svd_vals[ data_index ] =  eigenvalues;
        // Dummy return matrix
        DMatrix return_matrix( 1UL, 1UL, 0 );
        return return_matrix;
    };

    function< DMatrix( root_data &, vector< const root_data * >, const DMatrix &, const DMatrix &, const size_t ) > root_op_svd
        = [&]( root_data & data,
               vector< const root_data * >,
               const DMatrix & son1_data,
               const DMatrix & son2_data,
               const size_t  ) -> DMatrix
    {
        // Transform the transfer tensor according to son transformations.
        data = trans( son1_data ) * data * son2_data;
        svd_vals[ 0 ] =  DynamicVector< value_t >(1UL);
        svd_vals[ 0 ][ 0 ] = 1;
        // Dummy return matrix.
        DMatrix return_matrix( 1UL, 1UL, 0.0 );
        return return_matrix;
    };

    // The matrix 'result' (1x1) contains an upper bound for the total
    // relative truncation error.
    DMatrix result = leaves_to_root_algorithm( ext_operands, leaf_op_svd, inner_op_svd, root_op_svd, father_data, sumReduction );

    return svd_vals;
}


//
// Fills the tensor with random data from the interval [a,b].
//
void HTensor::random_fill( value_t a, value_t b )
{
    if ( node_used_ )
    {
        for ( size_t i = 0; i < data_.size(); ++i )
        {
            for ( size_t j = 0; j < data_[ i ].size(); ++j )
            {
                //was forEach but blaze renamed it to map
                data_[ i ][ j ] = blaze::map( data_[ i ][ j ], [ a, b ](value_t)
                {
                    return rand< value_t >( a, b );
                } );
            }
        }
    }
}

//
// Fills the tensor with random integers between a and b.
//
void HTensor::random_integer_fill( int a, int b )
{
    if ( node_used_ )
    {
        for ( size_t i = 0; i < data_.size(); ++i )
        {
            for ( size_t j = 0; j < data_[ i ].size(); ++j )
            {
                //was forEach but blaze renamed it to map
                data_[ i ][ j ] = blaze::map( data_[ i ][ j ], [ a, b ](value_t)
                {
                    return static_cast<value_t>( rand< int >( a, b ) );
                } );
            }
        }
    }
}

//
// Prints the content to std::cout.
//
void HTensor::display_content() const
{

    int my_rank = 0;

    vector< const Cluster * > nodes_on_this_rank = tree_.all_nodes_of_rank( my_rank );

    for ( size_t i = 0; i < nodes_on_this_rank.size(); ++i )
    {
        if ( i > 0 )
        {
            std::cout << "----------------------------" << "\n";
        }
        std::cout << *(nodes_on_this_rank[ i ]) << "\n";
        std::cout << "----------------------------" << "\n";
        for ( size_t j = 0; j < data_[ i ].size(); ++j )
        {
            if ( j > 0 )
            {
                std::cout << "----------------------------" << "\n";
            }
            std::cout << data_[ i ][ j ];
        }
        std::cout << "\n";
    }
    std::cout << "\n";
    if ( !node_used_ )
    {
        std::cout << "  --- Not used. ---" << "\n";
    }

}


//
// Prints the dimensions of the leafes and transfer nodes to std::cout.
//
void HTensor::display_dimensions() const
{
    vector<size_t> r(3);
    int my_rank = 0;

    vector< const Cluster * > nodes_on_this_rank = tree_.all_nodes_of_rank( my_rank );

    for ( size_t i = 0; i < nodes_on_this_rank.size(); ++i )
    {
        r[0] = data_[i].size();
        r[1] = data_[i][0].rows();
        r[2] = data_[i][0].columns();

        if(r[0] == 1)
        {
            std::cout << *(nodes_on_this_rank[ i ]) << " : " << "( " << r[1] <<", "<< r[2] << " )\n";
        }
        else
        {
            std::cout << *(nodes_on_this_rank[ i ]) << " : " << "( "<< r[0] << " , " << r[1] <<" , "<< r[2] << " )\n";
        }

        std::cout << "----------------------------" << "\n";

        std::cout << "\n";
    }
    std::cout << "\n";
    if ( !node_used_ )
    {
        std::cout << "  --- Not used. ---" << "\n";
    }

}



//
//  Returns the ranks of one node
//
vector<size_t> HTensor::get_rank(size_t node) const
{
    vector<size_t> r(3);
    r[0] = data_[node].size();
    r[1] = data_[node][0].rows();
    r[2] = data_[node][0].columns();
    return r;
}

//
// Prints the algebraic ranks of the HTensor to std::cout.
//
void HTensor::display_ranks() const
{

    int my_rank = 0;

    vector< const Cluster * > nodes_on_this_rank = tree_.all_nodes_of_rank( my_rank );
    size_t last = nodes_on_this_rank[ 0 ]->last();
    size_t max_rank = 0, min_rank = 10000;
    size_t block_counter = 0;
    size_t block_size = static_cast<size_t>(std::max(std::floor(nodes_on_this_rank[0]->size() / 4), 1.0));


    for ( size_t i = 0; i < nodes_on_this_rank.size(); ++i )
    {

        block_counter++;

        if (nodes_on_this_rank[i]->is_root())
        {
            cout << *(nodes_on_this_rank[ i ]) << " : " << "( " << data_[i][0].rows() << " , " << data_[i][0].columns() << " )";
        }
        else if (nodes_on_this_rank[i]->is_leaf())
        {
            cout << *(nodes_on_this_rank[ i ]) << " : " << "( " << data_[i][0].columns() << " ) \t";
        }
        else
        {
            cout << *(nodes_on_this_rank[ i ]) << " : " << "( " << data_[ i ].size() << " , " << data_[i][0].rows() << " , " << data_[i][0].columns() << " )\t";
        }

        if ( block_counter % block_size == 0 )
        {
            cout << "\n";
        }

        if (max_rank < data_[i][0].columns())
            max_rank = data_[i][0].columns();

        if (min_rank > data_[i][0].columns())
            min_rank = data_[i][0].columns();

        if (last == nodes_on_this_rank[ i ]->last() )
        {
            block_counter = 0;
            cout << "\n" << "\n";
        }
    }
    cout << "\n";
    cout << "Maximal rank : \t " << max_rank << "\t Minimal rank : \t " << min_rank << "\n";
    if ( !node_used_ )
    {
        cout << "  --- Not used. ---" << "\n";
    }

}

//
// Checks if there is any NAN-part (not a number) in the tensor representation.
//
bool HTensor::isnan() const
{
    int my_rank = 0;
    vector< const Cluster * > nodes_on_this_rank = tree_.all_nodes_of_rank( my_rank );

    for ( size_t i = 0; i < nodes_on_this_rank.size(); ++i )
    {
        for ( size_t j = 0; j < data_[ i ].size(); ++j )
        {
            if ( blaze::isnan( data_[ i ][ j ] ) )
            {
                return true;
            }
        }
    }

    return false;
}

//
// Auxiliary for entry().
//
DynamicVector< value_t, rowVector > HTensor::get_row( const vector< size_t > & index, size_t i ) const
{
    DynamicVector< value_t, rowVector >  row_vec;

    int my_rank = 0;
    vector< const Cluster * > nodes_on_this_rank = tree_.all_nodes_of_rank( my_rank );


    if ( node_used_ )
    {
        if ( nodes_on_this_rank[ i ]->is_leaf() )
        {
            // This is a 'global' leaf of the whole tree. Extract row from data_.
            row_vec = row( data_[ i ][ 0 ], index[ nodes_on_this_rank[ i ]->leaf_number() ] );
        }
        else
        {
            if ( nodes_on_this_rank[ i ]->son1()->mpi_rank() == my_rank )
            {
                size_t i_son1 = 0;
                size_t i_son2 = 0;
                for ( size_t j = i; j < nodes_on_this_rank.size(); ++j )
                {
                    if ( nodes_on_this_rank[ j ] == nodes_on_this_rank[ i ]->son1() )
                    {
                        i_son1 = j;
                    }
                    else if ( nodes_on_this_rank[ j ] == nodes_on_this_rank[ i ]->son2() )
                    {
                        i_son2 = j;
                    }
                }
                DynamicVector< value_t, rowVector >  row_vec_son1 = get_row( index, i_son1 );
                DynamicVector< value_t, rowVector >  row_vec_son2 = get_row( index, i_son2 );

                // Compute row_vec.
                row_vec.resize( data_[ i ].size() );
                for ( size_t j = 0; j < row_vec.size(); ++j )
                {
                    row_vec[ j ] = row_vec_son1 * data_[ i ][ j ] * trans( row_vec_son2 );
                }
            }
            else
            {
            }// else
        }// else
    }// if ( node_used_ )

    return row_vec;
}

//
// Auxiliary for dot().
//
void HTensor::inner_product_matrix( const HTensor & X, DynamicMatrix< value_t, columnMajor > & G, size_t i ) const
{
    int my_rank = 0;
    vector< const Cluster * > nodes_on_this_rank = tree_.all_nodes_of_rank( my_rank );

    if ( node_used_ )
    {
        if ( nodes_on_this_rank[ i ]->is_leaf() )
        {
            // This is a 'global' leaf of the whole tree. Compute the inner product matrix.
            if ( data_[ i ].size() > 0 && X.data_[ i ].size() > 0 )
            {
                G = trans( data_[ i ][ 0 ] ) * X.data_[ i ][ 0 ];
            }
            else
            {
                G.resize(0, 0);
            }
        }
        else
        {
            if ( nodes_on_this_rank[ i ]->son1()->mpi_rank() == my_rank )
            {
                //find the sons;
                size_t i_son1 = 0;
                size_t i_son2 = 0;
                for ( size_t j = i; j < nodes_on_this_rank.size(); ++j )
                {
                    if ( nodes_on_this_rank[ j ] == nodes_on_this_rank[ i ]->son1() )
                    {
                        i_son1 = j;
                    }
                    else if ( nodes_on_this_rank[ j ] == nodes_on_this_rank[ i ]->son2() )
                    {
                        i_son2 = j;
                    }
                }

                DynamicMatrix< value_t, columnMajor >    G_son1;
                DynamicMatrix< value_t, columnMajor >    G_son2;

                inner_product_matrix( X, G_son1, i_son1 );
                inner_product_matrix( X, G_son2, i_son2 );

                // Compute inner product matrix.
                const std::size_t G_rows = data_[ i ].size();
                const std::size_t G_columns = X.data_[ i ].size();

                G.resize( G_rows, G_columns );
                G = 0.0;

                for ( std::size_t l2 = 0; l2 < G_columns; ++l2 )
                {
                    DynamicMatrix< value_t, columnMajor > G1_X_G2 = G_son1 * X.data_[ i ][ l2 ] * blaze::trans( G_son2 );
                    for ( std::size_t l1 = 0; l1 < G_rows; ++l1 )
                    {
                        //this was
                        // value_t dot = 0.0;
                        // for ( size_t j = 0; j < G1_X_G2.columns(); ++j )
                        // {
                        //     dot += blaze::dot( column( G1_X_G2, j ), column( data_[ i ][ l1 ], j ) );
                        // }
                        // G( l1, l2 ) = dot;
                        //now we use this
                        //maybe we should assert if G1_X_G2 and data_[i][l1] have the same size
                        G( l1, l2 ) = blaze::sum(G1_X_G2 % data_[i][l1]);
                    }
                }
            }// if ( nodes_on_this_rank[ i ]->son1()->mpi_rank() == my_rank )
        }// else

    }// if ( node_used_ )
}

//
// Auxiliary for dot() with restricted index range.
//
void HTensor::inner_product_matrix_restricted( const HTensor & X, DynamicMatrix< value_t, columnMajor > & G, const vector< size_t > & lower_index, const vector< size_t > & upper_index, size_t i ) const
{
    int my_rank = 0;
    vector< const Cluster * > nodes_on_this_rank = tree_.all_nodes_of_rank( my_rank );
    G.resize( 0, 0 );

    if ( node_used_ )
    {
        if ( nodes_on_this_rank[ i ]->is_leaf() )
        {
            // This is a 'global' leaf of the whole tree. Compute the inner product matrix.
            if ( data_[ i ].size() > 0 && X.data_[ i ].size() > 0 )
            {
                size_t index_start = lower_index[ nodes_on_this_rank[ i ]->leaf_number() ];
                size_t range_size  = upper_index[ nodes_on_this_rank[ i ]->leaf_number() ] - lower_index[ nodes_on_this_rank[ i ]->leaf_number() ] + 1;

                Submatrix< const DynamicMatrix< value_t, columnMajor > > submatrix_this = submatrix( data_[ i ][ 0 ], index_start, 0UL, range_size, data_[ i ][ 0 ].columns() );
                Submatrix< const DynamicMatrix< value_t, columnMajor > > submatrix_X = submatrix( X.data_[ i ][ 0 ], index_start, 0UL, range_size, X.data_[ i ][ 0 ].columns() );

                G = trans( submatrix_this ) * submatrix_X;
            }
        }
        else
        {
            if ( nodes_on_this_rank[ i ]->son1()->mpi_rank() == my_rank )
            {
                DynamicMatrix< value_t, columnMajor >    G_son1;
                DynamicMatrix< value_t, columnMajor >    G_son2;

                //both where not initalized
                size_t i_son1 = 0;
                size_t i_son2 = 0;
                for ( size_t j = i; j < nodes_on_this_rank.size(); ++j )
                {
                    if ( nodes_on_this_rank[ j ] == nodes_on_this_rank[ i ]->son1() )
                    {
                        i_son1 = j;
                    }
                    else if ( nodes_on_this_rank[ j ] == nodes_on_this_rank[ i ]->son2() )
                    {
                        i_son2 = j;
                    }
                }
                inner_product_matrix_restricted( X, G_son1, lower_index, upper_index, i_son1 );
                inner_product_matrix_restricted( X, G_son2, lower_index, upper_index, i_son2 );

                // Compute inner product matrix.
                G.resize( data_[ i ].size(), X.data_[ i ].size() );
                DynamicMatrix< value_t, columnMajor > tmp1;
                for ( size_t l2 = 0; l2 < G.columns(); ++l2 )
                {
                    tmp1 = G_son1 * X.data_[ i ][ l2 ] * trans( G_son2 );
                    for ( size_t l1 = 0; l1 < G.rows(); ++l1 )
                    {
                        value_t dot = 0.0;
                        for ( size_t j = 0; j < tmp1.columns(); ++j )
                        {
                            dot += ( column( tmp1, j ), column( data_[ i ][ l1 ], j ) );
                        }
                        G( l1, l2 ) = dot;
                    }
                }
            }
        }

    }// if ( node_used_ )
}

//
// Auxiliary for dot() with restricted index range.
//
void HTensor::inner_product_matrix_restricted( const HTensor & X, DynamicMatrix< value_t, columnMajor > & G, size_t mu, const vector< size_t > & indices, size_t i ) const
{
    int my_rank = 0;
    vector< const Cluster * > nodes_on_this_rank = tree_.all_nodes_of_rank( my_rank );
    G.resize( 0, 0 );

    if ( node_used_ )
    {
        if ( nodes_on_this_rank[ i ]->is_leaf() )
        {
            // This is a 'global' leaf of the whole tree. Compute the inner product matrix.
            if ( data_[ i ].size() > 0 && X.data_[ i ].size() > 0 )
            {
                if ( nodes_on_this_rank[ i ]->leaf_number() == mu )
                {
                    G = DynamicMatrix< value_t, columnMajor >( data_[ i ][ 0 ].columns(), X.data_[ i ][ 0 ].columns(), 0.0 );
                    for ( size_t j = 0; j < indices.size(); ++j )
                    {
                        G = G + trans( row( data_[ i ][ 0 ], indices[ j ] ) ) * row( X.data_[ i ][ 0 ], indices[ j ] );
                    }
                }
                else
                {
                    G = trans( data_[ i ][ 0 ] ) * X.data_[ i ][ 0 ];
                }
            }
        }
        else
        {
            if ( nodes_on_this_rank[ i ]->son1()->mpi_rank() == my_rank )
            {
                DynamicMatrix< value_t, columnMajor >    G_son1;
                DynamicMatrix< value_t, columnMajor >    G_son2;

                //both where not initalized
                size_t i_son1 = 0;
                size_t i_son2 = 0;
                for ( size_t j = i; j < nodes_on_this_rank.size(); ++j )
                {
                    if ( nodes_on_this_rank[ j ] == nodes_on_this_rank[ i ]->son1() )
                    {
                        i_son1 = j;
                    }
                    else if ( nodes_on_this_rank[ j ] == nodes_on_this_rank[ i ]->son2() )
                    {
                        i_son2 = j;
                    }
                }
                inner_product_matrix_restricted( X, G_son1, mu, indices, i_son1 );
                inner_product_matrix_restricted( X, G_son2, mu, indices, i_son2 );

                // Compute inner product matrix.
                G.resize( data_[ i ].size(), X.data_[ i ].size() );
                DynamicMatrix< value_t, columnMajor > tmp1;
                for ( size_t l2 = 0; l2 < G.columns(); ++l2 )
                {
                    tmp1 = G_son1 * X.data_[ i ][ l2 ] * trans( G_son2 );
                    for ( size_t l1 = 0; l1 < G.rows(); ++l1 )
                    {
                        value_t dot = 0.0;
                        for ( size_t j = 0; j < tmp1.columns(); ++j )
                        {
                            dot += ( column( tmp1, j ), column( data_[ i ][ l1 ], j ) );
                        }
                        G( l1, l2 ) = dot;
                    }
                }
            }
            else
            {
                CustomMatrix< value_t, aligned, usePadding, columnMajor >    G_son1;
                CustomMatrix< value_t, aligned, usePadding, columnMajor >    G_son2;
                // Compute inner product matrix.
                G.resize( data_[ i ].size(), X.data_[ i ].size() );

                DynamicMatrix< value_t, columnMajor > tmp1;
                for ( size_t l2 = 0; l2 < G.columns(); ++l2 )
                {
                    tmp1 = G_son1 * X.data_[ i ][ l2 ] * trans( G_son2 );
                    for ( size_t l1 = 0; l1 < G.rows(); ++l1 )
                    {
                        value_t dot = 0.0;
                        for ( size_t j = 0; j < tmp1.columns(); ++j )
                        {
                            dot += ( column( tmp1, j ), column( data_[ i ][ l1 ], j ) );
                        }
                        G( l1, l2 ) = dot;
                    }
                }
            }
        }

        if ( !nodes_on_this_rank[ i ]->is_root() )
        {
            if ( nodes_on_this_rank[ i ]->father()->mpi_rank() != my_rank )
            {
            }
        }
    }// if ( node_used_ )
}

//
// Auxiliary for dot() with restricted index range.
//
void HTensor::inner_product_matrix_restricted( const HTensor & X, DynamicMatrix< value_t, columnMajor > & G, const vector< vector< size_t > > & indices, size_t i ) const
{
    int my_rank = 0;
    vector< const Cluster * > nodes_on_this_rank = tree_.all_nodes_of_rank( my_rank );
    G.resize( 0, 0 );


    if ( node_used_ )
    {
        if ( nodes_on_this_rank[ i ]->is_leaf() )
        {
            // This is a 'global' leaf of the whole tree. Compute the inner product matrix.
            size_t mu = nodes_on_this_rank[ i ]->leaf_number();
            if ( data_[ i ].size() > 0 && X.data_[ i ].size() > 0 )
            {
                if ( !indices[ mu ].empty() )
                {
                    G = DynamicMatrix< value_t, columnMajor >( data_[ i ][ 0 ].columns(), X.data_[ i ][ 0 ].columns(), 0.0 );
                    for ( size_t j = 0; j < indices[ mu ].size(); ++j )
                    {
                        G = G + trans( row( data_[ i ][ 0 ], indices[ mu ][ j ] ) ) * row( X.data_[ i ][ 0 ], indices[ mu ][ j ] );
                    }
                }
                else
                {
                    G = trans( data_[ i ][ 0 ] ) * X.data_[ i ][ 0 ];
                }
            }
        }
        else
        {
            if ( nodes_on_this_rank[ i ]->son1()->mpi_rank() == my_rank )
            {
                DynamicMatrix< value_t, columnMajor >    G_son1;
                DynamicMatrix< value_t, columnMajor >    G_son2;

                //both where not initalized
                size_t i_son1 = 0;
                size_t i_son2 = 0;
                for ( size_t j = i; j < nodes_on_this_rank.size(); ++j )
                {
                    if ( nodes_on_this_rank[ j ] == nodes_on_this_rank[ i ]->son1() )
                    {
                        i_son1 = j;
                    }
                    else if ( nodes_on_this_rank[ j ] == nodes_on_this_rank[ i ]->son2() )
                    {
                        i_son2 = j;
                    }
                }
                inner_product_matrix_restricted( X, G_son1, indices, i_son1 );
                inner_product_matrix_restricted( X, G_son2, indices, i_son2 );

                // Compute inner product matrix.
                G.resize( data_[ i ].size(), X.data_[ i ].size() );
                DynamicMatrix< value_t, columnMajor > tmp1;
                for ( size_t l2 = 0; l2 < G.columns(); ++l2 )
                {
                    tmp1 = G_son1 * X.data_[ i ][ l2 ] * trans( G_son2 );
                    for ( size_t l1 = 0; l1 < G.rows(); ++l1 )
                    {
                        value_t dot = 0.0;
                        for ( size_t j = 0; j < tmp1.columns(); ++j )
                        {
                            dot += ( column( tmp1, j ), column( data_[ i ][ l1 ], j ) );
                        }
                        G( l1, l2 ) = dot;
                    }
                }
            }
            else
            {
                CustomMatrix< value_t, aligned, usePadding, columnMajor >    G_son1;
                CustomMatrix< value_t, aligned, usePadding, columnMajor >    G_son2;

                // Compute inner product matrix.
                G.resize( data_[ i ].size(), X.data_[ i ].size() );

                DynamicMatrix< value_t, columnMajor > tmp1;
                for ( size_t l2 = 0; l2 < G.columns(); ++l2 )
                {
                    tmp1 = G_son1 * X.data_[ i ][ l2 ] * trans( G_son2 );
                    for ( size_t l1 = 0; l1 < G.rows(); ++l1 )
                    {
                        value_t dot = 0.0;
                        for ( size_t j = 0; j < tmp1.columns(); ++j )
                        {
                            dot += ( column( tmp1, j ), column( data_[ i ][ l1 ], j ) );
                        }
                        G( l1, l2 ) = dot;
                    }
                }
            }
        }
    }// if ( node_used_ )
}


inline void multiply_with_son_factors(const blaze::DynamicMatrix< value_t, blaze::columnMajor >& R_son1, const blaze::DynamicMatrix< value_t, blaze::columnMajor >& R_son2, blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > >& data_i)
{
    const std::size_t data_i_size = data_i.size();

    constexpr std::size_t data_i_threshold = 71;

    if ( htensor_use_shared_memory_ && ( data_i_size > data_i_threshold ) )
    {
        ParallelFor(0UL, data_i_size, [&R_son1, &R_son2, &data_i](std::size_t j)
        {
            data_i[j] = R_son1 * data_i[j] * blaze::trans( R_son2 );
        });
    }
    else
    {
        std::for_each(
            //if we use c++17 we can parallelize this easy
            //std::execution::par_unseq,
            data_i.begin(),
            data_i.end(),
            [&R_son1, &R_son2](blaze::DynamicMatrix< value_t, blaze::columnMajor >& item)
        {
            item = R_son1 * item * blaze::trans( R_son2 );
        });
    }
}

//
// Auxiliary for orthogonalize(), orthogonalizes node i.
// Right factor is stored in R.
//
void HTensor::orthogonalize_node( DynamicMatrix< value_t, columnMajor >& R, size_t i )
{
    int my_rank = 0;
    const vector< const Cluster * > nodes_on_this_rank = tree_.all_nodes_of_rank( my_rank );


    if ( node_used_ )
    {
        if ( !nodes_on_this_rank[ i ]->is_leaf() )
        {
            // Get right factors from sons.
            if ( nodes_on_this_rank[ i ]->son1()->mpi_rank() == my_rank )
            {
                DynamicMatrix< value_t, columnMajor > R_son1;
                DynamicMatrix< value_t, columnMajor > R_son2;

                //both where not initalized
                size_t i_son1 = 0;
                size_t i_son2 = 0;
                for ( size_t j = i; j < nodes_on_this_rank.size(); ++j )
                {
                    if ( nodes_on_this_rank[ j ] == nodes_on_this_rank[ i ]->son1() )
                    {
                        i_son1 = j;
                    }
                    else if ( nodes_on_this_rank[ j ] == nodes_on_this_rank[ i ]->son2() )
                    {
                        i_son2 = j;
                    }
                }

                orthogonalize_node( R_son1, i_son1 );
                orthogonalize_node( R_son2, i_son2 );

                // Multiply with son factors.

                // blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > >& data_i = data_[ i ];
                // multiply_with_son_factors(R_son1, R_son2, data_i);
                multiply_with_son_factors(R_son1, R_son2, data_[ i ]);
            }
        }

        // QR decomposition (if not on root node).
        if ( !nodes_on_this_rank[ i ]->is_root() )
        {
            if ( nodes_on_this_rank[ i ]->is_leaf() )
            {
                // QR decomposition for leaves
                remove_small_non_zeros( data_[ i ][ 0 ], pow( std::numeric_limits< value_t >::min(), 0.25 ) );

                qr( data_[ i ][ 0 ], data_[ i ][ 0 ], R );
            }
            else
            {
                // QR decomposition for inner nodes
                qr_transfer_tensor( data_[ i ], R );
            }

        }
    }// if ( node_used_ )
}

//is correct
void HTensor::orthogonalize_node_with_respect_to_father( std::size_t orthogonalize_node_number, std::size_t father_number )
{
    blaze::DynamicMatrix<value_t, blaze::columnMajor> R;
    if ( is_leaf(orthogonalize_node_number) )
    {
        //we are an leaf of size n_1 x r_1 and our parent is a transfer tensor or the root tensor of size r_1 x r_2 x r_12 or r_1 x r_2
        blaze::qr(data_[orthogonalize_node_number][0], data_[orthogonalize_node_number][0], R);
        //R is r_1 x r_1 after that
    }
    else
    {
        //we are an inner node of size r_1 x r_2 x r_12 and our parent is a transfer tensor or the root tensor of size r_12 x r_34 x r_1234
        qr_transfer_tensor(data_[orthogonalize_node_number], R);
    }
    if ( is_left_son_of_parent(orthogonalize_node_number, father_number ) )
    {
        for ( auto& data_block : data_[father_number])
        {
            data_block = R * data_block;
        }
    }
    else
    {
        for ( auto& data_block : data_[father_number])
        {
            data_block = data_block * blaze::trans(R);
        }
    }

}

void HTensor::orthogonalize_node_with_respect_to_child(std::size_t orthogonalize_node_number, std::size_t respect_to_node_number)
{
    const std::vector<std::size_t> child_number = get_child_numbers(orthogonalize_node_number);
    //if we need to orth with respect to our left child
    if (child_number[0] == respect_to_node_number)
    {
        orthogonalize_node_with_respect_to_child_number(orthogonalize_node_number, respect_to_node_number, 0UL);
    }
    else
    {
        orthogonalize_node_with_respect_to_child_number(orthogonalize_node_number, respect_to_node_number, 1UL);
    }
}

void HTensor::orthogonalize_node_with_respect_to_child_number(std::size_t orthogonalize_node_number, std::size_t respect_to_node_number, std::size_t child_number)
{
    if ( is_root(orthogonalize_node_number) )
    {
        //our left child is in r_1 x r_2 x r_12 or in n_1 x r_12 and we are in r_12 x r_34
        blaze::DynamicMatrix<value_t, blaze::columnMajor> R;
        if (0 == child_number)
        {
            // blaze::qr( blaze::trans( data_[orthogonalize_node_number][0] ), data_[orthogonalize_node_number][0], R);
            // blaze::lq( data_[orthogonalize_node_number][0], R, data_[orthogonalize_node_number][0]);
            // blaze::transpose(data_[orthogonalize_node_number][0]);
            // blaze::transpose(R);

            blaze::rq( data_[orthogonalize_node_number][0], R, data_[orthogonalize_node_number][0]);
            // blaze::transpose(data_[orthogonalize_node_number][0]);
            // blaze::transpose(R);
        }
        else
        {
            // blaze::qr(data_[orthogonalize_node_number][0], data_[orthogonalize_node_number][0], R);
            blaze::ql(data_[orthogonalize_node_number][0], data_[orthogonalize_node_number][0], R);
            blaze::transpose(R);
        }

        if ( is_leaf(respect_to_node_number) )
        {
            //3 resp_node is the left_son, and leaf and therefore n_1 x r_1 and we are the root of r_1 x r_2
            data_[respect_to_node_number][0] *= R;
        }
        else
        {
            // resp_node is the left_son, then it would have r_1 x r_2 x r_12 as rank and we are the root of size r_12 x r_34

            const std::size_t size = data_[respect_to_node_number].size();
            const std::size_t rows = data_[respect_to_node_number][0].rows();
            const std::size_t cols = data_[respect_to_node_number][0].columns();

            blaze::DynamicVector<blaze::DynamicMatrix<double, blaze::columnMajor>> temp( size,  blaze::DynamicMatrix<double, blaze::columnMajor>(rows, cols, 0.0 ) );

            if (0 == child_number)
            {
                for (std::size_t i = 0; i < size; ++i)
                {
                    for (std::size_t j = 0; j <= i; ++j)
                    {
                        temp[i] += data_[respect_to_node_number][j] * R(j, i);
                    }
                }
            }
            else
            {
                for (std::size_t i = 0; i < size; ++i)
                {
                    for (std::size_t j = 0; j <= i; ++j)
                    {
                        temp[i] += data_[respect_to_node_number][j] * R(j, i);
                    }
                }
            }
            data_[respect_to_node_number] = temp;
        }
    }
    else
    {
        blaze::DynamicMatrix<value_t, blaze::columnMajor> R;
        if (0 == child_number)
        {
            //we are r_1 x r_2 x r_12 and we reshape into r_1 x (r_2 * r_12)
            blaze::DynamicMatrix<double, blaze::columnMajor> reshaped_data = reshape_3D_into_2D(data_[orthogonalize_node_number], child_number);

            blaze::rq(reshaped_data, R, reshaped_data);

            const auto size_dim = get_tensor_rank(orthogonalize_node_number);
            data_[orthogonalize_node_number] = reshape_2D_into_3D(reshaped_data, child_number, std::get<1>(size_dim), std::get<2>(size_dim), std::get<0>(size_dim) );
            //then reshape back from (r_1 * r_2) x r_12 to r_1 x r_2 x r_12
        }
        else
        {
            //we are r_1 x r_2 x r_12 and we reshape into (r_1 * r_12) x r_2

            const std::size_t dim_0 = data_[orthogonalize_node_number][0].rows();
            const std::size_t dim_1 = data_[orthogonalize_node_number][0].columns();
            const std::size_t dim_2 = data_[orthogonalize_node_number].size();

            blaze::DynamicMatrix<double, blaze::columnMajor> reshaped_data = blaze::trans( reshape_3D_into_2D(data_[orthogonalize_node_number], child_number) );

            blaze::ql(reshaped_data, reshaped_data, R);
            blaze::transpose(R);

            data_[orthogonalize_node_number] = reshape_2D_into_3D(blaze::trans( reshaped_data ), child_number, dim_0, dim_1, dim_2 );

        }
        // std::cout << " diff inner leaf 1 " << (*this - save_this).norm() << "\n";

        // std::cout << " diff inner leaf 3 " << (*this - save_this).norm() << "\n";
        // std::cout << " norm r " << blaze::norm(R) << "\n";
        if ( is_leaf(respect_to_node_number) )
        {
            //3 resp_node is the left_son, and leaf and therefore n_1 x r_1 and we are the inner node of r_1 x r_2 x r_12
            data_[respect_to_node_number][0] *= R;
        }
        else
        {
            //3 resp_node is the left_son, then it would have r_1 x r_2 x r_12 as rank and we are the inner of size r_12 x r_34
            const std::size_t size = data_[respect_to_node_number].size();
            const std::size_t rows = data_[respect_to_node_number][0].rows();
            const std::size_t cols = data_[respect_to_node_number][0].columns();

            blaze::DynamicVector<blaze::DynamicMatrix<double, blaze::columnMajor>> temp( size,  blaze::DynamicMatrix<double, blaze::columnMajor>(rows, cols, 0.0 ) );

            for (std::size_t i = 0; i < size; ++i)
            {
                for (std::size_t j = 0; j < size; ++j)
                {
                    temp[i] += data_[respect_to_node_number][j] * R(j, i);
                }
            }

            data_[respect_to_node_number] = temp;
        }
    }
}

void HTensor::orthogonalize_node_with_respect_to( std::size_t orthogonalize_node_number, std::size_t respect_to_node_number )
{
    //if orthogonalize_node_number == respect_to_node_number means we orthogonlize with respect to our father
    if ( respect_to_node_number == orthogonalize_node_number )
    {
        orthogonalize_node_with_respect_to_father(orthogonalize_node_number, get_parent_number( respect_to_node_number ) );
    }
    else if ( get_parent_number( orthogonalize_node_number ) == respect_to_node_number)
    {
        orthogonalize_node_with_respect_to_father(orthogonalize_node_number, respect_to_node_number);
    }
    else
    {
        orthogonalize_node_with_respect_to_child(orthogonalize_node_number, respect_to_node_number);
    }
}

//
// Auxiliary for joint_orthonormal_bases().
//
void joint_orthonormal_bases_node( const vector< HTensor * > & ht_pt, DynamicMatrix< value_t, columnMajor > & R, size_t i )
{
    int my_rank = 0;

    vector< const Cluster * > nodes_on_this_rank = ht_pt[ 0 ]->tree_.all_nodes_of_rank( my_rank );


    if ( ht_pt[ 0 ]->node_used_ )
    {
        if ( !nodes_on_this_rank[ i ]->is_leaf() )
        {
            // Get right factors from sons.
            if ( nodes_on_this_rank[ i ]->son1()->mpi_rank() == my_rank )
            {
                DynamicMatrix< value_t, columnMajor > R_son1;
                DynamicMatrix< value_t, columnMajor > R_son2;

                //both where not initialized
                size_t i_son1 = 0;
                size_t i_son2 = 0;
                for ( size_t j = i; j < nodes_on_this_rank.size(); ++j )
                {
                    if ( nodes_on_this_rank[ j ] == nodes_on_this_rank[ i ]->son1() )
                    {
                        i_son1 = j;
                    }
                    else if ( nodes_on_this_rank[ j ] == nodes_on_this_rank[ i ]->son2() )
                    {
                        i_son2 = j;
                    }
                }
                joint_orthonormal_bases_node( ht_pt, R_son1, i_son1 );
                joint_orthonormal_bases_node( ht_pt, R_son2, i_son2 );

                // Multiply with son factors.
                size_t off_1 = 0;
                size_t off_2 = 0;
                size_t R1_rows = R_son1.rows();
                size_t R2_rows = R_son2.rows();
                for ( size_t l = 0; l < ht_pt.size(); ++l )
                {
                    size_t R1_cols = ht_pt[ l ]->data_[ i ][ 0 ].rows();
                    size_t R2_cols = ht_pt[ l ]->data_[ i ][ 0 ].columns();
                    for ( size_t j = 0; j < ht_pt[ l ]->data_[ i ].size(); ++j )
                    {
                        ht_pt[ l ]->data_[ i ][ j ] = submatrix( R_son1, 0UL, off_1, R1_rows, R1_cols ) * ht_pt[ l ]->data_[ i ][ j ] * trans( submatrix( R_son2, 0UL, off_2, R2_rows, R2_cols ) );
                    }
                    off_1 += R1_cols;
                    off_2 += R2_cols;
                }
            }
            else
            {
                CustomMatrix< value_t, aligned, usePadding, columnMajor > R_son1;
                CustomMatrix< value_t, aligned, usePadding, columnMajor > R_son2;
                // Multiply with son factors.
                size_t off_1 = 0;
                size_t off_2 = 0;
                size_t R1_rows = R_son1.rows();
                size_t R2_rows = R_son2.rows();
                for ( size_t l = 0; l < ht_pt.size(); ++l )
                {
                    size_t R1_cols = ht_pt[ l ]->data_[ i ][ 0 ].rows();
                    size_t R2_cols = ht_pt[ l ]->data_[ i ][ 0 ].columns();
                    for ( size_t j = 0; j < ht_pt[ l ]->data_[ i ].size(); ++j )
                    {
                        ht_pt[ l ]->data_[ i ][ j ] = submatrix( R_son1, 0UL, off_1, R1_rows, R1_cols ) * ht_pt[ l ]->data_[ i ][ j ] * trans( submatrix( R_son2, 0UL, off_2, R2_rows, R2_cols ) );
                    }
                    off_1 += R1_cols;
                    off_2 += R2_cols;
                }
            }
        }

        // QR decomposition (if not on root node).
        if ( !nodes_on_this_rank[ i ]->is_root() )
        {
            if ( nodes_on_this_rank[ i ]->is_leaf() )
            {
                // Join leaf frames
                size_t rows = ht_pt[ 0 ]->data_[ i ][ 0 ].rows();
                size_t cols = 0;
                for ( size_t l = 0; l < ht_pt.size(); ++l )
                {
                    cols += ht_pt[ l ]->data_[ i ][ 0 ].columns();
                }
                DynamicMatrix< value_t, columnMajor > joint_leaves( rows, cols );
                size_t off = 0;
                for ( size_t l = 0; l < ht_pt.size(); ++l )
                {
                    size_t sub_cols = ht_pt[ l ]->data_[ i ][ 0 ].columns();
                    submatrix( joint_leaves, 0UL, off, rows, sub_cols ) = ht_pt[ l ]->data_[ i ][ 0 ];
                    off += sub_cols;
                }

                // QR decomposition for leaves
                remove_small_non_zeros( joint_leaves, pow( std::numeric_limits< value_t >::min(), 0.25 ) );
                reduced_qr( joint_leaves, joint_leaves, R );

                // Update the respective node for all HTensors in ht_pt
                for ( size_t l = 0; l < ht_pt.size(); ++l )
                {
                    ht_pt[ l ]->data_[ i ][ 0 ] = joint_leaves;
                }
            }
            else
            {
                // Joint transfer tensors
                size_t layers = 0;
                for ( size_t l = 0; l < ht_pt.size(); ++l )
                {
                    layers += ht_pt[ l ]->data_[ i ].size();
                }
                DynamicVector< DynamicMatrix< value_t, columnMajor > > joint_data( layers );
                size_t off = 0;
                for ( size_t l = 0; l < ht_pt.size(); ++l )
                {
                    size_t sub_layers = ht_pt[ l ]->data_[ i ].size();
                    for ( size_t j = 0; j < sub_layers; ++j )
                    {
                        joint_data[ off + j ] = ht_pt[ l ]->data_[ i ][ j ];
                    }
                    off += sub_layers;
                }

                // QR decomposition for inner nodes
                reduced_qr_transfer_tensor( joint_data, R );

                // Update the respective node for all HTensors in ht_pt
                for ( size_t l = 0; l < ht_pt.size(); ++l )
                {
                    ht_pt[ l ]->data_[ i ] = joint_data;
                }
            }

        }
    }// if ( node_used_ )
}

inline void multiply_with_son_eigenvalues(const blaze::DynamicMatrix< value_t, blaze::columnMajor >& eigenvectors_i_son1, const blaze::DynamicMatrix< value_t, blaze::columnMajor >& eigenvectors_i_son2, blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > >& data_i)
{
    const std::size_t data_i_size = data_i.size();

    constexpr std::size_t data_i_threshold = 71;

    if ( htensor_use_shared_memory_ && ( data_i_size > data_i_threshold ) )
    {
        ParallelFor(0UL, data_i_size, [&eigenvectors_i_son1, &eigenvectors_i_son2, &data_i](std::size_t j)
        {
            data_i[j] = blaze::trans( eigenvectors_i_son1 ) * data_i[j] * eigenvectors_i_son2;
        });
    }
    else
    {
        std::for_each(
            //if we use c++17 we can parallelize this easy
            //std::execution::par_unseq,
            data_i.begin(),
            data_i.end(),
            [&eigenvectors_i_son1, &eigenvectors_i_son2](blaze::DynamicMatrix< value_t, blaze::columnMajor >& item)
        {
            item = blaze::trans( eigenvectors_i_son1 ) * item * eigenvectors_i_son2;
        });
    }
}


inline void compute_eigenvalues_and_eigenvectors_of_acc_transfer_tensor(std::size_t tensor_dim, double eps, std::size_t k, const blaze::DynamicMatrix< value_t, blaze::columnMajor >& acc_tr_tensor, blaze::DynamicVector< value_t, blaze::columnVector >& eigenvalues, blaze::DynamicMatrix< value_t, blaze::columnMajor >& eigenvectors, double & sq_rel_error)
{
    SymmetricMatrix< DynamicMatrix< value_t, columnMajor > > sym_acc_tr = declsym( acc_tr_tensor );
    blaze::eigen( sym_acc_tr, eigenvalues, eigenvectors );

    size_t k_eps = eigenvalues.size();
    if ( eps > 0.0 )
    {
        double sum_lambda = 0.0;
        const double kquasi_best_factor = 2.0 * tensor_dim - 3.0;
        for ( std::size_t i = 0; i < eigenvalues.size(); ++i )
        {
            sum_lambda += eigenvalues[ i ];
        }
        double sum_tr_lambda = 0.0;
        for ( std::size_t i = 0; i < eigenvalues.size(); ++i )
        {
            sum_tr_lambda += eigenvalues[ i ];
            if ( ( sum_tr_lambda / sum_lambda < std::pow( eps, 2 ) / kquasi_best_factor) && (k_eps > 0) )
            {
                --k_eps;
            }
            else
            {
                break;
            }
        }
    }

    // Only keep the eigenvectors for the k0 largest eigenvalues, k0 := min( k, k_eps ).
    std::size_t k0 = std::min( k, k_eps );

    eigenvectors = submatrix( eigenvectors, 0UL, eigenvectors.columns() - k0, eigenvectors.rows(), k0 );


    // Squared relative truncation error:
    double tensor_norm = 0.0;
    for ( std::size_t j = 0; j < eigenvalues.size(); ++j )
    {
        tensor_norm += std::fabs( eigenvalues[ j ] );
    }
    sq_rel_error = 0.0;
    for ( size_t j = 0; j < eigenvalues.size() - k0; ++j )
    {
        sq_rel_error += fabs( eigenvalues[ j ] );
    }
    sq_rel_error /= tensor_norm;
}

inline blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > > truncate_inner_node(const blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > >& data_i,  const blaze::DynamicMatrix< value_t, blaze::columnMajor >& eigenvectors_i)
{
    const std::size_t keigenvectors_columns = eigenvectors_i.columns();
    const std::size_t keigenvectors_rows = eigenvectors_i.rows();

    DynamicVector< DynamicMatrix< value_t, columnMajor > >  new_data( keigenvectors_columns, blaze::DynamicMatrix< value_t, blaze::columnMajor >(data_i[ 0 ].rows(), data_i[ 0 ].columns(), 0.0 ) );

    constexpr std::size_t keigenvectors_rows_cols_threshold = 63;

    if ( htensor_use_shared_memory_ && ( ( keigenvectors_rows > keigenvectors_rows_cols_threshold ) || ( keigenvectors_columns > keigenvectors_rows_cols_threshold ) ) )
    {
        ParallelFor(0UL, keigenvectors_columns, [&new_data, &eigenvectors_i, keigenvectors_rows, &data_i](std::size_t j)
        {
            for ( size_t l = 0; l < keigenvectors_rows; ++l )
            {
                new_data[ j ] += eigenvectors_i( l, j ) * data_i[ l ];
            }
        });
    }
    else
    {
        for ( size_t j = 0; j < keigenvectors_columns; ++j )
        {
            for ( size_t l = 0; l < keigenvectors_rows; ++l )
            {
                new_data[ j ] += eigenvectors_i( l, j ) * data_i[ l ];
            }
        }
    }
    return new_data;
}
//
// Auxiliary for the truncation of orthogonalized tensors.
// The relative accuracy is controlled by eps, whereas k is an upper bound for all ranks.
// If both, eps and k are specified, the option which results in a lower tensor rank will be chosen.
// To specify only eps, choose k = -1 which results in k = max( size_t ).
// To specify only k, choose eps < 0, e.g. eps = -1.
//
void HTensor::truncate_orthogonalized_node( const vector< DynamicMatrix< value_t, columnMajor > > & acc_tr, vector< DynamicMatrix< value_t, columnMajor > > & eigenvectors, vector< double > & sq_rel_errors, size_t i, size_t k, double eps )
{

    int my_rank = 0;
    const vector< const Cluster * > nodes_on_this_rank = tree_.all_nodes_of_rank( my_rank );


    if ( node_used_ )
    {
        if ( eigenvectors.size() != data_.size() )
        {
            eigenvectors.resize( data_.size() );
        }

        // Vector of eigenvalues in ascending order.
        DynamicVector< value_t, columnVector >   lambda;
        if ( !nodes_on_this_rank[ i ]->is_root() )
        {
            // Compute eigenvalues and eigenvectors of acc. transfer tensor.

            compute_eigenvalues_and_eigenvectors_of_acc_transfer_tensor(tree_.size(), eps, k, acc_tr[ i ], lambda, eigenvectors[ i ], sq_rel_errors[i]);


            // Truncate.
            if ( nodes_on_this_rank[ i ]->is_leaf() )
            {
                //data_[ i ][ 0 ] = data_[ i ][ 0 ] * eigenvectors[ i ];
                data_[ i ][ 0 ] *= eigenvectors[ i ];
            }
            else
            {
                // const std::size_t keigenvectors_columns = eigenvectors[ i ].columns();

                // DynamicVector< DynamicMatrix< double, columnMajor > >  new_data( keigenvectors_columns, blaze::DynamicMatrix< double, blaze::columnMajor >(data_[ i ][ 0 ].rows(), data_[ i ][ 0 ].columns(), 0.0 ) );

                // const blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >& data_i = data_[ i ];
                const blaze::DynamicMatrix< value_t, blaze::columnMajor >& eigenvectors_i = eigenvectors[ i ];

                data_[i] = truncate_inner_node(data_[ i ], eigenvectors_i) ;

                // truncate_inner_node(data_i, eigenvectors_i, new_data);
                // data_[i] = std::move(new_data);
            }
        }

        if ( !nodes_on_this_rank[ i ]->is_leaf() )
        {
            if ( nodes_on_this_rank[ i ]->son1()->mpi_rank() == my_rank )
            {
                size_t i_son1 = 0;
                size_t i_son2 = 0;
                for ( size_t j = i; j < nodes_on_this_rank.size(); ++j )
                {
                    if ( nodes_on_this_rank[ j ] == nodes_on_this_rank[ i ]->son1() )
                    {
                        i_son1 = j;
                    }
                    else if ( nodes_on_this_rank[ j ] == nodes_on_this_rank[ i ]->son2() )
                    {
                        i_son2 = j;
                    }
                }
                truncate_orthogonalized_node( acc_tr, eigenvectors, sq_rel_errors, i_son1, k, eps );
                truncate_orthogonalized_node( acc_tr, eigenvectors, sq_rel_errors, i_son2, k, eps );

                //this was
                // for ( size_t l = 0; l < data_[ i ].size(); ++l )
                // {
                //     data_[ i ][ l ] = trans( eigenvectors[ i_son1 ] ) * data_[ i ][ l ] * eigenvectors[ i_son2 ];
                // }

                const DynamicMatrix< value_t, columnMajor >& eigenvectors_i_son1 = eigenvectors[ i_son1 ];
                const DynamicMatrix< value_t, columnMajor >& eigenvectors_i_son2 = eigenvectors[ i_son2 ];

                // blaze::DynamicVector<DynamicMatrix< value_t, columnMajor >>& data = data_[ i ];

                multiply_with_son_eigenvalues(eigenvectors_i_son1, eigenvectors_i_son2, data_[ i ]);

                // std::for_each(
                //     //if we use c++17 we can parallelize this easy
                //     //std::execution::par_unseq,
                //     data_[ i ].begin(),
                //     data_[ i ].end(),
                //     [&eigenvectors_i_son1, &eigenvectors_i_son2](blaze::DynamicMatrix< value_t, blaze::columnMajor >& item)
                // {
                //     item = blaze::trans( eigenvectors_i_son1 ) * item * eigenvectors_i_son2;
                // });

            }


        }
    }// if ( node_used_ )

}


//
// Auxiliary for accumulated_transfer_tensors().
//
inline void compute_acc_tr_sons(const blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > >& data_i, const DynamicMatrix< value_t, columnMajor >& acc_tr_i, DynamicMatrix< value_t, columnMajor >& acc_tr_son1, DynamicMatrix< value_t, columnMajor >& acc_tr_son2)
{
    const std::size_t kdata_size = data_i.size();

    if (kdata_size != 0)
    {
        blaze::DynamicVector<DynamicMatrix< value_t, columnMajor >> acc_tr_helper( data_i );

        const std::size_t kdata_rows = data_i[0].rows();
        const std::size_t kdata_cols = data_i[0].columns();

        constexpr std::size_t kdata_size_add_threshold = 127;
        constexpr std::size_t kdata_cols_rows_threshold = 254;

        if ( htensor_use_shared_memory_ && ( ( kdata_size > kdata_size_add_threshold ) && ( ( kdata_rows > kdata_cols_rows_threshold ) || ( kdata_cols > kdata_cols_rows_threshold ) ) ) )
        {
            std::mutex mtx;
            ParallelFor(0UL, kdata_size, [&acc_tr_helper, &acc_tr_i, kdata_size, &data_i, &mtx, &acc_tr_son1, &acc_tr_son2](std::size_t l2)
            {
                acc_tr_helper[ l2 ] *= acc_tr_i( l2, l2 ) / 2.0;
                for ( size_t l1 = l2 + 1; l1 < kdata_size; ++l1 )
                {
                    acc_tr_helper[ l2 ] += acc_tr_i( l1, l2 ) * data_i[ l1 ];
                }
                DynamicMatrix< value_t, columnMajor > acc_tr_helper_son1_matrix( acc_tr_helper[ l2 ] * trans( data_i[ l2 ] ) );
                acc_tr_helper_son1_matrix += trans( acc_tr_helper_son1_matrix );
                DynamicMatrix< value_t, columnMajor > acc_tr_helper_son2_matrix( trans( acc_tr_helper[ l2 ] ) * data_i[ l2 ] );
                acc_tr_helper_son2_matrix += trans( acc_tr_helper_son2_matrix );
                {
                    std::lock_guard<std::mutex> lock(mtx);
                    acc_tr_son1 += std::move( acc_tr_helper_son1_matrix );
                    acc_tr_son2 += std::move( acc_tr_helper_son2_matrix );
                }
            });

        }
        else if ( htensor_use_shared_memory_ && ( kdata_size > kdata_size_add_threshold ) )
        {


            ParallelFor(0UL, kdata_size, [&acc_tr_helper, &acc_tr_i, kdata_size, &data_i](std::size_t l2)
            {
                acc_tr_helper[ l2 ] *= acc_tr_i( l2, l2 ) / 2.0;
                for ( size_t l1 = l2 + 1; l1 < kdata_size; ++l1 )
                {
                    acc_tr_helper[ l2 ] += acc_tr_i( l1, l2 ) * data_i[ l1 ];
                }
            });

            auto son1 = [&acc_tr_helper, &data_i, kdata_size, &acc_tr_son1]()
            {
                for ( size_t l2 = 0; l2 < kdata_size; ++l2 )
                {
                    acc_tr_son1 += acc_tr_helper[ l2 ] * trans( data_i[ l2 ] );
                }
                acc_tr_son1 += trans(acc_tr_son1);
            };
            std::thread t1(son1);


            for ( size_t l2 = 0; l2 < kdata_size; ++l2 )
            {
                acc_tr_son2 += trans( acc_tr_helper[ l2 ] ) * data_i[ l2 ];
            }
            acc_tr_son2 += trans(acc_tr_son2);

            t1.join();

        }
        else
        {
            for ( size_t l2 = 0; l2 < kdata_size; ++l2 )
            {
                const value_t prefactor = acc_tr_i( l2, l2 ) / 2.0;
                acc_tr_helper[ l2 ] *= prefactor;
            }

            for ( size_t l2 = 0; l2 < kdata_size; ++l2 )
            {
                for ( size_t l1 = l2 + 1; l1 < kdata_size; ++l1 )
                {
                    acc_tr_helper[ l2 ] += acc_tr_i( l1, l2 ) * data_i[ l1 ];
                }
            }

            for ( size_t l2 = 0; l2 < kdata_size; ++l2 )
            {
                acc_tr_son1 += acc_tr_helper[ l2 ] * trans( data_i[ l2 ] );
            }
            //should we try
            //acc_tr_son1 = declsym( acc_tr_son1 + trans(acc_tr_son1));
            acc_tr_son1 += trans(acc_tr_son1);

            for ( size_t l2 = 0; l2 < kdata_size; ++l2 )
            {
                acc_tr_son2 += trans( acc_tr_helper[ l2 ] ) * data_i[ l2 ];
            }
            //should we try
            //acc_tr_son2 = declsym( acc_tr_son2 + trans(acc_tr_son2));
            acc_tr_son2 += trans(acc_tr_son2);


        }
    }
}

//
// Auxiliary for truncate_orthogonalized_tensor().
// Computes the accumulated transfer tensors.
//
void HTensor::accumulated_transfer_tensors( vector< DynamicMatrix< value_t, columnMajor > >& acc_tr, size_t i ) const
{
    int my_rank = 0;
    const vector< const Cluster * > nodes_on_this_rank = tree_.all_nodes_of_rank( my_rank );


    if ( acc_tr.size() != data_.size() )
    {
        acc_tr.resize( data_.size() );
    }

    if ( node_used_ )
    {
        if ( !nodes_on_this_rank[ i ]->is_root() )
        {
        }
        else
        {
            // To avoid later distinction of root transfer and inner transfer tensors.
            acc_tr[ 0 ].resize( 1, 1 );
            acc_tr[ 0 ]( 0, 0 ) = 1.0;
        }

        // Compute acc. transfer tensors for sons.
        if ( !nodes_on_this_rank[ i ]->is_leaf() )
        {
            if ( nodes_on_this_rank[ i ]->son1()->mpi_rank() == my_rank )
            {
                size_t i_son1 = 0;
                size_t i_son2 = 0;
                for ( size_t j = 0; j < nodes_on_this_rank.size(); ++j )
                {
                    if ( nodes_on_this_rank[ j ] == nodes_on_this_rank[ i ]->son1() )
                    {
                        i_son1 = j;
                    }
                    else if ( nodes_on_this_rank[ j ] == nodes_on_this_rank[ i ]->son2() )
                    {
                        i_son2 = j;
                    }
                }
                if ( data_[ i ].size() > 0 )
                {
                    acc_tr[ i_son1 ].resize( data_[ i ][ 0 ].rows(), data_[ i ][ 0 ].rows() );
                    acc_tr[ i_son2 ].resize( data_[ i ][ 0 ].columns(), data_[ i ][ 0 ].columns() );
                    blaze::reset( acc_tr[ i_son1 ] );
                    blaze::reset( acc_tr[ i_son2 ] );
                }


                const blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor > >& data_i = data_[ i ];
                const DynamicMatrix< value_t, columnMajor >& acc_tr_i = acc_tr[i];
                compute_acc_tr_sons(data_i, acc_tr_i, acc_tr[ i_son1 ],  acc_tr[ i_son2 ]);


                if ( !nodes_on_this_rank[ i ]->son1()->is_leaf() )
                {
                    accumulated_transfer_tensors( acc_tr, i_son1 );
                }
                if ( !nodes_on_this_rank[ i ]->son2()->is_leaf() )
                {
                    accumulated_transfer_tensors( acc_tr, i_son2 );
                }
            }
        }
    }// if ( node_used_ )
}

double norm(const HTensor& ht)
{
    return std::sqrt( std::abs( ht.dot( ht ) ) );
}


double HTensor::norm() const
{
    return lowrat::norm(*this);
}

double norm_orthogonalize(HTensor ht)
{
    ht.orthogonalize();
    vector< const Cluster * > nodes_on_this_rank = ht.tree_.all_nodes_of_rank( 0UL );

    double norm_value = 0.0;
    for ( size_t i = 0; i < (ht.data_).size(); ++i )
    {
        if ( nodes_on_this_rank[ i ]->is_root() )
        {
            norm_value = blaze::norm( (ht.data_)[ i ][ 0 ]);
            break;
        }
    }
    return norm_value;
}


double HTensor::norm_orthogonalize() const
{
    return lowrat::norm_orthogonalize(*this);
}

std::size_t dimension(const HTensor & ht)
{
    return ht.tree_.size();
}

std::size_t number_of_nodes(const HTensor & ht)
{
    return ht.tree_.all_nodes().size();
}

bool HTensor::is_leaf(std::size_t nodenumber) const
{
    const vector< const Cluster * > nodes = tree_.all_nodes( );
    return ( nodes[nodenumber]->is_leaf() );
}

bool HTensor::is_root(std::size_t nodenumber) const
{
    const vector< const Cluster * > nodes = tree_.all_nodes( );
    return ( nodes[nodenumber]->is_root() );
}

std::size_t HTensor::get_root_number() const
{
    const std::vector< const Cluster * > nodes = tree_.all_nodes( );

    std::size_t root_number = 0;
    for ( std::size_t i = 0; i < nodes.size(); ++i )
    {
        if ( nodes[ i ]->is_root() )
        {
            root_number = i;
            break;
        }
    }
    return root_number;
}

std::vector<std::size_t> HTensor::get_child_numbers(std::size_t parent_number) const
{
    const std::vector< const Cluster * > nodes = tree_.all_nodes( );

    std::vector<std::size_t> child_numbers;
    //garanty that son1 is before son2
    for ( std::size_t i = 0; i < nodes.size(); ++i )
    {
        if ( nodes[ i ] == nodes[ parent_number ]->son1() )
        {
            child_numbers.emplace_back(i);
        }
    }

    for ( std::size_t i = 0; i < nodes.size(); ++i )
    {
        if ( nodes[ i ] == nodes[ parent_number ]->son2() )
        {
            child_numbers.emplace_back(i);
        }
    }
    return child_numbers;
}

std::size_t HTensor::get_parent_number(std::size_t child_number) const
{
    const std::vector< const Cluster * > nodes = tree_.all_nodes( );

    std::size_t parent_number = 0;
    for ( std::size_t i = 0; i < nodes.size(); ++i )
    {
        if ( nodes[ i ] == nodes[ child_number ]->father() )
        {
            parent_number = i;
        }
    }
    return parent_number;
}

std::vector<std::size_t> HTensor::get_sibling_numbers(std::size_t child_number) const
{
    const std::size_t parent_number = get_parent_number(child_number);
    std::vector<std::size_t> sibling_numbers = get_child_numbers( parent_number );
    std::remove(sibling_numbers.begin(), sibling_numbers.end(), child_number);
    return sibling_numbers;
}

std::size_t HTensor::get_brother_number(std::size_t child_number) const
{
    return ( get_sibling_numbers(child_number)[0] );
}

bool HTensor::is_left_son_of_parent(std::size_t son_number, std::size_t parent_number) const
{
    const std::vector< const Cluster * > nodes = tree_.all_nodes( );
    return ( nodes[ son_number ] == nodes[ parent_number ]->son1() );
}

HTensor operator+(HTensor left, const HTensor& right)
{
    left.add(1.0, right);
    return left;
}

HTensor operator-(HTensor left, const HTensor& right)
{
    left.add( ( -1.0), right);
    return left;
}

HTensor operator*(HTensor left, value_t right)
{
    left.scale(right);
    return left;
}

HTensor operator*(value_t left, HTensor right)
{
    right.scale(left);
    return right;
}

HTensor operator/(HTensor left, value_t right)
{
    left.scale( 1.0 / right );
    return left;
}

HTensor& HTensor::operator+=(const HTensor& right)
{
    this->add(1.0, right);
    return *this;
}

HTensor& HTensor::operator-=(const HTensor& right)
{
    this->add(-1.0, right);
    return *this;
}

HTensor& HTensor::operator*=(value_t right)
{
    this->scale(right);
    return *this;
}

HTensor& HTensor::operator/=(value_t right)
{
    this->scale( 1.0 / right );
    return *this;
}

//
// returns a HTensor with the same dimension tree, which returns 1 for every tensorindex
//
HTensor HTensor::ones() const
{

    return HTensor(*this, 1.0);

}







// this is probably not really a member function
double HTensor::maximum_als(tensorindex & current_index, std::size_t k_max) const
{
    // genrate random values, chooes the biggest and hopefully converge iterativly into a maximum
    const std::size_t d = current_index.size();
    const tensorindex n = sizes();

    double max_val = std::fabs( entry(current_index) );
    bool changed = true;
    // get sizes

    // number of iterations
    for (std::size_t k = 0; k < k_max && changed; ++k)
    {
        changed = false;
        // go through the dimensions
        for (std::size_t i = 0; i < d; ++i)
        {
            std::size_t idx = current_index[i];
            for (std::size_t j = 0; j < n[i]; ++j)
            {
                current_index[i] = j;
                value_t cur_entry = entry(current_index);
                if ( cur_entry > max_val)
                {
                    idx = j;
                    max_val = std::fabs( cur_entry );
                    changed = true;
                }
            }
            current_index[i] = idx;
        }
        k_max = k;
    }
    return max_val;
}

void HTensor::vector_iteration(double & max_val, const std::size_t k_max, const double eps) const
{
    // gibt in irgendeiner form das globale maximum, aber desto mehr maxima in der naehe sind desto langsamer konvergiert es
    HTensor yk(*this, 1.0);
    HTensor qk(*this);
    HTensor zk(*this);
    HTensor u(*this);
    std::vector<double> lambda_k(k_max);

    int my_rank = 0;

    // set y0 = Kron_1^d _1_ / n_nu
    vector< const Cluster * > nodes_on_this_rank = yk.tree_.all_nodes_of_rank( my_rank );

    for ( std::size_t i = 0; i < nodes_on_this_rank.size(); ++i )
    {
        if (nodes_on_this_rank[i]->is_leaf())
        {
            yk.data_[i] *= 1.0 / (yk.data_[i][0].rows());
        }
    }
    // std::cout << " Start vector iteration with y0: " << std::"\n" ;
    // yk.display_content();

    // start iteration
    for (std::size_t i = 0; i < k_max; ++i)
    {
        // TODO: Das muss doch effizienter gehen, a.hadamard(b,c) wo a = b o c

        // qk = u o y_{-1}
        u.hadamard(yk);
        qk.data_ = u.data_;
        u.data_ = data_;

        // lambda = <qk,y_{k-1}>
        lambda_k[i] = ( yk.dot(qk) );

        zk.data_ = qk.data_;
        // y_k = Trunc(qk / norm qk)
        zk.scale(1.0 / sqrt(qk.dot(qk)));
        zk.truncate(eps);
        yk.data_ = zk.data_;
    }
    //const std::size_t ka = 1;
    //yk.truncate(ka);
    //yk.display_content();

    //std::cout << lambda_k;
    max_val = std::fabs( lambda_k[k_max - 1] );
}


void qr_transfer_tensor( DynamicVector< DynamicMatrix< value_t, columnMajor > > & tensor, DynamicMatrix< value_t, columnMajor > & R )
{
    if ( 0 == tensor.size() )
    {
        //maybe throw
        R.resize(0, 0);
        return;
    }
    const size_t layer_rows       = tensor[ 0 ].rows();
    const size_t layer_columns    = tensor[ 0 ].columns();

    DynamicMatrix< value_t, columnMajor >    matricization( layer_rows * layer_columns, tensor.size() );

    std::unique_ptr<value_t[]> layer_data = std::make_unique<value_t[]>(layer_rows * layer_columns);
    for ( std::size_t j = 0; j < tensor.size(); ++j )
    {
        CustomMatrix< value_t, unaligned, unpadded, columnMajor >    layer( layer_data.get(), layer_rows, layer_columns );
        layer = tensor[ j ];
        CustomVector< value_t, unaligned, unpadded, columnVector >   layer_as_column( layer_data.get(), layer_rows * layer_columns );
        column( matricization, j ) = layer_as_column;
    }

    // for ( std::size_t j = 0; j < tensor.size(); ++j )
    // {
    //     const std::size_t tensor_j_columns = tensor[j].columns();
    //     const std::size_t tensor_j_rows = tensor[j].rows();
    //     auto column_mat_j = blaze::column( matricization, j );
    //     for (std::size_t i = 0; i < tensor_j_columns; ++i)
    //     {
    //         blaze::subvector( column_mat_j , i * tensor_j_rows, tensor_j_rows ) = blaze::column( tensor[ j ], i );
    //     }
    // }

    blaze::qr( matricization, matricization, R );
    tensor.resize( matricization.columns() );


    // for ( std::size_t j = 0; j < tensor.size(); ++j )
    // {
    //     const std::size_t tensor_j_columns = tensor[j].columns();
    //     const std::size_t tensor_j_rows = tensor[j].rows();
    //     auto column_mat_j = blaze::column( matricization, j );

    //     for (std::size_t i = 0; i < tensor_j_columns; ++i)
    //     {
    //         blaze::column( tensor[ j ], i ) = blaze::subvector( column_mat_j , i * tensor_j_rows, tensor_j_rows );
    //     }
    // }

    std::unique_ptr<value_t[]> column_data = std::make_unique<value_t[]>(layer_rows * layer_columns);
    for ( size_t j = 0; j < tensor.size(); ++j )
    {
        CustomVector< value_t, unaligned, unpadded, columnVector >   layer_as_column( column_data.get(), layer_rows * layer_columns );
        layer_as_column = column( matricization, j );
        CustomMatrix< value_t, unaligned, unpadded, columnMajor >    layer( column_data.get(), layer_rows, layer_columns );
        tensor[ j ] = layer;
    }
}

//
// Computes the reduced QR decomposition of a transfer tensor for the HT format.
// The transfer tensor gets overwritten by Q.
//
void reduced_qr_transfer_tensor( DynamicVector< DynamicMatrix< value_t, columnMajor > > & tensor, DynamicMatrix< value_t, columnMajor > & R )
{
    size_t layer_rows       = 0;
    size_t layer_columns    = 0;
    if ( tensor.size() > 0 )
    {
        layer_rows      = tensor[ 0 ].rows();
        layer_columns   = tensor[ 0 ].columns();
    }

    DynamicMatrix< value_t, columnMajor >    matricization( layer_rows * layer_columns, tensor.size() );

    std::unique_ptr<value_t[]> layer_data = std::make_unique<value_t[]>(layer_rows * layer_columns);
    for ( size_t j = 0; j < tensor.size(); ++j )
    {
        //value_t * layer_data = new value_t[ layer_rows * layer_columns ];
        CustomMatrix< value_t, unaligned, unpadded, columnMajor >    layer( layer_data.get(), layer_rows, layer_columns );
        layer = tensor[ j ];
        CustomVector< value_t, unaligned, unpadded, columnVector >   layer_as_column( layer_data.get(), layer_rows * layer_columns );
        column( matricization, j ) = layer_as_column;
        //delete[] layer_data;
    }

    reduced_qr( matricization, matricization, R );
    tensor.resize( matricization.columns() );

    std::unique_ptr<value_t[]> column_data = std::make_unique<value_t[]>(layer_rows * layer_columns);
    for ( size_t j = 0; j < tensor.size(); ++j )
    {
        //value_t * column_data = new value_t[ layer_rows * layer_columns ];
        CustomVector< value_t, unaligned, unpadded, columnVector >   layer_as_column( column_data.get(), layer_rows * layer_columns );
        layer_as_column = column( matricization, j );
        CustomMatrix< value_t, unaligned, unpadded, columnMajor >    layer( column_data.get(), layer_rows, layer_columns );
        tensor[ j ] = layer;
        //delete[] column_data;
    }
}


HTensor ht_construct_from_canonical(const std::vector<blaze::DynamicMatrix<value_t, blaze::columnMajor>>& cp_cores, double eps)
{
    std::size_t dim = cp_cores.size();

    std::vector<std::size_t> mode_sizes(0);

    for (const auto& core : cp_cores)
    {
        mode_sizes.push_back(core.rows());
    }

    std::size_t rank = cp_cores[0].columns();

    HTensor ht(dim, mode_sizes, rank);

    std::vector< blaze::DynamicVector< blaze::DynamicMatrix< value_t, blaze::columnMajor>>> data;
    const Cluster ht_tree = ht.clustertree();
    const std::vector< const Cluster * > tree_nodes = ht_tree.all_nodes();


    //because we use a vector of pointers, we have to write const auto and not const auto&
    for ( std::size_t i = 0; i < tree_nodes.size(); ++i )
    {
        DynamicVector< DynamicMatrix< value_t, columnMajor > > new_object;
        if ( tree_nodes[i]->is_root() )
        {
            new_object.resize( 1 );
            new_object[ 0 ] = blaze::DynamicMatrix< value_t, blaze::columnMajor >( rank, rank, 0.0 );
            blaze::diagonal(new_object[ 0 ]) = 1.0;
            data.push_back( new_object );
        }
        else if ( tree_nodes[i]->is_leaf() )
        {
            new_object.resize( 1 );
            new_object[ 0 ] = blaze::DynamicMatrix< value_t, blaze::columnMajor >(  mode_sizes[ tree_nodes[i]->leaf_number() ], rank, 0.0 );
            new_object[ 0 ] = cp_cores[ tree_nodes[i]->leaf_number() ];
            data.push_back( new_object );
        }
        else // inner node
        {
            new_object.resize( rank );
            for ( std::size_t j = 0; j < rank; ++j )
            {
                new_object[ j ] = blaze::DynamicMatrix< value_t, blaze::columnMajor >(  rank, rank, 0.0 );
                new_object[j](j, j) = 1.0;
            }
            data.push_back( new_object );
        }
    }

    ht.set_data( std::move( data ) );

    if (eps > 0.0)
    {
        ht.truncate(eps);
    }
    else
    {
        ht.orthogonalize();
    }

    return ht;
}

void add_cp_to_ht(const std::vector<blaze::DynamicMatrix<value_t, blaze::columnMajor>>& cp_cores, HTensor & htensor, double eps)
{
    htensor += ht_construct_from_canonical(cp_cores, eps);
    if (eps > 0.0)
    {
        htensor.truncate(eps);
    }
    else
    {
        htensor.orthogonalize();
    }
}

std::size_t maximal_rank_on_level(const HTensor & ht, std::size_t level)
{
    const std::size_t start = std::pow(2UL, level) - 1;
    const std::size_t node_number = number_of_nodes(ht);
    const std::size_t end = std::min(node_number, static_cast<std::size_t>(std::pow(2UL, level + 1) - 1) );
    std::size_t max_rank = 0;
    for (std::size_t i = start; i < end; ++i)
    {
        std::tuple<std::size_t, std::size_t, std::size_t> rank = ht.get_tensor_rank(i);
        if (std::get<2>(rank) > max_rank)
        {
            max_rank = std::get<2>(rank);
        }
        if ( !ht.is_leaf(i) )
        {
            if ( std::get<1>(rank) > max_rank )
            {
                max_rank = std::get<1>(rank);
            }
            if ( ( !ht.is_root(i) ) && ( std::get<0>(rank) > max_rank ) )
            {
                max_rank = std::get<0>(rank);
            }
        }
    }
    return max_rank;
}

//std::size_t maximal_rank(const HTensor & ht)
//{
//    // const std::size_t level = std::ceil( std::log2( dimension( ht ) ) ) + 1;
//    // std::size_t max_rank = maximal_rank_on_level(ht, 0UL);
//    // for (std::size_t i = 1; i < level; ++i)
//    // {
//    //     std::size_t max_rank_temp = maximal_rank_on_level(ht, i);
//    //     if (max_rank_temp > max_rank)
//    //     {
//    //         max_rank = max_rank_temp;
//    //     }
//    // }
//    // return max_rank;
//    std::vector< std::size_t > ranks = ht.all_ranks();
//    return *(std::max_element(ranks.begin(), ranks.end()));
//}

HTensor hadamard(HTensor X, const HTensor& Y)
{
    X.hadamard(Y);
    return X;
}

std::size_t dimension_number_of_leaf_number(const HTensor & ht, std::size_t leaf_number)
{
    std::size_t dimension_number = 0;
    for (std::size_t i = 0; i < dimension(ht); ++i )
    {
        if ( ht.node_number(i) == leaf_number )
        {
            dimension_number = i;
            break;
        }
    }
    return dimension_number;
}

HTensor truncate(HTensor ht, double eps)
{
    ht.truncate(eps);
    return ht;
}

std::vector<std::size_t> HTensor::random_tensorindex()
{
    std::vector<std::size_t> mode_sizes = sizes();
    std::size_t dimension = mode_sizes.size();
    std::vector<std::size_t> random_index(dimension, 0);

    for (std::size_t i = 0; i < dimension; ++i)
    {
        std::uniform_int_distribution<std::size_t> rf(0, mode_sizes[i]-1);
        // random_index[i] = rand<std::size_t>(0, mode_sizes[i] - 1);
        random_index[i] = rf(lowrat::random::mt_);
    }

    return random_index;
}

} //namespace lowrat
