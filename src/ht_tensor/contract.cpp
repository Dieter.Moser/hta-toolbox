#include "lowrat/ht_tensor/contract.h"

#include <cstddef> //for std::size_t
#include <vector>//for std::vector

#include <blaze/Blaze.h> //for blaze::DynamicMatrix, blaze::DynamicVector, blaze::columnMajor
#include "lowrat/ht_tensor/permute.h" //lowrat::permute

namespace lowrat
{

//gets an x_1 x x_2 x_12 tensor and an y_1 x y_2 x y_12 tensor and contracts to
//if
// blaze::DynamicMatrix< double, blaze::columnMajor > contract(const blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >& x, const blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >& y, const std::vector<std::size_t>& dim_x, const std::vector<std::size_t>& dim_y) noexcept
// {

// }

std::vector<std::vector<std::vector<blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >>>> contract(const std::vector<std::vector<blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >>>& x, const blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >& y, std::size_t dim)
{
    switch (dim)
    {
    case 0:
    {
        //x is a x_1 x x_2 x x_11 x x_12 x x_22 tensor and the y is a x_1 x y_2 x y_12 tensor
        //and merge them into a x_2 x y_2 x y_12 x x_11 x x_12 x x_22 tensor
        // const std::size_t x_size_dim_1 = x[0].columns();
        // const std::size_t y_size_dim_1 = y[0].columns();
        const std::size_t rows = x[0][0][0].columns();
        const std::size_t cols = y[0].columns();

        const std::size_t y_size_dim_2 = y.size();

        const std::size_t x_size_dim_2 = x[0][0].size();
        const std::size_t x_size_dim_3 = x[0].size();
        const std::size_t x_size_dim_4 = x.size();

        std::vector<std::vector<std::vector<blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >>>> result_tensor(x_size_dim_4, std::vector<std::vector<blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >>>( x_size_dim_3, std::vector<blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >>( x_size_dim_2, blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >(y_size_dim_2, blaze::DynamicMatrix< double, blaze::columnMajor >(rows, cols, 0.0)) )));
        for (std::size_t l = 0; l < x_size_dim_4; ++l)
        {
            for (std::size_t j = 0; j < x_size_dim_3; ++j)
            {
                for (std::size_t i = 0; i < x_size_dim_2; ++i)
                {
                    for (std::size_t k = 0; k < y_size_dim_2; ++k)
                    {
                        ( ( (result_tensor[l] )[j] )[i] )[k] = blaze::trans( ( ( x[l] )[j] )[i] ) * y[k];
                    }
                }
            }
        }
        return result_tensor;
    }
    case 1:
    {
        //x is a x_1 x x_2 x x_11 x x_12 x x_22 tensor and the y is a y_1 x x_2 x y_12 tensor
        //and merge them into a x_1 x y_1 x y_12 x x_11 x x_12 x x_22 tensor
        const std::size_t rows = x[0][0][0].rows();
        const std::size_t cols = y[0].rows();

        const std::size_t y_size_dim_2 = y.size();

        const std::size_t x_size_dim_2 = x[0][0].size();
        const std::size_t x_size_dim_3 = x[0].size();
        const std::size_t x_size_dim_4 = x.size();

        std::vector<std::vector<std::vector<blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >>>> result_tensor(x_size_dim_4, std::vector<std::vector<blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >>>( x_size_dim_3, std::vector<blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >>( x_size_dim_2, blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >(y_size_dim_2, blaze::DynamicMatrix< double, blaze::columnMajor >(rows, cols, 0.0)) )));
        for (std::size_t l = 0; l < x_size_dim_4; ++l)
        {
            for (std::size_t j = 0; j < x_size_dim_3; ++j)
            {
                for (std::size_t i = 0; i < x_size_dim_2; ++i)
                {
                    for (std::size_t k = 0; k < y_size_dim_2; ++k)
                    {
                        ( ( (result_tensor[l] )[j] )[i] )[k] = ( ( x[l] )[j] )[i] * blaze::trans( y[k] );
                    }
                }
            }
        }
        return result_tensor;
    }
    case 2:
    {
        //x is a x_1 x x_2 x x_12 tensor and the y is a y_1 x y_2 x x_12 tensor
        //and merge them into a x_2 x y_2 x y_12 x x_12 tensor
        throw "contract the case for dim equals two must be implemented.";
    }
    default:
        throw "In contract the dim must be between 0 and 2.";
    }
}

//will maybe work
std::vector<std::vector<blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >>> contract(const std::vector<blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >>& x, const blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >& y, std::size_t dim)
{
    switch (dim)
    {
    case 0:
    {
        //x is a x_1 x x_2 x x_11 x x_12 tensor and the y is a x_1 x y_2 x y_12 tensor
        //and merge them into a x_2 x y_2 x y_12 x x_11 x x_12 tensor
        // const std::size_t x_size_dim_1 = x[0].columns();
        // const std::size_t y_size_dim_1 = y[0].columns();
        const std::size_t rows = x[0][0].columns();
        const std::size_t cols = y[0].columns();

        const std::size_t y_size_dim_2 = y.size();

        const std::size_t x_size_dim_2 = x[0].size();
        const std::size_t x_size_dim_3 = x.size();

        std::vector<std::vector<blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >>> result_tensor(x_size_dim_3, std::vector<blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >>( x_size_dim_2, blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >( y_size_dim_2, blaze::DynamicMatrix< double, blaze::columnMajor >(rows, cols, 0.0) )));
        for (std::size_t j = 0; j < x_size_dim_3; ++j)
        {
            for (std::size_t i = 0; i < x_size_dim_2; ++i)
            {
                for (std::size_t k = 0; k < y_size_dim_2; ++k)
                {
                    ( ( result_tensor[j] )[i] )[k] = blaze::trans( ( x[j] )[i] ) * y[k];
                }
            }
        }
        return result_tensor;
    }
    case 1:
    {
        //x is a x_1 x x_2 x x_11 x x_12 tensor and the y is a y_1 x x_2 x y_12 tensor
        //and merge them into a x_1 x y_1 x y_12 x x_11 x x_12 tensor
        const std::size_t rows = x[0][0].rows();
        const std::size_t cols = y[0].rows();
        const std::size_t y_size_dim_2 = y.size();

        const std::size_t x_size_dim_2 = x[0].size();
        const std::size_t x_size_dim_3 = x.size();

        std::vector<std::vector<blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >>> result_tensor(x_size_dim_3, std::vector<blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >>( x_size_dim_2, blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >( y_size_dim_2, blaze::DynamicMatrix< double, blaze::columnMajor >(rows, cols, 0.0) )));
        for (std::size_t j = 0; j < x_size_dim_3; ++j)
        {
            for (std::size_t i = 0; i < x_size_dim_2; ++i)
            {
                for (std::size_t k = 0; k < y_size_dim_2; ++k)
                {
                    ( ( result_tensor[j] )[i] )[k] =  ( x[j] )[i]  * blaze::trans( y[k] );
                }
            }
        }
        return result_tensor;
    }
    case 2:
    {
        //x is a x_1 x x_2 x x_12 tensor and the y is a y_1 x y_2 x x_12 tensor
        //and merge them into a x_2 x y_2 x y_12 x x_12 tensor
        throw "contract the case for dim equals two must be implemented.";
    }
    default:
        throw "In contract the dim must be between 0 and 2.";
    }
}

//SHOULD BE CORRECT
//gets an x_1 x x_2 x x_12 and an y_1 x y_2 x y_12 tensor and dim where the should be contracted
//e.g. if dim = 0, then x_1 = y_1 and the corresponding tensor would be x_2 x y_2 x y_12 x x_12
//e.g. if dim = 1, then x_2 = y_2 and the corresponding tensor would be x_1 x y_1 x y_12 x x_12
//e.g. if dim = 2, then x_12 = y_12 and the corresponding tensor would be x_1 x y_1 x y_2 x x_2
std::vector<blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >> contract(const blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >& x, const blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >& y, std::size_t dim)
{
    switch (dim)
    {
    case 0:
    {
        const std::size_t rows = x[0].columns();
        const std::size_t columns = y[0].columns();

        const std::size_t x_size_dim_2 = x.size();
        const std::size_t y_size_dim_2 = y.size();

        std::vector<blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >> result_tensor(x_size_dim_2, blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >( y_size_dim_2, blaze::DynamicMatrix< double, blaze::columnMajor >(rows, columns, 0.0 ) ));
        for (std::size_t i = 0; i < x_size_dim_2; ++i)
        {
            for (std::size_t k = 0; k < y_size_dim_2; ++k)
            {
                ( result_tensor[i] )[k] = blaze::trans( x[i] ) * y[k];
            }
        }
        return result_tensor;
    }
    case 1:
    {
        const std::size_t rows = x[0].rows();
        const std::size_t columns = y[0].rows();

        const std::size_t x_size_dim_2 = x.size();
        const std::size_t y_size_dim_2 = y.size();

        std::vector<blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >> result_tensor(x_size_dim_2, blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >( y_size_dim_2, blaze::DynamicMatrix< double, blaze::columnMajor >(rows, columns, 0.0)));
        for (std::size_t i = 0; i < x_size_dim_2; ++i)
        {
            for (std::size_t k = 0; k < y_size_dim_2; ++k)
            {
                ( result_tensor[i] )[k] = x[i] * blaze::trans( y[k] );
            }
        }
        return result_tensor;
    }
    case 2:
    {
        throw "contract the case for dim equals two must be implemented.";
        // const std::vector<std::size_t> new_index{0UL, 2UL, 1UL};
        // const blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > > reorderd_x = permute(x, new_index);
        // const blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > > reorderd_y = permute(y, new_index);

        // const std::size_t x_size_dim_2 = reorderd_x.size();
        // const std::size_t y_size_dim_2 = reorderd_y.size();

        // std::vector<blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >> result_tensor(x_size_dim_2, blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >( y_size_dim_2));
        // for (std::size_t i = 0; i < x_size_dim_2; ++i)
        // {
        //     for (std::size_t k = 0; k < y_size_dim_2; ++k)
        //     {
        //         ( result_tensor[i] )[k] = reorderd_x[i] * blaze::trans( reorderd_y[k] );
        //     }
        // }
        // return result_tensor;
    }
    default:
        throw "In contract the dim must be between 0 and 2.";
    }
}


} //namespace lowrat