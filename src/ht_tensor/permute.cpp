#include "lowrat/ht_tensor/permute.h"

#include <cstddef> //for std::size_t
#include <vector>//for std::vector

#include <blaze/Blaze.h> //for blaze::DynamicMatrix, blaze::DynamicVector, blaze::columnMajor, blaze::column, blaze::row, blaze::trans

namespace lowrat
{
//reorder tensor from n_0 x n_1 x n_2 into an n_{dimorder[0]} x n_{dimorder[1]} x n_{dimorder[2]} tensor
// e.g. dimorder = {2, 0, 1}, then n_0 x n_1 x n_2 is reorderd into an n_2 x n_0 x n_1 tensor
blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > > permute(const blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >& node, const std::vector<std::size_t>& dimorder)
{
    const std::size_t size_dim_0 = node[0].rows();
    const std::size_t size_dim_1 = node[0].columns();
    const std::size_t size_dim_2 = node.size();

    switch (dimorder[0])
    {
    case 0:
    {
        switch (dimorder[1])
        {
        case 0:
        {
            throw "In permute an illegal order of dimensions was choosen.";
            break;
        }
        case 1:
        {
            switch (dimorder[2])
            {
            case 2:
            {
                //0 1 2
                return node;
                break;
            }
            default:
                throw "In permute an illegal order of dimensions was choosen.";
            }
            break;
        }
        case 2:
        {
            switch (dimorder[2])
            {
            case 1:
            {
                //0 2 1
                blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > > result(size_dim_1, blaze::DynamicMatrix< double, blaze::columnMajor >(size_dim_0,  size_dim_2, 0.0));
                for (std::size_t i = 0; i < size_dim_2; ++i)
                {
                    for (std::size_t k = 0; k < size_dim_1; ++k)
                    {
                        blaze::column( result[ k ], i ) = blaze::column( node[i], k );
                        // for (std::size_t j = 0; j < size_dim_0; ++j)
                        // {
                        //     result[ k ](j, i) = node[i](j, k);
                        // }
                    }
                }
                return result;
                break;
            }
            default:
                throw "In permute an illegal order of dimensions was choosen.";
            }
            break;
        }
        default:
            throw "In permute an illegal order of dimensions was choosen.";
        }
        break;
    }
    case 1:
    {
        switch (dimorder[1])
        {
        case 0:
        {
            switch (dimorder[2])
            {
            case 2:
            {
                //1 0 2
                blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > > result(size_dim_2, blaze::DynamicMatrix< double, blaze::columnMajor >(size_dim_1,  size_dim_0, 0.0));
                for (std::size_t i = 0; i < size_dim_2; ++i)
                {
                    result[ i ] = blaze::trans(node[i]);
                    // for (std::size_t k = 0; k < size_dim_1; ++k)
                    // {
                    //     for (std::size_t j = 0; j < size_dim_0; ++j)
                    //     {
                    //         result[ i ](k, j) = node[i](j, k);
                    //     }
                    // }
                }
                return result;
                break;
            }
            default:
                throw "In permute an illegal order of dimensions was choosen.";
            }
            break;
        }
        case 1:
        {
            throw "In permute an illegal order of dimensions was choosen.";
            break;
        }
        case 2:
        {
            switch (dimorder[2])
            {
            case 0:
            {
                //1 2 0
                blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > > result(size_dim_0, blaze::DynamicMatrix< double, blaze::columnMajor >(size_dim_1,  size_dim_2, 0.0));
                for (std::size_t i = 0; i < size_dim_2; ++i)
                {
                    for (std::size_t j = 0; j < size_dim_0; ++j)
                    {
                        blaze::column(result[ j ], i ) = blaze::column( blaze::trans(node[i]), j );
                        // for (std::size_t k = 0; k < size_dim_1; ++k)
                        // {
                        //     result[ j ](k, i) = node[i](j, k);
                        // }
                    }
                }
                return result;
                break;
            }
            default:
                throw "In permute an illegal order of dimensions was choosen.";
            }
            break;
        }
        default:
            throw "In permute an illegal order of dimensions was choosen.";
        }
        break;
    }
    case 2:
    {
        switch (dimorder[1])
        {
        case 0:
        {
            switch (dimorder[2])
            {
            case 1:
            {
                //2 0 1
                blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > > result(size_dim_1, blaze::DynamicMatrix< double, blaze::columnMajor >(size_dim_2,  size_dim_0, 0.0));
                for (std::size_t i = 0; i < size_dim_2; ++i)
                {
                    for (std::size_t k = 0; k < size_dim_1; ++k)
                    {
                        blaze::row( result[ k ], i) = blaze::row( blaze::trans(node[i]), k);
                        // for (std::size_t j = 0; j < size_dim_0; ++j)
                        // {
                        //     result[ k ](i, j) = node[i](j, k);
                        // }
                    }
                }
                return result;
                break;
            }
            default:
                throw "In permute an illegal order of dimensions was choosen.";
            }
            break;
        }
        case 1:
        {
            switch (dimorder[2])
            {
            case 0:
            {
                //2 1 0
                blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > > result(size_dim_0, blaze::DynamicMatrix< double, blaze::columnMajor >(size_dim_2,  size_dim_1, 0.0));
                for (std::size_t j = 0; j < size_dim_0; ++j)
                {
                    for (std::size_t i = 0; i < size_dim_2; ++i)
                    {
                        blaze::row(result[ j ], i) = blaze::row(node[i], j);
                        // for (std::size_t k = 0; k < size_dim_1; ++k)
                        // {
                        //     result[ j ](i, k) = node[i](j, k);
                        // }
                    }
                }
                return result;
                break;
            }
            default:
                throw "In permute an illegal order of dimensions was choosen.";
            }
            break;
        }
        case 2:
        {
            throw "In permute an illegal order of dimensions was choosen.";
            break;
        }
        default:
            throw "In permute an illegal order of dimensions was choosen.";
        }
        break;
    }
    default:
        throw "In permute an illegal order of dimensions was choosen.";
    }
}


} //namespace lowrat