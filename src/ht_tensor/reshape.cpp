#include "lowrat/ht_tensor/reshape.h"

#include <cstddef> //for std::size_t
#include <vector>//for std::vector

#include <blaze/Blaze.h> //for blaze::DynamicMatrix, blaze::DynamicVector, blaze::columnMajor

namespace lowrat
{
//SHOULD WORK
blaze::DynamicVector< double, blaze::columnVector > reshape_3D_into_1D(const blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >& tensor_node)
{
    const std::size_t size_dim_0 = tensor_node[0].rows();
    const std::size_t size_dim_1 = tensor_node[0].columns();
    const std::size_t size_dim_2 = tensor_node.size();

    blaze::DynamicVector< double, blaze::columnVector > result(size_dim_0 * size_dim_1 * size_dim_2, 0.0);
    for (std::size_t i = 0; i < size_dim_2; ++i)
    {
        for (std::size_t k = 0; k < size_dim_1; ++k)
        {
            for (std::size_t j = 0; j < size_dim_0; ++j)
            {
                result[j + k * size_dim_0 + i * size_dim_0 * size_dim_1] = tensor_node[i](j, k);
            }
        }
    }
    return result;
}

//SHOULD WORK
blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > > reshape_1D_into_3D(const blaze::DynamicVector< double, blaze::columnVector >& vector, std::size_t size_dim_0, std::size_t size_dim_1,  std::size_t size_dim_2 )
{
    blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > > result(size_dim_2, blaze::DynamicMatrix< double, blaze::columnMajor >( size_dim_0, size_dim_1, 0.0));
    for (std::size_t i = 0; i < size_dim_2; ++i)
    {
        for (std::size_t k = 0; k < size_dim_1; ++k)
        {
            for (std::size_t j = 0; j < size_dim_0; ++j)
            {
                result[i](j, k) = vector[j + k * size_dim_0 + i * size_dim_0 * size_dim_1];
            }
        }
    }
    return result;
}

blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > > reshape_2D_into_3D(const blaze::DynamicMatrix< double, blaze::columnMajor >& matrix, std::size_t dim, std::size_t size_dim_0, std::size_t size_dim_1, std::size_t size_dim_2)
{
    switch (dim)
    {
    case 0:
    {
        blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > > result(size_dim_2, blaze::DynamicMatrix< double, blaze::columnMajor > (size_dim_0, size_dim_1, 0.0));
        for (std::size_t i = 0; i < size_dim_2; ++i)
        {
            for (std::size_t k = 0; k < size_dim_1; ++k)
            {
                for (std::size_t j = 0; j < size_dim_0; ++j)
                {
                    result[i](j, k) = matrix(j, k + i * size_dim_1);
                }
            }
        }
        return result;
    }
    case 1:
    {
        blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > > result(size_dim_2, blaze::DynamicMatrix< double, blaze::columnMajor > (size_dim_0, size_dim_1, 0.0));
        for (std::size_t i = 0; i < size_dim_2; ++i)
        {
            for (std::size_t j = 0; j < size_dim_0; ++j)
            {
                for (std::size_t k = 0; k < size_dim_1; ++k)
                {
                    result[i](j, k) = matrix(k, j + i * size_dim_0);
                }
            }
        }
        return result;
    }
    case 2:
    {
        blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > > result(size_dim_2, blaze::DynamicMatrix< double, blaze::columnMajor > (size_dim_0, size_dim_1, 0.0));
        for (std::size_t i = 0; i < size_dim_2; ++i)
        {
            for (std::size_t k = 0; k < size_dim_1; ++k)
            {
                for (std::size_t j = 0; j < size_dim_0; ++j)
                {
                    result[i](j, k) = matrix(i, j + k * size_dim_0);
                }
            }
        }
        return result;
    }
    default:
        throw "In reshape_2D_into_3D dim must be between 0 and 2.";
    }
}



//gets an tensor in x_1 x x_2 x x_12 and reshape it into a matrix
//if dim = 0 then the matrix is of size x_1 x (x_2 * x_12)
//if dim = 1 then the matrix is of size x_2 x (x_1 * x_12)
//if dim = 2 then the matrix is of size x_12 x (x_1 * x_2)
blaze::DynamicMatrix< double, blaze::columnMajor > reshape_3D_into_2D(const blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >& tensor_node, std::size_t dim)
{
    const std::size_t size_dim_0 = tensor_node[0].rows();
    const std::size_t size_dim_1 = tensor_node[0].columns();
    const std::size_t size_dim_2 = tensor_node.size();

    switch (dim)
    {
    case 0:
    {
        blaze::DynamicMatrix< double, blaze::columnMajor > result(size_dim_0, size_dim_1 * size_dim_2, 0.0);
        for (std::size_t i = 0; i < size_dim_2; ++i)
        {
            for (std::size_t k = 0; k < size_dim_1; ++k)
            {
                for (std::size_t j = 0; j < size_dim_0; ++j)
                {
                    result(j, k + i * size_dim_1) = tensor_node[i](j, k);
                }
            }
        }
        return result;
    }
    case 1:
    {
        blaze::DynamicMatrix< double, blaze::columnMajor > result(size_dim_1, size_dim_0 * size_dim_2, 0.0);
        for (std::size_t i = 0; i < size_dim_2; ++i)
        {
            //should we exchange j and k?
            for (std::size_t k = 0; k < size_dim_1; ++k)
            {
                for (std::size_t j = 0; j < size_dim_0; ++j)
                {
                    result(k, j + i * size_dim_0) = tensor_node[i](j, k);
                }
            }
        }
        return result;
    }
    case 2:
    {
        blaze::DynamicMatrix< double, blaze::columnMajor > result(size_dim_2, size_dim_0 * size_dim_1, 0.0);
        //should we exchane the i and k loop?
        for (std::size_t i = 0; i < size_dim_2; ++i)
        {
            for (std::size_t k = 0; k < size_dim_1; ++k)
            {
                for (std::size_t j = 0; j < size_dim_0; ++j)
                {
                    result(i, j + k * size_dim_0) = tensor_node[i](j, k);
                }
            }
        }
        return result;
    }
    default:
        throw "In reshape_3D_into_2D dim must be between 0 and 2.";
    }
}

//reshape 6D x_12_1 x x_2_2 x x_2_1 x x_12_2 x x_1_2 x x_1_1 tensor into 2D matrix of size (x_1_1 * x_2_1 * x_12_1) x (x_1_2 * x_2_2 * x_12_2)
blaze::DynamicMatrix< double, blaze::columnMajor > reshape_special_6D_into_2D(const std::vector<std::vector<std::vector<blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >>>>& tensor_node)
{
    const std::size_t size_dim_0 = tensor_node[0][0][0][0].rows();
    const std::size_t size_dim_1 = tensor_node[0][0][0][0].columns();
    const std::size_t size_dim_2 = tensor_node[0][0][0].size();
    const std::size_t size_dim_3 = tensor_node[0][0].size();
    const std::size_t size_dim_4 = tensor_node[0].size();
    const std::size_t size_dim_5 = tensor_node.size();

    blaze::DynamicMatrix< double, blaze::columnMajor > result(size_dim_5 * size_dim_2 * size_dim_0, size_dim_4 * size_dim_1 * size_dim_3, 0.0  );

    for (std::size_t i = 0; i < size_dim_5; ++i)
    {
        for (std::size_t j = 0; j < size_dim_4; ++j)
        {
            for (std::size_t l = 0; l < size_dim_3; ++l)
            {
                for (std::size_t m = 0; m < size_dim_2; ++m)
                {
                    for (std::size_t n = 0; n < size_dim_1; ++n)
                    {
                        for (std::size_t k = 0; k < size_dim_0; ++k)
                        {
                            result(i + m * size_dim_5 + k * size_dim_5 * size_dim_2, j + n * size_dim_4 + l * size_dim_4 * size_dim_1) = tensor_node[i][j][l][m](k, n);
                        }
                    }
                }
            }
        }
    }
    return result;
}



//gets an x_1 x x_2 x x_3 x x_4 tensor and reshape it into an 3D tensor
//if dim = 0 then the corresponding tensor would be x_1 x (x_2 * x_3) x x_4
//if dim = 1 then the corresponding tensor would be x_2 x (x_1 * x_3) x x_4
//if dim = 2 then the corresponding tensor would be x_3 x (x_1 * x_2) x x_4
blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > > reshape_4D_into_3D(const std::vector<blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > >>& tensor, std::size_t dim)
{
    const std::size_t size_of_last_dim = tensor.size();
    blaze::DynamicVector< blaze::DynamicMatrix< double, blaze::columnMajor > > result(size_of_last_dim);
    for (std::size_t i = 0; i < size_of_last_dim; ++i)
    {
        result[i] = reshape_3D_into_2D(tensor[i], dim);
    }
    return result;
}


} //namespace lowrat