#include "lowrat/cluster/cluster.h"

#include <cstddef>//for size_t
#include <algorithm>//for find, max
#include <iostream>//for ostream
#include <memory>//for unique_ptr, make_unique
#include <sstream>//for ostringstream
#include <stdexcept>//for invalid_argument
#include <vector>//for vector

namespace lowrat
{

using namespace std;

//
// Constructor (vector of tensor directions).
//
Cluster::Cluster( const vector< size_t >& modes )
    :   is_leaf_( true ),
        is_root_( true ),
        is_first_son_( false ),
        mpi_rank_( 0 ),
        father_( nullptr ),
        modes_( modes )
{}

//
// Constructor (first/last tensor direction).
//
Cluster::Cluster( size_t first, size_t last )
    :   is_leaf_( true ),
        is_root_( true ),
        is_first_son_( false ),
        mpi_rank_( 0 ),
        father_( nullptr )
{
    if (last < first)
    {
        throw std::invalid_argument( "last cluster direction cannot be before first cluster direction" );
    }
    size_t size = last - first + 1;
    modes_.resize( size );
    for ( size_t i = 0; i < size; ++i )
    {
        modes_[ i ] = first + i;
    }
}

Cluster::Cluster( Cluster* cl1, Cluster* cl2 )
{
    father_ = nullptr;
    is_leaf_ = false;
    is_root_ = true;
    is_first_son_ = false;
    mpi_rank_ = 0;

    //combine the two mode vectors
    size_t max_mode = *max_element(cl1->modes_.begin(),cl1->modes_.end());
    cl2->increase_modes(max_mode+1);

    modes_.resize(cl1->modes_.size()+cl2->modes_.size());
    size_t cntr = 0;
    for(size_t ent: cl1->modes_)
        modes_[cntr++] = ent;
    for(size_t ent: cl2->modes_)
        modes_[cntr++] = ent;

    set_sons(cl1,cl2);
}

//! Auxilairy function for Cluster(Cluster*,Cluster*)
//! Increases all modes in the tree by inc
void Cluster::increase_modes(const size_t inc)
{
    for(size_t& mode: modes_)
    {
        mode += inc;
    }
    if(son1_ != nullptr)
        son1_->increase_modes(inc);
    if(son2_ != nullptr)
        son2_->increase_modes(inc);
}

//
// Copy constructor.
//
Cluster::Cluster( const Cluster & cl )
{
    father_ = nullptr;

    if ( cl.son1_ )
    {
        //son1_ = std::make_unique<Cluster>(*cl.son1_.get());
        son1_ = std::make_unique<Cluster>(*cl.son1_);
        son1_->father_ = this;
    }

    if ( cl.son2_ )
    {
        //son2_ = std::make_unique<Cluster>(*cl.son2_.get());
        son2_ = std::make_unique<Cluster>(*cl.son2_);

        son2_->father_ = this;
    }

    is_leaf_        = cl.is_leaf_;
    is_root_        = cl.is_root_;
    is_first_son_   = cl.is_first_son_;
    mpi_rank_       = cl.mpi_rank_;
    modes_          = cl.modes_;
}

Cluster::Cluster(Cluster&& other)
{
    father_ = nullptr;

    if ( other.son1_ )
    {
        son1_ = std::move(other.son1_);
        son1_->father_ = this;
    }

    if ( other.son2_ )
    {
        son2_ = std::move(other.son2_);
        son2_->father_ = this;
    }

    is_leaf_        = other.is_leaf_;
    is_root_        = other.is_root_;
    is_first_son_   = other.is_first_son_;
    mpi_rank_       = other.mpi_rank_;
    modes_          = other.modes_;
}


//
// Destructor (in case of a tree recursively from root to leaves).
//
// Cluster::~Cluster()
//  {
//     son1_.reset();
//     son2_.reset();
//  }
// Cluster::~Cluster()
// {

// }

//
// Assignment. Creates a hard copy.
//
Cluster & Cluster::operator =( const Cluster & cl )
{
    father_ = nullptr;

    if ( cl.son1_ )
    {
        //son1_ = std::make_unique<Cluster>(*cl.son1_.get());
        son1_ = std::make_unique<Cluster>(*cl.son1_);
        son1_->father_ = this;
    }
    if ( cl.son2_ )
    {
        //son2_ = std::make_unique<Cluster>(*cl.son2_.get());
        son2_ = std::make_unique<Cluster>(*cl.son2_);
        son2_->father_ = this;
    }

    is_leaf_        = cl.is_leaf_;
    is_root_        = cl.is_root_;
    is_first_son_   = cl.is_first_son_;
    mpi_rank_       = cl.mpi_rank_;
    modes_          = cl.modes_;

    return (*this);
}

// move assignment
Cluster& Cluster::operator=(Cluster&& other)
{
    father_ = nullptr;

    if ( other.son1_ )
    {
        son1_ = std::move(other.son1_);
        son1_->father_ = this;
    }
    if ( other.son2_ )
    {
        son2_ = std::move(other.son2_);
        son2_->father_ = this;
    }

    is_leaf_        = other.is_leaf_;
    is_root_        = other.is_root_;
    is_first_son_   = other.is_first_son_;
    mpi_rank_       = other.mpi_rank_;
    modes_          = other.modes_;

    return (*this);
}

size_t Cluster::first() const
{
    if (modes_.size() > 0)
    {
        return modes_[ 0 ];
    }
    else
    {
        throw std::invalid_argument( "there is no first mode" );
        //was return -1;
    }
}

size_t Cluster::last() const
{
    if (modes_.size() > 0)
    {
        return modes_[ size() - 1 ];
    }
    else
    {
        throw std::invalid_argument( "there is no last mode" );
        //was return -1;
    }
}

//
// Returns the number of levels of the whole cluster tree.
//
size_t Cluster::levels() const
{
    return root()->num_levels();
}

//
// Returns the level (i.e. distance to root) of this.
//
size_t Cluster::level() const
{
    if ( father_ == nullptr )
    {
        return 0;
    }

    return father_->level() + 1;
}

//
// Checks if mode mu is contained.
//
bool Cluster::contains( size_t mu ) const
{
    if ( find( modes_.begin(), modes_.end(), mu ) != modes_.end() )
    {
        return true;
    }

    return false;
}

//
// Access to modes.
//
size_t Cluster::operator ()( size_t i ) const
{
    if (i < modes_.size())
    {
        return modes_[ i ];
    }
    else
    {
        throw std::invalid_argument( "mode size too small" );
    }
}

//
// Returns the leaf number of this, if this is a leaf, and -1 otherwise.
//
size_t Cluster::leaf_number() const
{
    if ( is_leaf_ )
    {
        size_t leaf_number = 0;

        for ( size_t i = 0; i < root()->size(); ++i )
        {

            if ( (*root())( leaf_number ) == (*this)( 0 ) )
            {
                break;
            }
            else
            {
                ++leaf_number;
            }

        }
        return leaf_number;
    }// if( is_leaf_ )
    else
    {
        throw std::invalid_argument( "this is no leaf" );
    }
}

//
// Sets the sons of this (and automatically sets this as father of the sons).
// son1 and son2 must be allocated by hand (as new Cluster *)
// and will automatically be deleted.
//
void Cluster::set_sons( Cluster* son1, Cluster* son2 )
{
    son1_ = std::make_unique<Cluster>(*son1);
    son2_ = std::make_unique<Cluster>(*son2);
    is_leaf_ = false;
    son1_->is_root_ = false;
    son2_->is_root_ = false;
    son1_->father_ = this;
    son2_->father_ = this;
    son1_->is_first_son_ = true;
    son2_->is_first_son_ = false;
    // Inherit the MPI rank of this.
    son1_->mpi_rank_ = mpi_rank_;
    son2_->mpi_rank_ = mpi_rank_;
}

//
// Sets son1 as 1st son of this (and automatically sets this as father of son1).
// son1 must be allocated by hand (as new Cluster *)
// and will automatically be deleted.
//
void Cluster::set_son1( Cluster * son1 )
{
    if (son1)
    {
        son1_ = std::make_unique<Cluster>(*son1);
        is_leaf_ = false;
        son1_->is_root_ = false;
        son1_->father_ = this;
        son1_->is_first_son_ = true;
        // Inherit the MPI rank of this.
        son1_->mpi_rank_ = mpi_rank_;
    }
    else
    {
        is_leaf_ = true;
    }
}

//
// Sets son2 as 2nd son of this (and automatically sets this as father of son2).
// son2 must be allocated by hand (as new Cluster *)
// and will automatically be deleted.
//
void Cluster::set_son2( Cluster * son2 )
{
    if (son2)
    {
        son2_ = std::make_unique<Cluster>(*son2);
        is_leaf_ = false;
        son2_->is_root_ = false;
        son2_->father_ = this;
        son2_->is_first_son_ = false;
        // Inherit the MPI rank of this.
        son2_->mpi_rank_ = mpi_rank_;
    }
    else
    {
        is_leaf_ = true;
    }
}

//
// Returns root of the cluster tree, which this belongs to.
//
const Cluster* Cluster::root() const
{
    if ( father_ == nullptr )
    {
        return this;
    }
    else
    {
        return father_->root();
    }
}

//
// Returns a vector of pointers to all nodes, that are on level lvl in the tree of this.
//
vector< const Cluster * > Cluster::level( size_t lvl ) const
{
    vector< const Cluster * > nodes;
    root()->level( lvl, nodes );
    return nodes;
}

//
// Returns a vector of all nodes in the tree of this.
//
vector< const Cluster * > Cluster::all_nodes() const
{
    vector< const Cluster * > nodes;

    for ( size_t lvl = 0; lvl < levels(); ++lvl )
    {
        vector< const Cluster * > level_vector = level( lvl );
        for ( size_t i = 0; i < level_vector.size(); ++i )
        {
            nodes.push_back( level_vector[ i ] );
        }
    }

    return nodes;
}

//
// Returns a vector of all nodes of a certain MPI rank.
//
vector< const Cluster * > Cluster::all_nodes_of_rank( int mpi_rank ) const
{
    vector< const Cluster * > nodes;

    for ( size_t lvl = 0; lvl < levels(); ++lvl )
    {
        vector< const Cluster * > level_vector = level( lvl );
        for ( size_t i = 0; i < level_vector.size(); ++i )
        {
            if ( level_vector[ i ]->mpi_rank() == mpi_rank )
            {
                nodes.push_back( level_vector[ i ] );
            }
        }
    }

    return nodes;
}

//
// Sets the MPI rank of this and all its successors.
//
void Cluster::set_mpi_rank_recursive( int mpi_rank )
{
    mpi_rank_ = mpi_rank;
    if ( son1_ )
    {
        son1_->set_mpi_rank_recursive( mpi_rank );
    }
    if ( son2_ )
    {
        son2_->set_mpi_rank_recursive( mpi_rank );
    }
}

//
// Builds a balanced cluster tree including _mpi_ranks such that the tree nodes
// on each MPI rank form a sub tree.
// num_ranks is the total number of MPI ranks available.
// num_ranks <= 0 results in each tree node having its own MPI rank.
//
void Cluster::build_balanced_tree( int num_ranks )
{
    build_balanced_tree_recursively();
    create_mpi_rank_distribution( num_ranks );
}

//
// Old version of build_balanced_tree() which for some cases results in a very
// unbalanced distribution of tree nodes to MPI ranks.
//
void Cluster::build_balanced_tree_old( int num_ranks )
{
    int mpi_rank = 0;
    --num_ranks;
    build_balanced_tree_recursively_old( mpi_rank, num_ranks );
}

//
// Sends the entire tree (starting from this) to ostream.
//
void Cluster::tree_to_ostream( ostream & os, size_t lvl ) const
{
    //not used
    //size_t root_size = root()->size();
    //therefore
    //root()->size();
    for ( std::size_t i = 0; i < lvl; ++i )
    {
        os << "\t";
    }
    os << " " << (*this);
    os << "\tMPI RANK:" << mpi_rank_;
    os << endl;

    if ( son1_ )
    {
        son1_->tree_to_ostream( os, lvl + 1 );
    }
    if ( son2_ )
    {
        son2_->tree_to_ostream (os, lvl + 1 );
    }
}

//
// Sends a cluster to ostream (without its father and sons).
//
ostream & operator <<( ostream & os, const Cluster & t )
{
    ostringstream oss( ostringstream::ate );
    oss << "{ ";
    for ( size_t i = 0; i < t.size() - 1; ++i )
    {
        oss << t.modes_[ i ];
        oss << ", ";
    }
    if ( t.size() > 0 )
    {
        oss << t.last();
    }
    oss << " }";

    os << oss.str();

    return os;
}

//
// Auxiliary function for build_balanced_tree.
// Distributes MPI ranks over a tree (level-wise, as long there are MPI ranks left).
//
void Cluster::create_mpi_rank_distribution( int num_ranks )
{
    size_t num_levels   = levels();
    //here was or is a big bug, size_t is normaly defined as unsigned integer so unsigned int -1 or -- is not that good
    //was size_t now int
    int ranks_left   = num_ranks;
    if ( ranks_left == 0 )
    {
        ranks_left = -1;
    }

    int mpi_rank = 0;

    for ( size_t l = 0; l < num_levels; ++l )
    {
        vector< Cluster * > lvl;
        level( l, lvl );
        //ranks_left < 0 is always false while this was a size_t now it is int again
        if ( ranks_left >= static_cast<int>(lvl.size()) || ranks_left < 0 )
        {
            for ( size_t i = 0; i < lvl.size(); ++i )
            {
                lvl[ i ]->set_mpi_rank_recursive( mpi_rank );
                mpi_rank++;
                if ( ranks_left > 0 )
                {
                    ranks_left--;
                }
            }
        }
        else if ( ranks_left >= 2 && l > 0 )
        {
            vector< Cluster * > father_lvl;
            level( l - 1, father_lvl );
            while ( ranks_left >= 2 && father_lvl.size() > 0 )
            {
                // Find largest subtree.
                Cluster * largest_subtree_son1 = nullptr;
                Cluster * largest_subtree_son2 = nullptr;
                size_t largest_subtree_index;
                size_t largest_subtree_size = 0;
                for ( size_t i = 0; i < father_lvl.size(); ++i )
                {
                    if ( father_lvl[ i ]->son1() != nullptr &&
                            father_lvl[ i ]->son2() != nullptr )
                    {
                        size_t num_nodes = father_lvl[ i ]->son1()->num_nodes() + father_lvl[ i ]->son2()->num_nodes();
                        if ( num_nodes > largest_subtree_size )
                        {
                            largest_subtree_son1    = father_lvl[ i ]->son1();
                            largest_subtree_son2    = father_lvl[ i ]->son2();
                            largest_subtree_index   = i;
                            largest_subtree_size    = num_nodes;
                        }
                    }
                }
                largest_subtree_son1->set_mpi_rank_recursive( mpi_rank );
                mpi_rank++;
                ranks_left--;
                largest_subtree_son2->set_mpi_rank_recursive( mpi_rank );
                mpi_rank++;
                ranks_left--;
                father_lvl.erase( father_lvl.begin() + largest_subtree_index );
            }// while ( ranks_left >= 2 && lvl.size() > 0 )
        }// else if ( ranks_left >= 2 )
    }// for ( size_t l = 0; l < num_levels; ++l )
}

//
// Auxiliary function for build_balanced_tree.
// Builds a balanced tree.
//
void Cluster::build_balanced_tree_recursively( Cluster * father )
{
    father_     = father;
    mpi_rank_   = 0;

    if ( father_ != nullptr )
    {
        is_root_ = false;
    }

    if ( size() > 1 )
    {
        is_leaf_ = false;

        // mid_index belongs to son2
        size_t mid_index = size() / 2;

        vector< size_t >  modes1;
        vector< size_t >  modes2;

        for ( size_t i = 0; i < size(); ++i )
        {
            if ( i < mid_index )
            {
                modes1.push_back( modes_[ i ] );
            }
            else
            {
                modes2.push_back( modes_[ i ] );
            }
        }// for

        son1_ = std::make_unique<Cluster>(modes1);
        son2_ = std::make_unique<Cluster>(modes2);

        son1_->build_balanced_tree_recursively( this );
        son2_->build_balanced_tree_recursively( this );

        son1_->is_first_son_ = true;
    }// if ( size() > 1 )
}

//
// Auxiliary function for build_balanced_tree_old.
// Builds a balanced tree including an MPI rank distribution.
//
void Cluster::build_balanced_tree_recursively_old( int & mpi_rank, int & num_ranks, Cluster * father )
{
    father_     = father;
    mpi_rank_   = mpi_rank;

    if ( father_ != nullptr )
    {
        is_root_ = false;
    }

    if ( size() > 1 )
    {
        is_leaf_ = false;

        // mid_index belongs to son2
        size_t mid_index = size() / 2;

        vector< size_t >  modes1;
        vector< size_t >  modes2;

        for ( size_t i = 0; i < size(); ++i )
        {
            if ( i < mid_index )
            {
                modes1.push_back( modes_[ i ] );
            }
            else
            {
                modes2.push_back( modes_[ i ] );
            }
        }// for
        son1_ = std::make_unique<Cluster>(modes1);
        son2_ = std::make_unique<Cluster>(modes2);

        if ( num_ranks < 0 )
        {
            mpi_rank++;
            son1_->build_balanced_tree_recursively_old( mpi_rank, num_ranks, this );
            mpi_rank++;
            son2_->build_balanced_tree_recursively_old( mpi_rank, num_ranks, this );
        }
        else
        {
            bool new_ranks_for_sons = false;
            if ( num_ranks > 1 )
            {
                num_ranks -= 2;
                new_ranks_for_sons = true;
            }

            if ( new_ranks_for_sons )
            {
                mpi_rank++;
            }
            son1_->build_balanced_tree_recursively_old( mpi_rank, num_ranks, this );
            if ( new_ranks_for_sons )
            {
                mpi_rank++;
            }
            son2_->build_balanced_tree_recursively_old( mpi_rank, num_ranks, this );
        }// else
        son1_->is_first_son_ = true;
    }// if
}

//
// Auxiliary function. Collects const Cluster * pointers to all nodes on level lvl in vector v.
//
void Cluster::level( size_t lvl, vector< const Cluster * > & v ) const
{
    if ( level() == lvl )
    {
        v.push_back( this );
    }
    if ( !is_leaf_ )
    {
        son1_->level( lvl, v );
        son2_->level( lvl, v );
    }
}

//
// Auxiliary function. Collects Cluster * pointers to all nodes on level lvl in vector v.
//
void Cluster::level( size_t lvl, vector< Cluster * > & v )
{
    if ( level() == lvl )
    {
        v.push_back( this );
    }
    if ( !is_leaf_ )
    {
        son1_->level( lvl, v );
        son2_->level( lvl, v );
    }
}

//
// Auxiliary function. Counts number of levels from this on.
//
size_t Cluster::num_levels( size_t lvl ) const
{
    if ( is_leaf_ )
    {
        return lvl;
    }
    else
    {
        if (son1_ && son2_)
        {
            return max( son1_->num_levels( lvl + 1 ),
                        son2_->num_levels( lvl + 1 ) );
        }
        else if (son1_)
        {
            return son1_->num_levels(lvl + 1);
        }
        else
        {
            return son2_->num_levels(lvl + 1);
        }
    }
}

//
// Counts the number of nodes of the (sub)tree starting at this.
//
size_t Cluster::num_nodes() const
{
    size_t count = 1;
    if ( son1_ )
    {
        count += son1_->num_nodes();
    }
    if ( son2_ )
    {
        count += son2_->num_nodes();
    }
    return count;
}

} //namespace lowrat

