// Class HTensor.
#include "lowrat/ht_tensor/htensor.h"
#include "lowrat/full_tensor/tensor.h"
#include "lowrat/cross/ht_cross.h"


// The HTensor class uses objects from the blaze library like
// blaze::DynamicMatrix< double, columnMajor > (matrix of doubles, stored in column-major order) or
// blaze::DynamicVector< double, columnVector > (column vector of doubles).
// The blaze library (together with a documentation) is available at https://bitbucket.org/blaze-lib/blaze.
using namespace blaze;
using namespace std;
using namespace lowrat;
// Tensor T with
//      T(p) = 1/ || p ||^2
//
class TestTensor : public Tensor
{
public:

    // Constructor, sets size = 5 for each dim.
    TestTensor(size_t d = 4, size_t s = 2 ) : Tensor( d )
    {
        for ( size_t i = 0; i < dim_; ++i )
        {
            size_[ i ] = s;
        }
    }

private:

    // Definition of tensor entries.
    virtual void eval( const vector< tensorindex >& indices, vector< double >& entries ) const override
    {
        double x = 0.0;
        double y = 1.0;
        for ( size_t i = 0; i < indices.size(); ++i )
        {
            x = 0.0;

            for ( size_t j = 0; j < dim_; ++j )
            {
                x += ( indices[ i ][ j ] + 1 ) * ( indices[ i ][ j ] + 1 ) ;
            }

            y = 1.0 / x;
            entries[ i ] = y;
        }// for( i )
    }
};

int main()
{


    // We create several HTensors from the TestTensor 'T' using the full,
    // the partial and the adaptive cross approximation, therefore we define
    // the following option parameter

    size_t d = 8;//8   // dimension of the problem
    size_t s = 5;//5   // size of

    // The options read from left to right are
    //      nested      -   use nested pivot elements
    //      K           -   size of random submatrices
    //      k           -   maximal rank of the low rank approximation
    //      eps         -   error tolerance, is only used for the adaptive cross approximation
    //      type        -   determines the type of the used cross approximation



    HTCrossSettings settings_full = {true, 50, 5, 1e-6, 0};
    HTCrossSettings settings_partial = {true, 50, 5, 1e-6, 1};
    HTCrossSettings settings_adaptive = {true, 50, 5, 1e-6, 2};

    TestTensor T(d, s);

    HTCross htc(settings_full);
    HTensor HT_full, HT_partial, HT_adaptive;


    htc.approximate(T, HT_full);

    htc.setSettings(settings_partial);
    htc.approximate(T, HT_partial);

    htc.setSettings(settings_adaptive);
    htc.approximate(T, HT_adaptive);

    // The method HTensor::display_content() displays the HTensor on the screen.

    HTensor HT_ones(HT_full, 1.0);


    cout << "Full cross approximation\n";
    HT_full.display_ranks();

    cout << "Partial cross approximation\n";
    HT_partial.display_ranks();

    cout << "Adaptive cross approximation\n";
    HT_adaptive.display_ranks();


    // In tensor.h we defined the type 'tensorindex' as std::vector< size_t >.
    // In our example we have indices of size 5 (since the tensor dimension is 5).
    tensorindex index;
    T.random_index(index);

    // We evaluate the different HTensor at the above index:
    cout << "T           (  " << index << " ) = " << T.entry( index ) << "\n";
    cout << "ht_full     (  " << index << " ) = " << HT_full.entry( index ) << "\n";
    cout << "ht_partial  (  " << index << " ) = " << HT_partial.entry( index ) << "\n";
    cout << "ht_adaptive (  " << index << " ) = " << HT_adaptive.entry( index ) << "\n" << "\n";
    // It may happen that the ranks of the respective HTensor format is not minimal,
    // therefore we may truncate the ranks up to a certain tolerance.

    // We can compute the euclidian norm of 'ht' by the square root of its
    // inner product with itself:
    double norm_2_f = sqrt( HT_full.dot( HT_full ) );
    double norm_2_p = sqrt( HT_partial.dot( HT_partial) );
    double norm_2_a = sqrt( HT_adaptive.dot( HT_adaptive ) );

    cout << "Euclidian norm (full)            = " << norm_2_f << "\n";
    cout << "Euclidian norm (partial)         = " << norm_2_p << "\n";
    cout << "Euclidian norm (adaptive)        = " << norm_2_a << "\n" << "\n";


    // Total number of entries computed from tensor2.
    size_t computed_entries = T.computed_entries();

    // Compute the error (for 100 randomly chosen tensor indices).
    double abs_err_2_f = 0.0;
    double rel_err_2_f = 0.0;

    double norm_2_te   = 0.0;

    double abs_err_2_p = 0.0;
    double rel_err_2_p = 0.0;

    double abs_err_2_a = 0.0;
    double rel_err_2_a = 0.0;

    size_t i = 0;
    for ( i = 0; i < 100; ++i )
    {
        tensorindex tmp_idx2;
        T.random_index( tmp_idx2 );
        double T_entry = T.entry( tmp_idx2 );
        abs_err_2_f += pow(  T_entry - HT_full.entry( tmp_idx2 ), 2 );
        abs_err_2_p += pow(  T_entry - HT_partial.entry( tmp_idx2 ), 2 );
        abs_err_2_a += pow(  T_entry - HT_adaptive.entry( tmp_idx2 ), 2 );

        norm_2_te += pow(T_entry, 2);
    }

    abs_err_2_f = sqrt( abs_err_2_f );
    abs_err_2_p = sqrt( abs_err_2_p );
    abs_err_2_a = sqrt( abs_err_2_a );

    norm_2_te    = sqrt( norm_2_te );
    rel_err_2_f = abs_err_2_f / norm_2_te;
    rel_err_2_p = abs_err_2_p / norm_2_te;
    rel_err_2_a = abs_err_2_a / norm_2_te;

    cout << " Error estimation (full)" << "\n";
    cout << "  abs_err_2 = " << abs_err_2_f << "\n";
    cout << "  rel_err_2 = " << rel_err_2_f << "\n" << "\n";

    cout << " Error estimation (partial)" << "\n";
    cout << "  abs_err_2 = " << abs_err_2_p << "\n";
    cout << "  rel_err_2 = " << rel_err_2_p << "\n" << "\n";

    cout << " Error estimation (adaptive)" << "\n";
    cout << "  abs_err_2 = " << abs_err_2_a << "\n";
    cout << "  rel_err_2 = " << rel_err_2_a << "\n" << "\n";

    cout << "  norm_2_te   = " << norm_2_te << "\n";
    cout << " " << computed_entries << " entries have been evaluated." << "\n";

    // Now we have an insight in the quality of the HTensor approximation
    // and it is possible to use the Tensor for example to find the maximum

    double m, m2;
    int k_max = 3, k_max2 = 3;

    HT_full.vector_iteration(m, k_max, 1e-4 );
    cout << "Start index " << index << "\n";

    m2 = HT_full.maximum_als( index, k_max2 );
    cout << "Maximum value of HT_FULL after "  << k_max << " vector iterations :" << m << "\n";
    cout << "Maximum value of HT_FULL after "  << k_max2 << " maximum als steps:" << m2 << "\n\t at the index" << index << "\n";
    cout << "Compared with the real maxium : " << 1.0 / d << " results in an error of " << std::fabs( m - 1.0 / d) << "\n" << "\n";


    // since the tensor has its maximum at the index = (0,..., 0), we evaluate the
    tensorindex z_idx(d, 0);
    cout << "T           (  " << z_idx << " ) = " << T.entry( z_idx ) << "\n";
    cout << "ht_full     (  " << z_idx << " ) = " << HT_full.entry( z_idx ) << "\n";
    cout << "ht_partial  (  " << z_idx << " ) = " << HT_partial.entry( z_idx ) << "\n";
    cout << "ht_adaptive (  " << z_idx << " ) = " << HT_adaptive.entry( z_idx ) << "\n" << "\n";


    return 0;
}
