// Class HTensor.
#include "lowrat/ht_tensor/htensor.h"
// Parallel I/O.
// #include "lowrat/util/parallel.h"


// The HTensor class uses objects from the blaze library like
// blaze::DynamicMatrix< double, columnMajor > (matrix of doubles, stored in column-major order) or
// blaze::DynamicVector< double, columnVector > (column vector of doubles).
// The blaze library (together with a documentation) is available at https://bitbucket.org/blaze-lib/blaze.
using namespace blaze;
using namespace std;
using namespace lowrat;

int main()
{
    int my_rank = 0;

    // For parallel I/O (cf io/parallel-io.h).

    // We create an HTensor 'ht' of dimension 5 with each tensor dimension of size 4. The initial
    // HT ranks are zero on each node of the HT-tree (i.e. the matrices in HT::_data are empty).
    // The vector HTensor::_data contains the tensor data for all HT-tree nodes
    // The HTensor constructor is supposed to also accept user defined trees (last argument),
    // although this is not tested yet. By default a balanced tree is used, in our case:
    //               { 0, 1, 2, 3, 4 }
    //         { 0, 1 }             { 2, 3, 4 }
    //      { 0 }    { 1 }       { 2 }       { 3, 4 }
    //                                    { 3 }    { 4 }
    HTensor ht( 5, 4, 0);

    // We retrieve a copy of the underlying HT-tree stored as an object of class Cluster.
    // An object of class Cluster contains an std::vector of tensor directions (Cluster::_modes).
    Cluster tree = ht.clustertree();
    // By the method Cluster::all_nodes_of_rank( int ) we get an std::vector< const Cluster * >
    // containing pointers to all Cluster objects of the tree with _mpi_rank = my_rank
    // (in correspondence to HT::_data).
    vector< const Cluster * >   local_nodes = tree.all_nodes_of_rank( my_rank );

    // We fill the HTensor ht with data.
    // We want to have the following HTensor, corresponding to the above balanced tree.
    // The layers of the 3-dimensional transfer tensors are
    // written layer-wise, delimited by '----------------------------':
    //
    // ----------------------------
    // (            1            3 )    on { 0, 1, 2, 3, 4 }
    // (            4            5 ),
    // ----------------------------
    //
    // ----------------------------
    // (          0.1         -0.2 )
    // (          0.3          0.8 )
    // ----------------------------     on { 0, 1 }
    // (          0.2         -0.4 )
    // (          0.6          1.6 ),
    // ----------------------------
    //
    // ----------------------------
    // (         -0.8          0.1 )
    // (         -0.5          0.3 )
    // ----------------------------     on { 2, 3, 4 }
    // (          1.6         -0.2 )
    // (            1         -0.6 ),
    // ----------------------------
    //
    // ----------------------------
    // (            4            1 )
    // (            3           -2 )
    // ----------------------------     on { 3, 4 }
    // (           -2         -0.5 )
    // (         -1.5            1 ),
    // ----------------------------
    //
    // ----------------------------
    // (            2            1 )
    // (            4            2 )    on { 0 }
    // (            6            3 )
    // (            2            1 ),
    // ----------------------------
    //
    // ----------------------------
    // (            1            3 )
    // (            1            3 )    on { 1 }
    // (           -1           -3 )
    // (            2            6 ),
    // ----------------------------
    //
    // ----------------------------
    // (            4           -2 )
    // (            1         -0.5 )    on { 2 }
    // (            2           -1 )
    // (            3         -1.5 ),
    // ----------------------------
    //
    // ----------------------------
    // (          0.8          0.4 )
    // (          0.6          0.3 )    on { 3 }
    // (          0.2          0.1 )
    // (           -1         -0.5 ),
    // ----------------------------
    //
    // ----------------------------
    // (          0.2          0.4 )
    // (          0.3          0.6 )    on { 4 }
    // (          0.1          0.2 )
    // (          0.1          0.2 ).
    // ----------------------------

    // (consistent with HTensor::_data):
    vector< DynamicVector< DynamicMatrix< double, columnMajor > > > data( local_nodes.size() );

    // Run through the local nodes and set data[ i ] accordingly:
    for ( size_t i = 0; i < local_nodes.size(); ++i )
    {
        if ( local_nodes[ i ]->first() == 0 && local_nodes[ i ]->last() == 4 )
        {
            // {0,1,2,3,4}
            data[ i ].resize( 1 );
            data[ i ][ 0 ]  = { { 1, 3 },
                { 4, 5 }
            };
        }
        else if ( local_nodes[ i ]->first() == 0  && local_nodes[ i ]->last() == 1 )
        {
            // {0,1}
            data[ i ].resize( 2 );
            data[ i ][ 0 ]  = { { 0.1, -0.2 },
                { 0.3, 0.8 }
            };
            data[ i ][ 1 ]  = { { 0.2, -0.4 },
                { 0.6, 1.6 }
            };
        }
        else if ( local_nodes[ i ]->first() == 2 && local_nodes[ i ]->last() == 4 )
        {
            // {2,3,4}
            data[ i ].resize( 2 );
            data[ i ][ 0 ]  = { { -0.8, 0.1 },
                { -0.5, 0.3 }
            };
            data[ i ][ 1 ]  = { { 1.6, -0.2 },
                { 1, -0.6 }
            };
        }
        else if ( local_nodes[ i ]->first() == 3 && local_nodes[ i ]->last() == 4 )
        {
            // {3,4}
            data[ i ].resize( 2 );
            data[ i ][ 0 ]  = { { 4, 1 },
                { 3, -2 }
            };
            data[ i ][ 1 ]  = { { -2, -0.5 },
                { -1.5, 1 }
            };
        }
        else if ( local_nodes[ i ]->first() == 0 && local_nodes[ i ]->last() == 0 )
        {
            // {0}
            data[ i ].resize( 1 );
            data[ i ][ 0 ]  = { { 2, 1 },
                { 4, 2 },
                { 6, 3 },
                { 2, 1 }
            };
        }
        else if ( local_nodes[ i ]->first() == 1 && local_nodes[ i ]->last() == 1 )
        {
            // {1}
            data[ i ].resize( 1 );
            data[ i ][ 0 ]  = { { 1, 3 },
                { 1, 3 },
                { -1, -3 },
                { 2, 6 }
            };
        }
        else if ( local_nodes[ i ]->first() == 2 && local_nodes[ i ]->last() == 2 )
        {
            // {2}
            data[ i ].resize( 1 );
            data[ i ][ 0 ]  = { { 4, -2 },
                { 1, -0.5 },
                { 2, -1 },
                { 3, -1.5 }
            };
        }
        else if ( local_nodes[ i ]->first() == 3 && local_nodes[ i ]->last() == 3 )
        {
            // {3}
            data[ i ].resize( 1 );
            data[ i ][ 0 ]  = { { 0.8, 0.4 },
                { 0.6, 0.3 },
                { 0.2, 0.1 },
                { -1, -0.5 }
            };
        }
        else if ( local_nodes[ i ]->first() == 4 && local_nodes[ i ]->last() == 4 )
        {
            // {4}
            data[ i ].resize( 1 );
            data[ i ][ 0 ]  = { { 0.2, 0.4 },
                { 0.3, 0.6 },
                { 0.1, 0.2 },
                { 0.1, 0.2 }
            };
        }
    }

    // Insert the tensor data into the HTensor 'ht':
    ht.set_data( data );

    // The method HTensor::display_content() displays the HTensor on the screen.
    ht.display_content();
    // or
    ht.display_dimensions();

    // The method HTensor::singular_values() yields the singular values of
    // each Matricization in the HTensor. Stored in the same order as the data vector.
    auto s_vals = ht.singular_values();
    cout << "Singular Values of the HTensor" << endl;
    for(size_t i = 0; i < local_nodes.size(); i++)
    {
        cout << i << " : "<< endl << s_vals[i];
    }

    // In tensor.h we defined the type 'tensorindex' as std::vector< size_t >.
    // In our example we have indices of size 5 (since the tensor dimension is 5).
    tensorindex index = { 1, 0, 2, 3, 1 };

    // We evaluate the HTensor 'ht' at the above index:
    cout << "ht(  " << index << " ) = " << ht.entry( index ) << endl;

    // We copy the contents of 'ht' to a second HTensor 'ht2'.
    HTensor ht2( ht );

    // We scale 'ht2' by the factor -0.2.
    // For that only the data of the root node has to be changed (cf. htensor.cpp).
    ht2.scale( -0.2 );
    // We add 3.5 times the tensor 'ht' to 'ht2' (which results in ht2 = 3.3 * ht).
    ht2.add( 3.5, ht );

    // This should now yield 3.3 times the corresponding entry of 'ht' (see above).
    cout << "ht2( " << index << " ) = " << ht2.entry( index ) << endl;

    // You may have noticed that the HT representation ranks of 'ht' and 'ht2'
    // are not minimal, i.e. we can truncate the HTensor down to lower rank (rank 1).
    // We do this for 'ht2'. We prescribe an upper bound of 1e-8 for
    // the relative error in the euclidian norm.
    ht2.truncate( 1e-8 );
    // We take a look at the contents of 'ht2', indeed we now have all
    // HT representation ranks equal to one.
    ht2.display_content();

    // We can compute the euclidian norm of 'ht' by the square root of its
    // inner product with itself:
    cout << "Euclidian norm         = " << sqrt( ht.dot( ht ) ) << endl;


    return 0;
}
